
		<?php require('header.php');?>
		<div class='content about-content'>
			<div class="container">
				<div class='row'>
					<div class='col-md-12 col-sm-12'>
						<div class='row'>
							<div class='col-md-12'>
								<div class='row'>
									<div class='col-md-12'>
										<div class="content-4 content-about">
											<div class="panel panel-primary text-justify">
								  				<div class="panel-heading">
								    				<h3 class="panel-title">Partnering Institutes</h3>
								  				</div>
										  		<div class="panel-body">
													<div class='row'>
														<div class="col-md-12">
															<h4>National</h4>
															<ul>
																<li>DM Wayanad Institute of Medical Sciences</li>
																<li>Directorate of Health Services</li>
																<li>Animal Husbandry Department</li>
																<li>District Medical Office, Wayanad</li>
																<li>District Animal Husbandry Office, Wayanad</li>
																<li>National Rural Health Mission</li>
																<li>Brahmagiri Development Society- Malabar Meat</li>
																<li>Departments in KVASU</li>
															</ul>
															<!--<h4>International</h4>-->
														</div>
													</div>
													<div class='row'>
														<div class="col-md-12">
															<h4>Individuals</h4>
															<ul>
															<?php
																require('dbconnect.php');
																$query = mysqli_query($conn,"select * from individuals");
																while($data = mysqli_fetch_array($query))
																{
																	echo "<li>{$data['name']} - {$data['skills']}</li>";
																}
															?>
															</ul>
															<!--<h4>International</h4>-->
														</div>
													</div>
													<div class='row'>
														<div class="col-md-12">
															<h4>Organizations</h4>
															<ul>
															<?php
																require('dbconnect.php');
																$query = mysqli_query($conn,"select * from organizations");
																while($data = mysqli_fetch_array($query))
																{
																	echo "<li>{$data['name']} - {$data['activities']}</li>";
																}
															?>
															</ul>
															<!--<h4>International</h4>-->
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="row news">
					<div class="col-md-12">
						<div class="container">
							<div class='row'>
								<div class='col-md-12'>
									<?php require('news-updates.php');?>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
<?php require('footer.php');?>		
<script>
	$(function()
	{
		$('table').addClass('table table-bordered table-striped');
	});
</script>