<?php session_start();
$id=$_SESSION['studentid'];
require('dbconnect.php');
$query = mysqli_query($conn,"select * from students where id='$id'");
$data=mysqli_fetch_array($query);
$query1 = mysqli_query($conn,"select * from projects where userid='$id'");
while($data1=mysqli_fetch_assoc($query1))
{
  $datas[]=$data1;
}
$rowcount=count($datas);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="coheart, kvasu, veterinary, one, health, university, kerala, animal, science, pookode,diploma,certification,course, education, hygiene,Education, Advocacy, Research, Training "/>
   	<meta name="description" content="One Health concept is based on the understanding that the health of the humans, animals and the environment is inextricably linked, and that promoting the well being of all species can only be achieved through a holistic multidisciplinary approach at the human-animal-ecosystems interface. Kerala Veterinary and Animal Sciences University (KVASU) has taken a pioneer step in this regard following the establishment of the Centre for One Health Education Advocacy Research and Training, the first of its kind in India aiming at the sustained health of the community by addressing various issue of concern today like the food safety and security, zoonoses, diseases from natural origins like soil, water and air."/>
    <meta name="google-site-verification" content="Sg2CCDPE0zio41cI6pF2SkUWVUX1nSGT8waFfR8S6f0" />
    <title>COHEART | Student Blog</title>
	<link rel="shortcut icon" href="dist/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="dist/images/favicon.ico" type="image/x-icon">
	<!--jscrollpane-->
	<link type="text/css" href="jScrollPane/jquery.jscrollpane.css" rel="stylesheet" media="all" />
	<!--Slider-->
	<link rel="stylesheet" href="dist/slider/responsiveslides.css">
	<!--flash-->
	<link rel="stylesheet" href="ticker/example/assets/webticker.css" type="text/css" media="screen">
	<!--FancyBox-->
	<link rel="stylesheet" href="dist/lightbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<!-- Bootstrap -->
	<link rel="stylesheet" href="modal/css/style.css"> <!-- Gem style -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
	<link href="dist/css/style.css" rel="stylesheet">
	<link href="admin/table/css/footable.core.css" rel="stylesheet"/>
	<link href="admin/table/css/footable.metro.css" rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
	<!--<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>-->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/ajaxupload.js"></script>
	<script src="dist/ckeditor/ckeditor.js"></script>
	<script src="modal/js/modernizr.js"></script> <!-- Modernizr -->
<script>
$(function(){
  //alert("hai");
		var btnUpload=$('#browse');
	new AjaxUpload(btnUpload, {
		action: 'upload_image',
		name: 'file',
		 onSubmit: function(file, ext){$("#loadgif1").css("display","block");

                 if (! (ext && /^(jpg|jpeg|gif|png)$/.test(ext))){
//alert("hai");
                    // extension is not allowed
				//document.getElementById("loadgif").style.display='block';
				//$("#loadgif1").css("display","none");
              // $("#image").css("display","block");
		   
		   alert("Only jpg,jpeg,png,gif images are allowed");

                    return false;

               }
		},
		onComplete: function(file, response){ //alert(file+"---"+response);
		//alert(response);
		$("#prof_img").html("");
		var img='<img src="uploads/'+response+'" class="img-responsive img-thumbnail">';
		$("#prof_img").html(img);
		}
	});
	});</script>
  </head>
  <body>

		<div class='banner'>
			<div class='container'>
				<div class='row hidden-xs'>
					<div class='col-md-1 col-md-offset-1 col-xs-1 col-md-offset-1 text-center'>
						<embed height="150" width="130" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" wmode="transparent" menu="false" quality="high" src="dist/images/coheart-logo.swf" style="display: block !important;">
					</div>
					<div class="col-md-6 col-xs-6">
						<div class="row">
							<div class="col-md-12 text-center">
								<h3 style="color: #fff;"><strong>CENTER for ONE HEALTH EDUCATION ADVOCACY RESEARCH and TRAINING</strong></h3>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 text-center">
								<h4>Kerala Veterinary and Animal Sciences University</h4>
								<h4>Pookode, Wayanad Kerala - 673576</h4>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-xs-4">
						<div class="row">
							<div class="col-md-12 text-right">
								<h4 class="man"><i>"For multidisciplinary holistic approach for harmony among Man, Animal and Nature (M.A.N)"</i></h4>
							</div>
						</div>
						<div class="row">
							<div class="col-md-offset-3 col-md-9 text-right google-search">
								<!--<script>
								  (function() {
								    var cx = '001763096455298922418:gkxx4zxqozc';
								    var gcse = document.createElement('script');
								    gcse.type = 'text/javascript';
								    gcse.async = true;
								    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
								        '//www.google.com/cse/cse.js?cx=' + cx;
								    var s = document.getElementsByTagName('script')[0];
								    s.parentNode.insertBefore(gcse, s);
								  })();
								</script>
								<gcse:search></gcse:search>-->
							</div>
						</div>
					</div>
				</div>
			</div><!--/container-->
		
			<div class='row visible-xs'>
				<div class='col-xs-12 text-center'>
					<a href="index"><img src='dist/images/kvasu-coheart-logo-800X212.png' alt="kvasu logo" width='544' height='126' class='img-responsive'></a>
				</div>
			</div>
			<div class="row visible-xs">
				<div class="col-md-12">
					<!--<script>
					  (function() {
						var cx = '001763096455298922418:gkxx4zxqozc';
						var gcse = document.createElement('script');
						gcse.type = 'text/javascript';
						gcse.async = true;
						gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							'//www.google.com/cse/cse.js?cx=' + cx;
						var s = document.getElementsByTagName('script')[0];
						s.parentNode.insertBefore(gcse, s);
					  })();
					</script>
					<gcse:search></gcse:search>-->
				</div>
			</div>
		</div><!--/banner-->
	
	<div class="student-blog">
		<div class="container full-width table-container">
		<div class="row table-row">
			<div class="col-md-3 sidebar table-col no-padding">
				<div class="side-menu text-center">
					<a href="studentProfile"><span class="glyphicon glyphicon-circle-arrow-left"title="Go Back" data-toggle="tooltip" data-placement="bottom"></span></a>
					<br><br>
					<div id="prof_img"><?php if($data['prof_image']==""){ ?>
					<img src="dist/images/default-pic.png" class="img-responsive img-thumbnail">
					<?php }else{ ?>
					<img src="uploads/<?php echo $data['prof_image'] ?>" class="img-responsive img-thumbnail">
					<?php } ?>
					</div>
					<br>To update profile picture: <input type="file" name='file' style="cursor: pointer;margin-left: 20%" id="browse"><br>
					<div class="student-name"><?php echo $data['student_name']; ?></div>
					<br>
					<a href="javascript:void(0)" class="cd-signin" style="cursor: pointer;">Change Password</a>
					<br><br>
					<ul id="accordion-archive">
						<li> <a href="#"><span class="glyphicon glyphicon-play"></span> 2015 (<?php echo $rowcount; ?>)</a>
							<ul>
							  <?php for($i=0;$i<$rowcount;$i++)
{?>
<input type="hidden" id="proname<?php echo $i; ?>" value="<?php echo $datas[$i]['name']; ?>">
<input type='hidden' id='prodesc<?php echo $i; ?>' value='<?php echo $datas[$i]['description']; ?>'>
<?php
  $splitval=explode("/",$datas[$i]['date']);
 $splitval[0];?><li><a style="cursor: pointer;" onclick="datashow(<?php echo $i; ?>)">item <?php echo $i+1; ?></a></li>
 <?php
} ?>
								<!--<li><a href="#">item 1</a></li>
								<li><a href="#">item 2</a></li>
								<li><a href="#">item 3</a></li>
								<li><a href="#">item 4</a></li>-->
							</ul>
						</li>
						<!--<li> <a href="#"><span class="glyphicon glyphicon-play"></span> 2014 (34)</a>
							<ul>
								<li><a href="#">item 1</a></li>
								<li><a href="#">item 2</a></li>
								<li><a href="#">item 3</a></li>
								<li><a href="#">item 4</a></li>
							</ul>
						</li>
						<li> <a href="#"><span class="glyphicon glyphicon-play"></span> 2013 (10)</a>
							<ul>
								<li><a href="#">item 1</a></li>
								<li><a href="#">item 2</a></li>
								<li><a href="#">item 3</a></li>
								<li><a href="#">item 4</a></li>
							</ul>
						</li>
						<li> <a href="#"><span class="glyphicon glyphicon-play"></span> 2012 (22)</a>
							<ul>
								<li><a href="#">item 1</a></li>
								<li><a href="#">item 2</a></li>
								<li><a href="#">item 3</a></li>
								<li><a href="#">item 4</a></li>
							</ul>
						</li>
						<li> <a href="#"><span class="glyphicon glyphicon-play"></span> 2011 (18)</a>
							<ul>
								<li><a href="#">item 1</a></li>
								<li><a href="#">item 2</a></li>
								<li><a href="#">item 3</a></li>
								<li><a href="#">item 4</a></li>
							</ul>
						</li>-->
					</ul>
				</div>
			</div>
			<div class="col-md-9 table-col">
				<div id="blog-content">
					<div class="about-author">
						<h4>About Author</h4>
					</div>
					<p>
					<?php echo $data['profdet']; ?>
					</p>
					<form id="form1">
					<div class="project-title">
						<input type="text" class="form-control" id="projectName" name='projectName' value="" placeholder="Project Name">
					</div>
					<input type="hidden" name="studid" id="studid" value="<?php echo $id; ?>">
					<div class="project-content">
					  <label for="description">Description</label>
					  <textarea class="form-control" rows="3" id="prodescription" name="prodescription"></textarea>
					</div>
					</form>
					<div class="project-page-buttons">
						<div class="row">
							<!--<div class="col-xs-6">
								<a href="" class="btn btn-default">Download</a>
							</div>-->
							<div class="col-xs-6 text-right" style="padding-right: 4%;">
								<a class="btn btn-default" onclick="checkval()">Submit</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
<!-- login modal-->

	<div class="cd-user-modal"> <!-- this is the entire modal form, including the background -->
		<div class="cd-user-modal-container"> <!-- this is the container wrapper -->
			<div class="cd-user-modal-title">
				Reset Password
			</div>
			
			<div id="cd-login"> <!-- log in form -->
				<form class="cd-form" id="loginform">
				  <span id="log_error" style="color: #900;display: none;">Current Password is incorrect</span>
					<p class="fieldset">
						<label class="image-replace cd-password" for="signin-email">Current Password</label>
						<input class="full-width has-padding has-border" id="curr-pass" name="signin-email" type="email" placeholder="Current Password">
						<a href="javascript:void(0)" class="hide-password">Hide</a>
						<span style="display: none;color: #900" id="currpswderror">Enter current Password</span>
						<!--cd-error-message-->
					</p>

					<p class="fieldset">
						<label class="image-replace cd-password" for="signin-password">New Password</label>
						<input class="full-width has-padding has-border" id="new-pass" name="signin-password" type="text"  placeholder="New Password">
						<a href="javascript:void(0)" class="hide-password">Hide</a>
						<span class="cd-error-message" id="pswderror">Error message here!</span>
					</p>

					<p class="fieldset">
						<label class="image-replace cd-password" for="signin-password">Confirm Password</label>
						<input class="full-width has-padding has-border" id="con-pass" name="signin-password" type="text"  placeholder="Confirm Password">
						<a href="javascript:void(0)" class="hide-password">Hide</a>
						<span class="cd-error-message" id="pswderror">Error message here!</span>
					</p>

					<p class="fieldset">
						<input class="full-width" type="button" style="background: #2f889a;border: none;padding: 16px;color: #fff;" name="login" value="Reset" onclick="resetpass()">
					</p>
				</form>
				<!-- <a href="#0" class="cd-close-form">Close</a> -->
			</div> <!-- cd-login -->	
			<a href="#0" class="cd-close-form">Close</a>
		</div> <!-- cd-user-modal-container -->
	</div> <!-- cd-user-modal -->

	<!--end of login modal-->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-50095931-2', 'coheart.ac.in');
	  ga('send', 'pageview');
	</script>

<?php require('footer.php');?>
<script src="modal/js/main.js"></script>
<script src="metismenu/metisMenu.js"></script>
<script type="text/javascript" src="jScrollPane/jquery.mousewheel.js"></script>
<script type="text/javascript" src="jScrollPane/jquery.jscrollpane.min.js"></script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
	<!--Slider-->
	<script src="dist/js/jquery.popconfirm.js"></script>
	<script src="admin/tinymce/tinymce.min.js"></script>
	<script src="admin/confirm/bootstrap-dialog.min.js"></script>
	 <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
               // CKEDITOR.replace( 'prodescription' );
				//filebrowserBrowseUrl: '/browser/browse.php',
    //filebrowserUploadUrl: '/uploader/upload.php'
	/*CKEDITOR.replace( 'prodescription', {
    filebrowserBrowseUrl: '/browser/browse.php',
    filebrowserUploadUrl: '/uploader/upload.php'/browser/browse.php
});*/
	CKEDITOR.replace( 'prodescription', {
    filebrowserBrowseUrl: '/editoruploads',
    filebrowserUploadUrl: 'editorupload.php'
});
            </script>
<script type="text/javascript">
  function resetpass()
  {
	var h1=0,h2=0,h3=0;
	var currpass=$('#curr-pass').val();
	var newpass=$('#new-pass').val();
	var conpass=$('#con-pass').val();
	if(currpass=="")
	{
	  h1=1;
	 $("#curr-pass").css('border','1px solid #900');
	 document.getElementById("curr-pass").focus();
	  return false;
	}
	else{
	   h1=0;
	 $("#curr-pass").css('border','1px solid #DDDDDD');
	}
	if(newpass=="")
	{
	  h2=1;
	 $("#new-pass").css('border','1px solid #900');
	 document.getElementById("new-pass").focus();
	  return false;
	}
	else{
	   h2=0;
	 $("#new-pass").css('border','1px solid #DDDDDD');
	}
	if(conpass=="")
	{
	  h3=1;
	 $("#con-pass").css('border','1px solid #900');
	 document.getElementById("con-pass").focus();
	  return false;
	}
	else{
	   h3=0;
	 $("#con-pass").css('border','1px solid #DDDDDD');
	}
	if (newpass!==conpass) {
	 BootstrapDialog.alert("Password not matching");
	 h3=1;
	 return false;
	}else{
	  h3=0;
	}
	if (h1==0 && h2==0 && h3==0) {
	 var dat=$("#loginform").serialize();
	  $.ajax({
   url: 'passwordreset.php',
   type: 'POST',
   data:  dat,
   success: function(data) {
	//alert(data);
   if(data=="success")
	{
	  BootstrapDialog.alert("Password Updated");
	  window.location="studentAccount";
	}
	else{
	 $("#log_error").css("display","block");
	}
   }
});
	}
	
  }
		$(function () {
		  tinymce.init
			({
				selector: "textarea.tinymce",
				plugins:
				[
					"advlist autolink lists link image charmap print preview anchor",
					"searchreplace visualblocks code fullscreen",
					"insertdatetime media table contextmenu paste"
				],
				toolbar: "insertfile | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
				height: 200,
				skin: 'custom2'
			});
		  $('[data-toggle="tooltip"]').tooltip();
		  $('#accordion-archive').metisMenu({
				toggle: true //
				});
		  $('.project-content').jScrollPane(
					{
						showArrows: true,
						//arrowScrollOnHover: true
						contentWidth: '0px'
					}
				);
		});
		function checkval()
		{
		  var h1=0;
		   var h2=0;
		  var name=document.getElementById("projectName").value;
		   var studid=document.getElementById("studid").value;
		  //var desc=tinymce.get('prodescription').getContent();
		  var desc=CKEDITOR.instances.prodescription.getData();
		  if (name=="") {
			h1=1;
			BootstrapDialog.alert("Enter the project name");
			document.getElementById("projectName").focus();
			return false;
			}else{
			  h1=0;
			}
			 if (desc=="") {
			h2=1;
			BootstrapDialog.alert("Enter the project description");
			document.getElementById("prodescription").focus();
			return false;
			}else{
			  h2=0;
			}
			if (h1==0&&h2==0) {
	  $.ajax({
   url: 'addproject.php',
   type: 'POST',
   data:  {name:name,desc:desc,studid:studid},
   success: function(data) {
	//alert(data);
    if(data=="success")
	{
	 BootstrapDialog.alert("success");
	}
	else{
	BootstrapDialog.alert("error");
	}
   }
   
});
	  $("#form1")[0].reset();
			}
		}
		function datashow(num) {
		 var val=document.getElementById("proname"+num).value;
		 var valdesc=document.getElementById("prodesc"+num).value;
		 //alert(valdesc);
		 document.getElementById("projectName").value=val;
		 CKEDITOR.instances.prodescription.setData(valdesc);
		 //tinyMCE.get('prodescription').setContent(valdesc);
		 //document.getElementById("prodescription").value=valdesc;
		}
	</script>
