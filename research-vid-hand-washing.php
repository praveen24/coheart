<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>COHEART | About One Health</title>
	<link rel="shortcut icon" href="dist/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="dist/images/favicon.ico" type="image/x-icon">
	<!--Slider-->
	<link rel="stylesheet" href="dist/slider/responsiveslides.css">
	
	<!--FancyBox-->
	<link rel="stylesheet" href="dist/lightbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<!-- Bootstrap -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
	<link href="dist/css/style.css" rel="stylesheet">
	<link href="admin/table/css/footable.core.css" rel="stylesheet"/>
	<link href="admin/table/css/footable.metro.css" rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
	<!--<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>-->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>
    <div>
		<?php require('header.php');?>
		<nav class="navbar navbar-default visible-xs " role="navigation">
					<div class="container-fluid">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
						  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
						  <a class="navbar-brand" href="#">Menu</a>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li><a href="index">Home</a></li>
						<li class="dropdown active">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<li><a href="about-one-health">What is One Health</a></li>
								<li><a href="about-genesis">Genesis of COHEART</a></li>
								<li><a href="about-mandate">Mandate, Vision and Mission of COHEART</a></li>
								<li><a href="about-scope">Scope of One Health</a></li>
								<li><a href="about-objectives">Objectives, Activities and Roap Map</a></li>
								<li><a href="about-faculties">Faculties</a></li>
							  </ul>
						</li>
						
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Courses <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<?php
									require('dbconnect.php');
									$query = mysqli_query($conn,"select id,course_name from courses");
									echo mysqli_error($conn);
									while($data = mysqli_fetch_array($query))
									{
										$course = strtolower($data['course_name']);
										$course = ucwords($course);
										echo "<li><a href='courses?id={$data['id']}'>$course</a></li>";
									}
								?>
							  </ul>
						</li>
						<li><a href="gallery">Gallery</a></li>
						<li><a href="partners">Partnering Institutes and supporters</a></li>
						<li><a href="downloads">Downloads</a></li>
						<li><a href="news">News & events</a></li>
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">COHEART Resources <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<li><a href="research-vid">Video Library</a></li>
								<li><a href="body">Human Body</a></li>
								<li><a href="map">COHEART Health Map</a></li>
							  </ul>
						</li>
						<li><a href="involve">Get Involved</a></li>
						<li><a href="contact">Contact Us</a></li>
							</ul>
							
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
		</nav>
		<div class='content'>
			<div class='row'>
				<div class='col-md-3 col-sm-4 visible-md visible-lg visible-sm'>

					<div class="panel-group" id="accordion">
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-parent="#accordion" href="index">
					          Home
					        </a>
					      </h4>
					    </div>
					   </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" >
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          About Us
					        </a>

					      </h4>
					    </div>
					    <div id="collapseTwo" class="panel-collapse collapse">
					      <div class="panel-body">
					        <ul>
					        	<li><a href="about-one-health" class="active">What is One Health</a></li>
								<li><a href="about-genesis">Genesis of COHEART</a></li>
								<li><a href="about-mandate">Mandate, Vision and Mission of COHEART</a></li>
								<li><a href="about-scope">Scope of One Health</a></li>
								<li><a href="about-objectives">Objectives, Activities and Roap Map</a></li>
								<li><a href="about-faculties">Faculties</a></li>
					        </ul>
					      </div>
					    </div>
					  </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          Courses
					        </a>
					      </h4>
					    </div>
					    <div id="collapseThree" class="panel-collapse collapse">
					      <div class="panel-body">
					        <ul>
					        	<?php
									require('dbconnect.php');
									$query = mysqli_query($conn,"select id,course_name from courses");
									echo mysqli_error($conn);
									while($data = mysqli_fetch_array($query))
									{
										$course = strtolower($data['course_name']);
										$course = ucwords($course);
										echo "<li><a href='courses?id={$data['id']}'>$course</a></li>";
									}
								?>
					        </ul>
					      </div>
					    </div>
					  </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="gallery">Gallery</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="partners">Partnering Institutes and supporters</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="downloads">Downloads</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="news">News & events</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour" class="active">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          COHEART Resources
					        </a>

					      </h4>
					    </div>
					    <div id="collapsefour" class="panel-collapse in">
					      <div class="panel-body">
					        <ul>
					        	<li><a href="research-vid" class="active">Video Library</a></li>
								<li><a href="body">Human Body</a></li>
								<li><a href="map">COHEART Health Map</a></li>
								
							</ul>
					      </div>
					    </div>
					  </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="involve">Get Involved</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="contact">Contact Us</a>
					      </h4>
					    </div>
					   </div>
					</div>
					<?php require('news-updates.php');?>
				</div>
				<div class='col-md-9 col-sm-8'>


					
					<div class='row'>
					<div class='col-md-12 div-vid'>
							<h3>Handwashing</h3>								
								<div class="flex-video widescreen" style="text-align: center">								
								<iframe  style="width:78%"  height="480" allowfullscreen="" src="//www.youtube.com/embed/Y2WIzIAqvkY" frameborder="0"></iframe>
								</div>								
							<div class='row'>
							<div class='col-md-12 col-sm-12 col-xs-12 div-vid'>
								<div class="content-1 content-vid">
									<div class="panel panel-default text-justify">
										<div class="panel-heading">
									<!--	<h3 class="panel-title"><b>Handwashing</b></h3> -->
										<div class="panel-body">
										<p>To wash hands properly, rub all parts of the hands and wrists with soap and water or 
										   an alcohol-based hand rub. Wash hands for at least 15 seconds or more. Pay special attention 
										   to fingertips, between fingers, backs of hands and base of the thumbs.
										</p>
										<ul>
											<li>Keep nails short </li>
											<li>Wash wrists and forearms if they are likely to have</li>
											<li>Remove watches, rings and bracelets been contaminated</li>
											<li>Do not use artificial nails</li>
											<li>Make sure that sleeves are rolled up and do not get</li>
											<li>Avoid chipped nail varnish wet during washing</li>
										</ul>
										<p>Hand washing steps</p>
										<hr>
											<div class='row'>


												<div class="col-md-4">		
													<img class='img-responsive img-floatl' src="img/0b.JPG"  >	
													<p align="center">Wet hands with water</p>
												</div>

												<div class="col-md-4">		
													<img class='img-responsive img-floatl' src="img/1b.JPG"  >	
													<p align="center">Apply enough soap to cover all hand surfaces</p>
												</div>

												<div class="col-md-4">
													<img class='img-responsive img-float1' src="img/2b.JPG" >	
													<p align="center">Rub hands palm to palm</p>
												</div>
											</div>	

											
											<div class='row'>
												<div class="col-md-4">		
													<img class='img-responsive img-floatl' src="img/3b.JPG"  >	
													<p align="center">Right palm over left dorsum with interlaced fingers and vice versa</p>
												</div>

												<div class="col-md-4">		
													<img class='img-responsive img-floatl' src="img/4b.JPG"  >	
													<p align="center">Palm to palm with fingers interlaced</p>
												</div>

												<div class="col-md-4">
													<img class='img-responsive img-float1' src="img/5b.JPG" >	
													<p align="center">Backs of fingers to opposing palms with fingers interlocked</p>
												</div>	

											</div>

											<div class='row'>
												<div class="col-md-4">		
													<img class='img-responsive img-floatl' src="img/6b.JPG"  >	
													<p align="center">Rotational rubbing of left thumb clasped in right palm and vice versa</p>
												</div>

												<div class="col-md-4">		
													<img class='img-responsive img-floatl' src="img/7b.JPG"  >	
													<p align="center">Rotational rubbing, backwards and forwards with clasped fingers of right hand in left palm 
													and vice versa</p>
												</div>

												<div class="col-md-4">
													<img class='img-responsive img-float1' src="img/8b.JPG" >	
													<p align="center">Rinse hands with water</p>
												</div>	

											</div>

											<div class='row'>
												<div class="col-md-4">		
													<img class='img-responsive img-floatl' src="img/9b.JPG"  >	
													<p align="center">Dry hands thoroughly with a single use towel</p>
												</div>

												<div class="col-md-4">		
													<img class='img-responsive img-floatl' src="img/10b.JPG"  >	
													<p align="center">Use towel to turn off faucet</p>
												</div>

												<div class="col-md-4">
													<img class='img-responsive img-float1' src="img/11b.JPG" >	
													<p align="center">Your hands are now safe</p>
												</div>	

											</div>
										</div>
								 	</div>
									 
								</div>
							</div>
							<strong>Watch more..</strong>
							
							<div class='row'>
							<div class="col-md-4">
								<div class="content-1 content-vid">
									<div class="panel panel-default text-justify">
										<div class="panel-heading">
										<h3 class="panel-title">Need of Antemortem Inspection of Animal</h3>
										<div class="panel-body">
											<!--<a href="research-vid-antemortem">-->
											<img class='img-responsive' src="img/c2.png"  >
											</a>
										<!--	<a href="research-vid-antemortem">
											<p class='readm' >Read More</p>
											</a>-->
											</div>
										</div>
								  
									</div>
								</div>
							</div>
							
							<div class='col-md-4  div-vid'>
								<div class="content-1 content-vid">
									<div class="panel panel-default text-justify">
										<div class="panel-heading">
										<h3 class="panel-title">Kitchen hygiene</h3>
										<div class="panel-body">											
										<!--	<a href="research-vid-kitchen"> -->
											<img class='img-responsive' src="img/c7.png"  >
											<!--</a>-->
											<p></p>
											<p></p>
											<!--<a href="research-vid-kitchen">
											<p class='readm' >Know more..</p>
											</a>-->
											</div>
								  
										</div>
									</div>
								</div>
							</div>
							<div class='col-md-4 div-vid'>
								<div class="content-1 content-vid">
									<div class="panel panel-default text-justify">
										<div class="panel-heading">
										<h3 class="panel-title">Storage of cooked food and meat</h3>
										<div class="panel-body">											
											<!--<a href="research-vid-food">-->
											<img class='img-responsive' src="img/c3.png"  >
											<!--</a>-->
											<p></p>
											<p></p>
											<!--<a href="research-vid-food">
											<p class='readm' >Know more..</p>
											</a>-->
											</div>
								  
										</div>
									</div>
								</div>
							</div>
							
							</div>
							<div class='row'>
							<div class='col-md-4 div-vid'>
								<div class="content-1 content-vid">
									<div class="panel panel-default text-justify">
										<div class="panel-heading">
										<h3 class="panel-title">Food Storage</h3>
										<div class="panel-body">											
											<!--<a href="research-vid-storage">-->
											<img class='img-responsive' src="img/c4.png"  >
										<!--	</a>-->
											<p></p>
											<p></p>
											<!--<a href="research-vid-storage">
											<p class='readm' >Know more..</p>
											</a>-->
											</div>
								  
										</div>
									</div>
								</div>
							</div>
							
							<div class='col-md-4 div-vid'>
								<div class="content-1 content-vid">
									<div class="panel panel-default text-justify">
										<div class="panel-heading">
										<h3 class="panel-title">Need of Postmortem Inspection of Meat</h3>
										<div class="panel-body">											
											<!--<a href="research-vid-meat-inspection">-->
											<img class='img-responsive' src="img/c6.png"  >
											<!--</a>-->
											<p></p>
											<p></p>
											<a href="research-vid-meat-inspection">
											<!--<p class='readm' >Know more..</p>
											</a>-->
											</div>
								  
										</div>
									</div>
								</div>
							</div>
							
							<div class='col-md-4 div-vid'>
								<div class="content-1 content-vid">
									<div class="panel panel-default text-justify">
										<div class="panel-heading">
										<h3 class="panel-title">Use of safe water</h3>
										<div class="panel-body">											
										<!--	<a href="research-vid-water">-->
											<img class='img-responsive' src="img/c8.png"  >
											<!--</a>-->
											<p></p>
											<p></p>
											<!--<a href="research-vid-water">
											<p class='readm' >Know more..</p>
											</a>-->
											</div>
								  
										</div>
									</div>
								</div>
							</div>
							</div>
					</div>
					</div>
					</div>
						<div class='row'>
							<div class='col-md-12 visible-xs'>
								<?php require('news-updates.php');?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php require('footer.php');?>	
