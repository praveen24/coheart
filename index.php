
			<?php require('header.php');?>
		
			<div class="content">
				<div class="row">
					<div class='col-md-12 col-sm-12'>
						<section id="news-demo">
					 		<ul class="pgwSlider">
	    						<li><a href="http://vetsinfo.coheart.ac.in" target="_blank"><img src="img/slider/1529381984Graphic1.JPG" alt="VETS WEEKLY" data-description="A Weekly Newsletter Published By COHEART, KVASU"></a></li>
	    						<li><a href="http://coheart.ac.in/map"><img src="img/slider/1529381984Graphic1.JPG" alt="HEALTH MAP" data-description="HealthMap brings data sources to achieve a unified view of the current global state of infectious diseases." data-large-src="img/slider/1529381984Graphic1.JPG"></a></li>
	    						<li>
	       						 <a href="http://coheart.ac.in/body"><img src="img/slider/1529381984Graphic1.JPG" alt="E-BOOK" data-description="Read Articles Online">
	        					</a>
	    						</li>
	    						<li>
	            					<a href="http://elearning2.coheart.ac.in/" target="_blank"><img src="img/slider/1529381984Graphic1.JPG" alt="ELEARNING" data-description="Elearning Portal For Online Courses"></a>
		    						
	    						</li>
		    				</ul>
						</section>
					</div>
				</div>
				<div class="container" style="width: 100%;">
					<div class="row">
						<div class='news-bar no-margin-bottom' style="border: 0px;">
							<div class='col-md-2 breaking-news'>
								<div class='brk-news'>News Updates</div>
							</div>
							<div class='col-md-10 flash-news malayalam' style="padding-left: 0px; padding-right: 0px;">
								<ul id="webticker">	
								<?php
									require('dbconnect.php');
									$query = mysqli_query($conn,"select * from newsflash order by date");
									if($query)
									while($data = mysqli_fetch_array($query))
									{
										echo "<li><a href='admin/uploads/news-flash/{$data['file']}' target='_blank'>{$data['title']} &nbsp;&nbsp;&nbsp;<span class='glyphicon glyphicon-download-alt'></span></a></li>";
									}
								?>	

				    			</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="row msg">
						<div class="col-md-5 text-center">
							<h1>MESSAGE FROM<br>VICE CHANCELLOR</h1>
						</div>
						<div class="col-md-7">
							<p class="pull-left">“Centre for One Health Education, Advocacy, Research and Training (COHEART)" is a new initiative of Kerala Veterinary and Animal Sciences University to propagate One Health programmes in India and most importantly to meet the global public health challenges of the 21st century.</p>
							<hr>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="row about_coheart">
						<div class="col-md-12">
							<div class="container">
								<div class="row">
									<div class="col-md-12 text-center">
										<h3>WHAT IS COHEART</h3>
									</div>
									<div class="col-md-7">
										<p>One Health concept is based on the understanding that the health of the humans, animals and the environment is inextricably linked, and that promoting the well being of all species can only be achieved through a multidisciplinary approach at the human-animal-ecosystems interface. Kerala Veterinary and Animal Sciences University (KVASU) has taken a pioneer step in this regard following the establishment of the Centre for One Health Education Advocacy Research and Training in February 2014. This is the first of its kind centre in India aiming at the sustained health of the community by addressing various issue of concern today like the food safety and security, zoonoses, environmental hazards associated with soil, water and air. This endeavor is built upon by roping in the emerging concept of One Health and disseminating this concept to the stakeholders involved through various educational means. WHO, OIE, FAO and the UN organizations have in various ways highlighted the alarming dangers that could affect health of humans and animals, and the interdependency of health between humans, plants and animals. These organizations have strongly endorsed the concept of One Health which in a simple manner means, we do not simply exist, but co-exist. COHEART aims to generate scientific knowledge on understanding One Health concept that will lead to healthy animal, healthy human and healthy environment.</p>
									</div>
									<div class="col-md-5">
										<iframe style="width:100%" height="400" allowfullscreen="" src="https://www.youtube.com/embed/42zh9bIgNgg" frameborder="0"></iframe>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="container-fluid">
					<div class="row news">
						<div class="col-md-12">
							<div class="container">
								<div class="row">
									<div class="col-md-12">
										<h3 class="text-center">NEWS & EVENTS</h3>
										<?php require('news-updates.php');?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3909.1594721733127!2d76.01799011435861!3d11.540415691805824!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba66d8c17ea8f15%3A0xf3c8bb4137d2de14!2sCenter+for+One+Health+Education%2C+Advocacy%2C+Research+and+Training!5e0!3m2!1sen!2sin!4v1531917696491" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
		<script>
		  	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  	ga('create', 'UA-50095931-2', 'coheart.ac.in');
		  	ga('send', 'pageview');
		</script>
		<?php require('footer.php');?>		
		<script src="dist/js/pgwslider.js"></script>
		<script>
			$(document).ready(function() {
	    		$('.pgwSlider').pgwSlider();
	    	});
	    	$(window).scroll(function(){
			  	var sticky = $('.banner'),
			      	scroll = $(window).scrollTop();
			  	if (scroll >= 100) 
			  		sticky.addClass('fixed');
			  	else 
			  		sticky.removeClass('fixed');
			});
		</script>