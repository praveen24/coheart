
<?php require('header.php');?>
<div class='content about-content'>
    <div class="container">
        <div class='row'>
            <div class='col-md-12 col-sm-12'>
                <div class='row'>
                    <div class='col-md-12 col-sm-12 div-about'>
                        <div class='row'>
                            <div class='col-md-12'>
                                <div class="content-4 content-about">
                                    <div class="panel panel-primary text-justify">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Advocacy News</h3>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        Search <input id="filter" type="text" class="form-control"/>
                                                    </div>
                                                    <div class="new-container">
                                                        <div id="news-results"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class='row news'>
            <div class="col-md-12">
                <div class="container">
                    <div class='row'>
                        <div class='col-md-12'>
                            <?php require('news-updates.php');?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require('footer.php');?>
<script>
    $(window).load(function()
    {
        var search = "";
        var page = "";
        var old ="";
        var table = 'advocacy_news';
        // $('table').addClass('table table-bordered');
        $('.fancybox').fancybox();
        $("#news-results" ).load( "fetch_items.php", {"table" : table},function(){
            $("#news-results").fadeIn();
            old = $('#news-results').html();
        });
        $("#news-results").on( "click", ".pagination a", function (e){
            e.preventDefault();
            $("#news-results").hide();
            $("#news-results").html('<div class="text-center text-muted">Loading...</div>');
            $("#news-results").fadeIn();
            var page = $(this).attr("data-page");
            $("#news-results").load("fetch_items.php",{"page":page,"search":search, "table" : table}, function(){
                //$("#news-results").hide();
                $("#news-results").fadeIn();
                //$('html, body').animate({scrollTop: "0px"}, 800);
            });
        });
        $('#filter').keyup(function(){
            search = $(this).val();
            $("#news-results").html('<div class="text-center text-muted">Loading...</div>');
            //$("#news-results").fadeIn();
            if(search != "")
            {
                $("#news-results").load("fetch_items.php",{"search":search, "table":table}, function(){
                    $("#news-results").hide();
                    $("#news-results").fadeIn();
                });
            }
            else
            {
                $("#news-results").hide();
                $('#news-results').html(old);
                $("#news-results").fadeIn();
            }
        });
    });
</script>