<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="en">
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="keywords" content="coheart, kvasu, veterinary, one, health, university, kerala, animal, science, pookode,diploma,certification,course, education, hygiene,Education, Advocacy, Research, Training "/>
	   	<meta name="description" content="One Health concept is based on the understanding that the health of the humans, animals and the environment is inextricably linked, and that promoting the well being of all species can only be achieved through a holistic multidisciplinary approach at the human-animal-ecosystems interface. Kerala Veterinary and Animal Sciences University (KVASU) has taken a pioneer step in this regard following the establishment of the Centre for One Health Education Advocacy Research and Training, the first of its kind in India aiming at the sustained health of the community by addressing various issue of concern today like the food safety and security, zoonoses, diseases from natural origins like soil, water and air."/>
	    <meta name="google-site-verification" content="Sg2CCDPE0zio41cI6pF2SkUWVUX1nSGT8waFfR8S6f0" />
	    <title>COHEART | Centre for One Health Education Advocacy Research and Training</title>
		<link rel="shortcut icon" href="dist/images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="dist/images/favicon.ico" type="image/x-icon">
		<!--Slider-->
		<link rel="stylesheet" href="dist/css/pgwslider.css">
		<link rel="stylesheet" href="dist/css/pgwslider.min.css">
		<!--flash-->
		<link rel="stylesheet" href="ticker/example/assets/webticker.css" type="text/css" media="screen">
		<!--FancyBox-->
		<link rel="stylesheet" href="dist/lightbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
		<!-- Bootstrap -->
	    <link href="dist/css/bootstrap.css" rel="stylesheet">
		<link href="dist/css/main.css" rel="stylesheet">
		<link href="admin/table/css/footable.core.css" rel="stylesheet"/>
		<link href="admin/table/css/footable.metro.css" rel="stylesheet"/>
		<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
		<!--<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>-->
	    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
	    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css">
  	</head>
  	<body>
    	<div class=''>		
		<div class='banner'>
			<div class='container-fluid hidden-xs hidden-sm'>
				<div class='row'>
					<div class="col-sm-12">
						<a href="http://www.kvasu.ac.in/" target="_blank"><img src="dist/images/kvasu-university-logo-500x500.png" class="img-responsive pull-right" style="margin-right: 15px;width: 40px;"></a>
						<p class="pull-right" style="font-size: 17px;"><i>Kerala Veterinary and Animal Sciences University</i> </p>
					</div>
					<div class='col-md-3 col-sm-2 col-xs-4 text-center'>
						<a href="index.php"><img src="img/logo.png" class="img-responsive logo"></a>
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="main-menu hidden-xs">
							<div class="col-sm-3 col-sm-offset-1 col-lg-3 col-lg-offset-1 col-md-3 col-md-offset-1 hidden-xs">
								<h3><span>C</span>ENTRE&nbsp; For&nbsp; <span>O</span>NE&nbsp; <span>H</span>EALTH</h3>
							</div>
							<div class="col-lg-8 col-md-8 col-sm-8 hidden-xs" style="padding-left: 0;">
								<ul class="menu pull-left">
									<li>
										<a href="#">
											<span>E</span>ducation <i class="fa fa-angle-down"></i>
											<div class="clearfix"></div>
											<small>&nbsp;</small>
										</a>
										<ul class="sub-menu">
											<li><a href="pg-diploma-in-one-health">PG Diploma in One Health</a></li>
											<li><a href="pg-certificate-in-one-health">PG Certificate in One Health Surveillance</a></li>
											<li><a href="pg-certificate-in-cbdm">PG Certificate in CBDM</a></li>
											<li><a href="how-to-apply">How to Apply</a></li>
											<div class="clearfix"></div>
											<li><a href="http://coheart.ac.in/elearning" target="_blank">E-learning</a></li>
											<div class="clearfix"></div>
											<li><a href="past-and-present-students">Past and Present Students</a></li>
										</ul>
									</li>
									<li>
										<a href="#">
											<span>A</span>dvocacy <i class="fa fa-angle-down"></i>
											<div class="clearfix"></div>
											<small>&nbsp;</small>
										</a>
										<ul class="sub-menu">
											<li><a href="#">Dr. Coheart channel</a></li>
											<li><a href="#">Vets Info</a></li>
											<div class="clearfix"></div>
											<li><a href="#">WISDOM</a></li>
											<div class="clearfix"></div>
											<li><a href="#">FSKC</a></li>
											<div class="clearfix"></div>
											<li><a href="#">VetSign Pro</a></li>
											<div class="clearfix"></div>
											<li><a href="#">Health Map</a></li>
											<div class="clearfix"></div>
											<li><a href="#">Curofy</a></li>
											<div class="clearfix"></div>
											<li><a href="#">Important Links</a></li>
											<div class="clearfix"></div>
											<li><a href="advocacy-news">Advocacy News</a></li>
										</ul>
									</li>
									<li>
										<a href="#">
											<span>R</span>esearch <i class="fa fa-angle-down"></i>
											<div class="clearfix"></div>
											<small>&nbsp;</small>
										</a>
										<ul class="sub-menu">
											<li><a href="research-projects">Research projects</a></li>
											<li><a href="research-highlights">Research Highlights</a></li>
											<div class="clearfix"></div>
											<li><a href="publications">Publications</a></li>
											<div class="clearfix"></div>
											<li><a href="#">Publication partner links</a></li>
										</ul>
									</li>
									<li>
										<a href="#">
											<span>T</span>raining <i class="fa fa-angle-down"></i>
											<div class="clearfix"></div>
											<small style="font-size: 10px;position: relative;top: -13px;">Capacity Building</small>
										</a>
										<ul class="sub-menu">
											<li><a href="training">Training</a></li>
											<div class="clearfix"></div>
											<li><a href="seminar">Seminar</a></li>
											<div class="clearfix"></div>
											<li><a href="workshop">Workshop</a></li>
											<div class="clearfix"></div>
											<li><a href="important-day-commemoration">Important Day Commemoration</a></li>
											<li><a href="student-events">Student Events</a></li>
											<div class="clearfix"></div>
											<li><a href="camp">Camp</a></li>
											<div class="clearfix"></div>
											<li><a href="awareness-class">Awareness Class</a></li>
											<div class="clearfix"></div>
											<li><a href="interface-meet">Interface Meet</a></li>
										</ul>
									</li>
								</ul>
								<div class="pull-right">
									<div class="google-search">
										<script>
										  	(function() {
										    	var cx = '001763096455298922418:gkxx4zxqozc';
										    	var gcse = document.createElement('script');
										    	gcse.type = 'text/javascript';
										    	gcse.async = true;
										    	gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
										        '//www.google.com/cse/cse.js?cx=' + cx;
										    	var s = document.getElementsByTagName('script')[0];
										    	s.parentNode.insertBefore(gcse, s);
										  	})();
										</script>
										<gcse:search></gcse:search>
									</div>	
								</div>
							</div>
						</div>
						<span class="harmony pull-right"><i>For Harmony among Man, Animal and Nature</i></span> 
						<ul class="subMenu hidden-xs">
							<li><a href="index.php">Home</a></li>
							<li>
								<a href="#">About us</a>
								<ul>
									<li><a href="about-one-health">What is One Health</a></li>
									<li><a href="about-scope">Scope of One Health</a></li>
									<li><a href="about-genesis">Genesis of COHEART</a></li>
									<li><a href="about-mandate">More about COHEART</a></li>
									<li><a href="about-faculties">Faculties</a></li>
								</ul>
							</li>
							<li><a href="gallery">Gallery</a></li>
							<li><a href="news">News</a></li>
							<li><a href="partners">Partners</a></li>
							<li><a href="involve">Get Involved</a></li>
							<li><a href="downloads">Downloads</a></li>
							<li><a href="contact">Contact</a></li>
						</ul>
					</div>
				</div>
			</div><!--/container-->
		
			<div class='row visible-xs visible-sm'>
				<div class='col-xs-4 text-center'>
					<a href="index"><img src='img/logo.png' alt="kvasu logo" class='img-responsive logo'></a>
				</div>
				<div class="col-xs-8">
					<script>
					  	(function() {
							var cx = '001763096455298922418:gkxx4zxqozc';
							var gcse = document.createElement('script');
							gcse.type = 'text/javascript';
							gcse.async = true;
							gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							'//www.google.com/cse/cse.js?cx=' + cx;
							var s = document.getElementsByTagName('script')[0];
							s.parentNode.insertBefore(gcse, s);
					  	})();
					</script>
					<gcse:search></gcse:search>
				</div>
			</div>
			<nav class="navbar navbar-default visible-xs visible-sm" role="navigation">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
					  	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
					  	</button>
					  	<a class="navbar-brand" href="#">Menu</a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li><a href="index.php">Home</a></li>
							<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Education <b class="caret pull-right"></b></a>
								<ul class="dropdown-menu">
									<li><a href="pg-diploma-in-one-health">PG Diploma in One Health</a></li>
									<li><a href="pg-certificate-in-one-health">PG Certificate in One Health Surveillance</a></li>
									<li><a href="pg-certificate-in-cbdm">PG Certificate in CBDM</a></li>
									<li><a href="how-to-apply">How to Apply</a></li>
									<div class="clearfix"></div>
									<li><a href="http://coheart.ac.in/elearning" target="_blank">E-learning</a></li>
									<div class="clearfix"></div>
									<li><a href="past-and-present-students">Past and Present Students</a></li>
								</ul>
							</li>
							<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Advocacy <b class="caret pull-right"></b></a>
								<ul class="dropdown-menu">
									<li><a href="#">Dr. Coheart channel</a></li>
									<li><a href="#">Vets Info</a></li>
									<div class="clearfix"></div>
									<li><a href="#">WISDOM</a></li>
									<div class="clearfix"></div>
									<li><a href="#">FSKC</a></li>
									<div class="clearfix"></div>
									<li><a href="#">VetSign Pro</a></li>
									<div class="clearfix"></div>
									<li><a href="#">Health Map</a></li>
									<div class="clearfix"></div>
									<li><a href="#">Curofy</a></li>
									<div class="clearfix"></div>
									<li><a href="#">Important Links</a></li>
									<div class="clearfix"></div>
									<li><a href="advocacy-news">Advocacy News</a></li>
								</ul>
							</li>
							<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Research <b class="caret pull-right"></b></a>
								<ul class="dropdown-menu">
									<li><a href="research-projects">Research projects</a></li>
									<li><a href="research-highlights">Research Highlights</a></li>
									<div class="clearfix"></div>
									<li><a href="publications">Publications</a></li>
									<div class="clearfix"></div>
									<li><a href="#">Publication partner links</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Training <b class="caret pull-right"></b>
								<ul class="dropdown-menu">
									<li><a href="training">Training</a></li>
									<div class="clearfix"></div>
									<li><a href="seminar">Seminar</a></li>
									<div class="clearfix"></div>
									<li><a href="workshop">Workshop</a></li>
									<div class="clearfix"></div>
									<li><a href="important-day-commemoration">Imp Day commemoration</a></li>
									<li><a href="student-events">Student Events</a></li>
									<div class="clearfix"></div>
									<li><a href="camp">Camp</a></li>
									<div class="clearfix"></div>
									<li><a href="awareness-class">Awareness Class</a></li>
									<div class="clearfix"></div>
									<li><a href="interface-meet">Interface meet</a></li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">About us <b class="caret pull-right"></b></a>
								<ul class="dropdown-menu">
									<li><a href="about-one-health">What is One Health</a></li>
									<li><a href="about-scope">Scope of One Health</a></li>
									<li><a href="about-genesis">Genesis of COHEART</a></li>
									<li><a href="about-mandate">More about COHEART</a></li>
									<li><a href="about-faculties">Faculties</a></li>
								</ul>
							</li>
							<li><a href="gallery">Gallery</a></li>
							<li><a href="news">News</a></li>
							<li><a href="partners">Partners</a></li>
							<li><a href="involve">Get Involved</a></li>
							<li><a href="downloads">Downloads</a></li>
							<li><a href="contact">Contact</a></li>
						</ul>
					</div>
				</div>
			</nav>
		</div>