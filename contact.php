
		<?php require('header.php');?>
		<div class='content about-content'>
			<div class="container">
				<div class='row'>
					<div class='col-md-12 col-sm-12'>
						<div class='row'>
							<div class='col-md-12 div-about'>
								<div class="content-4 content-about">
									<div class="panel panel-primary text-justify">
								  		<div class="panel-heading">
								  			<h3 class="panel-title">Contact Us</h3>
								  		</div>
								  		<div class="panel-body">
											<div class="row">
												<div class="col-md-6 col-sm-6 col-xs-12">
													<img src='dist/images/faculty1.jpg' alt="prejith" class="img-thumbnail img-responsive">
													<br/><br/>
													<address>
									  					<strong>Dr. PREJIT</strong><br>
									  					Officer-In-Charge<br>
									  					Centre for One Health Education, Advocacy, Research and Training,<br>
									  					Kerala Veterinary and Animal Sciences University,<br>
									  					Pookode, Wayanad<br>
									 	 				Kerala- 673576<br>	
									  					Telephone: +91-9497679630<br>
									  					Email: <a href="javascript: void(0);">onehealthindia@gmail.com</a>,
									  		 			<a href="javascript: void(0);">prejit@kvasu.ac.in</a>
													</address>
												</div>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<img src='dist/images/faculty2.jpg' alt="vinod" class="img-thumbnail img-responsive">
													<br/><br/>
													<address>
									  					<strong>Dr. VINOD VK</strong><br>
									  					Co-ordinator<br>
									  					Centre for One Health Education, Advocacy, Research and Training,<br>
									  					Department of Veterinary Public Health,<br>
									  					Kerala Veterinary and Animal Sciences University,<br>
									  					Pookode, Wayanad<br>
									  					Kerala- 673576<br>	
									  					Telephone: +91-9447677539<br>
									  					Email: <a href="javascript: void(0);">vinodvk@kvasu.ac.in</a>							  	 
													</address>
												</div>
											</div>
											<h3>Location</h3>
											<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d125093.24295107095!2d76.019915!3d11.540102000000017!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba66d8c17ea8f15%3A0xf3c8bb4137d2de14!2sCOHEART(center+for+one+health+education%2C+advocacy%2C+research+and+training)!5e0!3m2!1sen!2sin!4v1397663183453" width="100%" height="400" frameborder="0" style="border:0" class='map'></iframe>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="row news">
					<div class="col-md-12">
						<div class="container">
							<div class='row'>
								<div class='col-md-12'>
									<?php require('news-updates.php');?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php require('footer.php');?>
	