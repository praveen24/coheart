
		<?php require('header.php');?>
		<div class='content about-content'>
			<div class="container">
				<div class='row'>				
					<div class='col-md-12 col-sm-12'>
						<div class='row'>
							<div class='col-md-12 div-about'>
								<div class="content-4 content-about">
									<div class="panel panel-primary text-justify">
								  		<div class="panel-heading">
											<h3 class="panel-title">Albums</h3>
										</div>
								  		<div class="panel-body">
										 	<ul id="sidenav02" class="accordion nav nav-department nav-tabs nav-stacked">
											<?php require('dbconnect.php');
												$query = mysqli_query($conn,"select * from albums where name!='Home page slider'");
												while($data = mysqli_fetch_array($query))
												{
													echo "<li class='accordian-group'>";
													echo "<a href='javascript: void(0);' data-toggle='collapse' data-target='#subnav{$data['id']}', data-parent='#sidenav02' class='accordian-toggle collapsed'><span class='glyphicon glyphicon-chevron-down pull-right'></span>{$data['name']}</a>";
													echo "<ul id='subnav{$data['id']}' class='nav nav-list collapse'>";
													echo "<div class='well' style='margin-bottom:0; border:0; border-radius:0; padding:9px 10px;background-color:transparent; font-size: 15px;'>{$data['description']}</div>";
													echo "<li>";
													$files=glob("admin/uploads/album/{$data['id']}/thumbnail/*.*");
													$files2=glob("admin/uploads/album/{$data['id']}/*.*");
													$i = count($files);
													$flag = 0;
													for($j=0;$j<$i;$j++)
													{
														echo "<a class='fancybox' href='$files2[$j]' data-fancybox-group='gallery{$data['id']}'><img src='$files[$j]' alt='coheart kvasu' class='img-responsive img-thumbnail' style=''/></a>";
														$flag = 1;
													}
													if($flag == 0)
														echo "<a class='fancybox' href='javascript:void(0);' style='color: #000; font-size: 13px;'>No images to display...</a>";
													echo "</li>";
													echo "</ul>";
													echo "</li>";
												}
											?>
											</ul>
										</div>
									</div>
								</div>
							<!--/nav-list menu-->
							</div>
						</div>
						<div class='row'>
							<div class='col-md-12 visible-xs'>
								<?php require('news-updates.php');?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php require('footer.php');?>		
		<script>
			$(function()
			{
				$('#course-table').footable();
				$('.fancybox').fancybox();
				$('#sidenav02 li:first-child a').removeClass('collapsed');
				$('#sidenav02 li:first-child ul').removeClass('collapse').addClass('in');
				
			});
		</script>