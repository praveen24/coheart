<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="coheart, kvasu, veterinary, one, health, university, kerala, animal, science, pookode,diploma,certification,course, education, hygiene,Education, Advocacy, Research, Training "/>
   	<meta name="description" content="One Health concept is based on the understanding that the health of the humans, animals and the environment is inextricably linked, and that promoting the well being of all species can only be achieved through a holistic multidisciplinary approach at the human-animal-ecosystems interface. Kerala Veterinary and Animal Sciences University (KVASU) has taken a pioneer step in this regard following the establishment of the Centre for One Health Education Advocacy Research and Training, the first of its kind in India aiming at the sustained health of the community by addressing various issue of concern today like the food safety and security, zoonoses, diseases from natural origins like soil, water and air."/>
    <meta name="google-site-verification" content="Sg2CCDPE0zio41cI6pF2SkUWVUX1nSGT8waFfR8S6f0" />
    <title>COHEART | Student Home Page</title>
	<link rel="shortcut icon" href="dist/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="dist/images/favicon.ico" type="image/x-icon">
	<!--Slider-->
	<link rel="stylesheet" href="dist/slider/responsiveslides.css">
	<!--flash-->
	<link rel="stylesheet" href="ticker/example/assets/webticker.css" type="text/css" media="screen">
	<!--FancyBox-->
	<link rel="stylesheet" href="dist/lightbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<!--login modal-->
	<link rel="stylesheet" href="modal/css/style.css"> <!-- Gem style -->
	<!-- Bootstrap -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
	<link href="dist/css/style.css" rel="stylesheet">
	<link href="admin/table/css/footable.core.css" rel="stylesheet"/>
	<link href="admin/table/css/footable.metro.css" rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
	<!--<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>-->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script src="modal/js/modernizr.js"></script> <!-- Modernizr -->
  </head>
  <body>
    <div class=''>
		<div class='banner'>
			<div class='container'>
				<div class='row hidden-xs'>
					<div class='col-md-1 col-md-offset-1 col-xs-1 col-md-offset-1 text-center'>
						<embed height="150" width="130" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" wmode="transparent" menu="false" quality="high" src="dist/images/coheart-logo.swf" style="display: block !important;">
					</div>
					<div class="col-md-6 col-xs-6">
						<div class="row">
							<div class="col-md-12 text-center">
								<h3 style="color: #fff;"><strong>CENTER for ONE HEALTH EDUCATION ADVOCACY RESEARCH and TRAINING</strong></h3>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 text-center">
								<h4>Kerala Veterinary and Animal Sciences University</h4>
								<h4>Pookode, Wayanad Kerala - 673576</h4>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-xs-4">
						<div class="row">
							<div class="col-md-12 text-right">
								<h4 class="man"><i>"For multidisciplinary holistic approach for harmony among Man, Animal and Nature (M.A.N)"</i></h4>
							</div>
						</div>
						<div class="row">
							<div class="col-md-offset-3 col-md-9 text-right google-search">
								<script>
								  (function() {
								    var cx = '001763096455298922418:gkxx4zxqozc';
								    var gcse = document.createElement('script');
								    gcse.type = 'text/javascript';
								    gcse.async = true;
								    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
								        '//www.google.com/cse/cse.js?cx=' + cx;
								    var s = document.getElementsByTagName('script')[0];
								    s.parentNode.insertBefore(gcse, s);
								  })();
								</script>
								<gcse:search></gcse:search>
							</div>
						</div>
					</div>
				</div>
			</div><!--/container-->
		
			<div class='row visible-xs'>
				<div class='col-xs-12 text-center'>
					<a href="index"><img src='dist/images/kvasu-coheart-logo-800X212.png' alt="kvasu logo" width='544' height='126' class='img-responsive'></a>
				</div>
			</div>
			<div class="row visible-xs">
				<div class="col-md-12">
					<script>
					  (function() {
						var cx = '001763096455298922418:gkxx4zxqozc';
						var gcse = document.createElement('script');
						gcse.type = 'text/javascript';
						gcse.async = true;
						gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							'//www.google.com/cse/cse.js?cx=' + cx;
						var s = document.getElementsByTagName('script')[0];
						s.parentNode.insertBefore(gcse, s);
					  })();
					</script>
					<gcse:search></gcse:search>
				</div>
			</div>
		</div><!--/banner-->
		<div class="container" style="width: 100%;">
		<!--<div class="row">
			<div class='news-bar no-margin-bottom' style="border: 0px;">
				<div class='col-md-2 breaking-news'>
					<div class='brk-news'>News Updates</div>
				</div>
				<div class='col-md-10 flash-news malayalam' style="padding-left: 0px; padding-right: 0px;">
					<ul id="webticker">	
					<?php
						require('dbconnect.php');
						$query = mysqli_query($conn,"select * from newsflash order by date");
						if($query)
						while($data = mysqli_fetch_array($query))
						{
							echo "<li><a href='admin/uploads/news-flash/{$data['file']}' target='_blank'>{$data['title']} &nbsp;&nbsp;&nbsp;<span class='glyphicon glyphicon-download-alt'></span></a></li>";
						}
					?>	

	    			</ul>
				</div>
			</div>
		</div>-->
		</div>
		<nav class="navbar navbar-default visible-xs " role="navigation">
					<div class="container-fluid">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
						  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
						  <a class="navbar-brand" href="#">Menu</a>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li><a href="index">Home</a></li>
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<li><a href="about-one-health">What is One Health</a></li>
								<li><a href="about-genesis">Genesis of COHEART</a></li>
								<li><a href="about-mandate">Mandate, Vision and Mission of COHEART</a></li>
								<li><a href="about-scope">Scope of One Health</a></li>
								<li><a href="about-objectives">Objectives, Activities and Roap Map</a></li>
								<li><a href="about-faculties">Faculties</a></li>
							  </ul>
						</li>
						
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Courses <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<?php
									require('dbconnect.php');
									$query = mysqli_query($conn,"select id,course_name from courses");
									echo mysqli_error($conn);
									while($data = mysqli_fetch_array($query))
									{
										$course = strtolower($data['course_name']);
										$course = ucwords($course);
										echo "<li><a href='courses?id={$data['id']}'>$course</a></li>";
									}
								?>
							  </ul>
						</li>
						<li><a href="gallery">Gallery</a></li>
						<li><a href="partners">Partnering Institutes and supporters</a></li>
						<li class="active"><a href="studentProfile">Student Profile</a></li>
						<li><a href="downloads">Downloads</a></li>
						<li><a href="news">News & events</a></li>
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">COHEART Resources <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<li><a href="research-vid">Video Library</a></li>
								<li><a href="body">Human Body</a></li>
								<li><a href="map">COHEART Health Map</a></li>
							  </ul>
						</li>
						<li><a href="involve">Get Involved</a></li>
						<li><a href="contact">Contact Us</a></li>
							</ul>
							
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
		</nav>
		<div class="content">
			<div class='row'>
				<div class='col-md-3 col-sm-4 visible-md visible-lg visible-sm'>
					<div class="panel-group" id="accordion">
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-parent="#accordion" href="index">
					          Home
					        </a>
					      </h4>
					    </div>
					   </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          About Us
					        </a>

					      </h4>
					    </div>
					    <div id="collapseTwo" class="panel-collapse collapse">
					      <div class="panel-body">
					        <ul>
					        	<li><a href="about-one-health">What is One Health</a></li>
								<li><a href="about-genesis">Genesis of COHEART</a></li>
								<li><a href="about-mandate">Mandate, Vision and Mission of COHEART</a></li>
								<li><a href="about-scope">Scope of One Health</a></li>
								<li><a href="about-objectives">Objectives, Activities and Roap Map</a></li>
								<li><a href="about-faculties">Faculties</a></li>
					        </ul>
					      </div>
					    </div>
					  </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          Courses
					        </a>
					      </h4>
					    </div>
					    <div id="collapseThree" class="panel-collapse collapse">
					      <div class="panel-body">
					        <ul>
					        	<?php
									require('dbconnect.php');
									$query = mysqli_query($conn,"select id,course_name from courses");
									echo mysqli_error($conn);
									while($data = mysqli_fetch_array($query))
									{
										$course = strtolower($data['course_name']);
										$course = ucwords($course);
										echo "<li><a href='courses?id={$data['id']}'>$course</a></li>";
									}
								?>
					        </ul>
					      </div>
					    </div>
					  </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="gallery">Gallery</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="partners">Partnering Institutes and supporters</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="studentHome" class="active">Student Profile</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="downloads">Downloads</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="news">News & events</a>
					      </h4>
					    </div>
					   </div>
					     <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          COHEART Resources
					        </a>

					      </h4>
					    </div>
					    <div id="collapsefour" class="panel-collapse collapse">
					      <div class="panel-body">
					        <ul>
					        	<li><a href="research-vid">Video Library</a></li>
								<li><a href="body">Human Body</a></li>
								<li><a href="map">COHEART Health Map</a></li>
								
							</ul>
					      </div>
					    </div>
					  </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="involve">Get Involved</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="contact">Contact Us</a>
					      </h4>
					    </div>
					   </div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-info panel-custom " style='margin-top: 20px; margin-bottom: 20px;'>
								<!--<div class="panel-heading panel-update">
									<h3 class="panel-title text-center">DE's Message</h3>
								</div>-->
								<div class="panel-body text-justify">
									<div class="de text-center">
										<img src="dist/images/vc.jpg" alt="ramkumar" class="img-rounded"><br>
										<strong>Dr B Ashok</strong><br>
										Vice Chancellor
									</div>
									<br>
									<p>
										"COHEART" is a new initiative of Kerala Veterinary and Animal Sciences University....  
										<a href="vc-message" class="read-more pull-right">Read More...</a> 
									</p>
									
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="">
										<a href="research-vid-bird"  style="text-decoration: none;">
										
										<img src="dist/images/coheart-bird-flue.JPG" align="" class="img-thumbnail img-responsive"/>
										</a>
									</div>
							
						</div>
					</div>
					
					<?php require('news-updates.php');?>
				</div>
				<div class='col-md-9 col-sm-8'>
					<div class="row student-profile">
						<div class="col-md-12 text-center">
							<div class="title">Student Home</div>
							<a href="javascript:void(0)" class="text-link cd-signin-2">Login</a>
							<div class="div-student-home-panels">
								<div class="row text-center">
									<div class="col-md-offset-2 col-md-4 col-sm-6">
										<div class="panel panel-default">
										  <div class="panel-body text-center">
										    <a href="studentProfile?categ=student" class="cd-signin"><img src="dist/images/student-blog.png" class="img-responsive" width="200"></a>
										  </div>
										  <div class="panel-footer"><a href="studentProfile?categ=student" class="text-link">Student Profile</a></div>
										</div>
									</div>
									<div class="col-md-4 col-sm-6">
										<div class="panel panel-default">
										  <div class="panel-body text-center">
										    <a href="studentProfile?categ=member"><img src="dist/images/student-blog.png" class="img-responsive" width="200"></a>
										  </div>
										  <div class="panel-footer"><a href="studentProfile?categ=member" class="text-link">Member Profile</a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- login modal-->

	<div class="cd-user-modal"> <!-- this is the entire modal form, including the background -->
		<div class="cd-user-modal-container"> <!-- this is the container wrapper -->
			<div class="cd-user-modal-title">
				Sign in
			</div>
			
			<div id="cd-login"> <!-- log in form -->
				<form class="cd-form" id="loginform">
				  <span id="log_error" style="color: #900;display: none;">Invalid Login</span>
					<p class="fieldset">
						<label class="image-replace cd-email" for="signin-email">E-mail</label>
						<input class="full-width has-padding has-border" id="signin-email" name="signin-email" type="email" placeholder="E-mail">
						<!--cd-error-message-->
					</p>

					<p class="fieldset">
						<label class="image-replace cd-password" for="signin-password">Password</label>
						<input class="full-width has-padding has-border" id="signin-password" name="signin-password" type="text"  placeholder="Password">
						<a href="javascript:void(0)" class="hide-password">Hide</a>
						<span class="cd-error-message" id="pswderror">Error message here!</span>
					</p>

					<p class="fieldset">
						<input type="checkbox" id="remember-me" checked>
						<label for="remember-me">Remember me</label>
					</p>

					<p class="fieldset">
						<input class="full-width" type="button" style="background: #2f889a;border: none;padding: 16px;color: #fff;" name="login" value="login" onclick="logincheck()">
					</p>
				</form>
				
				<p class="cd-form-bottom-message"><a href="#0">Forgot your password?</a></p>
				<!-- <a href="#0" class="cd-close-form">Close</a> -->
			</div> <!-- cd-login -->

			
			<div id="cd-reset-password"> <!-- reset password form -->
				<p class="cd-form-message">Lost your password? Please enter your email address. You will receive a link to create a new password.</p>

				<form class="cd-form">
					<p class="fieldset">
						<label class="image-replace cd-email" for="reset-email">E-mail</label>
						<input class="full-width has-padding has-border" id="reset-email" type="email" placeholder="E-mail">
						<span class="cd-error-message">Error message here!</span>
					</p>

					<p class="fieldset">
						<input class="full-width has-padding" type="submit" value="Reset password">
					</p>
				</form>

				<p class="cd-form-bottom-message"><a href="#0">Back to log-in</a></p>
			</div> <!-- cd-reset-password -->
			<a href="#0" class="cd-close-form">Close</a>
		</div> <!-- cd-user-modal-container -->
	</div> <!-- cd-user-modal -->

	<!--end of login modal-->


	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-50095931-2', 'coheart.ac.in');
	  ga('send', 'pageview');
	</script>

<?php require('footer.php');?>
<script src="modal/js/main.js"></script>		
<script type="text/javascript">
  function logincheck() {
	var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/; 
	var mail=document.getElementById("signin-email").value;
	var pswd=document.getElementById("signin-password").value;
	var h1=0;
	var h2=0;
	if (mail=="") {
	  $("#signin-email").css('border','1px solid #900');  
	 h1=1;
	document.getElementById("signin-email").focus();
	 return false;
	}else if (mail!=="") {
	 if(mail.match(re))
		{
		   $("#signin-email").css('border','1px solid #DDDDDD'); 
		  h1=0;
		}else{
			  $("#signin-email").css('border','1px solid #900');
			  document.getElementById("signin-email").focus();
	 h1=1;
	 return false;
		}
	}
	if (pswd=="") {
	$("#signin-password").css('border','1px solid #900'); 
	 h2=1;  
	 document.getElementById("signin-password").focus();
	 return false;
	}else{
	   $("#signin-password").css('border','1px solid #DDDDDD');
	  h2=0;
	}
	if (h1==0 && h2==0) {
	  var dat=$("#loginform").serialize();
	  $.ajax({
   url: 'logincheck.php',
   type: 'POST',
   data:  dat,
   success: function(data) {
    if(data=="success")
	{
	  window.location="studentAccount";
	}
	else{
	 $("#log_error").css("display","block");
	}
   }
});
	}
  }
		$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
		})
	</script>