<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>COHEART | Bhakshya Suraksha Article</title>
		<link rel="icon" href="../dist/images/favicon.ico" type="image/x-icon"> 
		<link rel="stylesheet" type="text/css" href="css/jquery.jscrollpane.custom.css" />
		<link rel="stylesheet" type="text/css" href="css/bookblock.css" />
		<link rel="stylesheet" type="text/css" href="css/custom.css" />
		<script src="js/modernizr.custom.79639.js"></script>
	</head>
	<body>
		<div id="container" class="container">	

			<div class="menu-panel">
				<h3>Table of Contents</h3>
				<ul id="menu-toc" class="menu-toc">
					<li class="menu-toc-current"><a href="#item1">1. എന്താണ് ഭക്ഷ്യ സുരക്ഷ</a></li>
					<li><a href="#item2">2. ലക്ഷണങ്ങൾ</a></li>
					<li><a href="#item3">3. മുൻ കരുതലുകൾ</a></li>
				</ul>
				
			</div>

			<div class="bb-custom-wrapper">
				<div id="bb-bookblock" class="bb-bookblock">
					<div class="bb-item" id="item1">
						<div class="content">
							<div class="scroller" align="center">
								<h3>എന്താണ് ഭക്ഷ്യ സുരക്ഷ</h3>
								<br>
								<div>
									<img src="images/suraksha/coheart-bhakshya-suraksha-1.jpg" alt="coheart bhakshya suraksha" width="80%" >	
								</div>	
							</div>
						</div>
					</div>
					<div class="bb-item" id="item2">
						<div class="content">
							<div class="scroller" align="center">
								<h3>ലക്ഷണങ്ങൾ</h3>
								<br>
								<div>
									<img src="images/suraksha/coheart-bhakshya-suraksha-2.jpg" alt="coheart bhakshya suraksha" width="80%" >	
								</div>	
							</div>
						</div>
					</div>
					<div class="bb-item" id="item3">
						<div class="content">
							<div class="scroller" align="center">
								<h3>മുൻ കരുതലുകളും നിയന്ത്രണവും</h3>
								<br>
								<div>
									<img src="images/suraksha/coheart-bhakshya-suraksha-3.jpg" alt="coheart bhakshya suraksha" width="80%" >	
								</div>	
							</div>
						</div>
					</div>
				</div>
				
				<nav>
					<span id="bb-nav-prev">&larr;</span>
					<span id="bb-nav-next">&rarr;</span>
				</nav>

				<span id="tblcontents" class="menu-button">Table of Contents</span>

			</div>
				
		</div><!-- /container -->
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script src="js/jquery.mousewheel.js"></script>
		<script src="js/jquery.jscrollpane.min.js"></script>
		<script src="js/jquerypp.custom.js"></script>
		<script src="js/jquery.bookblock.js"></script>
		<script src="js/page.js"></script>
		<script>
			$(function() {

				Page.init();

			});
		</script>
	</body>
</html>
