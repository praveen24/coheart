<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>COHEART | Human Body Article</title>
		<link rel="icon" href="../dist/images/favicon.ico" type="image/x-icon"> 
		<link rel="stylesheet" type="text/css" href="css/jquery.jscrollpane.custom.css" />
		<link rel="stylesheet" type="text/css" href="css/bookblock.css" />
		<link rel="stylesheet" type="text/css" href="css/custom.css" />
		<script src="js/modernizr.custom.79639.js"></script>
	</head>
	<body>
		<div id="container" class="container">	

			<div class="menu-panel">
				<h3>Table of Contents</h3>
				<ul id="menu-toc" class="menu-toc">
					<li class="menu-toc-current"><a href="#item1">Home</a></li>
					<li><a href="#item2">1.	Introduction</a></li>
					<li><a href="#item3">2.	Brain</a></li>
					<li><a href="#item4"></a></li>
					<li><a href="#item5">3.	HYPOTHALMUS</a></li>
					<li><a href="#item6">4.	THE FIVE SENSORY ORGANS</a></li>
					<li><a href="#item6"></a></li>
					<li><a href="#item7"></a></li>
					<li><a href="#item8"></a></li>
					<li><a href="#item9"></a></li>
					<li><a href="#item10"></a></li>
					<li><a href="#item11">5. The internal secreting Glands</a></li>
					<li><a href="#item12"></a></li>
					<li><a href="#item13"></a></li>
					<li><a href="#item14"></a></li>
					<li><a href="#item15"></a></li>
					<li><a href="#item16">6. Spine</a></li>
					<li><a href="#item17">7. The circulatory system</a></li>
					<li><a href="#item18"></a></li>
					<li><a href="#item19"></a></li>
					<li><a href="#item20">8. The Respiratory system - Lung</a></li>
					<li><a href="#item21">9. The Digestive System</a></li>
					<li><a href="#item22"></a></li>
					<li><a href="#item23"></a></li>
					<li><a href="#item24"></a></li>
					<li><a href="#item25"></a></li>
					<li><a href="#item26">10.	Pancreas</a></li>
					<li><a href="#item27">11.	Liver</a></li>
					<li><a href="#item28">12.	The urinary tract </a></li>
					<li><a href="#item29"></a></li>
					<li><a href="#item30"></a></li>
					<li><a href="#item31"></a></li>
					<li><a href="#item32">13. The Reproductive organs</a></li>
					
					</ul>
				
			</div>

			<div class="bb-custom-wrapper">
				<div id="bb-bookblock" class="bb-bookblock">
					<div class="bb-item" id="item1">
						<div class="content">
							<div class="scroller" align="center">
								<h2>The wonder that is Our body </h2>
								<h1>“The life so short the craft, so long to learn”</h1>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp- Hippocrates
								
								<div>
							<img src="human.jpg" alt="human body coheart" width="90%" >	

							</div>	
							</div>
						</div>
					</div>
					<div class="bb-item" id="item2">
						<div class="content">
							<div class="scroller">
								<h2>INTRODUCTION</h2>
								<img src="images/human-body-coheart.png" alt="human body" width="90%" >
								
								<P>This section attempts  to describe in simple terms the nature and functions of various parts of the human body, their common ailments, the methods of tests and treatments now available for them. This will help you explore the topics further and to specialize in your areas of interest. </p>
								<div align="center">
								<h1>OUR WONDERFUL BODY ORGANS </h1>
								<h1>Know thy </h1>
								</div>
							</div>
						</div>
					</div>
					<div class="bb-item" id="item3">
						<div class="content">
							<div class="scroller">
								<h2>BRAIN,<sub>know thyself</sub> </h2>
								<img src="images/brain.jpg" alt="brain" width="90%" >
								<h1>Know thy brain, know thyself is a popular saying.</h1>
								<p>Scientifically speaking, man is mainly his brain. This about 1.5 kg jelly, the grey matter controls all his actions, reactions, dreams, desires, moods, memories, so on and so forth.  It is his entire mental makeup, what he calls his personality. It is his brain that gives him a different thinking and makes him an individual, so unique and so different among mankind. </p>

								<p> In look, the brain is very unattractive - A shapeless, tightly- packed mass of grey and white matter. It is as if beauty and brain seldom go together. But even one of its simplest functions can compete and outdo the greatest wonders of the world!</p>

								<p>Take, for example, its effective communication system. Every second it is jammed with millions of signals and impulses. No matter how many, it takes but a fraction of second for it to decode them. High speed directions are then passed instantly to the other parts of the body for adequate responses. Unwanted signals are at once thrown in to the recycle bin.  All these before you open and shut our eyes!</p>

								<p>The Brain not only receives and processes signals but recognises instantly if any of them has a future use!    Often this is to give our body warning. For instance, when we lose our step and is about to slip, it is the brain that wakes up into sudden action. It helps us balance our body .There is quick intimation after it about the nature of injury. The whole event is then stored up for future. We take care not to let something like it happen in similar circumstances all over again!</p>

								<p>The very storage capacity of the brain is surprising. Each information has its own tidy place.</p>

								<p>It is the brain that controls our breathing. When there is too much carbon dioxide and less oxygen, it is the brain that tells the body to increase the breathing level. This is to give importance to our life’s breath, Oxygen. It also needs a continuous supply of glucose. Lack of blood supply, if lasts for minutes, can cause death.</p>

								<p>It is the interpreter, the decision maker, the analyzer and synthesizer of all our impressions - the most vital organ of our body.</p>

								<p>It is no exaggeration that we need so many exclamation marks to describe the brain functions. Every one of them is a piece of wonder.</p>
								<p>Let us see whatever make up this mass of jelly, the supreme controller of both mind and matter.</p>
							</div>
						</div>
					</div>
					<div class="bb-item" id="item4">
						<div class="content">
							<div class="scroller">
								<h1>POSITION AND STRUCTURE </h1>
								<p>The organization of brain itself is a wonder.</p>

								<p>The brain is sheltered in a thick covering called the skull. Outside is the hair. In the case of man his hairy protection seems to have gradually concentrated on his head .If allowed to grow and kept in folds, it alone can be a good outside protection for the brain. Look at the skin under the hair .It is unusually thick. Then there is another thick coating of skin. The skull is under it .It is made of eight flat bones so cleverly welded together.  There are three membranes inside the skull. They are called meninges. It was once believed that this membrane was responsible for the birth of all other membranes.    It was therefore called the Mater.( Mater is Latin for Mother ) The outer layer is called the Dura Mater ,that near the brain ,the middle one , the Archanoid Mater  (Archanoid means spider- like) and that lying close to the brain is the Pia Mater (soft mother ). </p>

								<p>It has been found that the brain was a natural evolution of the spine. As it developed, so did the canals inside and they became the ventricles. There are four types of ventricles. First two are on the sides and are called the lateral ventricles. The third one is in the form of a canal. The fourth ventricle lies between the cerebellum and the pon. It goes down to the spinal in the form of a canal .It goes upward to become a hole. All ventricles are related to one another.</p>

								<p>The watery fluid inside is to protect the brain from unexpected shock. It is called the Cerro-Spinal Fluid; CSF for short .It is produced by the ventricles inside the brain .Its base is nothing but the blood and is produced by a kind of sieving process as is the case with urine. Ninety-nine percent of it consists of water. The fluid gets changed within hours constantly. It is also the function of it to help the brain keep its position .Without it,the brain will easily flatten and go out of shape. A barrier divides the pumped in blood and the brain. Only useful things are allowed in. If it is glucose,it is always necessary and so allowed in. If it is harmful bacteria,its entry is suddenly blocked .The CSF is constantly produced by them and so they are always filled with them.</p>
								<strong>Plan </strong>

								<p>The bottommost part of the brain is called the medullawhich is short for medulla oblongata. This is the developed form of the spine. It is difficult to say where the spine ends for the medulla to begin. The spine goes through a big hole called the Foramen Magnum. The axons from the right crosses to the left and from the left crosses to the right in the medulla. (The reason for such crossings is still unexplained!). These crossings are called the Decussate Pyramids. The signals for body movements pass through them.</p>

								<p>The place above is convex .This is called pons. Cerebellum hides it from behind. The fourth ventricle lies between these. </p>
								<p>Cerebellum means little brain.  </p>
								<p>It is in the form of a globe and divides itself into two. The theory of evolution maintains that the cerebellum has grown thrice in a gap of two lakh years. In weight it is one eighth of the total brain. It has deep canals and so it can enlarge so much.    The cortex is three folded and has complex cells within. Its neurons are so special they       have innumerable of synapses, connections with other neurons. It is three -footed .These are actually axons .Two setsgets intoit, the other one to outside.  </p>
								<p>The place above the pun is called the Midbrain. It is in the form two pillars less than an inch. Behind it are Follicular or Rectum (upper roof).They look like four hillocks, two above and two below. Together the medulla, pons and the midbrain are called the Brainstem. This is considered the most ancient part of the brain. It’s this part that controls all our basic requirements. It is the brainstem that takes care of our breathing, heart beats digestion, urination, etc.</p>
								<p>The upper part is called the forebrain. When we think of brain it is in the form of forebrain. This is because it hides the midbrain. Its surface part is called the Cortex (= surface of trees, earth).It consists mostly of nerve cells called neurons and soma. (They give the forebrain a grey colour and are therefore the brain is sometimes called Grey matter. He lacks grey matter means He is an idiot) .The cortex is two to five millimeter thick. It is uneven on the surface .The deep channels are called Fissures. The longest of these extends from the middle top to the back. It is called the LongitudinalFissure .The fissures develop because the skull does not grow as the brain grows, so it gets compressed within. If the skull grows too, the birth of child will not be easy. </p>
								<p>The sensory Cortex is behind the middle fissure. It recognizes touch, smell etc.  However, sights and sound are recognized by other areas. There is visual cortex for sight, and on sides lie the auditory area. Its association areas interpret facts, give us ability to think and take decisions,</p>
								<p>The area in front of the middle area is called the Motor cortex It has cells in the form of pyramids some of these Pyramid cells are called the Betzcells. The axons of the Betz cells go down the spine and from there start their journey to the neurons that that send nerves to the muscles. A Pyramid cell is supposed to move about 15000 nerve endings of our muscles .It is called the Pyramid System.  The path of the axons that cross the medulla and go down to get spread the spine is called the Pyramidal tract. There are also other parts of the brain that help the muscle movements. This is called the Extra Pyramidal System.  </p>
								<p>The cerebral cortex is divided but at the same time they are connected .They are connected with a bridge called the Corpus Callosum. It is the intertwining axons that make it so thick. The axons make them connected to each other. Without the connection, each part of the cerebrum will independently behave. If so one will become a split personality</p>
								<p>Beneath the cortex the brain is white matter. These are fibers which we call axons. They are rooted in the neurons, and most of them are covered with myelin .This is the reason for their white colour. </p><p> 
   Brain mappers divide the two hemispheres further into lobes .Each hemisphere has four divisions: the frontal (front), parietal (top), temporal (side) and occipital (back) lobes.</p><p>
   It is the left hemisphere that controls the actions of the right and the right hemisphere, thoseof the left parts. The left-handed people are under the control of the right hemisphere. We normal people use the right hand .It means we are dominated by the left hemisphere. </p><p>
  It has been observed that the left hemisphere is responsible for our ability to talk, to walk, to read and write, to learn via repetition or to do mathematics.</p><p>
  The areas of the left hemisphere that show the ability to talk are called the Broca and Vernica areas.</p><p>
  The right hemisphere helps things like recognizing two or three dimensional objects, making spatial judgments, learning elementary lessons of music etc. 
  One curious fact about the two hemispheres is this: Whenever there is the need of detailed examination and interpretation of data, the job is grabbed by the left hemisphere from the right hemisphere. Thus if simple music lessons are learned by the left.  This is called Hemisphere Dominance or lateralization of the other side.</p><p>
  One important result of the study of the brain divisions is in the difference observed in man and woman. The dominance of one hemisphere is not seen in the brains of females as they do in males. As a result the damage of one part does not affect much if the patient is a female. A stroke can make a man speechless, where as a woman can speak even after it.</p><p>
 It has been observed that the steroid hormones make the early differentiation with the help of genes. The testosterone has the major role in it.
 The Sensory Cortex is behind the middle fissure. Itrecognises touch, smell etc.  However, sights and sound are recognized by other areas. There is visual cortex for sight, and on sides lie the auditory area.</p><p>
 The area in front of the middle area is called the Motor Cortex. It has cells in the form of pyramids. Some of these Pyramid cells are called the Betz cells. The axons of the Betzcells go down the spine and from there start their journey to the neurons that that send nerves to the muscles. A Pyramid cell is supposed to move about 15000 nerve endings of our muscles .It is called the Pyramid System. The path of the axons that cross the medulla and go down to get spread the spine is called the Pyramidal tract. There are also other parts of the brain that help the muscle movements. This is called the Extra Pyramidal System.  </p><p>
 The cerebral cortex are divided but at the same time they are connected .They are connected with a bridge called the Corpus Callosum. It is the intertwining axons that make it so thick. The axons make them connected to each other. Without the connection, each part of the cerebrum will independently behave. If so one will become a split personality </p>

							</div>
						</div>
					</div>
					<div class="bb-item" id="item5">
						<div class="content">
							<div class="scroller">
							<h2>HYPOTHALAMUS,&nbsp;&nbsp; the &lsquo;Central Switchboard&rsquo; of the human body&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </h2>

<img src="images/hypo.jpg" alt="brain" width="90%" >
							<p>The hypothalamus is so small in&nbsp; structure but so big in&nbsp; its role and stature .</p>
<p>Scientists call it the Primary Sensory Sending Structure.&nbsp; Its work is typical, for it can function both in normal and reverse mode. It changes its plans easily and as per the needs of the hour. It is not a player like other organs of the body but makes the brain, the pituitary or the adrenalin play their key-roles perfectly. The hypothalamus is therefore considered a <strong>coordinator or motivator</strong> ,not an original performer. It is also the oldest in our evolution - One with a history of about 10 million years!</p>
<p>Power , Position, Size and Appearance</p>
<p>The hypothalamus has highly sensitive sensors .It has also both direct and indirect links with so many other nerve-endings. It is a wide area network, really. It is these varied connections that help it perform the various&nbsp; managerial duties on time.&nbsp;</p>
<p>It&nbsp; has its comfortable position beneath the brain. And like our brain, it is also is very unattractive to look at. It is like a dry plum&nbsp; in appearance , pink and grey in colour. In size , it is only&nbsp; a 300<sup>th</sup> part of the brain. Of course, small is <strong>notalways</strong> beautiful.</p>
<p>Demanding type</p>
<p>&nbsp; The hypothalamus is always in need of a rich flow of blood .No other part of the brain has such an intake. In fact , if there is&nbsp; constant short supply even for a short time , it is as good as damage. Managers are usually demanding types!</p>
<p>Main functions</p>
<p>The&nbsp;&nbsp; hypothalamus is quite unavoidable for our body to function. In fact, its very origin or evolution was for life&rsquo;s survival .It is its chief duty is to maintain the balance of body. It is hypothalamus that&nbsp; wakes up the brain and the master glands like the pituitary and adrenal for their timely action.&nbsp; Without it, we will not be able to know <strong><em>when weneed food, water, when we are hot or cold or we are to have sex or to get rid of an infection.</em></strong></p>
<p>Ways of&nbsp; Management.</p>
<p><strong>Dr. J.D. Ratcliffe</strong> who has written good articles on human organs in plain readable English calls it the <em>centralswitchboard</em> of the human body.The comparison is very accurate, for it sums up the overall- supervisory function of this tiny structure in our brain.</p>
<p>Performance</p>
<p>It is the&nbsp; hypothalamus&nbsp; that steadily keeps up the body heat as 90<sup>O</sup>F ,no matter how&nbsp; hot or icy the climate is.</p>
<p>In times of heat &hellip;&hellip;</p>
<p>When our body is unusually hot, it is the hypothalamus that knows it first. Its action then is immediate. Signals are sent to the Pituitary gland situated at the base of the brain. (<em>Pituitary</em> is an endocrine gland, i.e., a gland that gives out secretions like the hormones from inside. (<em>Pituitary</em> means <em>phlegm</em> and <em>endo</em> means <em>inner</em>.).The nervous system is aroused .The surface vessels are dilated (=enlarged, widened). Also so many of our sweat glands open up. The sweat they produce reduces our body heat. At the same time the hypothalamus wakes up the brain to step up the breathing .With the sweating and breathing, the body gets rid of the extra heat&nbsp; it does not&nbsp; find required.</p>
<p>The reverse mode.</p>
<p>When the body temperature is very low, the hypothalamus realises the danger instantly. It sends signals to the adrenal and the pituitary glands. They see to it that the liver releases more blood sugar. This is to give extra energy to the muscles. The muscles act also as the furnaces of our body. They get into action. Shivering begin. Sweat glands get shut down. The blood from the surface of the body takes other paths. Otherwise it will be chilled. Once animals had hirsute (=hairy) protection .Their hair stood on end when it was unusually cold .It was actually the work result of the hypothalamus. Even though we don&rsquo;t have such hairy protection in extreme cold we sometimes get gooseflesh (a roughened condition of skin in very cold state.)</p>
<p>Water Management.</p>
<p>The human body is for good part water . A baby is&nbsp; 75 % water. As an adult we are 50 %&nbsp; of it .The&nbsp; body is in constant need of water as&nbsp; we sweat&nbsp; and urinate and the lungs evaporate a part&nbsp;</p>
<p>When the body has little water our blood becomes salty .The hypothalamus quickly detects it.&nbsp; As usual&nbsp; it&nbsp; prompts the pituitary gland for a special work. . An <strong>a</strong>nti -<strong>d</strong>iuretic <strong>h</strong>ormone (<strong>ADH)</strong> is released .The kidneys absorb more water .The urine becomes concentrated. The flow of saliva is reduced. All amounts are reserved .One feels thirsty. A glass of water is drank .Thirst is reduced balance restored.</p>
<p>Again the Reverse mode</p>
<p>If the blood is too watery the pituitary is given the opposite signal . The amount of ADH is released in to the blood stream . The kidneys are not in need of reserving the water. Urine is released in full flow and&nbsp; in&nbsp;&nbsp; greater amount.</p>
<p>Metabolic changes</p>
<p>When there is infection&hellip;&hellip;&hellip;.</p>
<p>The bacteria make the sensors of hypothalamus hot when there is infection. The normal work temperature of the hypothalamus increases. As a result Its sensors&nbsp; temporarily become over-sensitive. The surface blood vessels of the body get compressed .This constriction of the blood vessels and the shivering of muscles raise the body heat. The hypothalamus at the same time makes the body to lose heat. When the infection disappears ,the sensors become as usual.</p>
<p>When you have , have no Sex drive&hellip;&hellip;</p>
<p>The hypothalamus stimulates the pituitary&nbsp; gland for sexual activities. Our interest in sex can disappear if one of the areas of Hypothalamus is destroyed. This shows that without it unifies our sex drive also. If there is pressure or irritation , the pituitary releases the sex hormone. We then feel&nbsp; too much desire and drive for sexual activity.</p>
<p>When we are hungry &hellip;&hellip;..</p>
<p>It is Hypothalamus that tells when we are hungry.</p>
<p>Thousands of information floods in to the Hypothalamus. Blood sugar falls , mild fatigue hits the muscles , The hypothalamus therefore sends signals to step up production of gastric juices , and saliva and taste buds become sensitive.&nbsp;&nbsp; The message becomes clear&nbsp; :&nbsp; Food first !.</p>
<p>Two of the cell groups of Hypothalamus are typical . If one is damaged ,one eats too much. The damage of the other, on the other hand , takes</p>
<p>When we are angry&hellip;&hellip;.</p>
<p>Blood is stored up for the muscles to use. Vessels of the muscles dilate ,those of the skin constrict. Breathing and heart beats&nbsp; increase. Pumping of blood , doubled.&nbsp;</p>
<p>When we&nbsp;&nbsp; are afraid or&nbsp; tense &hellip;..</p>
<p>There is&nbsp;&nbsp; tension of&nbsp; various muscles&nbsp; .&nbsp; The cranial nerve prepares the eyes , face muscles ,pharynx and heart for a&nbsp; stress response . The salivating&nbsp; glands are shut down . The mouth goes dry.</p>
<p>Likely disorders , diseases</p>
<p>As a rule the hypothalamus has little to fear .But a possibility is the&nbsp; spreading of tumour from adjacent areas . It is a very small structure but it needs a continuous flow of blood supply .Any break in blood as result of an accident or injury can spell disaster.&nbsp;</p>
							
							
							
							
								

								
							</div>
						</div>
					</div>
					<div class="bb-item" id="item6">
						<div class="content">
							<div class="scroller">
							<h2>THE FIVE SENSORY ORGANS</h2>
							<img src="images/sensory.png" alt="brain" width="90%" >
								<p>For our 
								VISUAL ( sight ), AUDITORY  ( hearing ) , OALFACTORY   ( smell ) , GUSTATORY ( taste ) AND TACTILE ( touch ) TREATS                                
									And
								for many other functions besides.
								</p>	

								
							</div>
						</div>
					</div>
					<div class="bb-item" id="item7">
						<div class="content">
							<div class="scroller">
								<h2>EYE</h2>
								<p>It is an organ with connections in multifarious millions forabout 1.5 million messages in any given time.                                                                                                                    
Is it the most important organ of our body?
Our ancestors had no doubt about it at all. They rightly called the organ of sight Nethra, the organ that takes care of the nethruthaor leadership of all other organs.
It is trite that without the eyes nothing positive can ever be achieved by the body.
The very study of its structure and functions canbe a source of sustained wonder.
</p>
<h1>Structure</h1>
<p>The two eye balls are well –protected within bony sockets (orbits) and are guarded by eyelids, brows and always by a tear film. The optic nerves connect them to the brain .For movement the eyes have the support of six muscles around the eyeball.</p><p>
The eye is by its very nature not prepared for viewing things that are nearby. Our ancestors who were hunters mainly needed the sight of birds and animals far away .The eyes were originally prepared for them. This is why near-by objects when in close focus for a long time still soon tire our eyes. And we damage them by our constant close watching of electronic gadgets andTV nowadays. </p><p>
Do you know what the lens of our eyes are made of? </p><p>
It is nothing but fluid!</p><p>
In structure, it is like a small pill and is encircled by small, strongfibers. These are called ciliary muscles. It is these surrounding muscles that help the lens assume shapes .When the muscles becomes tense andtight the lenses tend to be fat .This gives us near vision.  If the muscles relax the lens becomes flat and we sees things far away. The muscles of the eyes are the tiniest but they are also the strongest. They move about ten thousand times a day to makes things in close focus.</p><p>
If the muscles are tense for long to give us nearbyvision, naturally the eyes get soon tired. </p><p>
The lenses of the eyes have two fluid deposits behind. It is interesting to know the nature of this fluid. It is like water in the front and is very thick behind it! They ought to be clear for best vision. When the eye cells get damaged their parts get deposited in it .This happens from our very birth. The swimming spots we see as we age are actually these waste of cells that slowly get collected in it.</p><p>
The retina is like a small copying paper. For duplicate copies we use light oily papers commonly called onion skins .The retina is very much like this.  Examination of its structure will definitely spring surprise on suit .It is less than tree centimeters but contains 137 trillion cells to receive data (the cell receptors)!</p><p>
It is known that the young read better in dim light. It is when we are old reading in dim light becomes difficult and harmful.</p>
<h1>Function	</h1>
<p>The eyes take time to work together perfectly. At birth we have but wandering vision. Later this becomes long sight and then the two go in two directions for a time. Partnership of the two eyes begins after a few months and becomesperfect and remains for a life time by the time one become six years old. </p><p> 
The cornea of the eyes actually bend the light that enters it to form necessary patterns .The pupil of the eye actually work very much like the lens of a camera. In a bright day, it closes instantly and throughout; it is wide open if it is pitch dark.</p><p>
The light entering the eyes get focused by the cornea and lens to form an image on retina. These cells are of two kinds .130 trillion are rod cells and 7 trillion are cones.</p><p>
The rod cells give us the black and white vision, the cones colour.</p><p>
The rod cells contain a purplish red pigment within them. It is called rhodopsin. When light enters it gets bleached. A very, very small production of electricity results. But it gets into the optic nerve all of a sudden and reach the brain. Its speed is estimated as 45 K.m. per hour. It is now sudden work in the coordination centre, the brain .The processing,interpretation, communication all take place within 0.22 second!</p><p>
<h1>Acute vision, the work of cone cells.</h1>	
<p>The eyes have cone cells thickly packed in the fovea, a small yellow hole like space in the eye’s back chamber .It is believed that the cone cells can understand both the primary and secondary colours and all the variations. And any damage to them can make us colour blind. The cones need bright light to understand colour. In dark it is the rod cells that operate. </p><p>
Actually it is not the eye but the brain that does the final work. A severe blow can therefore result in a complete loss of vision for without the assistance of the brain the eyes will really grope for the totality of images...</p>
<h1>Protection and Maintenance,</h1>
<p>The eyes are sheltered in the orbits .This is natural protection. Apart from this thelacrimal glands keep the eyes ever moist. The tear remain in it like a thin film and keeps off dust and tries to wash off any foreign body that may enter it . It has in it a substance called lysozyme. It kills the microbes that enter the eyes. The eyelids are powerful guards. So are the cheekbones and forehead .They are projected so that the eyes are always in protection. They open and close at least half a dozen times a minute to keep the eyes clean and wet. Blinking is another means .The eyes take rest one by one. </p>
<h1>Likely Diseases</h1>
<h1>Glaucoma</h1>
<p>When too much fluid enters the eyes or too much is lost pressure builds up and blood supply is reduced and the optic nerves become short of blood .This is glaucoma.<p>
<h1>Astigmatism </h1>
<p>The cornea is affected and the spherical surface gets damaged.</p> 
<h1>Detached Retina </h1>
<p>The onionskin of the retina develops small holes and we see flashes of light and distorted images, blurring spots.</p>
<h1>Clouded cornea,clouded lens </h1>
<p>These can lead to permanent blindness.</P><p>
Ageing can affect vision .The lenses become less transparent, the ciliary muscles become less strong, blood supply become low. But there are remedies to get around most of them.</p>
	
							</div>
						</div>
					</div>
					<div class="bb-item" id="item8">
						<div class="content">
							<div class="scroller">
								<h2>Ear</h2><h1>the organ of hearing and balance</h1>
<p><strong>Blindness or deafness?</strong></p>
<p>You will not opt one for the other.&nbsp; Yet there are people who think blindness is more endurable than deafness. This may be because sounds have a direct emotional appeal.</p>
<p>Our auditory function is absolute miracle. The hearing organ actually consist of so many parts, all of them the smallest we can imagine.</p>
<p><strong>Structure </strong></p>
<p>The external ear is so shaped as to receive the sounds that are in range. It is therefore sometimes called the trumpet. This auricle or pinna joins with the auditory canal to form the outer ear.The canal that goes to the ear drum is just two centimeter in length. It is not a straight canal. It is shaped so in order to protect the inner devices. The canal is kept warm. And the hair and the wax glands that produce a greasy, sticky substance&nbsp;&nbsp; prevent dust, small insects, etc get in to it. The middle ear and the inner ear are protected within the skull.</p>
<p>The eardrum is a tough membrane. It is tight in form and less than two centimeters long.</p>
<p>The air waves that carry the sounds first strike the ear drum. It is so sensitive that even a small vibration can turn it inward. Such a slightest change becomes the cause of a series of consequent changes in the hearing devices. Ultimately it reaches the brain and it is rightly interpreted in the brain.</p>
<p>The middle ear is like a bean in shape. There are three different looking bones called icicles .They are also known as anvil, hammer and stirrup as they look like them .They amplify the sounds twenty two times.Through the oval window attached to the stirrup, the amplified sounds then reach the inner ear. The middle ear also includes the Eustachian tube which connects the ear to the back of the nose.</p>
<p>The inner ear is a cave. It has a watery fluid inside. In it resides the cochlea, the main hearing device. It is in the form of a snail. It has thousands of nerve cells that are very much like hair.&nbsp; The middle ear stirrup knocks on the oval window, the fluid inside the inner ear vibrates, then the hair like cells, wave in the lymph fluid. A small electric current results. It straight goes in to the auditory nerve .This nerve is so small but it is estimated that it has more than thirty thousand circuits! From the auditory nerve the electric current passes to the brain. Processing and recognition of the sound are then instantaneous. As in the case of sight, the ultimate work is done by the brain.</p>
<p>The inner ear includes the semicircular canals and the vestibules which can be called the Organs of balance.</p>
<p>Another hearing trick is by the conduction of bone. When we speak a part of the sound reaches the ear drum and the other through the jaw bone reaches the fluid in the inner ear. The way we hear is different and so we hear our own sound also a little differently. It can sometimes result in colon hemorrhage</p>
<p>There are semicircular canals filled with fluids .It is these fluid-filled tubes that give us body balance. They take care of our up and down, forward, backward or lateral motions.</p>
<p>It is a fact that from the moment we are born our hearing powers are steadily on the decline.&nbsp; With each hearing the tissues lose elasticity, the nerve cells lose sensitivity, calcium accumulates. If at the time of our birth we are able to hear16000 to 30000 vibrations per second, it comes down to 20000 in teens and a long life of eighty years can claim only 4000 cycles.</p>
<p>Sound sensitivity is measured in a particular frequency as decibels .As we progress in life, the decibel rate also declines. If we have a 40 decibel lose, people tell their name and we say: Pardon? .We want it repeated!</p>
<p>Likely Disorders</p>
<p>Drum puncture</p>
<p>Middle ear infections</p>
<p>Bone freeze (conduction deafness)</p>
							</div>
						</div>
					</div>
					<div class="bb-item" id="item9">
						<div class="content">
							<div class="scroller">
								
<h2>Nose</h2>
<p>The nose is as important as the other sense organs in the head. In one way, it is more important than them. For it is the organ that serves us loyally even others strike work completely. Literally, the nose remains with us to the last breath.</p>
<p><strong>Structure  </strong></p>
<p>Like the lung the eyes or the brain, the nose is structurally not one but two. The septum or the bridge bifurcates it. It is connected at one end on the roof of the mouth and the other to the brain. The inside is hollow. There are caverns in the nose bone also. These and the hollow space above the brows in the cheeks at the back of the nose, etc make the eight sinuses. These sinuses responsible for the humidity of the nose and its contribution to our voice quality.</p>
<p><strong>Functions</strong></p>
<p>We know that it is through the two passages of the nose that we inhale and exhale the breath. In doing this the nose is not passive at all. It is its main business to process the air, clean and make it fit for the lungs.No matter what the climate is, the lungs need fresh air, air that is free of bacteria and the nose tries tirelessly to achieve this. The nose conditions the air with the use of a tiny area, this actually has to be contrasted with the heavy, big conditioners we usually boast of.</p>
<p>&nbsp; The air going to the lungs ought to be humidified. For this the nose gives out almost a liter of mucus every day. To produce mucus the nose has spongy red linings in the passage. The hair that grows in the passage does the initial cleaning job. The mucus then takes over. It traps bacteria and any unwanted particle that enter the nose through the air. The mucus itself can get polluted .So there is also mucus change every twenty minutes or so. .</p>
<p>It is the cilia removes the mucus. They the cilia are minute brooms that can be seen only through a microscope. It is their job to throw the mucus back to the throat... If it enters the stomach, its strong acids destroy the bacteria. This cleaning job of the cilia go on all the time. But we realize it only in cold days when there is over production of mucus and the cilia, paralyzed, cannot do the cleaning job well.</p>
<p>As in the eyes, there is lysozyme in the nose too. They kill all microbes and makes the air as fresh as possible.</p>
<p>The air is also to be warmed. For this the nose has turbinates, the three chip bones. A blood-filled tissue covers them they act as radiators .When we breathe cold ear the bone tissues swell and give greater area of warmth.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Smell detection</strong></p>
<p>Scientists tell us that the nose is able to recognize about 4000 variations of smell and the really sensitive one about 10000. But they are not certain as to how the nose achieves it .There are two small spots on the roof of the nasal cavities with ten million receptor cells. Each has tiny hairs of them. Now, if a thing smells, it means it gives out molecules. The receptors must be catching them to pass it to the brain lying just three centimeters away.</p>
<p>It is not clear from the theories how exactly the recognition of odors is achieved. All sense organs ultimately pass data to the brain and the end result is from there. There is reason to assume that just as there are primary colours, there are primary smells and the variations are recognized by the intricate processes in the brain. By the time one passes the middle age, the power of smell degenerates. But the nose the most loyal of the sense organs, sees to it that its work is carried out to the very end.</p>
<p>Likely diseases</p>
<p>Attack of microbes of syphilis/ tuberculosis</p>
<p>Nasal blocks, tissue damage sue to excessive use of snuff,etc</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Treatments/ tests&nbsp;</p>
							</div>
						</div>
					</div>
					<div class="bb-item" id="item10">
						<div class="content">
							<div class="scroller">
								
<h2>Tongue</h2>
<p>In living creatures, the tongue plays a wide variety roles. In frogs it is an insect catcher of lightning speed, in a snake a path-finder and so on. In humans it role is many-sided. It helps the&nbsp; munching&nbsp; of food with saliva&nbsp; swallowing it&nbsp; , realizing tastes&nbsp; and&nbsp;&nbsp; is&nbsp; also&nbsp; a&nbsp; main&nbsp; speech organ&nbsp; ( It helps us deliver words with clarity) .<strong>Structure </strong>&nbsp;&nbsp;&nbsp;&nbsp;</p>
<p>&nbsp;The Tongue is actually a slab of mucus membrane&nbsp; that covers a complex arrangement of muscles and nerves .On its surface are&nbsp; papillae of many kinds , those&nbsp; little nipples with taste buds in some of them. The taste buds contain taste cells that are responsible for taste. The frenulum is under the tongue. It is this cord that gives the tongue movement. If it is too short, one will have difficulty to utter words.</p>
<p><strong>Functions</strong></p>
<p>As the eyes picks up the primary colours and send them to the brain for processing, the taste buds obtain the primary tastes and the rest is done by the brain.</p>
<p>It is a fact that food ought to be first liquefied for taste. It then gets connected to the taste receptors. A minute electric current is generated which is then passed on to the taste recognizing terminals in the brain.</p>
<p>Taste is in part inheritance. The tongue can also adapt itself to it.</p>
<p>The&nbsp;&nbsp; taste areas of the tongue can be marked .The upper middle part recognises whatever bitter, the right side below it sour, the next spot salt and the tip anything sweet and so on. There is still uncertainty about the <em>gustatory </em>(= tasting)process as even the throat has taste buds and involvement in it.</p>
<p>Taste experience is not similar in all people. Some chemicals are tasted by people differently. Also taste and hearing are closely linked.</p>
<p>Tongue is examined by doctors to diagnose certain diseases like pernicious anemia, jaundice, and is called the mirror of diseases. In some cases the examination of tongue has been proved wrong .A person suffering from indigestion may have a clear tongue and one with a greenish-white coating on his tongue may not be having constipation at all</p>
<p><strong>Likely diseases</strong></p>
<p>Dyspepsia &ndash; Distorted taste</p>
<p>Hypogeusia &ndash;flavour decrease</p>

								
							</div>
						</div>
					</div>
					<div class="bb-item" id="item11">
						<div class="content">
							<div class="scroller">
								
<h2>SKIN</h2>
<p>The skin is perhaps the most under-estimated of our body organs. In fact, we even refuse to admit that it is one. Everything is taken granted but we complain even of a slight change we have on it.</p>
<p><strong>Structure </strong></p>
<p>Actually, it is very difficult to define skin in clear cut terms. It is the body covering, the hair, the finger nails, the wart, or the callus of the sole of our feet - all at once.</p>
<p>The skin is in three layers. The epidermis tissue. hich is the outer layer, the middle dermis and the subcutaneous tissue. Subcutaneous is a big word .But it just means <em>beneath</em> or <em>under. </em>It also has fat portions and two million sweat glands that serve their function in maintaining body heat.</p>
<p>The epidermis is only paper thin. It has no blood supply and the cells are maintained by a diffusion from below .The epidermis has small cells growing in millions under it every day. As the outer cells decay, these push on to the front to become the harder <em>keratin</em> .The keratin layer is nothing but dead cells that are constantly being replaced from within. It is found that the keratin layer has but 27days endurance.&nbsp; After this a new layer replaces it.</p>
<p>There are millions of cells on the skin that we call melanocytes which produce the pigment called melanin,which decides the colour of our skin, hair, andeyes. If the skin lacked it we would have endless torture from the sun. The ultraviolet rays would definitely shorten our lives, if it were not for this wonderful substance.</p>
<p>&nbsp;</p>
<p>Functions.</p>
<p>No one knows how wonderful the jobs skin can actually perform.&nbsp;&nbsp;&nbsp; The skin is behind&nbsp;&nbsp; the body&rsquo;s normal temperature of 98.6<sup>0</sup> F.&nbsp; It is estimated to have two million sweat glands .They lie in the form of tightly coiled tubes, deep in the dermis .Their ducts rise to the surface of the skin. If extended they will measure out to a total of 9 kilometers!</p>
<p>If we consider the functions, the <em>subcutaneous</em> is the most important portion of the skin.</p>
<p>It works as a shock absorber for our organs within, helps maintain the body heat and also is in part responsible for our body shape.</p>
<p>Likewise the skin also has&nbsp; a remarkable ability to&nbsp; both keep in keep off water .If the water is not maintained&nbsp; within one would die , and if it is not able to keep off water one would become&nbsp; soon water- logged&nbsp; when one gets into water.</p>
<p>&nbsp;It has also a role in the production of Vitamin D, which is known as the Sunshine Vitamin.</p>
<p>It is the skin that activates the testosterone, produced by the testes.</p>
<p>It can be called a fortress that guards the body from outside. What we usually forget is the fact that but for the skin, we would have been under constant attack of bacteria.</p>
<p>In a hot day when we walk too fast for a long time or do exercises vigorously, the skin becomes red. It means the blood vessels of the skin are dilated. Heat is radiated to the outside. The sweat glands are tireless performers. Even on a normally hot day, they give out half kilo water and excess salt .If a good football team is asked to weigh themselves after a game, up to six kilo decrease can be seen. This is nothing but loss of six kilo water, the result of the extra work of the sweat glands.</p>
<p>If it is cold day the blood vessel are shut down .Blood takes different roots to the inside. One becomes pale in colour. Similarly when angry, we find the skin flushing and getting pale when in fear .We freeze!</p>
<p>The sebaceous or fat tissues.</p>
<p>These are glands that produce the semi-liquid oil. They are for the most part attached to the hair follicles. The hair and the surrounding skin are lubricated by it. .In the case of arboreal apes, our ancestors, the only natural escape from cold was their hirsute (=hairy) protection. The hair then was water-proof and hence very useful!</p>
<p>Hair manufacture.</p>
<p>The skin has ten follicles per square centimeter .In structure it is a bulb &ndash;like root fixed deep in to the skin and a long fibre like ending. It is these follicles that produce hair continuously, leaving behind the dead cells on the surface.</p>
<p>The thousands of nerve endings on the skin protect us from burns, cuts, too much cold giving signals to the brain.</p>
<p>The skin withers with age .The quality begins to fade with middle age. .It becomes transparent, it begins to show there are&nbsp;&nbsp; veins within. The loss of fat is the reason .Wrinkles becomes common.</p>
<p>Likely Diseases</p>
<p>The skin is actually subject to a wide variety of diseases. More than 2000 skin diseases have been identified!</p>
<p>The most serious of these is cancer. Over exposure to the sun affects the skin on the forehead nose and ears the growths on the skin, wounds that do not heal, etc need special attention.</p>
<p>Psoriasis</p>
<p>The red scaly patches are days caused by epidermis cells that form and decay too fast the normal time for a skin cell to die is 27 days but in a psoriasis-affected person the epidermis cells are removed in just 5 days and it becomes an affected, diseased condition</p>
<p>Shingles is another skin disease and it is caused by the chicken pox virus.</p>
								
							</div>
						</div>
					</div>
					
					<div class="bb-item" id="item12">
						<div class="content">
							<div class="scroller">
								
							<h2>The internal secreting Glands</h2>
							<img src="images/glands.png" alt="brain" width="90%" >	
							</div>
						</div>
					</div>
					<div class="bb-item" id="item13">
						<div class="content">
							<div class="scroller">
							<h2>PITUITARY, the &ldquo;conductor of the endocrine symphony&rdquo;.</h2>
<p><strong>W</strong>e owe to the Pituitary Gland our very entry into the world (it helps the mother&rsquo;s body to push us out), and from the very start of our existence its secretions play many decisive roles. It is the pituitary that decides the very size and normality of each of us as a human being. It also plays an important role in our sex life and in the process of our getting old. Hence it is no wonder that this small tissue in the brain is called the <strong>master gland.</strong></p>
<p><strong>Structure </strong></p>
<p>The pituitary is a small pinkish gland that hangs from the hypothalamus in the brain. In appearance is like a cherry, weighs only half a gram and what is strange is that 85 percent of it is nothing but water!</p>
<p>It is divided into two lobes and these are called the anterior and posterior (=front and back positioned) lobes. The anterior lobe is believed to produce about ten hormones (No one is sure about the correct number). And it is the business of the posterior lobe is to keep inside two hormones produced by the hypothalamus.</p>
<p>It is well protected under the hypothalamus. Injury is rare. But If it comes at all, the results would be disastrous (terrible)</p>
<p>The quantum of the hormone that the Pituitary produces is but nominal .Just 1/1000000 of a gram! But it is such stuff that we gape to hear of its miracles.</p>
<p><strong>Functions</strong></p>
<p>&nbsp;It is given many responsible jobs and one of them is the supervision and monitoring of other glands. J.D.Ratcliffe therefore rightly calls it the Chemical boss of the human body.</p>
<p>The pituitary produces a hormone that has direct link to the thyroid gland.&nbsp; It is therefore called the thyrotropic hormone.&nbsp; If the pituitary instill the secretion into the thyroid in any incorrect quantity, the results would be quite shocking .Too much secretion would make him hungry as a wolf, too little a quantity, a dullard or nit-wit. (A dull or unintelligent person).</p>
<p><strong>The growth hormone</strong></p>
<p>Youth gives it a main role.</p>
<p>Until a person&rsquo;s bone ends closes and he stops to grow any further it is the growth hormone that works steadily for it.&nbsp; But even after this, it remains a watchdog. When one meets with an accident and bones are broken, when one bleeds on account of even a small nick by a blade, the growth hormone prompts the sudden growth of new tissues.</p>
<p>Excesses are ever bad. If the secretion is too much, the result would be too big a jaw or a huge, Socratic nose!</p>
<p><strong>For men </strong></p>
<p>The pituitary plays prominent role in man&rsquo;s sex life. The two hormones that encourage the production of the sperm cells and their transportation through the ducts make the pituitary linked to man&rsquo;s testicles.</p>
<p><strong>For women </strong></p>
<p>The pituitary produces the FSH and ICSH for women. The former is short for Follicle stimulating Hormone and the latter for Intestinal Cell Stimulating Hormone. These hormones make it sure that only one egg matures in a month .If the quantity of this hormone varies in men it affects his sex drives considerably. Too little FSH and ICSH or too much of them means too Cold or sex-greedy.</p>
<p>&nbsp;</p>
<p><strong>Likely injury</strong></p>
<p>The one serious problem with the Pituitary that it may develop tumour .This will upset the production of the secretion ACTH that controls the adrenal glands and will spell trouble. There will be sudden excess of fat, loss of calcium, weakness of spine, sex drive, and heart failure. Removal of pituitary and regular injection of hormones would be the only option then...</p>		

								
							</div>
						</div>
					</div>
					<div class="bb-item" id="item14">
						<div class="content">
							<div class="scroller">
							<h2>Thyroid</h2>
<p>The role of the thyroid is also often underestimated. When it comes to believing, seeing definitely has big role (we say seeing is believing). We tend to ignore or forget the importance of anything that is not right before our eyes. This is the case with all of our inner glands</p>
<p>In size and shape, the thyroid merits no attention.&nbsp; Its secretion is so little (1/2800000 Grams a day).&nbsp; But if it is not its proper stuff, miseries will definitely last a lifetime.</p>
<p><strong>Structure</strong></p>
<p>This pinkish gland that situates in the throat below the Adam&rsquo;s apple in case of men is in the form of a butterfly weighing about 20 grams.&nbsp; Its position in the throat actually makes it vulnerable to a lot of troubles. And for it functions, it is also dependent on other parts like the hypothalamus and the pituitary and other items in the body like iodine.&nbsp;</p>
<p>Give picture</p>
<p><strong>Functions</strong></p>
<p>The variation in the thyroid hormones at the time of birth would give life a false start. The baby will be thick-lipped, flat nosed and without the possibility of normal size. And it can be dull-witted for life.</p>
<p>The thyroid has the monitoring of the rate of our lives. It all depends on it whether we are fast or dead slow. The hormone that the thyroid produces decides how the numerous cells are to burn food into energy, takes materials from blood and converts them into hormones. The major hormones are two third of iodine and so it is an unavoidable requirement. The daily requirement is only 1/5000 grams but it decides everything. Without it you have no pep, love of work, easiness &ndash; anything that suggests you are at home and healthy.</p>
<p>The thyroid hormone is required by us differently on different occasions. During pregnancy a woman needs it more. Exercise, walking, standing up, mental activity all need it in different quantities.</p>
<p><strong>Calcitonin</strong>, thyroid hormone (discovered only in 1960s) has been found to have the efficiency to balance the calcium level, when the parathyroid take away the calcium from the bones to mix it in the blood.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>How the correct production takes place</strong></p>
<p>The thyroid produces the hormones as stimulated by two other glands .The Hypothalamus and the Pituitary. When the body needs a particular amount of energy for a particular action, the hypothalamus first stimulates the pituitary which prompts the thyroid in turn to produce hormone and transport it to the needed spots. If an excess amount of thyroid is produced, the pituitary is shut- off by it .There will be no further stimulus and no further production. It is thus a steady balance of secretion is maintained in normal people.</p>
<p><strong>Thyroid and hyper activity</strong></p>
<p>The thyroid is both&nbsp;&nbsp; nervously and chemically controlled. This is why failure, domestic worries, death, all influence its production of hormone. First the hypothalamus overestimates them and over stimulates the pituitary which does the same to the thyroid. There will be unnecessary orders to the body to produce too much hormone and supply it at faster rate. The pace of the body is unnecessarily doubled. After a time one becomes an emotional bundle as result of such chain reactions.</p>
<p>&nbsp;</p>
<p>Hereditary defects (by birth). Use of certain drugs or certain disease can make the thyroid without the necessary enzymes .The production of the hormone is slowed down or blocked.</p>
<p>&nbsp;</p>
<p><strong>The wonderful chemical process</strong></p>
<p>The thyroid gets iodine from the digestive tracts not as iodine itself but as iodide .it has many enzymes and one of them converts into iodine. The next business is to hook it to the Tyrosine,&nbsp;&nbsp; an amino acid .This chemical process produce two chief hormones. They are then attached to the proteins of the blood so as to reach anywhere they have to reach.</p>
<p>&nbsp;If the hormone is not enough, one would become dull, unintelligent and unusual in appearance. Face will be bulged and puffy. If the hormone quantity is in excess, desire for food will be limitless and no matter how much one eats, one will always remain lean. But this is not serious when compared with other misfortunes.</p>
<p>In extreme condition, the eyes would bulge to unusual size and the lids fail to cover them, nervousness and indecisions will make one a wreck. Heart beats would gear up with no reason. Even death would result.</p>
<p><strong>Likely disorders </strong></p>
<p>The thyroid function becomes disorderly when its hormone production is too fast or too slow<strong>.</strong></p>
<p>Fast rate</p>
<p>If there is an over-production of hormones it is known as toxic goiter. The Pituitary in a condition of tumor gives wrong signals to the thyroid and it produces thyrotropin in excess. Over production of this hormone can also be caused by the presence of excessive iodine in it.</p>
<p><strong>Slow rate</strong></p>
<p>Hereditary defects, diseases, the use of certain drugs, etc, can block the enzymes that produce the hormones. Sometimes the thyroid strikes work for no reason .This is called its atrophy. There is also chance that the thyroid tissue is replaced by one that fails to function. The pituitary sometimes fail to stimulate the thyroid properly and as a result there would be too little production of it.</p>
<p><strong>Lack of iodine, Over- size of thyroid</strong></p>
<p>Sea foods and vegetables grown on coastal areas, use of iodised salt, etc supplement the deficiency. If iodine is absent the thyroid makes wild efforts to capture it. In such frantic attempts, the thyroid cells grow larger. Its normal, twenty eight gram weight shoots up to a hundred .This is called Non-toxic goitre</p>
<p><strong>Cancer </strong></p>
<p>Its cancer is often benign.&nbsp; It rarely spreads and is removable. It is even possible to make it harmless or reduce its size by thyroid pills.</p>
<p><strong>Treatments/surgery</strong></p>
<p>The thyroid is the blessed one among the endocrine glands .The disorders can be understood by the&nbsp; blood tests to know the amount of hormone in the protein , tremors of the patients outstretched hands, nervousness, sleeping troubles ,hunger for food and so on . And many of its disorders are treatable .In surgery the doctors decide the correct portion of the thyroid to be removed.&nbsp;</p>	
								
							</div>
						</div>
					</div>
					<div class="bb-item" id="item15">
						<div class="content">
							<div class="scroller">
							<h2>Thymus </h2>
<p>It took a lot of time for the medical field to evaluate the thymus. Its role was not properly identified. In fact, there was a time when it was believed it was just an unwanted part like the appendix.&nbsp; But patient researches have now shown its major role in aiding the body&rsquo;s immune system.</p>
<p>Perhaps the underrating of the thymus was due to its early performance and gradual bowing out. Its major work is in the early period of life and by the time one crosses the middle age, it stops works. But without it the miseries of life will be countless.</p>
<p>Structure</p>
<p>The thymus is a small bead of tissue, yellow-grey in colour.&nbsp; It is located between the lung and above the breastbone. Its size is not always the same .It changes with age. When the baby is in womb the thyroid is larger than its heart or lungs. It is because its only defence then against infection is what passes as immune factors from the mother&rsquo;s blood. The immunity from mother&rsquo;s blood soon disappears and the baby needs the thyroid for future immunity. Here emerges the role of thymus. If babies are born without thymus, it will have deadly effects and often it will result in death.</p>
<p><strong>Function</strong></p>
<p>The thyroid is rightly termed the <strong>throne of immunity.</strong></p>
<p>The thyroid at the time a baby is born receives the little white cells called lymphocytes from the bone marrows through the blood stream. They are reared there to maturity and sent to the spleen, lymphatic system intestine, etc. The immunity system takes shape with the full growth of these cells in them.</p>
<p><strong>&nbsp;</strong></p>
<p><strong>Immunity in us</strong></p>
<p>It is the body&rsquo;s defence against the intruders. Any foreign body that enters the system is looked up on as harm. Bacteria, viruses, poison &ndash;even a transplanted skin or liver. The thymus joins with the other defenders of the body,the <strong>tonsils</strong>, lymph nodes, adenoids, the relevant portions of the intestine, etc to fight the enemies .The immune system attacks the invader and protects the body from harm.</p>
<p><strong>Lymphocytes</strong></p>
<p>The lymphocytes produced by the thymus and the intestine make up a fourth of the white blood cells.</p>
<p>If any bacteria or virus happens to enter the system, or if there is a small nick of a blade or a knife injury, the lymphocytes start giving out antibodies. It has been found that there are specific lymphocytes for specific diseases. These antibodies fight the bacteria or viruses .The lymphocytes at the same time join the phagocytes that eat the dead parts of the bacteria or viruses. This is the successful survival effort of the body.</p>
<p>If the lymphocytes fail in their work one will have rheumatoid arthritis. This is because of the wrong estimate of a necessary tissues and the unnecessary lymphocyte attack on the joint linings.</p>
<p><strong>Allergy </strong></p>
<p>Allergy is not a disease. It is just a condition .The lymphocytes sometimes overestimates the harm that can result from a foreign body. The response becomes too much and produces a lot of symptoms. The over -active tendency of the lymphocytes and the disturbances that result from it is called allergy.&nbsp; We can go as far as to say that the presence of allergy shows the perfect working condition of the immune system!</p>
<p>The immune system in the intestine takes care of the bacterial and viral infections. The lymphocytes have the main job of attacking the <strong>allergens</strong>like the fungus infections or foreign tissue.</p>
<p>There is evidence for the gradual decline of the immune system. In some cases the immune system losses its power for a time and then reappears ready for action. The result will be a miracle like the appearance of cancer and after a surgical removal or initial treatment, complete recovery.</p>
<p><strong>Thyroxin</strong></p>
<p>&nbsp; The thymus allows the thyroxin, a recently discovered hormone, to get in to the blood stream. It then stimulates the spleen and the lymphatic system produces lymphocytes in sufficient numbers. It has been observed that by the time one is 50 years of age the production almost ceases and the ageing process begins.&nbsp; A daymay come when researches on thymus will lead to the slowing down of the ageing process.</p>
<p><strong>Injury </strong></p>
<p>What really harms the thymus is stress. It is known that it is an enemy of all internal organs .In the case of thymus if the stress lasts for long, the thymus shrinks in to a very small size. This show s its link with stress. But the exact functions are yet to be known.</p>	

								
							</div>
						</div>
					</div>
					<div class="bb-item" id="item16">
						<div class="content">
							<div class="scroller">
							<h2>The Adrenal</h2>
<p>The adrenal is yet another small complex chemical plant in our body. Our&nbsp;&nbsp; mental and bodily health depend on the hormones it produces and they are therefore considered life&rsquo;s essential. Without adrenal&rsquo;s hormones&nbsp; there is no question of our survival .If it is to be surgically removed in an emergency&nbsp; the only alternative will be the injection of the artificial hormones on a regular basis. As a rule, the gland does not demand anything special from us. Avoid too much worry and hurry, and then there would not be any serious problem for it</p>
<p><strong>Structure </strong></p>
<p>In pair, the adrenal is placed on the top of the kidneys.&nbsp; In size, it is just&nbsp;&nbsp; a little larger than a fingertip and weighs very little. Its daily production of hormones is only a thousandth of 28 grams. But still it is an inevitable gland .Made of a very complex network of blood vessels, blood passes constantly through them in quantities more than it weight (six times more!). Only ten percent of the adrenal tissue is required to assure the necessary supply of hormones.</p>
<p>The adrenal has two parts, the medulla and the cortex .They produce two separate set of hormones.</p>
<p><strong>Functions </strong></p>
<p>The production of Adrenalin and Noradrenalin.</p>
<p>The brain has connection with adrenalin. A sudden rage, fear or any strong emotion immediately reaches the medulla of the adrenalin and it instantly produces the hormone adrenalin and noradrenalin in a confused sort of way, as if knowing which to produce in such an emergency.&nbsp; To this the liver suddenly reacts to its presence in the blood stream and releases sugar. In the meantime, the hormones down the shutters of the blood vessels of the skin. This is why we go pale. Blood reaches the muscles and internal organs .The heart begins to beat fast. Arteries tighten and blood level rises. No digestion.&nbsp; Clotting will be quick in case, there is injury. The adrenal achieves all these in seconds!</p>
<p>When the adrenaline thus speeds up everything, balance is to be maintained .The cortex does this emergency work .For this hypothalamus produces a substance known as ACTH. The ACTH stimulates the cortex to produce hormones that maintain the blood pressure and the flow of blood to the vital organs. Fat and protein are converted into sugar. Energy is kept in reserve.&nbsp;</p>
<p>It is the adrenalin hormones that makes one capable for extraordinary feats. It is therefore right to say that all supermen are blessed with perfect adrenalin or adrenal hormones.</p>
<p>As we have seen, the spontaneous secretion of the hormones is stopped by the ACTH-stimulated cortex .It controls blood flow to all vital organs and maintains balance.</p>
<p><strong>The three types of hormones</strong></p>
<p>The cortisone family is a set of hormones that look after the metabolism of fats, carbohydrates and proteins.</p>
<p>&nbsp;Another set supervises the mineral balance in the body</p>
<p>The third is sex hormones which aid the main hormones produced for sexual activities.</p>
<p>What is important to observe is that the adrenalin goes on manufacturing the hormones endlessly. What about the excess? It is here the liver comes to aid. Any excess is destroyed by it. The hormones the cortex produces actually remain for just two hours. Fresh supply replaces it.</p>
<p>Once damage to cortex meant death. Now the artificial hormones makes successful surgery possible.</p>
<p>Disease or injury can damage the cortex. The symptoms appear in many forms. The skin becomes bronze in colour, blood level drops, and nausea vomiting, diarrhoea etc make the condition worse. In the end Death would close everything. In our time the availability of hormones and its injection can help the patient get around all of them.</p>
<p><strong>Too much cortisol</strong></p>
<p>If there is an excess of cortisol the muscle protein will be converted into sugar and one will have shrivelled arms and legs .The bones will lose their power because the cortisole will drain all minerals. Fat would accumulate in the body. Thinking power weakens, and blood pressure would rise.</p>
<p><strong>Aldosterone</strong></p>
<p>It is aldosterone that keep up the water/mineral balance .If it is a little too much there would be&nbsp; too much loss of potassium, and sodium will be retained .This makes the muscles weak ,blood level high and&nbsp; can become&nbsp; source of continuous headache. Often tumour steps up the production of this hormone and its removal can normalize it.&nbsp;</p>	
								
							</div>
						</div>
					</div>
					<div class="bb-item" id="item17">
						<div class="content">
							<div class="scroller">
							<h2>Spine</h2>
<p>There was a time when our arboreal(living on trees) ancestors comfortably lived with their spine. (BertrandRussell imagines this in his characteristic way: - eating coconuts when hungry, throwing them at each other when they were not!)&nbsp; They were not erect and the spine was crosswise like a bridge .Troubles began when they eventually started to rise up and raise it like a flexible pole and &nbsp;&nbsp;began to hold the head. Thus began all the miseries the ribs and joints now jointly or severally cause us in our lifetime.</p>
<p><strong>Structure</strong></p>
<p>The spine is not merely a pole that holds up the head and support the body weight.&nbsp; It is protector of a whitish, 45 centimeter and 1centimeter thick cable called the Spinal cord&nbsp;&nbsp; .The cord resides inside a bony covering and a fluid in the bony covering acts as shock absorber .The cable in its turn is wrapped in three sheaths so that it cannot be damaged easily. There are thirty one nerves that extend from the cord .Those that take messages to the brain are called the sensory nerves,and motor nerves are those that take messages from the brain to various muscles. It has also ability for reflex action in emergencies.</p>
<p>If the cord is almost totally out of harm so different are the 33vertebrae.Many thing can happen to them a striking feature is the consequential pain. The damage of liver. Kidneys, prostate, arthritis, etc can inflict pain in the form of backache.&nbsp; What happens actually is this .The muscles become unusually tight when we&nbsp;&nbsp; worry about the diseases and a dull pain appears in the form of back ache.</p>
<p>The backbone has about 400 muscles and 1000 ligaments in its supporting system.&nbsp; Its&nbsp; three part structure begins with&nbsp; the seven&nbsp; cervical vertebrae at&nbsp; the top .They are capable of&nbsp; varied movements so that we can look up , down and also 180 degrees laterally ( to left and&nbsp; right ) . The 12 thoracic vertebrae do not have such mobility. They are not for movement. The ribs are hooked on to them and they stand immobile. Bach ache is seldom here. The lower part is called the lumbar vertebrae. It carries most of our body weight. The fourth and fifth lumbar vertebrae need special care as they can cause real trouble.</p>
<p><strong>Benefit from change.</strong></p>
<p>When a baby is born its spine is almost straight .Later when it begins to hold its head high, there comes a small bend at the top and when it learns to walk there comes another bend lower down. These are changes for good because the vague <em>s</em> shape does the useful function of absorbing shock.</p>
<p><strong>Discs </strong></p>
<p>These cushions are placed between each pair. They are strong coverings of cartilage with a jelly like substance inside.</p>
<p>A serious injury can completely damage the disc and in such cases they are surgically removed and the pairs of vertebrae are joined together. In cases of minor injury the jelly inside the disc comes out and creates misery. It&nbsp; falls on nerves and the&nbsp; nerves ,when&nbsp; irritated ,&nbsp; give&nbsp; the muscles a&nbsp; tendency to ripple .These spasms give us&nbsp; great pain .The muscle aloe try to make the spine immobile, an additional source of trouble. It can also make the body bend forward .A damaged disc acts on&nbsp;&nbsp; the sciatic nerve and pain reaches even up to the tip of the toe.</p>
<p>A punch gradually becomes a burden for the spine, an additional weight of about four kilos when the stomach muscles begin to slowly relax .Pregnancy also inflicts burden on it and back ache troubles many women during the period of pregnancy. Most of our sitting postures are wrong and the stuffed up and cushioned furniture can also create a lot of&nbsp;&nbsp; harm.</p>
<p>It is a known fact that the muscles and ligaments of the spine are very vulnerable and in advanced age and so heavy lifting is better avoided. Discs lose their quality as we advance in age.</p>
<p>The cervical part can spark trouble in the form of ruptures .Neck pain is often the result of stretched or swollen muscles or back. A broken neck is to be attended to with due care</p>
<p>If an accident victim is not able to move his hands and legs better not touch him. If the body is lifted up without sufficient care, permanent paralysis can result owing to the chances of spinal cord damage.</p>
<p>In old age the bones become weak due to the loss of calcium. The discs will soften, and the vertebrae become less thick and the body begin to bend. Muscles also become weak which can be slightly strengthened by exercises.&nbsp;&nbsp;</p>
<p>Treatments</p>
<p>The&nbsp; bending ,twisting , lifting , not&nbsp; sitting&nbsp; or staying up right &ndash; all these can&nbsp; give us back ache. The ropes of the muscles on the other side of the back bone stretch and strain but more harm is on account of the weakness of the three abdominal muscles on either side. The five lumbar vertebrae begin to overarch .The top of the pelvis begin to bend forward.</p>
<p><strong>Dr Garrick&rsquo;s simple remedy.</strong></p>
<p>Lie on your back. Pull your feet in until you are bent 90 degrees. This will tilt the pelvis and flatten the back.&nbsp;&nbsp;&nbsp;&nbsp; There should be no gap.</p>
<p>Now the exercise can begin. Rest the arm on the chest. Count five .Slowly raise the head and shoulders up .The shoulder blade should rise from the floor. . Exhale when the head and shoulder are raised. Do not sit all the way up. Count five, inhaling and lowering the shoulder .The rule is exhale on exertion, inhale on relaxation. Five repetitions three times a week. Dr. Garric claims that here will be drastic change.</p>


								
							</div>
						</div>
					</div>
					<div class="bb-item" id="item18">
						<div class="content">
							<div class="scroller">
							<h2>The circulatory system</h2>
								<p>The heart</p>
								<p>Lung and</p>
								<p>Bloodstream</p>	
							<img src="images/heart.png" alt="brain" width="90%" >	

								
							</div>
						</div>
					</div>
					<div class="bb-item" id="item19">
						<div class="content">
							<div class="scroller">
							<h2>The heart</h2>
<p>The heart is indeed a miracle. It isactually&nbsp; trite to say that it is the most precious of our organs. In a right living environment, the organ guarantees a superb performance of at least 70 years, with no great, expensive demands for maintenance. Often it is our lack of body exertion, bad life style, polluted environment, etc, that make its functions difficult, repair expensive and ,in some cases , altogether impossible.</p>
<p><strong>Structure</strong></p>
<p>The heart is in structure a hollow muscle positioned in the middle of our chest. It actually hangs from some ligaments there and weighs 340 grams. In length it is about 15 centimeters, and its expanded width is 10 centimeters. A wall of muscle called septum divides it into two sides (left and right) and a valve in to two main chambers: the upper atrium and alower ventricle.</p>
<p><strong>Functions</strong></p>
<p>&nbsp;The heart muscle contracts and squeezes blood through the atria and then through the ventricles .The oxygenated blood from the lungs enters the left atrium through the pulmonary veins. The aorta then transports it to all parts of the body. The deoxygenated blood reaches the heart from the body through the vena cava into the right atrium through the right ventricle and then goes out through the pulmonary veins to the lungs again. This is to make the blood oxygenated again.</p>
<p>The heart&rsquo;s capacity to pump blood is amazing and its muscles are very tough and capable of steady contraction and expansion. Our blood vessels, if extend, will make us measure 90000 kilometers and imagine the heart daily pumping blood through them!</p>
<p>For the heart, the rest is assured between its beats. it is longevity, life ! In the three tenth of a second the big ventricle takes to&nbsp; contract&nbsp; with the purpose of pushing the blood out in to the body , the heart has a rest of half a minute&nbsp; each time. It has also a rest when one sleeps and the capillaries are inactive. This is one of the ruses of evolution. If rest is prolonged it is death. If rest is intermittent, punctuates as between the beats, thereis longevitylife!</p>
<p><strong>The heart beats</strong></p>
<p>The normal rate is sixty to eighty.&nbsp; But in times of stress or as we exercise this may shoot up to two hundred.</p>
<p>Sometimes it may occur that we suddenly feel we have missed a heartbeat. This is just because of this: The heart has the ability to produce its own electricity. This is for its contraction. Sometimes the rhythm is felt lost because one electric impulse comes atop another.</p>
<p><strong>The brake.</strong></p>
<p>During the night, sometimes the heart is found to beat as per the events in our wild dreams. When we fight in a dream it joins the fight, when we run, it runs. Worries double the beats. But if we are not able to quieten ourselves, the vagus nerve applies the brake. A gentle message of the vagus nerve is found to calm the heart!</p>
<p><strong>Undue worries </strong></p>
<p>Those who do sedentary work ,i.e.,&nbsp; persons tied to their&nbsp; desks -clerks, computer addicts , etc , are likely to have sudden severe chest pain .They may&nbsp; suspect the chest pain as heart failure. It is actually a sharp pain coming from stomach as result of a heavy meal. <em>Do not panic</em> is a first rule, if one does not want to be a heart-neurotic.</p>
<p>The heart gives trouble only when there is no sufficient nourishment. The nourishment is actually the need of 1/20 percent of the blood supply. Compared with the amount of blood needed by other organs, it is ten times higher.</p>
<p>The heart gets nourishment not from the blood directly but through the arteries .They are weakest carriers. There can be congenital defects, or fatty deposits that will block their passage .This will eventually lead to heart collapse. A clot is a sudden blockage but a gradual closing-in can also result.</p>
<p><strong>Danger signals</strong></p>
<p>When the arteries get blocked, a portion of the heart muscle depending on it weakens and soon dies. The size and position of the muscle damage decides its seriousness.</p>
<p>Sometimes, we do not know the mild heart attacks .The block makes dead tissues and the heart Okays thearea of damage with a scar tissue.</p>
<p>Heredity can be behind an attack. But with care, the gravity of it can be lessened.</p>
<p>Overweight,smoking,drinks, etc can make the heart&rsquo;s performance difficult and, in extreme case,it will collapse completely.</p>
<p><strong>The soaring blood pressure </strong></p>
<p>The Normal pressure rate is 140/90.This means pressure should be 140. While the heart is contracting and 90 when there is rest between the beats. The rise of the lower figure means the heart badly needs rest.</p>
<p>If excess weight is reduced, the blood pressure will surprisingly drop. Smoking will block the arteries. Two packets of cigarettes mean 18 to twenty milligrams of nicotine a day, not to mention other dangerous mixes.</p>
<p><strong>Stress </strong></p>
<p>It makes the adrenal produce the hormones adrenalin and noradrenalin in excess and the blocks of arteries, rise of blood pressure, etc will result.</p>
<p><strong>Exercise, Diet</strong></p>
<p>The heart would like us do mild regular exercise than a heavy dose once in a week. It is better to avoid food that contain a lot of fat and can eventually develop plaque in the arteries.</p>	
								
							</div>
						</div>
					</div>
					<div class="bb-item" id="item20">
						<div class="content">
							<div class="scroller">
							<h2>Blood stream</h2>
<p>Perhaps the greatest of wonders about the human body is the <em>red river</em>that is constantly on the move with a lotof essentials and waste, a river that has a day a twelve thousand kilometer route, meeting the demands of sixty trillion cells, removing their waste and supplying the essentials.</p>
<p><strong>The Red cells</strong></p>
<p>The blood carries the red cells about seventy five thousand times from the heart to the other parts of the body. They have a 120 day life span and then are produced afresh in the marrow in the ribs, skulls and&nbsp;&nbsp; vertrabrae produce new cells, it is estimated that about half a tone red cells are produced by the bones marrow, especially in the marrow in the ribs this way.</p>
<p>&nbsp;&nbsp; The heart actually surges the blood with great force and it is the duty of the blood stream itself to normalize it course .The big arteries contract with each pumping and expand between the beats. By the time the blood is to return to the heart, the pressure is near zero. To keep it moving the blood vessels have valve system .It is when the valves get damaged the blood flow is upset .The valves become unable to prevent the back flow and the result will be varicose vein .</p>
<p><strong>The contents </strong></p>
<p>&nbsp; The blood is not mere fluid. It contains various items like the red cells , numerous leucocytes, white cells , granulocytes, lymphocytes, monocytes, platelets and a variety of other soluble items like cholesterol , sugar, salts, fats ,enzymes, liquid plasma that float everything in.</p>
<p>&nbsp; The blood needs a proper level always. It absorbs all water and the body leaves excesses in the form of urine, sweat, etc.&nbsp;&nbsp;&nbsp;</p>
<p><strong>The Basic groups</strong></p>
<p>O, A, B and AB are the main groups. There are also a variety of other factors like M, N, P, Rh etc.</p>
<p>Many things still remain to be known about it. The blood is also considered very unique and differs from individual to individual like the fingerprints.</p>
<p>The blood flow is also unique .The blood that the heart pumps out has great surge .It then grows narrower in the arteries and again grows smaller to get in to the capillaries. Through the capillaries the red cells moves along sometimes twisted and in different shapes. Oxygen is unloaded and carbon dioxide is&nbsp;&nbsp; absorbed back to unload.</p>
<p>Each cell will demand a particular thing &ndash; items as different as cobalt, minerals ,vitamins, hormones, glucose and the like .The cells&nbsp; need almost all&nbsp; these things in exertion. This is why the skin gets red in heavy activity. It shows the hyper activity of the capillaries. In sleep the most of the capillaries close down as there is no exertion.</p>
<p>The health of a person is actually in direct proportion to the health of the capillaries. The physicians often examine the capillaries of the retinas to understand the nature of the body functions.</p>
<p><strong>Defence </strong></p>
<p>The blood stream has astonishing ways of defence. When there is even a minor cut the platelets rush to the spot and makes a temporary coating. The blood also has the components of Fibrin which is necessary for the proper healing. But since it is poison, the blood stream does not carry Fibrin as it is but keeps its components, and the enzymes necessary to make it in emergency! New materials for permanent healing are used only after this.</p>
<p><strong>The Great defenders</strong></p>
<p>The flu virus, pollen, splinters etc, create greater threats and to attack them the blood has antibodies. What is surprising is that foe each enemy there is a separate antibody. They store the memory of one attack and repeat it next time and hence we do not have repeated attacks of certain diseases. When the viruses or other attacking bodies perish, the phagocytes eat them out. The antibodies are also constantly replaced in the blood.</p>
<p><strong>Challenges</strong></p>
<p>The blood stream is subject to a lot of disorders and illnesses. The hardening of arteries affects the blood flow and the toes in dire need of blood supply will develop gangrene .Stroke and heart attack can be the result of artery block.</p>
<p><strong>Likely diseases</strong></p>
<p><strong><em>Diabetes, Hypoglycemia</em>.</strong></p>
<p>Too much sugar in blood makes one diabetic .Hypoglycemia is the opposite condition, the outcome of too little sugar. Palpitation, sweat, weakness, etc will be the outcome.</p>
<p><strong>Agranulocytosis, leukemia</strong>.</p>
<p>Agranulocytosis is the dangerous drop in white cell count. Infection will result in death if not prevented by antibiotics. The normal cell count is 6000. If it shoots up it becomes <em>leukaemia.Haemophilia, pupura</em>,etc , are other disorders.</p>
<p><strong>Prevention of everyday disorders</strong>.</p>
<p>The blood pressure is to be maintained at any cost. Sometimes medications will be necessary to maintain the pressure. Excercises can make the circulation in good condition.</p>	

								
							</div>
						</div>
					</div>
					<div class="bb-item" id="item21">
						<div class="content">
							<div class="scroller">
							<h2>Lung</h2>
<p>The lung, one of the vital organs of our respiratory system, meets all the body&rsquo;s oxygen demands. Its work has no stop &ndash; it is a marathon activity, like that of the heart, from birth to death<em>. &lsquo;Untomy last breath&rsquo;</em> makes it clear. Forgetting its inestimable worth we make a lot of damages to it in our own different ways.</p>
<p><strong>Structure</strong></p>
<p>The lungs hang comfortably from some ligaments in the chest. It is an odd pair. The right lung which performs its main functions is slightly larger than the left.&nbsp; It has three sections called lobes whereas the left has only two. There is a little vacuum in the lung compartment .When we inhale, the lung expand and when we exhale, it collapses.</p>
<p>&nbsp;At the time of birth, it appears pink in colour but gradually our inhaling dirty air, smoking, etc, make it fade and appear slightly grey. It is not hollow as it appears to us. If we closely examine, it will rather look like a <em>rubber bath sponge</em> and in a matured body weighs about half -a - kilo.</p>
<p>&nbsp;The ten metre - wind-pipe goes down to divide itself in the two lungs into two bronchial tubes, one for the right and the other for the left lung. The bronchi and the bronchioles are air passages .The bronchioles are smaller, only the size of 1/40 centimetre.</p>
<p>The alveoli (Plural form. Singular form is alveolus) in appearance resemble grape bunches. They are compressed .and .if we flatten them, they can cover a large area. Each alveolus is filled with capillaries. It is their work to push blood in to the heart with their capillary. There is a small membrane in the capillary wall. Red cells reach the capillary wall, spread carbon dioxide to alveoli and from them take the oxygen. The capillary is blue when it unloads carbon dioxide, red when it absorbs oxygen from the alveoli.</p>
<p>The lungs have both voluntary and automatic working methods. Its &ldquo;<em>automatic control station</em> &lsquo;&rsquo;is the medulla oblongata, that bulging part where the spinal cord shades off into the brain.</p>
<p>When we do exercise, the muscles are compelled to work harder. As a result, oxygen gets used up rapidly, and carbon dioxide, the waste ,gets accumulated in the blood. Naturally, the blood turns acid. The <em>medulla oblongata </em>detects this. It stimulates the lung to work faster. If the&nbsp; work&nbsp; becomes really&nbsp; uncontrollable , the automatic control system medical men&nbsp; call <em>the medulla oblongata</em> gives us the <em>second wind ( As an English usage ,&nbsp; this means&nbsp; a </em><em>second chance of work after great exertion and a&nbsp; time of rest . )</em></p>
<p>Deeper breathing takes place. In children sometimes, wild temper and breathing trouble occur and they turn blue. It is the automatic control of the medulla that take care of the breathing then.</p>
<p>An adult actually needs liters of fresh air every minute. It is estimated that nine liters of fresh air is required when one lies in bed, not doing anything at all. If one is sitting up, he needs eighteen litres, walking, twenty seven litres, running, fifty six. A desk clerk, on the other hand, will require only very little! A normal person breathes about taking in half a litre of air each time.</p>
<p>The lung at once has to be moist and warm. The lachrymal and other moisture secreting glands in the nose and throat produce the half a litre fluid needed to humidify the air. It is the surface vessels that remain wide open in cold days and closed up on hot days as to do the heating job.</p>
<p><strong>Damages</strong></p>
<p>The lung is a very delicate organ.&nbsp; Polluted air containing, sulphurdioxide, benopyrene,lead, nitrogen -dioxide, etc can inflict serious damages and it can cause real trouble. The lung has natural protections in the form of hair and sticky mucus in the nose, throat and bronchial passages. The cilia in the air passage are an effective cleaner .It is a small brush with millions of small hair and they wave a dozen times a second. The mucus is taken from the lower passages in to the throat every time.</p>
<p>Smoking gradually affects the cilia and the only option for the lung is to cough out the thick mucus. The smoker&rsquo;s cough is owing to the damage done to the cilia. To the smoker the suppressing medicines actually do much harm.</p>
<p>The lysozyme in the nose, throat, etc and the phagocytes eat out the various bacteria and viruses that naturally enter the&nbsp;&nbsp; air passages and thus protect the lungs .Polluted air does a lot of troubles.</p>
<p><strong>Safety measures</strong></p>
<p>The lungs expect of us always a balanced diet, exercise in the form of climbing small hillocks if we are in villages, stairs if we live in towns, jogging in busy streets, etc. <em>Yoga</em> has unique breathing methods designed especially for the lungs. Deep breathing always does well.&nbsp;</p>
	

								
							</div>
						</div>
					</div>
					<div class="bb-item" id="item22">
						<div class="content">
							<div class="scroller">
							<h2>The Digestive System</h2>
							<img src="images/dig.png" alt="brain" width="90%" >
								
							</div>
						</div>
					</div>
					<div class="bb-item" id="item23">
						<div class="content">
							<div class="scroller">
							<h2><strong>TEETH</strong></h2>
<p><strong>Structure. </strong></p>
<p>The teeth are greater in number when we are born. In the gum there will appear 52 of its weaker version at the time of birth and while a thirty two remainundeveloped, twenty slowly begin to take shape. The first two that we call the front teeth try to make their appearance when the baby is six months old .The weaker version appears when it is eighteen. By the time it crosses eighteen months the child&rsquo;s second molars also appear. The first stage is thus over.</p>
<p>The first durable molars hide behind the baby teeth. They appear only when the baby is six and allow the child to chew .In the meantime the roots of the baby teeth get absorbed in to the body making the baby teeth shaky and fall in the process. By the time one attains twelve years of age the molars sternly appear .The wisdom tooth come when one is eighteen.</p>
<p><strong>The eye tooth</strong></p>
<p>The right canine was once called the eye tooth owing to the belief that its roots extended to eyes and the plucking of it damaged the eyes. It has a twin, the left canine and the pair below with adog team work</p>
<p><strong>What teeth are made of?</strong></p>
<p>The part of the tooth that projects from the gums has enamel coating .It is mainly calcium phosphate and partly natural organic stuff. It is actually innumerable hexagonal rods in bundles.</p>
<p>Under the enamel is dentine. It is sensitive to pain. Below it is the pulp. It contains nerves, blood vessels .The cementeda bonytissue ofnumerous fibres fix it firm in the socket in the jaw.</p>
<p><strong>Possible disorders </strong></p>
<p><strong>Decay </strong></p>
<p>The teeth is under constant threat of bacteria ,food particles that remain in the mouth ,in the tooth cavities, on the tongue ,etc form plaque .The small cuts in the enamel can let in the bacteria and can eventually damage the teeth.</p>
<p>The mouth, one of the most important cavities of humanbody isa most neglectedpart. Chewing, smoking, drinking, etc, cause harm toits softtissues&nbsp;&nbsp; and pose real hazards.</p>
<p>&nbsp;</p>
<p><strong>Malocclusion</strong></p>
<p>This results as the teeth are not properly paired. The tooth on the upper jaw do not pair well with the lower one or vice versa. One of them becomes idle and gradually decay.</p>
<p>The most commondiseases are those that affect the gum and teeth.Pyorrhea&nbsp;&nbsp; and cavities &nbsp;are the two&nbsp; most common ailments .The plaque and tartar attack ,dental caries (cavities), dentures,&nbsp;&nbsp;&nbsp; gingivitis , inflammation of the gum , loss of quality&nbsp; of blood coming to the mouth ,etc ,etc , can create problems .There will also be particular&nbsp; problems related to the aged&nbsp; like&nbsp; gum recession , frailty and loss of height&nbsp; of the alveolar bone , etc.&nbsp;</p>
<p>Children develop<strong> a variety of disorders and disease s related to the mouth. </strong></p>
<p><strong>Dentalcare</strong></p>
<p>As&nbsp; the types&nbsp; of diseases&nbsp; and disorders&nbsp;&nbsp;&nbsp; vary&nbsp;&nbsp; there&nbsp; are&nbsp; now&nbsp; various&nbsp;&nbsp; branches&nbsp; of dentistry&nbsp; like oral Medicine (Diagnosis)&nbsp; &amp; radiology ,Conservative Dentistry , orthodontics , etc, .&nbsp;&nbsp; A&nbsp; special&nbsp; branch&nbsp; called Community Dentistry&nbsp; makes it a mission to create awareness&nbsp; about the need of oral&nbsp; hygiene&nbsp; among&nbsp; the&nbsp; public&nbsp;&nbsp; and&nbsp;&nbsp; conducts&nbsp;&nbsp; surveys&nbsp; for&nbsp; the&nbsp; government.</p>	

								
							</div>
						</div>
					</div>
					<div class="bb-item" id="item24">
						<div class="content">
							<div class="scroller">
							<h2>Throat</h2>
<p>We ignore the throat except when something sore develops in it .It is usually taken as a simple tube leading from the mouth to the throat. But it is actually much more than that.&nbsp; Without it, we can&rsquo;t utter a word and what is pushed down by the tongue as chewed- up food will not reach anywhere, or air and fluids move up and down the food tube. These two activities are usually taken for granted, disregarding the fact that actually very precise mechanisms are involved for their success.</p>
<p><strong>Structure</strong></p>
<p>our&nbsp; neck&nbsp; is house&nbsp; of&nbsp; so many nerves ,blood vessels , vertebrae ,the throat tubes and so on .The first tube we find in the neck from the front is 12 centimeter - long .It is funnel shaped.&nbsp; It has a wide portion at the top. It begins behind the nose and ends behind the Adam&rsquo;s apple. The larynx in it contains the speech organs .It is its duty to clear the route for the stuffs that go up and down. It is cylindrical inform and gets narrower toward the end.&nbsp; It is about four centimeter in length. It is actually nine cartilages wrapped in mucous membrane and tied around with ligaments. It is part of it that projects as Adam&rsquo;s apple. What follows is known as esophagus and trachea. These are two tubes that lead to the stomach and the lungs respectively. These tubes are just two centimeters in diameter.</p>
<p>&nbsp;</p>
<p><strong>Functions </strong></p>
<p><strong>Delivering food in to the stomach</strong></p>
<p>The foods we place in the mouth remain for sometime there to get chewed-up and then the tongue throws it to the back of the mouth. The uvula at the back of the mouth hangs like a small finger.&nbsp; The moment food reaches the back of the mouth, it rises to block the passage to the nostrils (passages of the nose) .We know what will happen if otherwise. Food will spill in to the nasal passage. After this uvula rise, the tongue humps up once more to push the chewed up, saliva-mixed food to get in to the esophagus. From there the food does not get in to stomach all at once. There is chance that food will get in to the trachea or the wind pipe .To prevents this there is valve on top of it called the epiglottis. It is a flap valve that makes sure that the food does go to the food tube and not to the trachea or the wind pipe. The phoneticians examine many of these organs as they also are organs of speech.</p>
<p>The esophagus has enough muscles to push the food by and by into the stomach. If food is entirely pushed in to the stomach, the result would be indigestion. A valve like muscle at the end of the esophagus where it joins the stomach will open and close to make the entry of food in to it slower. When the valve malfunctions as it happens on rare occasions, the stomach acid will pop up and affect the soft membranes of the esophagus. This can produce uneasiness in the stomach.</p>
<p>Speech</p>
<p>Students of literature consider the speech mechanism in detail under Phonetics, the scientific study of speech mechanism and the varieties of utterances of a given language. To produce different types of sounds the throat has in it a very special, intricate muscle system that widens and narrows to produce different types of sounds.&nbsp; Perfect speech is actually the combined effort of the vocal chords, tongue, teeth, teeth ridge, the soft and hard palates and so on. The organs of speech need special study to fully understand how different types of utterances are made possible.</p>
<p>The vocal tract is 18 centimeters in length from the larynx to lips. The air from the lungs comes up and the vocal chords widen or narrows and the edge muscles of the chords stretch. The extent of these activities will decide the quality of the utterances. The edges of the chords stretch usually a centimeter but an opera singer can stretch it to one and half centimeters .Also vibration rate is higher in females.</p>
<p><strong>Lymph glands</strong></p>
<p>The throat also houses the lymph glands called the tonsils. The fifth is called <strong>adenoids </strong>and it is inside the nose. The two paired tonsils at the entrance of the throat are called facial tonsils .Farther down are the lingual tonsils. They are capable of enlargements and are not removable like the former. The inflammation of the tonsils&nbsp; is only an indication that&nbsp; they need nursing back to normal size , not their removal .They are not unnecessary items as was once believed.</p>
<p>Circumcision or tonsillectomy is now <strong>not</strong> resorted to as they can be reason for respiratory disorders. The tonsils are necessary as they can trap bacteria and the phagocytes in the blood can&nbsp;&nbsp; come and destroy them in spider-like fashion.</p>
<p><strong>Disorders</strong></p>
<p>Attack of fungi and bacteria can cause disorders ranging from mild to those of serious gravity.</p>
<p>Inflammation, polyp, tumour or cyst may affect the vocal chords and as they become unable for proper&nbsp;&nbsp; closure speech will be distorted. Larynx gets easily affected by polluted fumes, smoke, etc. Laryngitis is easy to develop. Voice loss will eventually result.</p>
<p><strong>Cough</strong> is called the <strong><em>watch dog of the throat</em></strong>.It is actually a protecting means because it is an expelling mechanism when the throat feels the presence of something irritable for long.</p>
<p>Larynges can also develop cancer which is readily treatable. If not it will have to be removed and replaced.&nbsp; &nbsp;</p>	

								
							</div>
						</div>
					</div>
					<div class="bb-item" id="item25">
						<div class="content">
							<div class="scroller">
							<h2>Stomach</h2>
<p>&nbsp;In human body, the stomach is actually a container of food with minimum work related to the digestion. The main advantage we have is that it helps us avoid taking food a dozen times or more a day, as it is able store food. Its nominal work is on protein, i.e., breaking it down into polypeptides. The intestines finish it with its other functions like the conversion of carbohydrates, fats and other items of intake.</p>
<p><strong>Structure</strong></p>
<p>The stomach is different in appearance when we look it from outside and inside. Internally it is silky and lies in folds and externally rosy-pink. In its empty state it is just like a flattened balloon. When full,it lies slanted and small at the bottom. It has a capacity of about two liters in a matured body.</p>
<p>&nbsp;At its sides the stomach has about 35 million tiny glands which are to produce the gastric juices needed for digestion. It will come to three liters a day. The main gastric juice is hydrochloric acid.&nbsp; It is this acid that helps the activation of pepsin, the enzyme that is needed to digest protein. Like this, the stomach also has enzymes to digest various items.</p>
<p>The stomach is not actually grinding every food together but taking it in layer by layer .The food items are mixed thus one by one with enzymes and made a thick gruel ,It is then slowly pushed into the pyloric valve has which has an opening to the duodenum, the name for the thirty centimeter first part of the stomach.</p>
<p>The duodenum is vulnerable to ulcers as the gastric juices when in excess can destroy its walls. But food reaches there through pyloric valves in small quantities and avoids the possibility of ulcers.</p>
<p>The stomach takes some four hours to digest an intake of food, if it is not so fatty. If breakfast is loaded with fatty items it will take the double of it. Heavy, fatty food makes the stomach induce the duodenum to produce a special hormone and the contraction of the muscles of the stomach become slower. So is cold. Digestion becomes slow work when we take cold food items like ice cream.</p>
<p>The stomach has a special lining of mucus which helps it from not digesting itself. If it is removed completely the stomach will begin to <em>eat itself!</em></p>
<p>The stomach has another remarkable ability. It reacts just as we react. When we are red with anger, pale with fear or ready for food the stomach joins us in these reactions. This is why it is said that when dejected and depressed it is better we don&rsquo;t eat. The stomach does not contract or produce the gastric juices in such times and so digestion does not take place.</p>
<p>There is direct relation between stress and stomach disorders. In times of stress the stomach produces acids that find a way in to any gap in the mucus of the stomach and gradually ulcer develops.</p>
<p>Normally the stomach is able to destroy microbes but some may outlive the gastric juices. Food must be taken therefore with great care. It is also important that we avoid too much pepper, mustard, etc that can redden the stomach and,yes avoid too much coffee, say no to nicotine and alcohol that can increase the production of acids.</p>
<p>Alkalosis and acidosis are two disorders of the stomach. Alkalosis affects the kidneys. So remedies that acti- vate alkalis or acids are to be avoided.&nbsp; When we eat or too much the brain prompts the stomach to vomit.&nbsp; Heart burn is another symptom. It happens when the pyloric valve does not open and the stomach fails to send the food into the duodenum. Gas forms and hydrochloric acid gets carried to the lower gullet.</p>
<p>A leisurely walk after a meal helps digest food. Stomach pain that lasts more than an hour needs medical attention. It is normal for people to mistake heart attack for stomach disorder and otherwise.</p>
<p>&nbsp;</p>
<p>The stomach is one of the most abused of human organs .It&nbsp; has a capacity to resist many of them but too much negligence can cause serious damage and become real threats to life . There is actually a saying that when it is illness, you can catch many in the stomach.</p>	

								
							</div>
						</div>
					</div>
					<div class="bb-item" id="item26">
						<div class="content">
							<div class="scroller">
								<h2>Intestine</h2>
<p>The intestinal tract does the final food processing for the body. It is its business to convert all the chewed up matter that reaches it from the mouth in to components of our blood stream. It thus meets the demands of four&nbsp;&nbsp; innumerable cells .Fat is converted into fatty acids and glycerol, protein in to amino acids, carbohydrates into sugar glucose. .The chemical process that takes place for these conversions is indeed miraculous.</p>
<p>Whatever the intestine digests is passed into the blood .The waste, countless dead bacteria, lubricating mucus and other indigestible items are pushed out in the process.</p>
<p><strong>Structure and functions</strong></p>
<p>The intestine is eight-meter in total. The first part is small intestine. Which is 25 -centimeter -long duodenum near the stomach .The jejunum is the next .It is two and a half meter long and its diameter is four centimeter. The ileum which is four meter in length follows and then the big gut,the two meter long large intestine. There are strong acids in the upper part and so there are no microbes. The lower part is full of them. About 50 varieties of microbes live there in innumerable numbers. The food the stomach receives gets mixed with strong acids. As they can cause damage to the lining of the intestines. This will eventually affect the work of enzymes. So the stomach delivers it to the intestine only by and by.</p>
<p>The acid of the stomach which mixes with the food can damage the intestines linings .To avoid this, the duodenum produces secretion and passes it to the blood. This in it turn prompts the pancreas to produce the digestive juice. Without it the acids cannot be neutralized. The pancreas secretion contains also the enzymes necessary to tear proteins, fats and carbohydrates.</p>
<p>The pancreas produces about a litre of the alkaline digestive juices and many other fluids this reach the intestine tract. They are saliva, gastric juices, bile and juices from various other glands. The saliva is required to moisten food, swallow it and to start the digestion of starches. The gastric juices destroy the bacteria in the food clots milk and breaks up protein. The bile further breaks up the fat globules first processed by the pancreas enzymes.</p>
<p>The daily total of all these is astonishing- Eightliters!</p>
<p>The intestine through the microscope will reveal a network of cavities and projections. This makes its inferior area very wide. The projections are called villi .It is their duty to take the converted food components for circulation throughout the body. Proteins and carbohydrates through the blood stream, fats via lymphatic system.</p>
<p>The entire length of the&nbsp; intestine&nbsp; have muscles&nbsp; to give it&nbsp; movement .This is necessary to churn out&nbsp; food and digestive juices .There is also wave like motion to carry the content along . The part of the intestine that is called the small gut has no rest at all. It is on continuous operation.</p>
<p>Food processing takes about eight hour for completion. The food reaches the big intestine in the form of gruel. It is its duty to extract the water and pass it into the blood .Without this most important function of the intestine the fluids will be lost and the body will be dry. After the extraction of water what remains as semi solid is kept in a part of the colon near the rectum.</p>
<p><strong>Likely disorders</strong></p>
<p>If nervous tension, drugs, bacteria etc speed up passage water is not fully extracted .This is called diarrhoea. If there is bad diet or worry the food processing and water extraction stops and constipation results. Diarrhoea is dangerous because of the possibility of dehydration and this creates the need to drink too much water.</p>
<p>Gas trouble is actually the food one swallowed and the intestine-produced methane and hydrogen .The intestine is also affected by our moods&nbsp;</p>
<p>Diverticulosis is the forming of small enlargements .They may get inflamed and the disorder is called diverticulitis.</p>
<p>Enteritis is an inflammation of intestine linings as result of the bacterial or viral attack, influence of chemicals. Ulcerative colitis is the development of ulcers in the lining of the big gut. It can sometimes result in colon hemorrhage. Spastic colon is often the result of stress.Worries can cause block in the passage of colon .It gets stiffened and food fails to move as it would in normal conditions.</p>
<p>For intestine it is always better to avoid onions, beans, cabbages, etc in large quantities, and also heavy meals. Fruits, leafy stuffs,etc,help it. Water is always welcome.</p>
<p><strong>Tests available.&nbsp;</strong></p>

								
							</div>
						</div>
					</div>
							<div class="bb-item" id="item27">
						<div class="content">
							<div class="scroller">
							<h2>Pancreas</h2>
<p>The pancreas is a relentless worker. It is its duty to see that the blood sugar level is steady and that the cell fibres have a constant supply of fuels.</p>
<p><strong>Structure </strong></p>
<p>The pancreas has actually a shameful weight and size .It weighs only eighty-five grams and is only fifteen centimeters long. A grey-pink colour distinguishes it. In form, it is actually two glands in one cover and lies in a crowded place where the liver, kidneys and the large intestine compete for their place. As a result,there is chance for it to get lot of damages during surgery, if the knife is not careful and pancreatitis will result.</p>
<p><strong>Function</strong></p>
<p>It is indeed wonder that about a litre of secretion oozes out from this merely eighty-five gram- organ every day. It is its business to neutralize the acid containing the gruel formed food. The acid is needed to break down the proteins in the food but its excess can damage the linings of the&nbsp; intestine .The pancreas produce alkaline in sufficient quantity to neutralize&nbsp; the food gruel and to save the intestine walls from damage.</p>
<p>The pancreas is also the producer of two important hormones that get into the blood stream .The glucose or blood sugar feeds the cells and is responsible for all energy .The insulin keeps up the level of sugar in the blood.</p>
<p><strong>The acini </strong></p>
<p>The pancreas has thousands of sack-like glands that are called acini. They produce alkaline juices when they receive hints from the nervous system .Through the pylorus the&nbsp; chyme or the gruel- like food reaches the duodenum and there a hormone secretion first protects it and activates the pancreas .The aciniare stimulated and the alkali is produced to neutralize the acid in the food gruel.</p>
<p>It is the pancreas that makes the food acceptable to the blood stream. It achieves this with the help of three enzymes: Trypsin, amylase and lipase. Trypsin converts protein into amino-acids needed for tissue building, Amylase converts starch into sugar, and lipase attacks globules into fatty acids and glycerin.</p>
<p>The saliva, gastric juices and the intestinal secretions may digest food. But without the work of pancreas, food digestion will be painful.</p>
<p><strong>Insulin production</strong>,</p>
<p>Until 1921,absence of insulin in the body meant diabetesrelated,slow, painful death. With the introduction of insulin extracted from animals began to be used and diabetes became controllable.</p>
<p>The pancreas has innumerable islets which produce the inevitable and immensely popular hormone called insulin. The cells need glucose to generate energy. They burn glucose continuously to energise the cells and it is the duty of the pancreas to see that there is always the sufficient quantity of glucose. A cell need 4.4 grams and the pancreas takes care of it</p>
<p>It also helps the burning of glucose. If the islets do not function the cells would begin to burn fat, and protein frommuscles would be used up as fuel for the cells. Hunger and thirst would be the symptoms and one gets also thinner and thinner. In other words <strong>one becomes achronicdiabetic</strong>.</p>
<p>The liver helps to control the excess of glucose in the blood and converting it in to glycogen and vice versa when there is shortage of it.</p>
<p><strong>Disorders, diseases</strong></p>
<p>The main disease is diabetes. Pancreatitis is an inflammation that may result from diseases like mumps, careless surgery on a neighboring organ , a disease of the artery, excessive use of alcohol ,etc can do it harm .Sometimes bile can get into the duct and damage or&nbsp; destroy it . Gall stone can block the duct passage and the enzymes may work on the pancreas itself and destroy it. Pancreatitis is a serious disease and can cause death <strong>if ignored.</strong></p>
<p>Tumors of pancreas are of different types. Adenoma is one of them .It makes the pancreas produce insulin in excess. After lung and colon-rectum cancer, that of pancreas is the most deadly killer.</p>
<p>What the pancreas demand from us is precisely this: Eat and drink sensibly and it can go on with its most delicate functions withno complaintwhatever.&nbsp;</p>	

								
							</div>
						</div>
					</div>
										<div class="bb-item" id="item28">
						<div class="content">
							<div class="scroller">
							<h2>Liver</h2>
<p>About 500 risky jobs 100 vitamins weighs one and half kilos &ndash; Is thereanorganto boldlybeat it?</p>
<p><strong>Structure </strong></p>
<p>The liver lives protected by the ribs and almost covers the upper part of the abdomen. It is the biggest organ in the human body.</p>
<p><strong>Functions </strong></p>
<p>It is responsible for antibodies and converts the amino acidsinto human protein. The excess is changed into urea and sent to the kidneys for disposal. The liver acts also as the safety valve for the human heart. The hepatic vein on its upper side absorbs any excess blood that happens to surge in to the heart and release it by and by.</p>
<p>Anything harmful is degraded or converted. Alcohol becomes water and carbon dioxide. Likewise any toxic element is changed into harmless substance. An exertion of body needs the burning of glucose .This generates the deadly lactic acid. The liver converts it into glycogen and keeps it in reserve.</p>
<p>It is the liver that changes the cane sugar into blood sugar or glucose. It also sees to it that too much glucose does not remain in the blood.&nbsp; Its end results will be coma and&nbsp;&nbsp; then death. The excess sugar is converted into starchy glycogen .The liver is able to store about 250 grams of sugar this way. In a wonderful way the liver converts this again in to glucose when the body needs sugar between the meals.</p>
<p>In human body the cells die in innumerable number every time. The liver takes up the breakdown products and helps the production of the cells again and for the production of green digestive juice. It goes first into the gallbladder and then into the duodenum to help digestion. It also removes the fat deposits in the liver channels.</p>
<p>The bile that enters in to the gall bladder contains two pigments the bilirubin and the bilverdin (red and green bile) if these pigments accidently passes into the blood it is called jaundice, the yellowing of the skin and eyes. Actually it is a symptom that announces the liver disorder.</p>
<p><strong>Disorders </strong></p>
<p><strong>Hepatitis</strong></p>
<p>It is the destruction of the innumerable working cells of the liver owing to viral infection .Normally the intensity of the attack comes down after days and the liver comes back to normal state.</p>
<p><strong>Infiltration</strong></p>
<p>Infiltration of fat is a serious disorder because the fat replaces the working cells and the liver becomes distended.</p>
<p><strong>Cirrhosis</strong></p>
<p>There is chance for the fat to enter the blood stream and produce block in the blood vessels and in the vital organs. Fat infiltration can also produce fibrous tissue which will replace the working cells. The liver becomes shrunken, hard, yellowish, with full of knobs .This is liver cirrhosis.</p>
<p>Infection , poisoning , drugs ,alcohol ,poor diet anything can cause cirrhosis .The liver keeps in reserve so many working cells and&nbsp; manages to go on steadily even when there is every chance for cirrhosis and surrenders only at last .</p>
<p>The symptoms of liver will appear in the form of unusual fatigue, loss of appetite, swollen abdomen, spider shaped vessels on the upper part of the body, or jaundice.</p>
<p>Cirrhosis is to a good extent treatable. The patient is fed with a high protein diet and liberal doses of vitamin and often there is chance for a fresh beginning if the conditions that made it are not repeated. One has to watch his weight, take a lot of vitamin B and keep off alcohol .This means one has concern for the liver.</p>	
								
							</div>
						</div>
					</div>
										<div class="bb-item" id="item29">
						<div class="content">
							<div class="scroller">
							<h2>The urinary tract</h2>	
							<img src="images/urinary.svg" alt="brain" width="90%" >	
							</div>
						</div>
					</div>
					<div class="bb-item" id="item30">
						<div class="content">
							<div class="scroller">
							<p><strong>Kidneys </strong></p>
<p>J.D. Ratcliffe is of the opinion that it is not the intestine but the kidney that is the main agent of waste disposal from the body.</p>
<p><strong>Structure </strong></p>
<p>Like many organs of the human body, this waste disposing agent has a partner and together they inhabit the lower part of the spine. In appearance it is reddish brown and looks like a big bean and weighs 140 grams.There are actually about a million small big-headed worm like components in it, which, if extended,will cover each 105 kilometers!</p>
<p><strong>Functions</strong></p>
<p>Its functions are significant and varied. Blood is filtered constantly and the deadly wastes like potassium or sodium chloride are withdrawn from it every time the blood passes through it. It has a role in the production of the red cells and maintains the balance of water in the body. It is also the business of the kidney to make sure that there is no excess of acid or alkaline in the blood. And there are doctors who believe that there are still more things to be known about it.</p>
<p>Together the kidneys filter twice the total of blood every hour. The purification is a wonderful process. Blood cells and the main particles of blood proteins are not kept back in the process. Instead they are reabsorbed in the kidney&rsquo;s tubules. What are required in the bloodstream, vitamins, amino acids, hormones, etc are sent back in necessary quantities. Any excess is let out through urine.</p>
<p>The kidneys absorb any excess of salt as it would prove harmful to the body. If it does not do this the excess salt will hold water and it can enter the blood and spaces between the cells. This will manifest in the face, feet and abdomen .They will become swollen. Excess fluid will affect the heart as it would find difficult to pump against such a heavy quantity of fluid which the body does not want at all.</p>
<p>&nbsp;</p>
<p>Another main activity of kidneys is the management of potassium .If there is no enough quantity of potassium the muscles will be affected. The breathing muscles suffer in particular. Too much will affect the heart .The kidneys also restrict the use when diet cannot supply it in required quantity.</p>
<p>The protein absorption of the body leaves urea as waste .It is the business of the kidneys to keep it in correct measure in the body .If urea is in very nominal quantity something is wrong with the liver and too much makes the deadly disorder called uremic poisoning or urine in the blood. The body has its own mechanism to remove it from the body .We sometimes find a frost like substance on the skin .It is the attempt of the body to get rid of the excess urea via sweat glands.</p>
<p>The urine disposal is an astonishing process. The fluid with all the waste passes through the tubules and first enters a small container which is connected to the bladder. It has further connection to outside. To reach the exit tubes the muscles of this move every 10 to 30 seconds. This process is slow in the night so that we can sleep in comfort.</p>
<p>The tendency to pass urine is high in cold climate. The route of blood flow changes from the skin to the internal organs and naturally the supply to the kidneys increase. More blood means more urine. When there is increased blood supply as result of our anger the same thing happens .in the case of alcoholics it is a bit different .Actually it is the pituitary that controls the urine. It can produce an antidiuretic hormone in normal condition. But when one is drunk it is upset. The kidney has no brake then and it produces urine in abundance. The body soon gets dehydrated and the drunkard in the morning drinks a lot of water by way of compensation. Coffee does the same work of alcohol .Cigarettes on the other hand stimulates the production of the antidiuretic hormone. Heavy smokers pass less urine!</p>
<p>After middle age one has chance to have floating kidneys. When the fats on which the kidneys hang stretch and the kidneys lose anchor and begin to float.</p>
<p><strong>Stone </strong>(Calculus) is another problem. Kidney stones, the calculi,normally pass through urine if they are small enough and their edges,not sharp. But some need removal by surgery. Kidney stones are, in fact,urine concentrations, calcium salts, uric acid, etc in the form of crystals.</p>
<p>Most of the food,we take in contain enough water .If meat is fifty percent water; banana is 90 percent and water melon 93 percent of it .But an additional glass will always do well.</p>
<p>NephronDamage is a serious disorder. This can happen as result of infection. The urinary tract will fume it up and the remedy is antibiotics. Injuries to the kidneys become serious when there is considerable damage to the nephrons</p>
<p>With age,the arteries harden and the blood supply to the kidneys gets reduced. The slow pumping of the aged heart also creates the same problem. As a result toxic items, sodium, potassium, chloride - all remain unfiltered and their balance gets upset.</p>
<p><strong>Tests </strong></p>
<p>Urinalysis, detection of the presence of fat, protein etc in the very form of tubules in the urine, etc,tell doctors the nature of the disorder. In blood tests the excess of urea can be revealed. It will show the failure of kidneys to get rid of the protein waste that is urea.</p>
<p>Sometimes doctors inject a dye into the blood stream .If the kidneys take abnormal time to dispose of the dye they are abnormal.</p>
<p>Diet control, normalizing blood pressure, mild exercises .etc, and an additional glass of water every day can help kidneys. Violent exercise is discouraged because it can produce lactic acid which will burden the kidneys.</p>
<p>Tendency to vomit, blurring of vision, unusual tedium, cloudy, smoky or mahogany -colored urine are warning signals .They indicate that it is time to attend the kidneys.</p>	

								
							</div>
						</div>
					</div>
					<div class="bb-item" id="item31">
						<div class="content">
							<div class="scroller">
							<h2>Bladder &lsquo;</h2>
<p>The intestine, the kidneys and the bladder do the disposal of waste. But the difference is that while&nbsp; the intestine can go out of order for weeks and wait for repair , the urinary system&nbsp; can remain thus only a few&nbsp; hours or days at the most. The kidneys, bladder, etc, often call for immediate medical attention.</p>
<p><strong>Structure</strong></p>
<p>The bladder looks somewhat like <em>a punching bag</em>. Its capacity varies from individual to individual. It ranges from 250 ml to 750 ml but normally capacious enough to hold half a litre. It gets the waste of kidneys after it purifies the blood in the form of urine via ureters, the two tiny tubes that is about 30 centimetres in length and is in the size of a pencil. Urine is passed in drops into the bladder every time and it is let outside via urethra .The amount of urine that reaches the bladder varies as per the amount of fluid lost by the body by other means, i.e. via sweat glands, lungs, etc. perspiration reduces its amount and so is sleep.</p>
<p><strong>Functions</strong></p>
<p>The bladder muscles contract when the bladder has to empty urine through urethra. .This is often influenced by so many factors like anxiety, fear, etc which raise our blood pressure and activate the kidneys to filter fast and dribble urine into the bladder in larger measure. A pregnant woman gets tempted to urinate often as the baby&rsquo;s pressure comes onto the bladder. In cold climate the blood routes from the skin gets diverted to the internal organs .Kidneys will have to filter more urine and so the number of calls to urinate multiply.</p>
<p>The bladder has two sphincters or valves .The valve at the bottom opens when the bladder swells up with urine. It is an automatic action. Below it is the second valve .Its control is voluntary. When the first valve opens we long to urinate, the second valve opens and the process is at work. The second valve is under our control if we have not failed to learn it by practice in childhood. Babies who fail to learn it make the bed wet for a time.</p>
<p>So many factors are behind it and it has been found that boys beat girls in bedwetting.&nbsp; Paralyzed people and people with paralyzed (neurogenic) bladder also become victims of bedwetting.</p>
<p>The urethra passes through the prostate. If it is enlarged or damages, it can reduce the flow of urine.&nbsp;</p>
<p><strong>Urine tests </strong></p>
<p>The urine is a very reliable indicator of what happens inside the body. Its weight in relation to simple pure water is considered to know of the kidneys function,dehydration, and the presence of uric acid to examine whether the kidneys have gout or stones. Urine tests can also reveal heart diseases, psoriasis, disorders of the glands of secretions and so on. It is a signal if the urine is unusually concentrated, deeply wax -coloured, cloudy or with a bad smell. A good physician alone can tell whether there is disorder or not .Sometimes the urine becomes cloudy for no serious reason. If there is blood in the urine it is something really serious.</p>
<p>Actually almost all organs empty waste or their excesses into the urine .The pregnancy test takes in to account the excess of the female hormone in it to decide on it.</p>
<p><strong>Disorders </strong></p>
<p>Strictures or blocks due to sex- related or other diseases, tumors, damage of pancreas, etc can affect the bladder function.</p>
<p>In serious types of tumours, the bladder is removed and the ureters are given connection to the intestine. Then we become bird-like, i.e., withouturinary bladder!</p>
<p>The bladder can also have stones in it. Stones,if they get in to the urinary tubes, can give unexplainable pain and urine that moves back to the kidneys can beeventually very serious and even result in death.</p>
<p>The minerals in the urine get concentrated and turns into stones .People that live in warmer climate are prone to stones .In rare instances, stonecan weigh as much as 7 kilos! There will be no harm as far as they do not block urine .Tiny stones pass easily through urine. It is another matter if they have jagged irregular ends. They will cut the tract that moves it along and can inflict dire pain in the process. Now new surgical and pounding methods make stones less serious.</p>
<p>Cystitis is infection&nbsp;&nbsp; caused by microbes, more common in women than in men. This is because their urethra is shorter. If the female urethra is only three centimeters long, men&rsquo;s are as long as 20 to 30 centimetres. Microbes take the three centimeter&nbsp; frequently and far more quickly. Antibiotics or sulpha drugs can easily remove the infection.</p>	

								
							</div>
						</div>
					</div>
					<div class="bb-item" id="item32">
						<div class="content">
							<div class="scroller">
							<h2>Prostate</h2>
<p>The human race owes its very existence to this performer of wonderful feats, the preserver of the seminal fluid right at the neck of the urinary bladder. In fact the exit tube of the bladder, the urethra passes through it.&nbsp; It is able to dictate the bladder to stop work when it is time for it to perform its miracle - the dilution of that life -sustaining fluid that carries about 200 million cells. It is the very nature of the fluid &ndash; a mixture of enzymes, fats, and sugars to nourish the sperm, to remove the acidity of the female tract with enough consistency for the sperm to move towards the female egg.</p>
<p><strong>Structure </strong></p>
<p>The prostate is red brown in appearance and is very small at the time of birth .It grows to its real size by the time one attains puberty. It is in three lobes and live side by side wrapped up. The urethra passes over the middle tube has grape like glands which produces the seminal fluid which it stores in its pouch until it receives nervous signal from the lower end of the spinal cord for release.</p>
<p><strong>Functions</strong></p>
<p>At the time of ejaculation the lower part of the spinal cord sends signal to the prostate. Many things happen at once in its areas. The sphincter valve at the bladder is shut to prevent the urine release .The prostate contracts with the seminal vesicles that lie near with the sperm deposit and it is passed out through urethra, Actually a 20 percent of seminal fluid is from the&nbsp; vesicles themselves .The prostate&nbsp; makes it 100 percent ,which is its main&nbsp; service.</p>
<p><strong>Disorders, diseases </strong></p>
<p>The urethra&nbsp; that passes over the middle section of the prostate enlarges by age&nbsp; and begins to create&nbsp; problems&nbsp; for men .As &nbsp;sexual activities diminish ,it actually ought to shrink but oddly enough the prostate enlarges with years .When one attains fifty,&nbsp; it may begin with a twenty percent enlargement , by 70 , fifty percent and by 80 , eighty&nbsp; percent .The <em>eunuchs</em>&nbsp; seldom have prostate complaints .So doctors believe that it has relation to the sex hormones.</p>
<p>The urine block that the prostate swelling creates blocks the urethra .Urine backs up to the bladder and becomes infected very soon. Complete blockage&nbsp;&nbsp; makes the urine backup to the kidneys .It spills over to the blood stream .This&nbsp; is called&nbsp; Uremic poisoning which can be mortal (able to cause death )</p>
<p>The enlargement of the prostate middle lobe poses problem only when the urine blockage occurs. The decrease in force and volume of urine will indicate it. One will have a tendency to frequently urinate or a nagging feeling that the urine has not gone out in full.</p>
<p>It is for the doctors to verify whether it is inflammation or real blockage. Once it is identified,better pepper, tea, coffee, alcohol etc are avoided for they will close the urethra further.</p>
<p>The urine tube is resorted to when there is complete blockage.&nbsp; It is inserted through urethra to the bladder and urine is drained out in emergency. If the prostate is found too large it is surgically removed. Sometimes a pencil like instrument that has an electrically powered cutter is inserted in the same way and the blocking tissue of the middle lobe is removed. Liquid nitrogen is used in some cases to freeze and kill the tissue which gradually comes out with the urine.</p>
<p>Cancer</p>
<p>Prostate cancer&nbsp; is&nbsp; treacherous .We do not know its growth and often the doctors find it too late .BY the time one becomes 50 there is five percent chance , by fifty percent . It is however slow growing in most cases .If its appearance is violent it can spell death in a matter of weeks.</p>
<p><strong>Treatments </strong></p>
<p>The surgeon removes the prostate in serious cases .But there are also methods that are not surgical but at the same time life- saving. The sex hormone being the culprit, castration, treatment using female hormone, radiation and hormone treatment together, etc,are some methods.</p>
<p>Serum acid phosphates test detects the singular prostate enzyme. Its presence in the blood assures the doctors that the prostate lobes are broken and damaged and it is a sign of cancer of the prostate.</p>
<p>Rectal examination once or twice year can help early detection of cancer affecting prostate.</p>
<p>An expert doctor can pass over his fingers on the surface of the body and see whether there is any button type swelling which is an indication of the oncoming prostatic cancer. A sample of the tissue is taken with a hollow needle to examine the nature of the disease. Total removal by surgery is the remedy then.</p>
<p>Frequency of urination, burning sensation, slow flow of urine, etc is indications of oncoming troubles. Rectum examination is important.</p>
		
							</div>
						</div>
					</div>
					<div class="bb-item" id="item33">
						<div class="content">
							<div class="scroller">
							<h2>The Reproductive organs</h2>
							<img src="images/reproduct.svg" alt="brain" width="90%" >	
							</div>
						</div>
					</div>
					<div class="bb-item" id="item34">
						<div class="content">
							<div class="scroller">	
							<p><strong>Testis </strong></p>
<p>The testis&nbsp;&nbsp; is not merely an indicator of maleness .Indeed, it is primarily a sex &ndash;related organ but at the same time is responsible for many chemical conversions and has strong influence on the male personality.</p>
<p><strong>Structure</strong></p>
<p>The left testis that has a partner in the right is pink-white and in shape oval. It weighs four grams and four centimeter long .Its maximum diameter is two centimeters .It contains a thousand thirty to sixty centimeter tubes soft as silk .They are connected to a collecting sac.</p>
<p><strong>Testosterone </strong></p>
<p>The two chief functions of the testis are to produce the sperm cells and the male hormone testosterone. The testis manufacture the sperms in the duct.&nbsp; It produces about half a million sperm cells every day. With all of them fertilizing, within two months, the world will be people, people,and peopleeverywhere.</p>
<p>It is the testosterone that is responsible for the making of muscles and bones, and other tissues. The male mind and male body owe a great deal to this chemical .The strength of thebody, the growth of the beard,the spirited nature, etc, the very macho image that mean theopposite of the fair sex are linked to it.</p>
<p>The Leyden cells in the duct produce the testosterone .A <sup>1</sup>/<sub>20 </sub><sup>the</sup> of this hormone is found in the woman&rsquo;s blood also. If this is in excess feminine attributes of a woman will give way to masculinity(the word that denotes this is amazonian).</p>
<p>The testes appear only after birth. Until then they lie inside the body .The inguinal canal allows it to come out after birth and closes up. If the closure is not perfect as it happens in some male birth, it will eventually lead to hernia.</p>
<p>The testes need a 95.6 0 F to produce sperm ,i.e. heat ,lower by three degrees than the normal body heat 98.6 0 F and the testes are allowed to go out to maintain this low&nbsp; temperature in the long run. It also has its own method of maintaining it. The sac which hold the testes, the scrotum has sweat glands aplenty. They cool by evaporation. Similarly the cord that suspends the testes can lengthen itself in warmth and shorten in cold so that the testes can be far from the body in warmth and near to it for warmth in cold.&nbsp; In a period of high fever the testes can paralyze and sterile itself.</p>

<p><strong>Extra ordinary sperms</strong></p>
<p>If the female eggs are the biggest of body cells, the sperm cells are the tiniest. If 1200 of them join together to make a mass, the size will be that of a printed full stop. They have small tails for movement and this give them a tadpole shape. The head contains all the components for life .They move at a speed of 18 centimeters an hour, which is speed indeed when we consider their size.</p>
<p>Another striking feature is that they have only 23 chromosomes while other cells have 46.The rest comes from the female egg. It is also quite astonishing to know that the small sized sperm with a particular kind of enzyme on the head is actually breaking the tough outer layer and claiming its entry into the egg for fertilization!</p>
<p>The nourishment and energy for the sperm comes with the dilution by the fluid from the prostate. It is sugar, protein and minerals. Sperm count will multiply when there is a gap in intercourse. If pregnancy is delayed it is better for the couple to wait for a period and then try again.&nbsp; The sperms die of age when not released at all but when released so often they will not get time to mature. The sperms need weeks to get back to normal production.</p>
<p>It is the pituitary gland that stimulates the testis to produce the sperm and the testosterone. It is this hormone that makes the boy grow and attain masculinity. He begins to get beard, his legs grow,and (he feels growing pains!) &nbsp;and in some rare cases he gets acne owing to the excess of the hormone. He becomes more confident and emotionally stable with the testosterone commencing its work at puberty.</p>
<p>The testosterone plays a role in man&rsquo;s sex life (But we know it is the mind that plays the major role!)</p>
<p>&nbsp;</p>
<p>Testosterone has great influences on our emotions and memory.</p>
<p>&nbsp;</p>
<p>With one crossing forty seven or so, the testes minimize their work. But there is no need to worry. The testes will produce enough quantity of the hormone to meet the demands of the body even in very old age Sperm may be present even when one has crossed ninety. Only the chances of the sperm fertilizing will be bleak.</p>
<p>A healthy body can be rest assured of&nbsp;&nbsp; generous, continuous service from the testes. In old age also,we require some of its chemical conversions and so we should constantly care our bodyand attend to its needs for well-being as services by way of return.&nbsp;</p>

							
							</div>
						</div>
					</div>
					<div class="bb-item" id="item35">
						<div class="content">
							<div class="scroller">	
							<h2>Womb</h2>
<p>The womb in the lower abdomen is the female organ that literally <em>measures up to the occasion</em>. From its initial weight of 57 grams it grows up to weigh one kilo in the ninth month and then reduces within one or two months to its normal shape when it&rsquo;s most valuable work is over.&nbsp; It is also famous for its untiring readiness every month to preserve the possible first stirring in it, to guard and transform &lsquo;<em>an infinitesimal aquatic parasite into a three kilo baby&rsquo;!</em></p>
<p><strong>Structure</strong></p>
<p>The womb is a small muscle-made pouch that hangs from ligaments in the lower abdomen. In its normal idle position it weighs only 57 grams but is capable of the great enlargement&nbsp;&nbsp; mentioned above. It has three openings:Two Fallopian tubes connected to the ovaries bring in the fertilized egg sent by one of the two ovaries. The third is the small tunnel through the neck of the womb called the cervix. The male sperm is received through it.</p>
<p><strong>Functions,</strong><em>the dazzling chemistry of monthly preparations&rsquo;</em></p>
<p>Every month the womb constructs new blood vessels, new glands, and new tissues. The estrogen hormone which comes from the ovaries stimulates the womb. Its endometrium, the <em>blood-red, velvet-smooth </em>lining thickens .The glands enlarge. The muscles begin to contract regularly .To relax it the ovaries produce Progesterone .It has two important functions: the womb lining gets prepared to accept life and the glands are stimulated to&nbsp;&nbsp; produce nourishment for the fertilized egg.</p>
<p>At the time the ovaries release the egg, the mucus glands in cervix secrete and the fluid allows the sperm to swim in the direction of the egg.</p>
<p><strong>Period</strong></p>
<p>Every month the womb gets ready to receive the fertilized ovum .New tissues, glands and blood vessels kept in reserve go unutilized if the fertilized ovum is not received. This is called period, a cleaning job done to get ready next month all over again.</p>
<p>The womb is quick to act the moment the egg is fertilized.&nbsp; Cell division process begins in the fallopian tube and its only nourishment there is the yolk. The egg as it reaches the womb develop tiny feelers and they are hooked on to the endometrium .Further nourishment is provided by the endometrium.</p>
<p>To feed the new arrival for the next nine months another complex structure of tissues named the placenta provide generous help .From its apparently insignificant size it gradually grows into one kilo weight and 18 centimeter length. It functions at once as the lungs, liver, kidneys and digestive tract for the new one.</p>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>The umbilical cord&nbsp; </strong></p>
<p>This actually consists of two arteries and a vein. It can be 12 to 25 centimeter long .The vein accepts nourishment from the placenta and the waste is passed in to it through the arteries .The waste subsequently spreads into the mother&rsquo;s blood stream and her liver and kidney and the lungs take care of its disposal.</p>
<p>The placenta&rsquo;s is a miraculous operation. It accepts the oxygen, vitamin, carbohydrates, amino acids, etc and does a clever filtering job .The greatest wonder is that it is always able to keep the mother&rsquo;s blood and her baby separate so as to avoid incompatibility!</p>
<p>The womb can enlarge up to 500 times.&nbsp; This is indeed a dire necessity because every month the size of the baby increases. It also gets stronger and changes its shape to accommodate what is within. The muscles get tougher to sustain the pushes and kicks of the <em>residen</em>t most probably in the head -first position in the long run.&nbsp; The womb may apply pressure on the womb and the mother may be tempted to urinate often. Digestive system can also be affected.</p>
<p><strong>Delivery </strong></p>
<p>To&nbsp; eject what&nbsp; has been inside nine months the womb&nbsp; widens its cervix from its apparently&nbsp; small slit like form&nbsp; to&nbsp; a gap of about 12 centimeter .This&nbsp; is the outcome of its slow ,steady contraction happening at two-to three minute interval each lasting a minute.</p>
<p>The baby&rsquo;s head also helps widen the cervix opening. The womb is able to make a force of 7 kilos but the delivery is actually a twelve kilo thrust .The abdomen and the diaphragm help the womb for the required force. And,hai,the baby is born!</p>
<p>The placenta is eventually removed .The womb again apply pressure on the exposed blood vessels and they soon close and escape hemorrhages</p>
<p>The womb continues its preparations and two or three times the entire process will be repeated. Menopause approaches by 42 years of age or after .The womb then shrinks to its normal size.</p>
<p><strong>Likely disorders </strong></p>
<p><strong>Dysmenorrhea.</strong></p>
<p>This is the strong pain accompanying each menstruation.</p>
<p>Fibrosis</p>
<p>The whitish matter on the muscular walls of the womb .It is unlikely that it can develop in to cancer and are more often curable.</p>
<p>When the womb linings do not grow or shed correctly every month there will be excessive bleeding .Dilation and Curettage (D&amp;C)&nbsp;&nbsp; is the most popular surgery in which the cervix opening is widened (dilation) and walls of the womb are scraped clean(curettage).</p>
<p>Cancer</p>
<p>90 percent is curable. Abnormal bleeding after 40 can be a warning signal. There can be many reasons for bleeding and so a thorough check up is necessary to decide it.</p>
<p>A yearly pap test is alsonecessary after one cross forty.</p>
							
							</div>
						</div>
					</div>
					<div class="bb-item" id="item36">
						<div class="content">
							<div class="scroller">	
							<h2>Ovary</h2>
<p>The ovaries are a unique pair. It is they that make a woman what she actually is. It&nbsp; re-moulds her very form&nbsp; a&nbsp; year or so before she steps into her teens , decides&nbsp; her moods ,&nbsp; wellbeing,&nbsp; and&nbsp; most important of all, helps&nbsp; her to become a loving affectionate mother.</p>
<p><strong>Structure</strong></p>
<p>The ovaries hang from either side of the pelvis by ligaments.</p>
<p>They are very small, weighing just seven grams, and measuring a length of only three centimeters. In appearance, they are somewhat almond in shape and white in color.&nbsp;</p>
<p>The ovaries are awfully small in girl&rsquo;s .But even then they contain numerous microscopic egg cells called oocytes. They contain all the factors that will determine the type of baby she will have when she is a woman.</p>
<p>The ovaries have a major role in the proper monthly menstruation. And when it is child bearing,it supplies one or two necessary items to sustain the baby&rsquo;s life.</p>
<p><strong>Functions</strong></p>
<p>The production of egg.</p>
<p>&nbsp;The pituitary gland secretes the Follicle stimulating hormone (FSH) during the menstrual cycle very early and the sleeping eggs awake. The fluid &ndash;filled follicle surrounds the eggs and the bubbles rise up .Only one among the egg, if no multiple birth, succeed in reaching the surface. The pituitary then secretes the<em>luteinizing</em>hormone to burst the follicle. What is inside bursts and in a wave of fluid the egg is taken along and the fallopian tube takes it into the womb with the possibility of getting fertilized on the way.</p>
<p>The egg extraordinary</p>
<p>It is a surprise that the egg waits patiently for some twenty years to contribute to creation. The 23 chromosomes in it awaits the 23 chromosomes in the male sperm .The mature egg is the largest celli the female body but it is still very small. And it is from this the baby eventually evolves.</p>
<p>The female egg achieves its end after so many imperfect ones get rejected. Late arrivals may become abnormal. But often a normal child can be expected even after forty.</p>
<p>The ovary is also a hormone producing gland .The estrogens is responsible for the growth and functioning of the breast, sex organs, etc. The ovaries also produce in small quantity the male hormone testosterone .Its excess&nbsp; can give the women a deep&nbsp; voice, or a beard&nbsp; that are&nbsp; male attributes .But as a rule the&nbsp; ovaries can convert testosterone into estrogen .</p>
<p>When the egg&nbsp;&nbsp; reaches the surface, the luteinizing hormone fill the cavity of the ovary with a fatty, yellowish substance .This is actually the formation of a new gland, the Corpus luteum. It produces progesterone, the new hormone that is <em>pro- pregnancy</em> or <em>pro gestation</em> as its name suggests. It slows the rhythmic contraction of the womb, thickens its walls and produces in it new blood vessels. It thus makes a comfortable condition and nourishment for the fertilized egg. If there is no pregnancy the new gland dies.</p>
<p>The ovaries maintain the balance of quantity of the estrogen and progesterone. Imbalance in their production can retain fluid, develop puffy feet or get depressed or bad tempered prior to and during menstruation. Such hormone imbalances are now treatable with pills.</p>
<p>Likely disorders, diseases.</p>
<p>The ovaries shrink to its initial size once women become 45 or 50.The production of estrogen becomes nominal. The dodders as result of its short supply can range from muscle stiffness, to coronary heart disease. Osteoporosis may strike and the bones can be really brittle.</p>
<p>Ovary cancer is a possibility. It is silent and so often it becomes very late when detected.</p>
							
							</div>
						</div>
					</div>
					
					
				</div>
				
				<nav>
					<span id="bb-nav-prev">&larr;</span>
					<span id="bb-nav-next">&rarr;</span>
				</nav>

				<span id="tblcontents" class="menu-button">Table of Contents</span>

			</div>
				
		</div><!-- /container -->
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script src="js/jquery.mousewheel.js"></script>
		<script src="js/jquery.jscrollpane.min.js"></script>
		<script src="js/jquerypp.custom.js"></script>
		<script src="js/jquery.bookblock.js"></script>
		<script src="js/page.js"></script>
		<script>
			$(function() {

				Page.init();

			});
		</script>
	</body>
</html>
