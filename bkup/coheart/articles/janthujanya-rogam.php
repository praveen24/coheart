<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>COHEART | Janthujanya Rogam</title>
		<link rel="icon" href="../dist/images/favicon.ico" type="image/x-icon"> 
		<link rel="stylesheet" type="text/css" href="css/jquery.jscrollpane.custom.css" />
		<link rel="stylesheet" type="text/css" href="css/bookblock.css" />
		<link rel="stylesheet" type="text/css" href="css/custom.css" />
		<script src="js/modernizr.custom.79639.js"></script>
	</head>
	<body>
		<div id="container" class="container">	

			<div class="menu-panel">
				<h3>Table of Contents</h3>
				<ul id="menu-toc" class="menu-toc">
					<li class="menu-toc-current"><a href="#item1">ജന്തുജന്യ രോഗങ്ങൾ</a></li>
					<li><a href="#item2">1. Introductuin</a></li>
					<li><a href="#item3">2. ചില രോഗങ്ങൾ</a></li>
					<li><a href="#item4">3. ലൈം ഡിസീസ്</a></li>
					<li><a href="#item5">4. കൈസാനൂർ ഫോറസ്റ്റ്ഡിസീസ്‌</a></li>
					<li><a href="#item6">5. എലിപ്പനി</a></li>
					<li><a href="#item6"></a></li>
					<li><a href="#item8">6. ഡെങ്കിപ്പനി</a></li>
				</ul>
				
			</div>

			<div class="bb-custom-wrapper">
				<div id="bb-bookblock" class="bb-bookblock">
					<div class="bb-item" id="item1">
						<div class="content">
							<div class="scroller" align="center">
								<div>
									<img src="images/janthujanya/janthujanya-rogam-1.jpg" alt="coheart janthujanya rogam" width="90%" >	
								</div>	
							</div>
						</div>
					</div>
					<div class="bb-item" id="item2">
						<div class="content">
							<div class="scroller" align="center">
								<div>
									<img src="images/janthujanya/janthujanya-rogam-2.jpg" alt="coheart janthujanya rogam" width="90%" >	
								</div>	
							</div>
						</div>
					</div>
					<div class="bb-item" id="item3">
						<div class="content">
							<div class="scroller" align="center">
								<div>
									<img src="images/janthujanya/janthujanya-rogam-3.jpg" alt="coheart janthujanya rogam" width="90%" >	
								</div>	
							</div>
						</div>
					</div>
					<div class="bb-item" id="item4">
						<div class="content">
							<div class="scroller" align="center">
								<div>
									<img src="images/janthujanya/janthujanya-rogam-4.jpg" alt="coheart janthujanya rogam" width="90%" >	
								</div>	
							</div>
						</div>
					</div>
					<div class="bb-item" id="item5">
						<div class="content">
							<div class="scroller" align="center">
								<div>
									<img src="images/janthujanya/janthujanya-rogam-5.jpg" alt="coheart janthujanya rogam" width="90%" >	
								</div>	
							</div>
						</div>
					</div>
					<div class="bb-item" id="item6">
						<div class="content">
							<div class="scroller" align="center">
								<div>
									<img src="images/janthujanya/janthujanya-rogam-6.jpg" alt="coheart janthujanya rogam" width="90%" >	
								</div>	
							</div>
						</div>
					</div>
					<div class="bb-item" id="item7">
						<div class="content">
							<div class="scroller" align="center">
								<div>
									<img src="images/janthujanya/janthujanya-rogam-7.jpg" alt="coheart janthujanya rogam" width="90%" >	
								</div>	
							</div>
						</div>
					</div>
					<div class="bb-item" id="item8">
						<div class="content">
							<div class="scroller" align="center">
								<div>
									<img src="images/janthujanya/janthujanya-rogam-8.jpg" alt="coheart janthujanya rogam" width="90%" >	
								</div>	
							</div>
						</div>
					</div>
				</div>
				
				<nav>
					<span id="bb-nav-prev">&larr;</span>
					<span id="bb-nav-next">&rarr;</span>
				</nav>

				<span id="tblcontents" class="menu-button">Table of Contents</span>

			</div>
				
		</div><!-- /container -->
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script src="js/jquery.mousewheel.js"></script>
		<script src="js/jquery.jscrollpane.min.js"></script>
		<script src="js/jquerypp.custom.js"></script>
		<script src="js/jquery.bookblock.js"></script>
		<script src="js/page.js"></script>
		<script>
			$(function() {

				Page.init();

			});
		</script>
	</body>
</html>
