<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="coheart, kvasu, veterinary, one, health, university, kerala, animal, science, pookode,diploma,certification,course, education, hygiene,Education, Advocacy, Research, Training "/>
   	<meta name="description" content="One Health concept is based on the understanding that the health of the humans, animals and the environment is inextricably linked, and that promoting the well being of all species can only be achieved through a holistic multidisciplinary approach at the human-animal-ecosystems interface. Kerala Veterinary and Animal Sciences University (KVASU) has taken a pioneer step in this regard following the establishment of the Centre for One Health Education Advocacy Research and Training, the first of its kind in India aiming at the sustained health of the community by addressing various issue of concern today like the food safety and security, zoonoses, diseases from natural origins like soil, water and air."/>
    <meta name="google-site-verification" content="Sg2CCDPE0zio41cI6pF2SkUWVUX1nSGT8waFfR8S6f0" />
    <title>COHEART | Center for One Health Education Advocacy Research and Training</title>
	<link rel="shortcut icon" href="dist/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="dist/images/favicon.ico" type="image/x-icon">
	<!--Slider-->
	<link rel="stylesheet" href="dist/slider/responsiveslides.css">
	<!--flash-->
	<link rel="stylesheet" href="ticker/example/assets/webticker.css" type="text/css" media="screen">
	<!--FancyBox-->
	<link rel="stylesheet" href="dist/lightbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<!-- Bootstrap -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
	<link href="dist/css/style.css" rel="stylesheet">
	<link href="admin/table/css/footable.core.css" rel="stylesheet"/>
	<link href="admin/table/css/footable.metro.css" rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
	<!--<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>-->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
  </head>
  <body>
    <div class=''>
		<div class='banner'>
			<div class='container'>
				<div class='row hidden-xs'>
					<div class='col-md-1 col-md-offset-1 col-xs-1 col-md-offset-1 text-center'>
						<embed height="150" width="130" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" wmode="transparent" menu="false" quality="high" src="dist/images/coheart-logo.swf" style="display: block !important;">
					</div>
					<div class="col-md-6 col-xs-6">
						<div class="row">
							<div class="col-md-12 text-center">
								<h3 style="color: #fff;"><strong>CENTER for ONE HEALTH EDUCATION ADVOCACY RESEARCH and TRAINING</strong></h3>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 text-center">
								<h4>Kerala Veterinary and Animal Sciences University</h4>
								<h4>Pookode, Wayanad Kerala - 673576</h4>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-xs-4">
						<div class="row">
							<div class="col-md-12 text-right">
								<h4 class="man"><i>"For multidisciplinary holistic approach for harmony among Man, Animal and Nature (M.A.N)"</i></h4>
							</div>
						</div>
						<div class="row">
							<div class="col-md-offset-3 col-md-9 text-right google-search">
								<script>
								  (function() {
								    var cx = '001763096455298922418:gkxx4zxqozc';
								    var gcse = document.createElement('script');
								    gcse.type = 'text/javascript';
								    gcse.async = true;
								    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
								        '//www.google.com/cse/cse.js?cx=' + cx;
								    var s = document.getElementsByTagName('script')[0];
								    s.parentNode.insertBefore(gcse, s);
								  })();
								</script>
								<gcse:search></gcse:search>
							</div>
						</div>
					</div>
				</div>
			</div><!--/container-->
		
			<div class='row visible-xs'>
				<div class='col-xs-12 text-center'>
					<a href="index"><img src='dist/images/kvasu-coheart-logo-800X212.png' alt="kvasu logo" width='544' height='126' class='img-responsive'></a>
				</div>
			</div>
			<div class="row visible-xs">
				<div class="col-md-12">
					<script>
					  (function() {
						var cx = '001763096455298922418:gkxx4zxqozc';
						var gcse = document.createElement('script');
						gcse.type = 'text/javascript';
						gcse.async = true;
						gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							'//www.google.com/cse/cse.js?cx=' + cx;
						var s = document.getElementsByTagName('script')[0];
						s.parentNode.insertBefore(gcse, s);
					  })();
					</script>
					<gcse:search></gcse:search>
				</div>
			</div>
		</div><!--/banner-->
		<div class="container" style="width: 100%;">
			<div class="row">
				<div class='news-bar no-margin-bottom' style="border: 0px;">
					<div class='col-md-2 breaking-news'>
						<div class='brk-news'>News Updates</div>
					</div>
					<div class='col-md-10 flash-news malayalam' style="padding-left: 0px; padding-right: 0px;">
						<ul id="webticker">	
						<?php
							require('dbconnect.php');
							$query = mysqli_query($conn,"select * from newsflash order by date");
							if($query)
							while($data = mysqli_fetch_array($query))
							{
								echo "<li><a href='admin/uploads/news-flash/{$data['file']}' target='_blank'>{$data['title']} &nbsp;&nbsp;&nbsp;<span class='glyphicon glyphicon-download-alt'></span></a></li>";
							}
						?>	

		    			</ul>
					</div>
				</div>
			</div>
		</div>
		<nav class="navbar navbar-default visible-xs " role="navigation">
					<div class="container-fluid">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
						  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
						  <a class="navbar-brand" href="#">Menu</a>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li class="active"><a href="index">Home</a></li>
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<li><a href="about-one-health">What is One Health</a></li>
								<li><a href="about-genesis">Genesis of COHEART</a></li>
								<li><a href="about-mandate">Mandate, Vision and Mission of COHEART</a></li>
								<li><a href="about-scope">Scope of One Health</a></li>
								<li><a href="about-objectives">Objectives, Activities and Roap Map</a></li>
								<li><a href="about-faculties">Faculties</a></li>
							  </ul>
						</li>
						
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Courses <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<?php
									require('dbconnect.php');
									$query = mysqli_query($conn,"select id,course_name from courses");
									echo mysqli_error($conn);
									while($data = mysqli_fetch_array($query))
									{
										$course = strtolower($data['course_name']);
										$course = ucwords($course);
										echo "<li><a href='courses?id={$data['id']}'>$course</a></li>";
									}
								?>
							  </ul>
						</li>
						<li><a href="gallery">Gallery</a></li>
						<li><a href="partners">Partnering Institutes and supporters</a></li>
						<li><a href="studentHome">Student Profile</a></li>
						<li><a href="downloads">Downloads</a></li>
						<li><a href="news">News & events</a></li>
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">COHEART Resources <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<li><a href="research-vid">Video Library</a></li>
								<li><a href="body">Human Body</a></li>
								<li><a href="map">COHEART Health Map</a></li>
							  </ul>
						</li>
						<li><a href="involve">Get Involved</a></li>
						<li><a href="contact">Contact Us</a></li>
							</ul>
							
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
		</nav>
		<div class="content">
			<div class='row'>
				<div class='col-md-3 col-sm-4 visible-md visible-lg visible-sm'>
					<div class="panel-group" id="accordion">
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-parent="#accordion" href="index" class="active">
					          Home
					        </a>
					      </h4>
					    </div>
					   </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          About Us
					        </a>

					      </h4>
					    </div>
					    <div id="collapseTwo" class="panel-collapse collapse">
					      <div class="panel-body">
					        <ul>
					        	<li><a href="about-one-health">What is One Health</a></li>
								<li><a href="about-genesis">Genesis of COHEART</a></li>
								<li><a href="about-mandate">Mandate, Vision and Mission of COHEART</a></li>
								<li><a href="about-scope">Scope of One Health</a></li>
								<li><a href="about-objectives">Objectives, Activities and Roap Map</a></li>
								<li><a href="about-faculties">Faculties</a></li>
					        </ul>
					      </div>
					    </div>
					  </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          Courses
					        </a>
					      </h4>
					    </div>
					    <div id="collapseThree" class="panel-collapse collapse">
					      <div class="panel-body">
					        <ul>
					        	<?php
									require('dbconnect.php');
									$query = mysqli_query($conn,"select id,course_name from courses");
									echo mysqli_error($conn);
									while($data = mysqli_fetch_array($query))
									{
										$course = strtolower($data['course_name']);
										$course = ucwords($course);
										echo "<li><a href='courses?id={$data['id']}'>$course</a></li>";
									}
								?>
					        </ul>
					      </div>
					    </div>
					  </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="gallery">Gallery</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="partners">Partnering Institutes and supporters</a>
					      </h4>
					    </div>
					   </div>
					    <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="studentHome">Student Profile</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="downloads">Downloads</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="news">News & events</a>
					      </h4>
					    </div>
					   </div>
					     <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          COHEART Resources
					        </a>

					      </h4>
					    </div>
					    <div id="collapsefour" class="panel-collapse collapse">
					      <div class="panel-body">
					        <ul>
					        	<li><a href="research-vid">Video Library</a></li>
								<li><a href="body">Human Body</a></li>
								<li><a href="map">COHEART Health Map</a></li>
								
							</ul>
					      </div>
					    </div>
					  </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="involve">Get Involved</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="contact">Contact Us</a>
					      </h4>
					    </div>
					   </div>
					</div>
					
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-info panel-custom " style='margin-top: 20px; margin-bottom: 20px;'>
								<!--<div class="panel-heading panel-update">
									<h3 class="panel-title text-center">DE's Message</h3>
								</div>-->
								<div class="panel-body text-justify">
									<div class="de text-center">
										<img src="dist/images/vc.jpg" alt="ramkumar" class="img-rounded"><br>
										<strong>Dr B Ashok</strong><br>
										Vice Chancellor
									</div>
									<br>
									<p>
										"COHEART" is a new initiative of Kerala Veterinary and Animal Sciences University....  
										<a href="vc-message" class="read-more pull-right">Read More...</a> 
									</p>
									
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="">
										<a href="research-vid-bird"  style="text-decoration: none;">
										
										<img src="dist/images/coheart-bird-flue.JPG" align="" class="img-thumbnail img-responsive"/>
										</a>
									</div>
							
						</div>
					</div>
					
					<?php require('news-updates.php');?>
				</div>
				<div class='col-md-9 col-sm-8'>
					<div class='row'>
						<div class="col-md-3 text-center">
							<a target="_blank" href="http://coheart.ac.in/body"><img src="img/links/coheart-ebook.png" class="img-responsive img-circle" style="display:inline-block; border:2px solid #C2D0DA; "></a>
						</div>
						<div class="col-md-3 text-center">
							<a target="_blank" href="http://elearning.coheart.ac.in/"><img src="img/links/coheart-elearning.png" class="img-responsive img-circle" style="display:inline-block; border:2px solid #C2D0DA; "></a>
						</div>
						<div class="col-md-3 text-center">
							<a target="_blank" href="http://foodsafety.coheart.ac.in/"><img src="img/links/coheart-foodsafety.png" class="img-responsive img-circle" style="display:inline-block; border:2px solid #C2D0DA; "></a>
						</div>
						<div class="col-md-3 text-center">
							<a target="_blank" href="http://coheart.ac.in/news"><img src="img/links/coheart-news.png" class="img-responsive img-circle" style="display:inline-block; border:2px solid #C2D0DA; "></a>
						</div>
					</div>
					<br>
					<div class='row'>
						<div class="col-md-3 text-center">
							<a target="_blank" href="http://coheart.ac.in/map"><img src="img/links/coheart-health-map.png" class="img-responsive img-circle" style="display:inline-block; border:2px solid #C2D0DA; "></a>
						</div>
						<div class="col-md-3 text-center">
							<a target="_blank" href="http://coheart.ac.in/downloads"><img src="img/links/coheart-members.png" class="img-responsive img-circle" style="display:inline-block; border:2px solid #C2D0DA; "></a>
						</div>
						<div class="col-md-3 text-center">
							<a target="_blank" href="http://www.coheart.ac.in/research-vid"><img src="img/links/coheart-video.png" class="img-responsive img-circle" style="display:inline-block; border:2px solid #C2D0DA; "></a>
						</div>
						<div class="col-md-3 text-center">
							<a target="_blank" href="http://coheart.ac.in/gallery"><img src="img/links/coheart-gallery.png" class="img-responsive img-circle" style="display:inline-block; border:2px solid #C2D0DA; "></a>
						</div>
					</div>
					<br/>
					<div class='row'>
						<div class='col-md-12'>
							<div class="panel panel-default panel-custom">
								<div class='well text-justify'>
									<p>
										One Health concept is based on the understanding that the health of the humans, animals and the environment is inextricably linked, and that promoting the well being of all species can only be achieved through a holistic multidisciplinary approach at the human-animal-ecosystems interface.  Kerala Veterinary and Animal Sciences University (KVASU) has taken a pioneer step in this regard following the establishment of the Centre for One Health Education Advocacy Research and Training, the first of its kind in India aiming at the sustained health of the community by addressing various issue of concern today like the food safety and security, zoonoses, diseases from natural origins like soil, water and air.  This innovative endeavor is built upon by roping in the emerging concept of One Health and disseminating this concept to the stakeholders involved through various educational means. WHO, OIE, FAO and the UN organizations have in various ways highlighted the alarming dangers that could affect health of humans and animals, and the interdependency of health between humans, plants and animals. These organizations have strongly endorsed the concept of One Health which in a simple manner means, we do not simply exist, but co-exist. 
									</p>
									<a href="">Read More..</a>
									<!--<p>
										Considering this global initiative KVASU was the first to initiate and advocate One Health through a centre called “Centre for One Health Education Advocacy Research and Training” (acronym COHEART). The formation of the centre itself gives a message on the efforts of most, if not all, disciplines, departments or agencies involved in public service to attain “Optimal health for people, animals and the environment”. The Centre has emerged as a need for Institutional responsibility of bringing in different dimensions for the welfare of society which was acknowledged by Dr. Gyanendra Gongal. Dr. Gongal, Scientist of World Health Organization, has appreciated the pioneer effort of University in establishing an institution addressing the future of humanity. Shaping innovative ideas in challenging health issues and communicating them effectively will be the priorities of this center in the years ahead and KVASU will serve society’s changing health needs.  We are building the future health project in present. Here, it is not the overlap of disciplines that is explored, but one holistic approach entangling the diverse aspects of each discipline is concentrated.
									</p>
									<p>
										Foreseeing the co- existence of Human and Animals and in line with this global requirement, the veterinary university has taken the decision to start 2 new courses viz., PG Certificate and PG Diploma in One Health. This is the first of its kind in the country. The centre also plans to focus on cutting edge research using One health tool in collaboration with varied Departments both nationally and globally. The University will soon enter into a co-operative agreement with One Health Centre for Excellence, University of Florida. All the research will focus on the prevention of risks and the mitigation of effects of crises that originate at the interface between humans, animals and their various environments. The research program will be designed to bridge the gap between various areas of animal, plant and human health to improve the wellbeing of all species.
									</p>-->
								
								</div>
							</div>
						</div>
					</div>
					
					<div class="row" style="margin-top: 10px;">
						<!--<div class="col-md-12 text-center">
						<div class="">
							<a href="Post-Graduate-Programme-on-ONE-HEALTH.pdf" target="_blank" style="text-decoration: none;">
							<strong>Post Graduate Programme on ONE HEALTH</strong> &nbsp;&nbsp;<span class="glyphicon glyphicon-save"></span>
							<img src="dist/images/Post-Graduate-Programme-on-ONE-HEALTH-800x193.jpg" align="Post Graduate Programme on ONE HEALTH" class="img-thumbnail img-responsive"/>
							</a>
						</div>
						</div>-->
						
						
						<div class="col-md-12 text-center">
						<div class="">
							<a href="research-video-man-animal-nature"  style="text-decoration: none;">
							
							<img src="dist/images/coheart-man-animal-nature.jpg" align="" class="img-thumbnail img-responsive"/>
							</a>
							<a href="img/coheart-poster-2015-apply-now.jpg" target="_blank" class="text-center"><img src="img/coheart-poster-2015-apply-now-cropped.jpg" class="img-responsive img-thumbnail" style="display: inline-block; margin-top: 20px;"></a>
						</div>
						</div>
						
					</div>
					<div class="row" style="margin-top: 20px;">
						<div class="col-md-4 text-center">
							<a href='http://www.kvasu.ac.in' target='_blank'><img src='dist/images/kvu.png' alt="kvasu logo" class="img-thumbnail img-responsive"></a><br/>
						</div>
						<div class="col-md-4 text-center">
							<a href='http://www.onehealthjournal.org/' target='_blank'><img src='dist/images/International-journal-one-health.png' alt="kvasu logo" class="img-thumbnail img-responsive"></a><br/>
						</div>
						<div class="col-md-4 text-center">
							<a href='http://www.onehealthinitiative.com' target='_blank'><img src='dist/images/ohi.png' alt="one health" class="img-thumbnail img-responsive"></a>
						</div>
						
					</div>
					<div class='row'>
						<div class='col-md-12 visible-xs'>
					<div class='row'>
						<div class='col-md-12 col-sm-12 text-center' style="margin-bottom: 10px;">
							<div class="panel panel-info panel-custom " style='margin-top: 20px; margin-bottom: 20px;'>
								<!--<div class="panel-heading panel-update">
									<h3 class="panel-title text-center">DE's Message</h3>
								</div>-->
								<div class="panel-body text-justify">
									<div class="de text-center">
										<img src="dist/images/vc.jpg" alt="ramkumar" class="img-rounded"><br>
										<strong>Dr B Ashok</strong><br>
										Vice Chancellor
									</div>
									<br>
									<p>
										"COHEART" is a new initiative of Kerala Veterinary and Animal Sciences University.... 
										<a href="vc-message" class="read-more pull-right">Read More...</a> 
									</p>
									
								</div>
							</div>
						</div>
						<div class='col-md-12 col-sm-12 text-center'>
							<div class="panel panel-info panel-custom " style='margin-top: 20px; margin-bottom: 20px;'>
								<!--<div class="panel-heading panel-update">
									<h3 class="panel-title text-center">DE's Message</h3>
								</div>-->
								<div class="panel-body text-justify">
									<div class="de text-center">
										<img src="dist/images/de.jpg" alt="ramkumar" class="img-rounded"><br>
										<strong>Dr. Ramkumar</strong><br>
										Director, Entrepreneurship, KVASU
									</div>
									<br>
									<p>
										“Centre for One Health Education, Advocacy, Research and Training” of the....
										<a href="message" class="read-more pull-right">Read More...</a> 
									</p>
									
								</div>
							</div>
						</div>
					</div>

							<?php require('news-updates.php');?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-50095931-2', 'coheart.ac.in');
	  ga('send', 'pageview');
	</script>
<?php require('footer.php');?>		