<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="coheart, kvasu, veterinary, one, health, university, kerala, animal, science, pookode,diploma,certification,course, education, hygiene,Education, Advocacy, Research, Training "/>
   	<meta name="description" content="One Health concept is based on the understanding that the health of the humans, animals and the environment is inextricably linked, and that promoting the well being of all species can only be achieved through a holistic multidisciplinary approach at the human-animal-ecosystems interface. Kerala Veterinary and Animal Sciences University (KVASU) has taken a pioneer step in this regard following the establishment of the Centre for One Health Education Advocacy Research and Training, the first of its kind in India aiming at the sustained health of the community by addressing various issue of concern today like the food safety and security, zoonoses, diseases from natural origins like soil, water and air."/>
    <meta name="google-site-verification" content="Sg2CCDPE0zio41cI6pF2SkUWVUX1nSGT8waFfR8S6f0" />
    <title>COHEART | Center for One Health Education Advocacy Research and Training</title>
	<link rel="shortcut icon" href="dist/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="dist/images/favicon.ico" type="image/x-icon">
	<!--Slider-->
	<link rel="stylesheet" href="dist/slider/responsiveslides.css">
	<!--flash-->
	<link rel="stylesheet" href="ticker/example/assets/webticker.css" type="text/css" media="screen">
	<!--FancyBox-->
	<link rel="stylesheet" href="dist/lightbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<!-- Bootstrap -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
	<link href="dist/css/style.css" rel="stylesheet">
	<link href="admin/table/css/footable.core.css" rel="stylesheet"/>
	<link href="admin/table/css/footable.metro.css" rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
	<!--<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>-->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	    <!-- add css and js for flipbook -->
    <link type="text/css" href="dist/css/bookstyle.css" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Play:400,700">
    <script src="js/jquery.js"></script>
    <script src="dist/js/turn.js"></script>              
	<script src="dist/js/jquery.fullscreen.js"></script>
    <script src="dist/js/jquery.address-1.6.min.js"></script>
    <script src="dist/js/wait.js"></script>
	<script src="dist/js/onload.js"></script>


    <!-- style css  -->
	<style>	
	    html,body {
          margin: 0;
          padding: 0;
		  overflow:auto !important;
		  
        }
	</style>
	
  </head>
  <body>
    <div class=''>
		<div class='banner'>
			<div class='container'>
				<div class='row hidden-xs'>
					<div class='col-md-1 col-md-offset-1 col-xs-1 col-md-offset-1 text-center'>
						<embed height="150" width="130" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" wmode="transparent" menu="false" quality="high" src="dist/images/coheart-logo.swf" style="display: block !important;">
					</div>
					<div class="col-md-6 col-xs-6">
						<div class="row">
							<div class="col-md-12 text-center">
								<h3 style="color: #fff;"><strong>CENTER for ONE HEALTH EDUCATION ADVOCACY RESEARCH and TRAINING</strong></h3>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 text-center">
								<h4>Kerala Veterinary and Animal Sciences University</h4>
								<h4>Pookode, Wayanad Kerala - 673576</h4>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-xs-4">
						<div class="row">
							<div class="col-md-12 text-right">
								<h4 class="man"><i>"For multidisciplinary holistic approach for harmony among Man, Animal and Nature (M.A.N)"</i></h4>
							</div>
						</div>
						<div class="row">
							<div class="col-md-offset-3 col-md-9 text-right google-search">
								<script>
								  (function() {
								    var cx = '001763096455298922418:gkxx4zxqozc';
								    var gcse = document.createElement('script');
								    gcse.type = 'text/javascript';
								    gcse.async = true;
								    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
								        '//www.google.com/cse/cse.js?cx=' + cx;
								    var s = document.getElementsByTagName('script')[0];
								    s.parentNode.insertBefore(gcse, s);
								  })();
								</script>
								<gcse:search></gcse:search>
							</div>
						</div>
					</div>
				</div>
			</div><!--/container-->
		
			<div class='row visible-xs'>
				<div class='col-xs-12 text-center'>
					<a href="index"><img src='dist/images/kvasu-coheart-logo-800X212.png' alt="kvasu logo" width='544' height='126' class='img-responsive'></a>
				</div>
			</div>
			<div class="row visible-xs">
				<div class="col-md-12">
					<script>
					  (function() {
						var cx = '001763096455298922418:gkxx4zxqozc';
						var gcse = document.createElement('script');
						gcse.type = 'text/javascript';
						gcse.async = true;
						gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							'//www.google.com/cse/cse.js?cx=' + cx;
						var s = document.getElementsByTagName('script')[0];
						s.parentNode.insertBefore(gcse, s);
					  })();
					</script>
					<gcse:search></gcse:search>
				</div>
			</div>
		</div><!--/banner-->
		
		<nav class="navbar navbar-default visible-xs " role="navigation">
					<div class="container-fluid">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
						  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
						  <a class="navbar-brand" href="#">Menu</a>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li class="active"><a href="index">Home</a></li>
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<li><a href="about-one-health">What is One Health</a></li>
								<li><a href="about-genesis">Genesis of COHEART</a></li>
								<li><a href="about-mandate">Mandate, Vision and Mission of COHEART</a></li>
								<li><a href="about-scope">Scope of One Health</a></li>
								<li><a href="about-objectives">Objectives, Activities and Roap Map</a></li>
								<li><a href="about-faculties">Faculties</a></li>
							  </ul>
						</li>
						
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Courses <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<?php
									require('dbconnect.php');
									$query = mysqli_query($conn,"select id,course_name from courses");
									echo mysqli_error($conn);
									while($data = mysqli_fetch_array($query))
									{
										$course = strtolower($data['course_name']);
										$course = ucwords($course);
										echo "<li><a href='courses?id={$data['id']}'>$course</a></li>";
									}
								?>
							  </ul>
						</li>
						<li><a href="gallery">Gallery</a></li>
						<li><a href="partners">Partnering Institutes and supporters</a></li>
						<li><a href="downloads">Downloads</a></li>
						<li><a href="news">News & events</a></li>
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Coheart Resourses<b class="caret"></b></a>
							  <ul class="dropdown-menu">
							<!--	<li><a href="research-vid">Videos</a></li> -->
								<li><a href="body">Human body</a></li>
								<li><a href="map">COHEART Health Map</a></li>
							<!--	<li><a href="magazine/">Magazine</a></li> -->
								<li><a href="broucher">Brouchers</a></li>
							
							  </ul>
						</li>
						<li><a href="involve">Get Involved</a></li>
						<li><a href="contact">Contact Us</a></li>
							</ul>
							
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
		</nav>
		<div class="content">
			<div class='row'>
				<div class='col-md-3 col-sm-4 visible-md visible-lg visible-sm'>

					<div class="panel-group" id="accordion">
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-parent="#accordion" href="index">
					          Home
					        </a>
					      </h4>
					    </div>
					   </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-parent="#accordion" href="about-one-health">
							<span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          About us
					        </a>
					      </h4>
					    </div>
					   </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-parent="#accordion" href="courses?id=2">
							<span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          Courses
					        </a>
					      </h4>
					    </div>
					   </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="gallery">Gallery</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="partners">Partnering Institutes and supporters</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="downloads">Downloads</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="news">News & events</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#"class="active">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          COHEART Resourcs
					        </a>

					      </h4>
					    </div>
					    <div id="collapsefour" class="panel-collapse in">
					      <div class="panel-body">
					        <ul>
					        <!--	<li><a href="research-vid">Videos</a></li> -->
								<li><a href="body">Human body</a></li>
								<li><a href="map">COHEART Health Map</a></li>
							<!--	<li><a href="magazine/">Magazine</a></li> -->
								<li><a href="broucher">Brouchers</a></li>
							</ul>
					      </div>
					    </div>
					  </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="involve">Get Involved</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="contact">Contact Us</a>
					      </h4>
					    </div>
					   </div>
					</div>
					<?php require('news-updates.php');?>
				</div>
				<div class='col-md-9 col-sm-8'>
					
					<div class='row'>
						<div class='col-md-12'>
							<div class="panel panel-default panel-custom">
								<div class='well text-justify'>
									<!-- DIV YOUR WEBSITE --> 
<div style="width:100%;margin:0 auto">
    
 
<!-- BEGIN FLIPBOOK STRUCTURE -->  
<div id="fb5-ajax">	
         
         
      <!-- BEGIN HTML BOOK -->      
      <div data-current="book5" class="fb5" id="fb5">      
            
            <!-- PRELOADER -->
            <div class="fb5-preloader">
            	<div id="wBall_1" class="wBall">
           			 <div class="wInnerBall">
           			 </div>
            	</div>
            	<div id="wBall_2" class="wBall">
            		<div class="wInnerBall">
           			 </div>
            	</div>
            	<div id="wBall_3" class="wBall">
            		<div class="wInnerBall">
            		</div>
            	</div>
            	<div id="wBall_4" class="wBall">
            		<div class="wInnerBall">
					</div>
            	</div>
            	<div id="wBall_5" class="wBall">
            		<div class="wInnerBall">
           			 </div>
            	</div>
            </div>      
      
                           
            <!-- BACKGROUND FOR BOOK -->  
            <div class="fb5-bcg-book"></div>                      
          
            <!-- BEGIN CONTAINER BOOK -->
            <div id="fb5-container-book">
     
                <!-- BEGIN deep linking -->  
                <section id="fb5-deeplinking">
                     <ul>
                          <li data-address="page1" data-page="1"></li>
                          <li data-address="page2-page3" data-page="2"></li>
                          <li data-address="page2-page3" data-page="3"></li>
                          <li data-address="page4-5" data-page="4"></li>
                          <li data-address="page4-5" data-page="5"></li>
                          <li data-address="page6-page7" data-page="6"></li>
                          <li data-address="page6-page7" data-page="7"></li>
                          <li data-address="page8-page9" data-page="8"></li>
                          <li data-address="page8-page9" data-page="9"></li>
                          <li data-address="10-11" data-page="10"></li>
                          <li data-address="10-11" data-page="11"></li>
                          <li data-address="end" data-page="12"></li>
                     </ul>
                 </section>
                <!-- END deep linking -->  
            
                
                <!-- BEGIN ABOUT -->
                <section id="fb5-about">
                
                </section>
                <!-- END ABOUT -->
                
                
                <!-- BEGIN BOOK -->
                <div id="fb5-book">
                
                          
                <!-- BEGIN PAGE 1-->          
                <div data-background-image="img/8.png" class="">
                       
                     <!-- container page book --> 
                     <div class="fb5-cont-page-book">
                       
                                <!-- description for page from WYSWIG --> 
                                <div class="fb5-page-book">
                                                 
                                </div> 
                                          
                                <!-- number page and title for page -->                
                                <div class="fb5-meta">
                                        <span class="fb5-num"></span>
                                        <span class="fb5-description"></span>
                                </div>                 
                        
                      </div> <!-- end container page book --> 
                        
                </div>
                <!-- END PAGE 1 -->
                  
                  
                  
                  
                <!-- BEGIN PAGE 2-->           
                <div data-background-image="" class="">
                       
                     <!-- container page book --> 
                     <div class="fb5-cont-page-book">
                       
                                <!-- description for page from WYSWIG --> 
                                <div class="fb5-page-book">
                                                 
                                </div> 
                                          
                                <!-- number page and title for page -->                
                                <div class="fb5-meta">
                                        <span class="fb5-num"></span>
                                        <span class="fb5-description"></span>
                                </div>                 
                        
                      </div> <!-- end container page book --> 
                        
                </div>
                <!-- END PAGE 2 -->              
                            
                
                
                <!-- BEGIN PAGE 3 -->          
                <div data-background-image="img/1.png" class="">
                       
                     <!-- container page book --> 
                     <div class="fb5-cont-page-book">
                       
                         <!-- description for page --> 
                         <div class="fb5-page-book">
                                         
                         </div> 
                                  
                        <!-- number page and title  -->                
                        <div class="fb5-meta">
                                <span class="fb5-description">COHEART 2014</span>
                                <span class="fb5-num">1</span>
                         </div> 
                        
                        
                     </div> <!-- end container page book --> 
                        
                </div>
                <!-- END PAGE 3 -->           
                                        
                  
                  
                <!-- BEGIN PAGE 4 -->          
                <div data-background-image="img/2.png" class="">
                       
                     <!-- container page book --> 
                     <div class="fb5-cont-page-book">
                       
                         <!-- description for page --> 
                         <div class="fb5-page-book">
                                         
                         </div> 
                                  
                        <!-- number page and title  -->                
                        <div class="fb5-meta">
                                <span class="fb5-description">COHEART 2014</span>
                                <span class="fb5-num">2</span>
                         </div> 
                        
                        
                     </div> <!-- end container page book --> 
                        
                </div>
                <!-- END PAGE 4 -->                
                
                
                
                  
                <!-- BEGIN PAGE 5 -->          
                <div data-background-image="img/3.png" class="">
                       
                     <!-- container page book --> 
                     <div class="fb5-cont-page-book">
                       
                         <!-- description for page --> 
                         <div class="fb5-page-book">
                                         
                         </div> 
                                  
                        <!-- number page and title  -->                
                        <div class="fb5-meta">
                                <span class="fb5-description">COHEART 2014</span>
                                <span class="fb5-num">3</span>
                         </div> 
                        
                        
                     </div> <!-- end container page book --> 
                        
                </div>
                <!-- END PAGE 5 -->
                
                
                
                
                  
                <!-- BEGIN PAGE 6 -->          
                <div data-background-image="img/4.png" class="">
                       
                     <!-- container page book --> 
                     <div class="fb5-cont-page-book">
                       
                                <!-- description for page from WYSWIG --> 
                                <div class="fb5-page-book">
                                                 
                                </div> 
                                          
                                <!-- number page and title for page -->                
                                <div class="fb5-meta">
                                        <span class="fb5-num">4</span>
                                        <span class="fb5-description">COHEART 2014</span>
                                </div>                 
                        
                      </div> <!-- end container page book --> 
                        
                </div>
                <!-- END PAGE 6 -->            
                
                            
                  
                <!-- BEGIN PAGE 7 -->          
                <div data-background-image="img/5.png" class="">
                       
                     <!-- container page book --> 
                     <div class="fb5-cont-page-book">
                       
                             <!-- description for page from WYSWIG --> 
                             <div class="fb5-page-book">
                                    
                                </div> 
                                          
                              <!-- number page and title for page -->                
                              <div class="fb5-meta">
                                      <span class="fb5-description">COHEART 2014</span>
                                      <span class="fb5-num">5</span>
                              </div> 
                        
                        
                      </div> <!-- end container page book --> 
                        
                </div>
                <!-- END PAGE 7-->
                
                
                   
                  
                <!-- BEGIN PAGE 8 -->          
                <div data-background-image="img/6.png" class="">
                       
                     <!-- container page book --> 
                     <div class="fb5-cont-page-book">
                       
                         <!-- description for page  --> 
                        <div class="fb5-page-book">
                         
                            </div>
                            <!-- end description for page  --> 
                                  
                            <!-- number page and title for page -->                
                            <div class="fb5-meta">
                                    <span class="fb5-num">6</span>
                                    <span class="fb5-description">COHEART 2014</span>
                             </div> 
                        
                        
                      </div> <!-- end container page book --> 
                        
                </div>
                <!-- END PAGE 8 -->                      
                  
                  
                  
                <!-- BEGIN PAGE 9 -->          
                <div data-background-image="" class="">
                       
                     <!-- container page book --> 
                     <div class="fb5-cont-page-book">
                       
                             <!-- description for page from WYSWIG --> 
                             <div class="fb5-page-book">
                                             
                             </div> 
                                      
                             <!-- number page and title for page -->                
                             <div class="fb5-meta">
                                    <span class="fb5-description"></span>
                                    <span class="fb5-num"></span>
                             </div>                  
                        
                     </div>
                      <!-- end container page book --> 
                        
                </div>
                <!-- END PAGE 9 -->
                            
                
                  
                
                
                
                  
                <!-- BEGIN PAGE 12 -->          
                <div data-background-image="img/7.png" class="">
                       
                     <!-- container page book --> 
                     <div class="fb5-cont-page-book">
                       
                                <!-- description for page from WYSWIG --> 
                                <div class="fb5-page-book">
                                                 
                                </div> 
                                          
                                <!-- number page and title for page -->                
                                <div class="fb5-meta">
                                        <span class="fb5-num"></span>
                                        <span class="fb5-description"></span>
                                </div>                 
                        
                      </div> <!-- end container page book --> 
                        
                </div>
                <!-- END PAGE 12 -->
                        
                  
              </div>
              <!-- END BOOK -->
                           
                
              <!-- arrows -->
              <a class="fb5-nav-arrow prev"></a>
              <a class="fb5-nav-arrow next"></a>
                
                
             </div>
             <!-- END CONTAINER BOOK -->
   </div>
   <!-- END HTML BOOK -->


    <!-- CONFIGURATION FLIPBOOK -->
    <script>    
    jQuery('#fb5').data('config',
    {
    "page_width":"550",
    "page_height":"715",
	"email_form":"office@somedomain.com",
    "zoom_double_click":"1",
    "zoom_step":"0.06",
    "double_click_enabled":"true",
    "tooltip_visible":"true",
    "toolbar_visible":"true",
    "gotopage_width":"30",
    "deeplinking_enabled":"true",
    "rtl":"false",
    'full_area':'false',
	'lazy_loading_thumbs':'false',
	'lazy_loading_pages':'false'
    })
    </script>


</div>
<!-- END FLIPBOOK STRUCTURE -->    



</div> 
<!-- END DIV YOUR WEBSITE --> 

								
								</div>
							</div>
						</div>
					</div>
					
					
					<div class='row'>
						<div class='col-md-12 visible-xs'>
					<div class='row'>
						<div class='col-md-12 col-sm-12 text-center' style="margin-bottom: 10px;">
							<div class="panel panel-info panel-custom " style='margin-top: 20px; margin-bottom: 20px;'>
								<!--<div class="panel-heading panel-update">
									<h3 class="panel-title text-center">DE's Message</h3>
								</div>-->
							
							</div>
						</div>
						<div class='col-md-12 col-sm-12 text-center'>
							<div class="panel panel-info panel-custom " style='margin-top: 20px; margin-bottom: 20px;'>
								<!--<div class="panel-heading panel-update">
									<h3 class="panel-title text-center">DE's Message</h3>
								</div>-->
								
							</div>
						</div>
					</div>


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	