<?php
session_start();
	if(isset($_POST['save']) && $_POST['save']!="")
	{
		$name = $_POST['name'];
		$phone = $_POST['phone'];
		$hotel = $_POST['hotel'];
		$location = $_POST['location'];
		$bill = $_POST['bill'];
		$complaint = $_POST['complaint'];
		$captcha = $_POST['captcha'];
		$original=$_SESSION['d']; 
		$flag = 0;
		
		require('dbconnect.php');

		if( (preg_match("/^\S(.*\S)?$/", $name)) == 0 )
		{
			echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please enter name</div>";
			$flag = 1;
		}


		if((is_numeric($phone)==0))
		{
			echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please enter a valid phone number! (Only numbers allowed)</div>";
			$flag = 1;
		}

		if($hotel=="")
		{		
			echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please select hotel</div>";
			$flag = 1;
		}

		if($location=="")
		{		
			echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please enter location</div>";
			$flag = 1;
		}

		if( $complaint=="")
		{
			echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please enter your complaint</div>";
			$flag = 1;
		}
		if( $captcha=="" )
		{
			echo "<div class='alert alert-danger alert-dismissable fade in'>
							<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please enter code in image</div>";
			$flag = 1;
		}
		if( $captcha!==$original )
		{
			echo "<div class='alert alert-danger alert-dismissable fade in'>
							<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Captcha code is incorrect</div>";
			$flag = 1;
		}
		
		if($flag == 0)
		{
			date_default_timezone_set("Asia/Kolkata"); 
			mysqli_autocommit($conn, false);
			$time = date("Y-m-d H:i:s");
			if($stmt = mysqli_prepare($conn,"INSERT into complaints(name,phone,hotel,bill,location,complaint,timeofcomp) values(?,?,?,?,?,?,?)"))
			{
				mysqli_stmt_bind_param($stmt,'ssissss',$name,$phone,$hotel,$bill,$location,$complaint,$time);
				echo mysqli_error($conn);
				if(mysqli_stmt_execute($stmt))
					mysqli_commit($conn);
				mysqli_stmt_close($stmt);
				echo "success";
				mysqli_close($conn);
			}
		}
	}
	else
		header("location: index.html");
?>