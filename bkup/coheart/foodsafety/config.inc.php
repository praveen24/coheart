<?php

$db_username 		= 'infoaide_fsadmin'; //database username
$db_password 		= 'fsadmin'; //dataabse password
$db_name 			= 'infoaide_foodsafety'; //database name
$db_host 			= 'localhost'; //hostname or IP
$item_per_page 		= 6; //item to display per page

$mysqli_conn = new mysqli($db_host, $db_username, $db_password,$db_name); //connect to MySql
//Output any connection error
if ($mysqli_conn->connect_error) {
    die('Error : ('. $mysqli_conn->connect_errno .') '. $mysqli_conn->connect_error);
}

?>