<?php
    session_start();
    if(isset($_SESSION['foodsafety_superadmin']) && $_SESSION['foodsafety_superadmin']!="")
    {
?>
<!DOCTYPE html>
<html lang="en-us">
<head>

    <meta charset="utf-8" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Food Safety Mission | Downloads</title>

    <meta name="author" content="abusinesstheme">
    <meta name="description" content="Palas is a Business HTML Template developed with the the latest HTML5 and CSS3 technologies.">

    <!-- CSS files -->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic|Montserrat:400,700' rel='stylesheet'>
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="plugins/elegant_font/html_css/style.css">
    
    <link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
    <link href="autocomplete/magicsuggest-min.css" rel="stylesheet">
    <!-- Main CSS file -->
    <link rel="stylesheet" href="css/style.css">
    <script src="plugins/jquery/jquery-2.1.0.min.js"></script>
    <script type="text/javascript" src="js/ajaxupload.js"></script>
  <script>
    $(function(){
      var btnUpload=$('#browse');
      new AjaxUpload(btnUpload, {
    action: 'upload.php',
    name: 'file',
     onSubmit: function(file, ext){$("#loadgif1").css("display","block");

                 if (! (ext && /^(jpg|jpeg|gif|png)$/.test(ext))){

                    // extension is not allowed
        //document.getElementById("loadgif").style.display='block';
        $("#loadgif1").css("display","none");
               $("#image").css("display","block");
       
        $("#image").html("only jpg,jpeg,png, images are allowed");

                    return false;

               }
    },
    onComplete: function(file, response){ //alert(file+"---"+response);
    $("#hidauto").css('display','block');
    $("#loadgif1").css("display","none");
     $("#image").html("");
    var r=response;
    divid=r.replace(new RegExp(".jpg", 'g'),'');
    divid=divid.replace(new RegExp(".jpeg", 'g'),'');
    divid=divid.replace(new RegExp(".png", 'g'),'');
    divid=divid.replace(new RegExp(".gif", 'g'),'');
    //alert(divid);   
    document.getElementById("imghidall").value=document.getElementById("imghidall").value+response+",";
    shw='<div style="float: left; height:135px; width:147px;" id='+divid+'><img src="uploads/'+response+'" width="125px" height="115px"  /><div style="width: 125px;"><input type="radio" name="checkPrimary"  title="Set as Album cover" value="'+divid+'" onClick="primaryimg('+"'"+r+"'"+');" style="margin:5px"><img src="img/close.png" width="17px" height="17px" title="delete" onClick="delete_image('+"'"+r+"'"+');" width="15px;" height="15px" style="float:right; margin-top:3px;"></div></div>';
           //alert(shw);
           $("#hidauto").append( shw ); 
        
          //location.reload();
    }
  });
      });
$(function(){
        var btnUpload=$('#file');
    //$("#showmessages").html("");
   // $("#hidauto").css('display','block');
    new AjaxUpload(btnUpload, {
        action: 'upload.php',
        name: 'file',
        onSubmit: function(file, ext)
                 {
                   // $("#showmessages").html("");
                   // $("#loadgif1").css("display","block");
                    if (! (ext && /^(jpg|png|gif|pdf|mp4|)$/.test(ext)))
                    {
                    setTimeout(function() {$('#loadgif1').fadeOut('slow');},500);
                    //$("#logo_error").css("display","block");
                   // $("#logo_error").html("Not an allowed extention");
           alert("Not an allowed extention");
                    //setTimeout(function() {$('#image').fadeOut('slow');}, 1000);
                    return false;
                    }
                },
        onComplete: function(file, response)
        {
            //alert(file+"---"+response);
           // alert(response);
           document.getElementById('filename').value=response;
       document.getElementById('primimage').value=response;
            $('#fl_up').html(response);
            
        }
    });
});
function addfile() {
  $("#filediv").css('display','block');
  $("#albumdiv").css('display','none');
  $("#fileh").css('color','#5F9EA0');
  $("#albumh").css('color','#000000');
}
function addalbum() {
  $("#filediv").css('display','none');
  $("#albumdiv").css('display','block');
  $("#albumh").css('color','#5F9EA0');
  $("#fileh").css('color','#000000');
}
function delete_image(name)
  {
  var answer = confirm ("Do you want to delete this image?")
  if (!answer)
        {
        }
  else{
      name=name+",";
      allname=document.getElementById("imghidall").value;
      e=document.getElementById("imghidall").value = allname.replace(new RegExp(name, 'g'),'');
      
      divid=name.replace(new RegExp(".jpg,", 'g'),'');
      divid=divid.replace(new RegExp(".jpeg,", 'g'),'');
      divid=divid.replace(new RegExp(".png,", 'g'),'');
      //alert(divid); 
      document.getElementById(divid).innerHTML="";
      $("#"+divid).css('width','0px');
    }
  }
  function primaryimg(nameprimry)
  {
    document.getElementById("primimage").value=nameprimry;

  }
</script>
</head>
<body>

<?php require('preloader.php'); ?>

<!-- Global Wrapper -->
<div id="wrapper">

    <div class="h-wrapper">

        <!-- Top Bar -->
        <div class="topbar hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <ul class="top-menu">
                            <li><a href="http://coheart.ac.in" target="_blank">COHEART , Kerala Veterinary & Animal Science University</a></li>
                        </ul>
                    </div>
                    <!-- This column is hidden on mobiles -->
                    <div class="col-sm-6">
                        <div class="pull-right top-links">
                            <a href="logout_superadmin"><i class="fa fa-user"></i> &nbsp;Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Header -->
        <header class="header-wrapper header-transparent with-topbar">
            <div class="main-header">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <!-- Logo - Read documentation to see how to change the logo-->
                            <a href="index" class="logo"></a>
                        </div>
                        <div class="col-sm-12 col-md-8">
                            <nav class="navbar-right">
                                <ul class="menu">
                                    <!-- Toggle Menu - For Mobile Devices -->
                                    <li class="toggle-menu"><i class="fa icon_menu"></i></li> 
                                    
                                    <li class="first">
                                        <a href="index">Home</a>
                                    </li> <!-- END Home -->
                                    <li>
                                        <a href="superadmin">Admin Page</a>
                                    </li> <!-- END Admin Page -->
                                    <li class="">
                                        <a href="restaurants">Restaurants</a>
                                    </li> <!-- END Restaurants -->
                                    <li>
                                        <a href="news">News</a>
                                    </li> <!-- END News -->
                                    <li>
                                        <a href="downloads">Downloads</a>
                                    </li> <!-- END Downloads -->
                                    <li>
                                        <a href="contact" class="last">Contact</a>
                                    </li> <!-- END Contact -->
                                    <li class="hidden-sm hidden-md hidden-lg">
                                        <a href="logout_superadmin"><i class="fa fa-user"></i> &nbsp;Logout</a>
                                    </li> <!-- END logout -->
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div> <!-- END Container -->
            </div> <!-- END Main Header -->
        </header>

    </div>

    <!-- Do not remove this class -->
    <div class="push-top"></div>

    <section class="section bg-img bg-admin-panel">
        <div class="bg-overlay op6"></div>
        <div class="container">
            <div class="row mt50 mb40">
                <div class="col-sm-12">
                    <h1 class="title-large title-larger color-on-img mb20 mt50">Admin Panel <span class="fs-small">( Downloads )</span></h1>
                    <div class="br-bottom mb0"></div>
                </div>
            </div>
        </div>
    </section>

    <!--<div class="search-form">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="pull-right">
                        <form action="#" role="form" class="pull-right">
                            <input type="text" class="input-search" placeholder="Search here" id="filter">
                            <button type="submit" class="hidden"></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>-->

    <!-- Tabs -->
  <section class="section">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="content" style="min-height: 480px;" id="file-addition">
            <div class="row">
              <div class="col-md-12">
                <ul class="nav nav-tabs nav-justified">
                    <li class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-file"></i> Add Files</a></li>
                    <li><a href="#tab2" data-toggle="tab"><i class="fa fa-camera"></i>  Add Album</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab1">
                      <form id="form-hotel" enctype="multipart/form-data">
                          <div class="form-vaidation-error1"></div>  
                          <div class="form-group">
                            <label for="subject-hotel">Enter Heading</label>
                            <input type="text" class="form-control" id="subject-hotel" placeholder="Enter Heading" name="subject-hotel">
                          </div>
                          <div class="form-group">
                            <label for="hotels">Enter description</label>
                            <textarea class="form-control" rows="4" name="message-hotel" id="message-hotel" placeholder="description"></textarea>
                          </div>
                          <div class="form-group">
                            <label for="message-hotel">File to upload</label>
                            <input type="file" name="file" id="file" style="color:#FFFFFF;">
                          <input type="hidden" name="filename" id="filename" value="">
                          <input type="hidden" id="primeimage" name="primeimage" value="" />
                          <div id="fl_up"></div>
                          </div>
                          <div class="form-group text-center">
                            <button type="button" class="btn btn-primary" id="button-hotel" value="send" name="send">Add</button>
                          </div>
                      </form>
                    </div>
                    <div class="tab-pane fade" id="tab2">
                      <form id="form-album" enctype="multipart/form-data">
                          <div class="form-vaidation-error2"></div>  
                          <div class="form-group">
                            <label for="subject-hotel">Enter Heading</label>
                            <input type="text" class="form-control" id="subject-album" placeholder="Enter Heading" name="subject-album">
                          </div>
                          <div class="form-group">
                            <label for="hotels">Enter description</label>
                            <textarea class="form-control" rows="4" name="descr-album" id="descr-album" placeholder="description"></textarea>
                          </div>
                          <div class="form-group">
                            <label for="message-hotel">File to upload</label>
                            <input type="file" name="browse" id="browse" style="color:#FFFFFF;">
                              <input type="hidden" name="browsename" id="browsename">
                                        </div>
                              <div id="image" style="float:left; width:110%; margin-top:20px; color:#F00;">
                            
                                  
                        </div> 
                        <div id="hidauto">
                                  
                                  </div>
                       <input type="hidden" id="primimage" name="primimage" value="" />
                      <input type="hidden" value="" id="imghid" name="imghid">
                          <input type="hidden" value="" id="imghidall" name="imghidall">
                          <div class="form-group text-center">
                            <button type="button" class="btn btn-primary" id="button-save" value="send" name="save">save</button>
                          </div>
                        </form>
                    </div>
                  
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </section>

    <?php require('footer.php');?>
    
</div> <!-- END Global Wrapper -->




    <!-- Javascript files -->
    <script src="plugins/jquery/jquery-2.1.0.min.js"></script>
    <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/jquery.appear.js"></script>
    <script src="plugins/retina.min.js"></script>
    <script src="plugins/stellar.min.js"></script>
    <script src="plugins/sticky.min.js"></script>
    
    <script src="plugins/sharrre/jquery.sharrre.min.js"></script>
    <script src="plugins/owl-carousel/owl.carousel.min.js"></script>
    <script src="plugins/jquery.zoom.min.js"></script>
    <script src="plugins/raty/jquery.raty.min.js"></script>
    <script src="autocomplete/magicsuggest-min.js"></script>
    
    <!-- Main javascript file -->
    <script src="js/script.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        var hotel = "";
      var ms1 = $('#magicsuggest').magicSuggest({
            data: 'ajax_suggest1.php',
            allowFreeEntries: false
        });
        $(ms1).on('collapse', function(){
            hotel = ms1.getValue();
        });
        $('#button-hotel').click(function()
        {   
            $(this).text('Sending').attr('disabled',true);
            var button = $(this).val();
            var subjectHotel = $('#subject-hotel').val();
            var messageHotel = $('#message-hotel').val();
      var filename = $('#filename').val();
            var primeimage=$('#primeimage').val();
            $.ajax(
              {
                url: 'addfileajax.php',
                type: "POST",
                data: {button:button,subject:subjectHotel,message:messageHotel,filename:filename,primeimage:primeimage},
                success: function(data,status)
                {
                    if(data=="success")
                    {
                        $('#button-hotel').text('Add').attr('disabled',false);
                        $('form#form-hotel') [0].reset();
                        $('.form-vaidation-error1').hide();
                        $('.form-vaidation-error1').html("<div class='alert alert-success' role='alert'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span>  Added!</div>");
                        $('.form-vaidation-error1').fadeIn();
             $('#fl_up').html("");
                        setTimeout(function(){ $('.form-vaidation-error1').fadeOut(); }, 3000);
                        ms1.clear();
                    }
                    else
                    {
                        $('#button-hotel').text('Add').attr('disabled',false);
                        $('.form-vaidation-error1').hide();
                        $('.form-vaidation-error1').html(data);
                        $('.form-vaidation-error1').fadeIn();
                    }
                }
            });
        });
    
    $('#button-save').click(function()
        {   
            $(this).text('Saving').attr('disabled',true);
            var button = $(this).val();
            var subjectAlbum = $('#subject-album').val();
            var descrAlbum = $('#descr-album').val();
      var imghidall = $('#imghidall').val();
       var primimage=$('#primimage').val();
            $.ajax(
              {
                url: 'addfileajax.php',
                type: "POST",
                data: {button:button,subject:subjectAlbum,message:descrAlbum,filename:imghidall,primeimage:primimage},
                success: function(data,status)
                {
                    if(data=="success")
                    {
                        $('#button-save').text('Save').attr('disabled',false);
                        $('form#form-album') [0].reset();
                        $('.form-vaidation-error2').hide();
                        $('.form-vaidation-error2').html("<div class='alert alert-success' role='alert'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span>  Added!</div>");
                        $('.form-vaidation-error2').fadeIn();
             $('#hidauto').html("");
                        setTimeout(function(){ $('.form-vaidation-error2').fadeOut(); }, 3000);
                        ms1.clear();
                    }
                    else
                    {
                        $('#button-save').text('Save').attr('disabled',false);
                        $('.form-vaidation-error2').hide();
                        $('.form-vaidation-error2').html(data);
                        $('.form-vaidation-error2').fadeIn();
                    }
                }
            });
        });
       
    });
    </script>

</body>
</html>
<?php
    }
    else
        header("location: index");
?>