<?php
	if(isset($_POST['save']) && $_POST['save']!="")
	{
		function isEmail($email)
		{
		    return(preg_match("/^[-_.[:alnum:]]+@((([[:alnum:]]|[[:alnum:]][[:alnum:]-]*[[:alnum:]])\.)+(ad|ae|aero|af|ag|ai|al|am|an|ao|aq|ar|arpa|as|at|au|aw|az|ba|bb|bd|be|bf|bg|bh|bi|biz|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|com|coop|cr|cs|cu|cv|cx|cy|cz|de|dj|dk|dm|do|dz|ec|edu|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gh|gi|gl|gm|gn|gov|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|in|info|int|io|iq|ir|is|it|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mil|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|museum|mv|mw|mx|my|mz|na|name|nc|ne|net|nf|ng|ni|nl|no|np|nr|nt|nu|nz|om|org|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|pro|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)$|(([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5])\.){3}([0-9][0-9]?|[0-1][0-9][0-9]|[2][0-4][0-9]|[2][5][0-5]))$/i", $email));
		}

		$name = $_POST['name'];
		$senderEmail = $_POST['email']; 
		$phone = $_POST['phone'];
		$location = $_POST['location'];
		$flag = 0;
		
		$conn = mysqli_connect("localhost", "infoaide_fsadmin", "fsadmin", "infoaide_foodsafety");

		if( (preg_match("/^\S(.*\S)?$/", $name)) == 0 )
		{
			echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please enter name</div>";
			$flag = 1;
		}

		if( (preg_match("/^\S(.*\S)?$/", $location)) == 0 )
		{
			echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please enter your location</div>";
			$flag = 1;
		}

		if(isEmail($senderEmail) == 0)
		{
			echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please enter a valid email address</div>";
			$flag = 1;
		}
		else
		{
			$query = mysqli_query($conn, "SELECT id from inspectors where email='$senderEmail'");
			if($data = mysqli_fetch_array($query))
			{
				echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Email address already exists</div>";
				$flag = 1;
			}
		}

		if((is_numeric($phone)==0))
		{
			echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please enter a valid phone number! (Only numbers allowed)</div>";
			$flag = 1;
		}
		else
		{
			$query = mysqli_query($conn, "SELECT id from inspectors where phone='$phone'");
			if($data = mysqli_fetch_array($query))
			{
				echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Phone number already exists</div>";
				$flag = 1;
			}
		}
		if($flag == 0)
		{
			date_default_timezone_set("Asia/Kolkata"); 
			mysqli_autocommit($conn, false);
			$time = date("Y-m-d H:i:s");
			if($stmt = mysqli_prepare($conn,"INSERT into inspectors(name,location,phone,email,dateofadd) values(?,?,?,?,?)"))
			{
				mysqli_stmt_bind_param($stmt,'sssss',$name,$location,$phone,$senderEmail,$time);
				if(mysqli_stmt_execute($stmt))
					mysqli_commit($conn);
				mysqli_stmt_close($stmt);
				echo "success";
				mysqli_close($conn);
			}
		}
	}
	else
		header("location: index.html");
?>