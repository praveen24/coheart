$(window).load(function()
	{
		var search = "";
		var page = ""
		var old =""
		
        $("#results" ).load( "fetch_reg_pages.php",function(){
            $("#results").hide();
            $('.remove-style *').removeAttr("style");
            $("#results").fadeIn();
            old = $('#results').html();
            $(".news-content h3").remove();
        });
        
        $("#results").on( "click", ".pagination a", function (e){
            e.preventDefault();
            $("#results").hide();
            $("#results").html('<div class="text-center text-muted">Loading...</div>');
            $("#results").fadeIn();
            var page = $(this).attr("data-page");
            $("#results").load("fetch_reg_pages.php",{"page":page,"search":search}, function(){
            	//$("#results").hide();
                $('.remove-style *').removeAttr("style");
                $("#results").fadeIn();
                //$('html, body').animate({scrollTop: "0px"}, 800);

            });
        });
        
        $('#filter').keyup(function(){
            search = $(this).val();
            $("#results").html('<div class="text-center text-muted">Loading...</div>');
            //$("#results").fadeIn();
            if(search != "")
            {
                $("#results").load("fetch_reg_pages.php",{"search":search}, function(){
                    $('.remove-style *').removeAttr("style");
                	//$("#results").hide();
                	$("#results").fadeIn();
                });
            }
            else
            {
            	//$("#results").hide();
                $('#results').html(old);
                $('.remove-style *').removeAttr("style");
                $("#results").fadeIn();
            }
        });
	});