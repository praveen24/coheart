$(document).ready(function(){

    var page = "";
    var old ="";
    var location = [];
    var types = "";
    var searchName ="";
    var searchLocation ="";

    function removeItem(array, item){
            for(var i in array){
                if(array[i]==item){
                    array.splice(i,1);
                    break;
                    }
            }
        }

    function callRaty(){
        $(".rating-system.rate-product").raty({
                starOn:"plugins/raty/img/star-on.png",
                starOff:"plugins/raty/img/star-off.png",
                starHalf:"plugins/raty/img/star-half.png",
                cancelOn:"plugins/raty/img/cancel-on.png",
                cancelOff:"plugins/raty/img/cancel-off.png",
                number:5,
                readOnly:true,
                score: function() {
                    return $(this).attr('data-score');
                  }
            }); 
    }
    // Initial fetching
    
    $("#results" ).load( "fetch-restaurants.php",{"location":location,"type":types},function(){
          $("#results").fadeIn();
          old = $('#results').html();
          callRaty();  
    });

    //Pagination
    $("#results").on( "click", ".pagination a", function (e){
            e.preventDefault();
            $("#results").hide();
            $("#results").html('<div class="text-center text-muted">Loading...</div>');
            $("#results").fadeIn();
            var page = $(this).attr("data-page");
            $("#results").load("fetch-restaurants.php",{"page":page,"searchName":searchName,"searchLocation":searchLocation,"location":location,"type":types}, function(){
                $("#results").hide();
                $("#results").fadeIn();
                $('html, body').animate({scrollTop: "400px"}, 800);
                callRaty(); 
            });
    });

    //filter by location
    $('#filter-location li').click(function()
        {
            $('#search-name, #search-location').val("");
            searchName = searchLocation = "";
            var clas = $(this).attr('class');
            if(clas == 'todo-done'){
                $(this).removeClass('todo-done');
                removeItem(location, $(this).text());
            }
            else{
                $(this).addClass('todo-done');
                location.push($(this).text());
            }

            if((types=="") && (location.length == 0))
            {
                $('#results').html(old);
                callRaty();
            }
            else
            {
                $("#results").hide();
                $("#results").html('<div class="text-center text-muted">Loading...</div>');
                $("#results").fadeIn();

                $("#results").load("fetch-restaurants.php",{"location":location,"type":types}, function(){
                    $("#results").hide();
                    $("#results").fadeIn();
                    //$('html, body').animate({scrollTop: "0px"}, 800);
                    callRaty(); 
                });
            }
        });
    
    //filter by type
    $('#filter-category li').click(function()
        {
            $('#search-name, #search-location').val("");
            searchName = searchLocation = "";
            var clas = $(this).attr('class');
            if(clas == 'todo-done'){
                $('#filter-category .todo-done').removeClass('todo-done');
                types = "";
            }
            else{
                $('#filter-category .todo-done').removeClass('todo-done');
                $(this).addClass('todo-done');
                types = ($(this).text());
            }

            if((types=="") && (location.length == 0))
            {
                $('#results').html(old);
                callRaty();
            }
            else
            {
                $("#results").hide();
                $("#results").html('<div class="text-center text-muted">Loading...</div>');
                $("#results").fadeIn();

                $("#results").load("fetch-restaurants.php",{"location":location,"type":types}, function(){
                    $("#results").hide();
                    $("#results").fadeIn();
                    //$('html, body').animate({scrollTop: "0px"}, 800);
                    callRaty(); 
                });
            }
        });

    //Search by name
    $('#search-name').keyup(function(){
            searchName = $(this).val();
            $('#filter-location li, #filter-category li').removeClass('todo-done');
            location = [];
            types = "";
            if(searchName != "")
            {
                $("#results").hide();
                $("#results").html('<div class="text-center text-muted">Loading...</div>');
                $("#results").fadeIn();

                $("#results").load("fetch-restaurants.php",{"searchName":searchName,"searchLocation":searchLocation}, function(){
                    $("#results").hide();
                    $("#results").fadeIn();
                    //$('html, body').animate({scrollTop: "0px"}, 800);
                    callRaty(); 
                });
            }
            else
            {
                if(searchLocation != "")
                      $('#search-location').trigger('keyup');
                else  
                    $('#results').html(old);
                callRaty();
            }
    });
    $('#search-location').keyup(function(){
            searchLocation = $(this).val();
            $('#filter-location li, #filter-category li').removeClass('todo-done');
            location = [];
            types = "";
            if(searchLocation != "")
            {
                $("#results").hide();
                $("#results").html('<div class="text-center text-muted">Loading...</div>');
                $("#results").fadeIn();

                $("#results").load("fetch-restaurants.php",{"searchName":searchName,"searchLocation":searchLocation}, function(){
                    $("#results").hide();
                    $("#results").fadeIn();
                    //$('html, body').animate({scrollTop: "0px"}, 800);
                    callRaty(); 
                });
            }
            else
            {
                if(searchName != "")
                {
                      $('#search-name').trigger('keyup');
                }
                else  
                    $('#results').html(old);
                callRaty();
            }
    });

        
    
});//end of ready