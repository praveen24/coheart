$(window).load(function()
	{
		var search = "";
		var page = ""
		var old =""
		
        $("#news-results" ).load( "fetch_news.php",function(){
            $("#news-results").hide();
            $('.remove-style *').removeAttr("style");
            $("#news-results").fadeIn();
            old = $('#news-results').html();
            $(".news-content h3").remove();
        });
        
        $("#news-results").on( "click", ".pagination a", function (e){
            e.preventDefault();
            $("#news-results").hide();
            $("#news-results").html('<div class="text-center text-muted">Loading...</div>');
            $("#news-results").fadeIn();
            var page = $(this).attr("data-page");
            $("#news-results").load("fetch_news.php",{"page":page,"search":search}, function(){
            	//$("#news-results").hide();
                $('.remove-style *').removeAttr("style");
                $(".news-content h3").remove();
                $("#news-results").fadeIn();
                //$('html, body').animate({scrollTop: "0px"}, 800);

            });
        });
        
        $('#filter').keyup(function(){
            search = $(this).val();
            $("#news-results").html('<div class="text-center text-muted">Loading...</div>');
            //$("#news-results").fadeIn();
            if(search != "")
            {
                $("#news-results").load("fetch_news.php",{"search":search}, function(){
                    $('.remove-style *').removeAttr("style");
                	$("#news-results").hide();
                    $(".news-content h3").remove();
                	$("#news-results").fadeIn();

                });
            }
            else
            {
            	$("#news-results").hide();
                $('#news-results').html(old);
                $('.remove-style *').removeAttr("style");
                $(".news-content h3").remove();
                $("#news-results").fadeIn();
            }
        });
	});