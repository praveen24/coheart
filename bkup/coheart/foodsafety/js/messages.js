$(document).ready(function(){
        var hotel = "";
        var inspector = "";
        var ms1 = $('#magicsuggest').magicSuggest({
            data: 'ajax_suggest1.php',
            allowFreeEntries: false
        });
        var ms2 = $('#magicsuggest2').magicSuggest({
            data: 'ajax_suggest2.php',
            allowFreeEntries: false
        });
        $(document).on('input[type=text]').addClass('form-control');
        $(ms1).on('collapse', function(){
            hotel = ms1.getValue();
        });
        $(ms2).on('collapse', function(){
            inspector = ms2.getValue();
        });
        $('#button-hotel').click(function()
        {   
            $(this).text('Sending').attr('disabled',true);
            var button = $(this).val();
            var subjectHotel = $('#subject-hotel').val();
            var messageHotel = $('#message-hotel').val();
            
            $.ajax(
              {
                url: 'send-message-hotel.php',
                type: "POST",
                data: {button:button,subject:subjectHotel,message:messageHotel,hotels:hotel},
                success: function(data,status)
                {
                    if(data=="success")
                    {
                        $('#button-hotel').text('Send Message').attr('disabled',false);
                        $('form#form-hotel') [0].reset();
                        $('.form-vaidation-error1').hide();
                        $('.form-vaidation-error1').html("<div class='alert alert-success' role='alert'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span>  Message sent!</div>");
                        $('.form-vaidation-error1').slideDown();
                        setTimeout(function(){ $('.form-vaidation-error1').fadeOut(); }, 3000);
                        ms1.clear();
                    }
                    else
                    {
                        $('#button-hotel').text('Send Message').attr('disabled',false);
                        $('.form-vaidation-error1').hide();
                        $('.form-vaidation-error1').html(data);
                        $('.form-vaidation-error1').slideDown();
                    }
                }
            });
        });
        $('#button-inspector').click(function()
        {   
            $(this).text('Sending').attr('disabled',true);
            var button = $(this).val();
            var subjectHotel = $('#subject-inspector').val();
            var messageHotel = $('#message-inspector').val();
            
            $.ajax(
              {
                url: 'send-message-inspector.php',
                type: "POST",
                data: {button:button,subject:subjectHotel,message:messageHotel,inspectors:inspector},
                success: function(data,status)
                {
                    if(data=="success")
                    {
                        $('#button-inspector').text('Send Message').attr('disabled',false);
                        $('form#form-inspector') [0].reset();
                        $('.form-vaidation-error2').hide();
                        $('.form-vaidation-error2').html("<div class='alert alert-success' role='alert'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span>  Message sent!</div>");
                        $('.form-vaidation-error2').slideDown();
                        setTimeout(function(){ $('.form-vaidation-error2').fadeOut(); }, 3000);
                        ms2.clear();
                    }
                    else
                    {
                        $('#button-inspector').text('Send Message').attr('disabled',false);
                        $('.form-vaidation-error2').hide();
                        $('.form-vaidation-error2').html(data);
                        $('.form-vaidation-error2').slideDown();
                    }
                }
            });
        });
    });