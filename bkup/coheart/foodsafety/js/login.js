$('#myButton').on('click',function(){
    		var btn = $(this).button('loading');
    		var username = $('#username').val();
    		var password = $('#password').val();
    		var button = $(this).val();

    		$.ajax(
	          {
	            url: 'login-submit.php',
	            type: "POST",
	            data: {username:username, password:password, login:button},
	            success: function(data,status)
	            {
	              if(data == "superadmin")              
	              {
	                window.location = "superadmin";
	              }
                  else if(data == "hoteladmin")
                  {
                    window.location = "hoteladmin";
                  }
                  else if(data == "inspector")
                  {
                    window.location = "inspector";
                  }
                  else
                  {
                    $('.form-vaidation-error').hide();
                    $('.form-vaidation-error').html(data);
                    $('.form-vaidation-error').fadeIn();
                    $('#myButton').button('reset');
                  }
	            }
	          });
    	});