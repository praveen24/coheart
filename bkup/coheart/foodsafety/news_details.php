<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en-us">
<head>

	<meta charset="utf-8" >
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Food Safety Mission | News Details</title>

	<meta name="author" content="abusinesstheme">
	<meta name="description" content="Palas is a Business HTML Template developed with the the latest HTML5 and CSS3 technologies.">

  	<!-- CSS files -->
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic|Montserrat:400,700' rel='stylesheet'>
	<link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="plugins/elegant_font/html_css/style.css">
	
	<link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
	
	<!-- Main CSS file -->
	<link rel="stylesheet" href="css/style.css">
	
</head>
<body>

<?php require('preloader.php'); ?>

<!-- Global Wrapper -->
<div id="wrapper">

	<div class="h-wrapper">

		<!-- Top Bar -->
		<div class="topbar hidden-xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<ul class="top-menu">
							<li><a href="http://coheart.ac.in" target="_blank">COHEART , Kerala Veterinary & Animal Science University</a></li>
						</ul>
					</div>
					<!-- This column is hidden on mobiles -->
					<div class="col-sm-6">
						<div class="pull-right top-links">
						<!-- login check -->
						<?php include('login_check1.php'); ?>
						<!--/ login check -->
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Header -->
		<header class="header-wrapper header-transparent with-topbar">
			<div class="main-header">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-4">
							<!-- Logo - Read documentation to see how to change the logo-->
							<a href="index" class="logo"></a>
						</div>
						<div class="col-sm-12 col-md-8">
							<nav class="navbar-right">
								<ul class="menu">
									<!-- Toggle Menu - For Mobile Devices -->
									<li class="toggle-menu"><i class="fa icon_menu"></i></li> 
									
									<li class="first">
										<a href="index">Home</a>
									</li> <!-- END Home -->
									<!--login check -->
									<?php include('login_check2.php'); ?>
									<!--/ login check -->
									<li class="">
										<a href="restaurants">Restaurants</a>
									</li> <!-- END Restaurants -->
									<li class="active">
										<a href="news">News</a>
									</li> <!-- END News -->
									<li>
										<a href="downloads">Downloads</a>
									</li> <!-- END Downloads -->
									<li>
										<a href="contact" class="last">Contact</a>
									</li> <!-- END Contact -->
									
								</ul>
							</nav>
						</div>
					</div>
				</div> <!-- END Container -->
			</div> <!-- END Main Header -->
		</header>

	</div>

	<!-- Do not remove this class -->
	<div class="push-top"></div>

	<section class="section-intro bg-img bg18">
		<div class="bg-overlay op6"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-8">
					<h1 class="intro-title mb20">News & Alerts</h1>
					<p class="intro-p mb20">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit tenetur reiciendis molestias nostrum excepturi porro dolorum amet!</p>
				</div>
			</div>
		</div>
	</section>

	<div class="page-breadcrumbs-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="pull-right">
						<div class="page-breadcrumbs">
							15 march <span class="separator"> / </span> 44 comments <span class="separator"> / </span> 125 shares
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<section class="section-page">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-9 col-md-push-3 space-left push-off">
					<div class="row">
						<div class="col-sm-12">
							<article class="blog-post-content remove-style">
								<?php
								$id=$_GET['id'];
								require("dbconnect.php");
								if(mysqli_connect_errno())
									die("Could not connect: ".mysqli_connect_error());
							
								$query = "SELECT title,news,added FROM news WHERE id=$id";
								$stmt = mysqli_prepare($conn, $query);
								mysqli_execute($stmt);
								mysqli_stmt_bind_result($stmt, $title, $news, $time);
								
								$flag = 0;
								date_default_timezone_set('Asia/Kolkata');

								while(($res = mysqli_stmt_fetch($stmt)))
								{
									$time = date_create($time);
									$date = date_format($time, "F j, Y, g:i a");
									//$date = date($time, "F j, Y, g:i a");
									
							        echo '<h3 class="post-title">'.$title.'</h3>';
									echo '<div class="sub-post-title">
																	<span>'.$date.'</span>
																</div>';
							        echo "<p>".$news."<p>";
							    }
							    ?>
								
							</article>
						</div>
					</div>
					<!--<div class="row blog-item mb70">
						<div class="col-sm-12 blog-caption">
							<div class="media">
							    <div class="pull-left hidden-xs">
							        <img class="media-object author-img" src="images/demo/team1.jpg" alt="author">
							    </div>
							    <div class="media-body">
							        <h4 class="media-heading mt10"><a href="#" class="user blog-author">John Doe</a></h4>
							        <p class="mb10">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, blanditiis, corporis odit ducimus sed nihil impedit vitae eius amet ut ea facilis sequi minima nam beatae! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic consequuntur beatae, minus ipsa non odit atque.</p>
							        <div class="pull-right author-post-social">
							        	<ul class="social-icon simple">
										 	<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
											<li><a href="#"><i class="fa fa-behance"></i></a></li>
										</ul>
							        </div>
							    </div>
							</div>	
						</div>
					</div>-->
					<div class="row mb70">
						<div class="col-sm-12">
							<h2 class="title-medium br-bottom mb30">Leave a comment - jino implement this one</h2>
							<div class="row">
								<div class="col-sm-12">
									<form class="form" method="post" action="#">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<div class="form-icon icon-user">
								 						<input type="text" name="user_name" required class="form-control" placeholder=" Full Name">
								 					</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<div class="form-icon icon-email">
								 						<input type="email" name="user_email" required class="form-control" placeholder=" Email Address">
								 					</div>
												</div>
											</div>
										</div>
										<label>
						 					<textarea name="message" id="message" cols="30" rows="10" required class="form-control" placeholder=" Message"></textarea>
						 				</label>
						 				<div class="mt30 clearfix">
								 			<button type="submit" class="btn btn-d rounded">Submit</button>
								 		</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<h2 class="title-medium br-bottom mb40">Comments</h2>
							<ul class="media-list">
								<li class="media">
									<div class="pull-left">
										<img src="images/demo/team1.jpg" alt="author" class="media-object">
									</div>
									<div class="media-body">
										<h4 class="media-heading"><a href="#" class="user">John Doe</a>
											<span class="date">Jan 11, 2015</span>
											<span class="reply"><a href="#">Reply</a></span>
										</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos, magni, impedit, accusantium officia mollitia optio repellendus culpa pariatur temporibus rem nostrum sapiente dignissimos sunt ex in ea possimus corrupti quo.</p>

										<!-- New media -->
										<div class="media">
											<div class="pull-left">
												<img src="images/demo/team2.jpg" alt="author" class="media-object ">
											</div>
											<div class="media-body">
												<h4 class="media-heading"><a href="#" class="user">Sophia Doe</a>
													<span class="date">Jan 11, 2015</span>
													<span class="reply"><a href="#">Reply</a></span>
												</h4>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos, magni, impedit, accusantium officia mollitia optio repellendus culpa pariatur temporibus rem nostrum sapiente dignissimos sunt ex in ea possimus corrupti quo.</p>
											</div> <!-- END Media Body -->
										</div> <!-- END Media -->

										<!-- New media -->
										<div class="media">
											<div class="pull-left">
												<img src="images/demo/team3.jpg" alt="author" class="media-object">
											</div>
											<div class="media-body">
												<h4 class="media-heading"><a href="#" class="user">Jack Doe</a>
													<span class="date">Jan 18, 2015</span>
													<span class="reply"><a href="#">Reply</a></span>
												</h4>
												<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos, magni, impedit, accusantium officia mollitia optio repellendus culpa pariatur temporibus rem nostrum sapiente dignissimos sunt ex in ea possimus corrupti quo.</p>
											</div> <!-- END Media Body -->
										</div> <!-- END Media -->
										
									</div> <!-- END Media Body -->
								</li>
								<li class="media">
									<div class="pull-left">
										<img src="images/demo/team4.jpg" alt="author" class="media-object">
									</div>
									<div class="media-body">
										<h4 class="media-heading"><a href="#" class="user">Justin Doe</a>
											<span class="date">Jan 21, 2015</span>
											<span class="reply"><a href="#">Reply</a></span>
										</h4>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos, magni, impedit, accusantium officia mollitia optio repellendus culpa pariatur temporibus rem nostrum sapiente dignissimos sunt ex in ea possimus corrupti quo.</p>
									</div>
								</li> <!-- END Media -->
							</ul>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-3 col-md-pull-9 pull-off">
					<aside class="sidebar sidebar-blog-post">
						<div class="sidebar-widget">
							<h3 class="sidebar-title br-bottom">Above and beyond</h3>
							<p>Lorem ipsum dolor sit amet, consect adipisicing elit. Impedit, recusandae corrupti voluptas atque voluptatibust!</p>
						</div>
						<div class="sidebar-widget">
							<h3 class="sidebar-title br-bottom">Get social</h3>
							<ul class="sidebar-socials">
								<li><a href="#"><i class="fa fa-twitter"></i> Follow us</a> <span>455 followers</span></li>
								<li><a href="#"><i class="fa fa-facebook"></i> Like us</a> <span>505 likes</span></li>
								<li><a href="#"><i class="fa fa-google-plus"></i> Circle us</a> <span>355 friends</span></li>
								<li><a href="#"><i class="fa fa-envelope"></i> Subscribe</a> <span>250 subscribers</span></li>
							</ul>
						</div>
						<!--<div class="sidebar-widget">
							<h3 class="sidebar-title br-bottom">Latest Tweets</h3>
							<div class="sidebar-tweet clearfix mb0">
								<i class="fa fa-twitter"></i>
								<p class="tweet-content"><a href="#" class="tweet-user">@abusinesstheme</a> <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span> <small>22 hours ago</small></p>
							</div>
						</div>-->
						<div class="sidebar-widget">
							<h3 class="sidebar-title br-bottom">Related blog posts</h3>
							<ul class="sidebar-posts">
								<li class="sidebar-post">
									<div class="image-post">
										<img src="images/demo/p03.jpg" alt="blog post">
										<div class="image-post-date">25 <br> jan</div>
									</div>
									<div class="info-post">
										<h5><a href="blog_single_left.html">As above so below</a></h5>
										<span>by John Doe</span>
									</div>
								</li>
								<li class="sidebar-post">
									<div class="image-post">
										<img src="images/demo/p04.jpg" alt="blog post">
										<div class="image-post-date">20 <br> feb</div>
									</div>
									<div class="info-post">
										<h5><a href="blog_single_left.html">Above and beyond</a></h5>
										<span>by Mark Doe</span>
									</div>
								</li>
								<li class="sidebar-post">
									<div class="image-post">
										<img src="images/demo/p05.jpg" alt="blog post">
										<div class="image-post-date">27 <br> feb</div>
									</div>
									<div class="info-post">
										<h5><a href="blog_single_left.html">Imagine Dragons</a></h5>
										<span>by Justin Doe</span>
									</div>
								</li>
								<li class="sidebar-post">
									<div class="image-post">
										<img src="images/demo/p01.jpg" alt="blog post">
										<div class="image-post-date">27 <br> feb</div>
									</div>
									<div class="info-post">
										<h5><a href="blog_single_left.html">If you say so</a></h5>
										<span>by Alex Doe</span>
									</div>
								</li>
							</ul>
						</div>
					</aside>
				</div>
			</div>
		</div>
	</section> <!-- END Blog Page-->

	<?php require('footer.php');?>
	
</div> <!-- END Global Wrapper -->




	<!-- Javascript files -->
	<script src="plugins/jquery/jquery-2.1.0.min.js"></script>
	<script src="plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="plugins/jquery.appear.js"></script>
	<script src="plugins/retina.min.js"></script>
	<script src="plugins/stellar.min.js"></script>
	<script src="plugins/sticky.min.js"></script>
	
	<script src="plugins/sharrre/jquery.sharrre.min.js"></script>
	<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="plugins/jquery.zoom.min.js"></script>
	<script src="plugins/raty/jquery.raty.min.js"></script>
	
	


	<!-- Main javascript file -->
	<script src="js/script.js"></script>
	

</body>
</html>