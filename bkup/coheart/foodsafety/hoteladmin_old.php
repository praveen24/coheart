<?php
   session_start();
    if(isset($_SESSION['foodsafety_hoteladmin']) && $_SESSION['foodsafety_hoteladmin']!="")
    {
        $id = $_SESSION['foodsafety_hoteladmin']['id'];
        $conn = mysqli_connect("localhost", "infoaide_fsadmin", "fsadmin", "infoaide_foodsafety");
        $query = mysqli_query($conn, "SELECT * from registrations where id=$id");
        $data = mysqli_fetch_assoc($query);
?>
<!DOCTYPE html>
<html lang="en-us">
<head>

    <meta charset="utf-8" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo ucwords($_SESSION['foodsafety_hoteladmin']['name']); ?> - Admin Page</title>

    <!-- CSS files -->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic|Montserrat:400,700' rel='stylesheet'>
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="plugins/elegant_font/html_css/style.css">
    
    <link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
    <link href="autocomplete/magicsuggest-min.css" rel="stylesheet">
    <!-- Main CSS file -->
    <link rel="stylesheet" href="css/style.css">
    
</head>
<body>

<?php require('preloader.php'); ?>

<!-- Global Wrapper -->
<div id="wrapper">

    <div class="h-wrapper">

        <!-- Top Bar -->
        <div class="topbar hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <ul class="top-menu">
                            <li><a href="http://coheart.ac.in" target="_blank">COHEART , Kerala Veterinary & Animal Science University</a></li>
                        </ul>
                    </div>
                    <!-- This column is hidden on mobiles -->
                    <div class="col-sm-6">
                        <div class="pull-right top-links">
                            <a href="logout_hoteladmin"><i class="fa fa-user"></i> &nbsp;Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Header -->
        <header class="header-wrapper header-transparent with-topbar">
            <div class="main-header">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <!-- Logo - Read documentation to see how to change the logo-->
                            <a href="index" class="logo"></a>
                        </div>
                        <div class="col-sm-12 col-md-8">
                            <nav class="navbar-right">
                                <ul class="menu">
                                    <!-- Toggle Menu - For Mobile Devices -->
                                    <li class="toggle-menu"><i class="fa icon_menu"></i></li> 
                                    
                                    <li class="first">
                                        <a href="index">Home</a>
                                    </li> <!-- END Home -->
                                    <li>
                                        <a href="superadmin">Admin Page</a>
                                    </li> <!-- END Admin Page -->
                                    <li class="">
                                        <a href="restaurants">Restaurants</a>
                                    </li> <!-- END Restaurants -->
                                    <li>
                                        <a href="news">News</a>
                                    </li> <!-- END News -->
                                    <li>
                                        <a href="downloads">Downloads</a>
                                    </li> <!-- END Downloads -->
                                    <li>
                                        <a href="contact" class="last">Contact</a>
                                    </li> <!-- END Contact -->
                                    <li class="hidden-sm hidden-md hidden-lg">
                                        <a href="logout_superadmin"><i class="fa fa-user"></i> &nbsp;Logout</a>
                                    </li> <!-- END logout -->
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div> <!-- END Container -->
            </div> <!-- END Main Header -->
        </header>

    </div>

    <!-- Do not remove this class -->
    <div class="push-top"></div>

    <section class="section bg-img bg-admin-panel">
        <div class="bg-overlay op6"></div>
        <div class="container">
            <div class="row mt50 mb40">
                <div class="col-sm-12">
                    <h1 class="title-large title-larger color-on-img mb20 mt50">Admin Panel <span class="fs-small">( <?php echo ucwords($_SESSION['foodsafety_hoteladmin']['name']); ?> )</span></h1>
                    <div class="br-bottom mb0"></div>
                </div>
            </div>
        </div>
    </section>

    <!--<div class="search-form">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="pull-right">
                        <form action="#" role="form" class="pull-right">
                            <input type="text" class="input-search" placeholder="Search here" id="filter">
                            <button type="submit" class="hidden"></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>-->

    <section class="section-page">
        <div class="container">

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">
                        <strong><?php echo $data['name']; ?></strong>
                    </h2>
                    <hr>
                </div>
                <div class="col-md-4">
                <?php
                if($data['image']=="")
                    echo "<img src='img/restaurant.png' class='img-responsive img-border-left'>";
                else 
                    echo "<img src='uploads/profilethumb/{$data['image']}' class='img-responsive img-border-left'>";
                ?>
                    
                </div>
                <div class="col-md-4">
                    <div id="description-content">
                        <?php
                        echo "<table class='hoteladmin-table'>";
                        echo "<tr>";
                        echo "<td width=100'>Name </td><td width='10'> : </td><td> ".ucwords($data['name'])."</td>";
                        echo "</tr>";
                        echo "<tr>";
                        echo "<td>Address </td><td> : </td><td> ".nl2br($data['address'])."</td>";
                        echo "</tr>";
                        echo "<tr>";
                        echo "<td>Contact </td><td> : </td><td> ".nl2br($data['phone'])."</td>";
                        echo "</tr>";
                        echo "<tr>";
                        echo "<td>Description </td><td> : </td> ";
                        if($data['description']!="")
                            echo "<td>Description : </td><td>".nl2br($data['description'])."</td>";
                        else
                            echo "<td>No description available yet!</td>";
                        echo "</tr>";
                        echo "</table>";
                        
                        ?>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="text-center"><img src="img/green-tick.png" width="150"></div>
                    <br><br>
                    <div class="hotel-rating text-center">
                    <?php
                        $query2 = mysqli_query($conn,"SELECT * from rating where hotel={$data['id']}");
                        $flag = 0;
                        $count = mysqli_num_rows($query2);
                        $rating = 0;
                        while($data2 = mysqli_fetch_assoc($query2))
                        {
                            $rating += $data2['rating'];
                            $flag = 1;
                        }
                        if($count != 0)
                            $rating = round($rating/$count,1);
                        if($flag == 1)
                        {
                            echo "<div class='rateit ' data-rateit-value='$rating' data-rateit-ispreset='true' data-rateit-readonly='true'></div>";
                            //echo "<span class='text-center'>($rating/5)</span>";
                        }
                        elseif($flag == 0)
                        {
                            echo "<div class='rateit ' data-rateit-value='$rating' data-rateit-ispreset='true' data-rateit-readonly='true'></div>";
                            //echo "<span class='text-center'>($rating/5)</span>";
                        }
                    ?>
                    </div>
                </div>
                
                <div class="clearfix"></div>

            </div>
        </div>
    <div class="row hotel-admin-panels">
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
              <div class="panel-heading" style="color: #FFF; background-color: #07541D;border-color: #ddd;">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                    Edit Your Profile <span class="glyphicon glyphicon-chevron-down pull-right"></span>
                  </a>
                </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="row">
                    <div class="box">
                        <div class="col-lg-12 col-md-12">
                            
                            <p><!--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat, vitae, distinctio, possimus repudiandae cupiditate ipsum excepturi dicta neque eaque voluptates tempora veniam esse earum sapiente optio deleniti consequuntur eos voluptatem.--></p>
                            <!--Registration form-->
                            <form role="form" id="registration-form" autocomplete="off">
                                <div class="form-vaidation-error col-md-12"></div>
                                <div class="row">
                                    <div class="form-group col-lg-4">
                                        <label>Name of Establishment</label>
                                        <input type="text" class="form-control" name="name" id="name" value="<?php echo $_SESSION['foodsafety_hoteladmin']['name'];?>">
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label>Type</label>
                                        <select class="form-control" name="type" id="type">
                                        <option value="">--Select one--</option>
                                        <option value="hotel" <?php if($_SESSION['foodsafety_hoteladmin']['type']=="hotel") echo "selected";?>>Hotel</option>
                                        <option value="bakery" <?php if($_SESSION['foodsafety_hoteladmin']['type']=="bakery") echo "selected";?>>Bakery</option>
                                        <option value="restaurant" <?php if($_SESSION['foodsafety_hoteladmin']['type']=="restaurant") echo "selected";?>>Resturant</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label>Location</label>
                                        <input type="text" class="form-control" name="location" id="location" value="<?php echo $_SESSION['foodsafety_hoteladmin']['location'];?>">
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label>Contact person</label>
                                        <input type="text" class="form-control" name="contactPerson" id="contactPerson" value="<?php echo $_SESSION['foodsafety_hoteladmin']['contactPerson'];?>" >
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label>Email Address</label>
                                        <input type="email" class="form-control" name="email" id="email" value="<?php echo $_SESSION['foodsafety_hoteladmin']['email'];?>" readonly>
                                    </div>
                                    <div class="form-group col-lg-4">
                                        <label>Phone Number</label>
                                        <input type="tel" class="form-control" name="phone" id="phone" value="<?php echo $_SESSION['foodsafety_hoteladmin']['phone'];?>" readonly>
                                    </div>
                                    
                                    <div class="clearfix"></div>
                                    <div class="form-group col-lg-12">
                                        <label>Address</label>
                                        <textarea class="form-control" rows="6" name="address" id="address"><?php echo $_SESSION['foodsafety_hoteladmin']['address'];?></textarea>
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <label>Description</label>
                                        <textarea class="form-control tiny" rows="6" name="descriptionHotel" id="descriptionHotel"><?php echo $_SESSION['foodsafety_hoteladmin']['description'];?></textarea>
                                    </div>
                                    <div class="form-group col-lg-12">
                                        <input type="hidden" name="save" value="contact">
                                        <button type="button" id="myButton" data-loading-text="Submitting..." class="btn btn-primary" autocomplete="off" value="save" name="save">
                                          Submit
                                        </button>
                                    </div>
                               
                            </form>
                        </div>
                    </div>
                </div>
                </div>
              </div>
            </div>
            
          </div>
    </div>
    </div>
    <div class="row hotel-admin-panels">
        <div class="panel-group" id="accordion2">
            <div class="panel panel-default">
              <div class="panel-heading" style="color: #FFF; background-color: #07541D;border-color: #ddd;">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
                    Upload Imgae <span class="glyphicon glyphicon-chevron-down pull-right"></span>
                  </a>
                </h4>
              </div>
              <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="row">
                    <div class="upload-wrapper">
                        <div class="upload-click">Click here to Upload Image</div>
                        <div class="upload-image"><img src="img/ajax-loader.gif" width="16" height="16"></div>
                        <input type="file" id="input-file-upload" />
                    </div>
                    <div id="server-response">
                    <?php
                    if($_SESSION['foodsafety_hoteladmin']['image']!="")
                        echo "<img src='uploads/profilethumb/{$_SESSION['foodsafety_hoteladmin']['image']}?".time()."'>";
                    ?>
                    </div>
                </div>
                </div>
              </div>
            </div>
            
        </div>
    </div>
    <div class="row hotel-admin-panels">
        <div class="panel-group" id="accordion3">
            <div class="panel panel-default">
              <div class="panel-heading" style="color: #FFF; background-color: #07541D;border-color: #ddd;">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion3" href="#collapseThree">
                    Add Menu Items <span class="glyphicon glyphicon-chevron-down pull-right"></span>
                  </a>
                </h4>
              </div>
              <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body">
                  <form role="form" id="menu-form" autocomplete="off">
                        <div class="form-vaidation-error2 col-md-12"></div>
                        <div class="row">
                            <div class="form-group col-lg-4">
                                <label>Name of Item</label>
                                <input type="text" class="form-control" name="item" id="item" >
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Enter Rate</label>
                                <input type="text" class="form-control" name="rate" id="rate">
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-lg-12">
                                <label>Description</label>
                                <textarea class="form-control tiny" rows="6" name="description" id="description"></textarea>
                            </div>
                            <div class="form-group col-lg-6">
                                <input type="hidden" name="save" value="contact">
                                <button type="button" id="myButton2" data-loading-text="Adding..." class="btn btn-default" autocomplete="off" value="add" name="add">
                                  Add Item
                                </button>
                            </div>
                            <div class="form-group col-lg-6">
                                <button type="button" class="btn btn-primary pull-right" data-toggle="modal" id="view-modal">View Menu Items</button>
                            </div>
                            
                            <!-- Large modal -->
                            

                            <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal">
                              <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">List of Items</h4>
                                  </div>
                                  <div class="modal-body" id="list-of-items">
                                    
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    
                                  </div>
                                </div>
                              </div>
                            </div>
                    </form>
                </div>
              </div>
            </div>
            
          </div>
    </div>
    </div>
    <div class="row hotel-admin-panels">
        <div class="panel-group" id="accordion4">
            <div class="panel panel-default">
              <div class="panel-heading" style="color: #FFF; background-color: #07541D;border-color: #ddd;">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion4" href="#collapseFour">
                    Aminities <span class="glyphicon glyphicon-chevron-down pull-right"></span>
                  </a>
                </h4>
              </div>
              <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="row">
                    
                    <div id="amenimages" class="col-md-9">
                        <?php
                    $query = mysqli_query($conn, "SELECT * from extradetails where hotelid=$id");
                echo mysqli_error($conn);
                if($data = mysqli_fetch_assoc($query))
                {
                    $exdata=explode(",",$data['amenities']);
                    $exdata1=explode(",",$data['cuisines']);
                    $exdata2=explode(",",$data['specialities']);
                    $cnt=count($exdata);
                    $cnt1=count($exdata1);
                    $cnt2=count($exdata2);
                    //print_r($exdata);
                }
                for($i=0;$i<$cnt;$i++)
                {
                    ?>
                        <img src="img/aminities/aminities-<?php echo $exdata[$i] ?>.png">
                       <?php
                }
                ?>
                    </div>
                    <div class="col-md-3">
                        <button class="btn btn-primary add-button pull-right" width="300" id="add-aminities-button">Add</button>
                    </div>
                  </div>
                </div>
        </div>
            </div>
            
          </div>
          <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal2">
                              <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">List of Aminities</h4>
                                    <div class="form-vaidation-error3"></div>
                                  </div>
                                  <div class="modal-body" id="list-of-aminities">
                                    <form id="aminities-form" autocomplete="off">
                                    <div class="form-group">
                                        <select id="amen" multiple class="form-control">
                                            
                                            <option value="cafe">Cafe</option>
                                            <option value="parking">Parking</option>
                                            <option value="dining">Dining</option>
                                            <option value="wifi">Wifi</option>
                                        </Select>
                                    </div>
                                    <div class="form-group text-center">        
                                        <button id="addament" type="button" value="saveaments" class="btn btn-success">Add</button>
                                    </div>
                                    
                                  
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" onclick="window.location='hoteladmin2.php'" data-dismiss="modal">Close</button>
                                    
                                  </div>
                                  </form>
                                  </div>
                                </div>
                              </div>
                            </div>
    </div>   
    <div class="row hotel-admin-panels">
        <div class="panel-group" id="accordion5">
            <div class="panel panel-default">
              <div class="panel-heading" style="color: #FFF; background-color: #07541D;border-color: #ddd;">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion5" href="#collapseFive">
                    Cuisines <span class="glyphicon glyphicon-chevron-down pull-right"></span>
                  </a>
                </h4>
              </div>
              <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-9">
                        <div class="row text-center">
                            <?php
                            for($j=0;$j<$cnt1;$j++)
                            {
                            ?>
                            <div class="col-md-3">
                                <img src="img/cuisine/<?php echo $exdata1[$j]; ?>.jpg" class="img-responsive img-rounded">
                                <div class="cuisine-title"><?php echo $exdata1[$j]; ?></div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <button class="btn btn-primary add-button pull-right" id="btn-addcuisin" width="300">Add</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
                
          </div>
    
    </div>
    
          
    
    
    
    <div class="row hotel-admin-panels">
        <div class="panel-group" id="accordion6">
            <div class="panel panel-default">
              <div class="panel-heading" style="color: #FFF; background-color: #07541D;border-color: #ddd;">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion6" href="#collapseSix">
                    Specialities <span class="glyphicon glyphicon-chevron-down pull-right"></span>
                  </a>
                </h4>
              </div>
              <div id="collapseSix" class="panel-collapse collapse">
                <div class="panel-body">
                  <div class="row">
                    <div class="col-md-9">
                        <ul>
                            <?php for($k=0;$k<$cnt2;$k++)
                            {?>
                            <li><?php echo $exdata2[$k] ?></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <button id="addspl" class="btn btn-primary add-button pull-right" width="300">Add</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
    </div>  
    </div>
   

    </div>
            
    </div>
    <!-- /.container -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal3">
                              <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">List of Cuisins</h4>
                                    <div class="form-vaidation-error4"></div>
                                  </div>
                                  <div class="modal-body" id="list-of-cuisins">
                                    <form id="cuisins-form" autocomplete="off">
                                    <div class="form-group">
                                        <select id="cuisin" multiple class="form-control">
                                            
                                            <option value="south-indian">South Indian</option>
                                            <option value="malabar">Malabar</option>
                                            <option value="dhaba">Dhaba</option>
                                            <option value="chinese">Chinese</option>
                                            <option value="arabian">Arabian</option>
                                            <option value="continental">Continental</option>
                                            
                                        </Select>
                                    </div>
                                    <div class="form-group text-center">        
                                        <button id="addcuisin" type="button" value="savecuisin" class="btn btn-success">Add</button>
                                    </div>
                                    
                                  
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    
                                  </div>
                                  </form>
                                  </div>
                                </div>
                              </div>
                            </div>
    </div>
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal4">
                              <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Specialities</h4>
                                    <div class="form-vaidation-error5"></div>
                                  </div>
                                  <div class="modal-body" id="list-of-spl">
                                    <form id="spl-form" autocomplete="off">
                                    <div class="form-group">
                                        <select id="spl" multiple class="form-control">
                                            
                                            <option value="Thattil Kootiya Dosa">Thattil Kootiya Dosa</option>
                                            <option value="Thalasseri Dum Biriyani">Thalasseri Dum Biriyani</option>
                                            <option value="Kappa Biriyani">Kappa Biriyani</option>
                                            
                                        </select>
                                    </div>
                                    <div class="form-group text-center">        
                                        <button id="addspls" type="button" value="savespl" class="btn btn-success">Add</button>
                                    </div>
                                    
                                  
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    
                                  </div>
                                  </form>
                                  </div>
                                </div>
                              </div>
                            </div>
    </div>
    </section> <!-- END Blog Page-->

    <?php require('footer.php');?>
    
</div> <!-- END Global Wrapper -->




    <!-- Javascript files -->
    <script src="plugins/jquery/jquery-2.1.0.min.js"></script>
    <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/jquery.appear.js"></script>
    <script src="plugins/retina.min.js"></script>
    <script src="plugins/stellar.min.js"></script>
    <script src="plugins/sticky.min.js"></script>
    
    <script src="plugins/sharrre/jquery.sharrre.min.js"></script>
    <script src="plugins/owl-carousel/owl.carousel.min.js"></script>
    <script src="plugins/jquery.zoom.min.js"></script>
    <script src="plugins/raty/jquery.raty.min.js"></script>
    <script src="autocomplete/magicsuggest-min.js"></script>
    <script type="text/javascript" src="tinymce/tinymce.min.js"></script>
    <!-- Main javascript file -->
    <script src="js/script.js"></script>
    
    <script>
    $(document).ready(function(){
        tinymce.init({
            selector: "textarea.tiny",
            skin: 'custom'
         });

        //$('.rateit').rateit();
        
        $('#add-aminities-button').click(function()
        {
            $('#modal2').modal();
        });
         $('#btn-addcuisin').click(function()
        {
            $('#modal3').modal();
        });
          $('#addspl').click(function()
        {
            $('#modal4').modal();
        });
        $('#view-modal').click(function()
        {
            var button = $(this).val();
            $('#modal').modal();
            $('#list-of-items').html("<div class='text-center'><img src='img/loading.gif'></div>");
            $.ajax(
              {
                url: 'fetch-items.php',
                type: "POST",
                data: {button:button},
                success: function(data,status)
                {
                  $('#list-of-items').html(data);
                }
              });
        });

        $('#myButton').on('click',function(){
            $('html, body').animate({
                scrollTop: "0px"
            }, 800);
            var btn = $(this).button('loading');
            var name = $('#name').val();
            var type = $('#type').val();
            var location = $('#location').val();
            var contactPerson = $('#contactPerson').val();
            var email = $('#email').val();
            var phone = $('#phone').val();
            var address = $('#address').val();
            tinyMCE.triggerSave();
            var descriptionHotel = $('#descriptionHotel').val();
            var button = $(this).val();

            $.ajax(
              {
                url: 'update.php',
                type: "POST",
                data: {save:button,name:name,type:type,email:email,phone:phone,location:location,contactPerson:contactPerson,address:address,description:descriptionHotel},
                success: function(data,status)
                {
                  if(data!="success")
                  {
                    $('.form-vaidation-error').hide();
                    $('.form-vaidation-error').html(data);
                    $('.form-vaidation-error').fadeIn();
                    $('#myButton').button('reset');
                  }
                  else
                  {
                    $('.form-vaidation-error').hide();
                    $('.form-vaidation-error').html("<div class='alert alert-success' role='alert'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span>  Updated!.....</div>");
                    $('.form-vaidation-error').fadeIn();
                    $('#myButton').button('reset');
                    //$('form#registration-form') [0].reset();
                    setTimeout(function(){ $('.form-vaidation-error').fadeOut(); }, 3000);
                    
                  }
                }
              });
        });
        
        
            $('#addament').on('click',function(){
            var btn = $(this).button('loading');
            var amen = $('#amen').val();
            var button = $(this).val();
//alert(amen);
            $.ajax(
              {
                url: 'addamen.php',
                type: "POST",
                data: {saveamen:button,ament:amen},
                success: function(data,status)
                {
                    //alert(view);
              if(data!="success")
                  {
                    $('.form-vaidation-error3').hide();
                    $('.form-vaidation-error3').html(data);
                    $('.form-vaidation-error3').fadeIn();
                    $('#addament').button('reset');
                  }
                  else
                  {
                    $('.form-vaidation-error3').hide();
                    $('.form-vaidation-error3').html("<div class='alert alert-success' role='alert'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span>  Added!.....</div>");
                    $('.form-vaidation-error3').fadeIn();
                    $('#addament').button('reset');
                    //$('form#registration-form') [0].reset();
                    setTimeout(function(){ $('.form-vaidation-error').fadeOut(); }, 3000);
                  }
                }
              });
        });
        
        $('#addspls').on('click',function(){
            var btn = $(this).button('loading');
            var spl = $('#spl').val();
            var button = $(this).val();
//alert(amen);
            $.ajax(
              {
                url: 'addspl.php',
                type: "POST",
                data: {savespl:button,spls:spl},
                success: function(data,status)
                {
                    //alert(view);
              if(data!="success")
                  {
                    $('.form-vaidation-error5').hide();
                    $('.form-vaidation-error5').html(data);
                    $('.form-vaidation-error5').fadeIn();
                    $('#addspls').button('reset');
                  }
                  else
                  {
                    $('.form-vaidation-error5').hide();
                    $('.form-vaidation-error5').html("<div class='alert alert-success' role='alert'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span>  Added!.....</div>");
                    $('.form-vaidation-error5').fadeIn();
                    $('#addspls').button('reset');
                    //$('form#registration-form') [0].reset();
                    setTimeout(function(){ $('.form-vaidation-error').fadeOut(); }, 3000);
                  }
                }
              });
        });
        
        
        
        
        
        $('#addcuisin').on('click',function(){
            var btn = $(this).button('loading');
            var cuisin = $('#cuisin').val();
            var button = $(this).val();
//alert(amen);
            $.ajax(
              {
                url: 'addcuisin.php',
                type: "POST",
                data: {savecuisin:button,cuisins:cuisin},
                success: function(data,status)
                {
                    //alert(view);
              if(data!="success")
                  {
                    $('.form-vaidation-error4').hide();
                    $('.form-vaidation-error4').html(data);
                    $('.form-vaidation-error4').fadeIn();
                    $('#addcuisin').button('reset');
                  }
                  else
                  {
                    $('.form-vaidation-error4').hide();
                    $('.form-vaidation-error4').html("<div class='alert alert-success' role='alert'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span>  Added!.....</div>");
                    $('.form-vaidation-error4').fadeIn();
                    $('#addcuisin').button('reset');
                    //$('form#registration-form') [0].reset();
                    setTimeout(function(){ $('.form-vaidation-error').fadeOut(); }, 3000);
                  }
                }
              });
        });
        
        
        

        $('#myButton2').on('click',function(){
            var btn = $(this).button('loading');
            var btn = $(this).button('loading');
            var item = $('#item').val();
            var rate = $('#rate').val();
            tinyMCE.triggerSave();
            var description = $('#description').val();
            var button = $(this).val();

            $.ajax(
              {
                url: 'add-item.php',
                type: "POST",
                data: {save:button,item:item,rate:rate,description:description},
                success: function(data,status)
                {
                  if(data!="success")
                  {
                    $('.form-vaidation-error2').hide();
                    $('.form-vaidation-error2').html(data);
                    $('.form-vaidation-error2').fadeIn();
                    $('#myButton2').button('reset');
                  }
                  else
                  {
                    $('.form-vaidation-error2').hide();
                    $('.form-vaidation-error2').html("<div class='alert alert-success' role='alert'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span>  Item added!.....</div>");
                    $('.form-vaidation-error2').fadeIn();
                    $('#myButton2').button('reset');
                    $('form#menu-form') [0].reset();
                    setTimeout(function(){ 
                        $('.form-vaidation-error2').fadeOut();

                         }, 3000);

                  }
                }
              });
        });
        /*file upload*/
        $(".upload-click").click(function(e){ 
        $('#input-file-upload').trigger('click');
        });
        
        $("#input-file-upload").change(function(){
            var loaded = false;
            if(window.File && window.FileReader && window.FileList && window.Blob){
                if($(this).val()){ //check empty input filed
                    oFReader = new FileReader(), rFilter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;
                    if($(this)[0].files.length === 0){return}
                    
                    var oFile = $(this)[0].files[0];
                    var fsize = $(this)[0].files[0].size; //get file size
                    var ftype = $(this)[0].files[0].type; // get file type
                    
                    if(!rFilter.test(oFile.type)) 
                    {
                        alert('Unsupported file type!');
                        return false;
                    }
                        
                    var allowed_file_size = 2194304;    
                                
                    if(fsize>allowed_file_size)
                    {
                        alert('File too big! Allowed size 2 MB');
                        return false;
                    }
                    
                    $(".upload-image").show();
                    $(".upload-click").hide();
                    
                    var mdata = new FormData();
                    mdata.append('image_data', $(this)[0].files[0]);
                    
                    jQuery.ajax({
                        type: "POST", // HTTP method POST or GET
                        processData: false,
                        contentType: false,
                        url: "upload_resize1.php", //Where to make Ajax calls
                        data: mdata, //Form variables
                        dataType: 'json',
                        success:function(response){
                            $(".upload-image").hide();
                            $(".upload-click").show();
                            
                            if(response.type == 'success'){
                                $("#server-response").html('<div class="success">' + response.msg + '</div>');
                            }else{
                                $("#server-response").html('<div class="error">' + response.msg + '</div>');
                            }
                        }
                    });
                    
                }
                
            }else{
                alert("Can't upload! Your browser does not support File API! Try again with modern browsers like Chrome or Firefox.</div>");
                return false;
            }
            
        });
    });
    </script>
    

</body>
</html>
<?php
    }
    else
        header("location: index");
?>