<!-- Footer wrapper -->
	<footer class="footer-wrapper footer-gray">
		<div class="container">
			<div class="row col-p30">
				<div class="col-sm-12 col-md-4">
					<div class="footer-widget">
						<h3 class="footer-title">Food Safety Mission</h3>
						<ul class="footer-links clearfix">
							<li><a href="#">Home</a></li>
							<li><a href="#">Restaurants</a></li>
							<li><a href="#">News</a></li>
							<li><a href="#">Downloads</a></li>
							<li><a href="#">Contact</a></li>
							
						</ul>
					</div>
				</div>
				<div class="col-sm-12 col-md-4">
					<div class="footer-widget">
						<h3 class="footer-title">Get social</h3>
						<ul class="footer-social clearfix">
							<li><a href="#" data-toggle="tooltip" title="Twitter"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#" data-toggle="tooltip" title="Facebook"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#" data-toggle="tooltip" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#" data-toggle="tooltip" title="Pinterest"><i class="fa fa-pinterest"></i></a></li>
							<li><a href="#" data-toggle="tooltip" title="Instagram"><i class="fa fa-instagram"></i></a></li>
							<li><a href="#" data-toggle="tooltip" title="Dribbble"><i class="fa fa-dribbble"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-sm-12 col-md-4">
					<div class="footer-widget">
						<h3 class="footer-title">Tweets</h3>
						<!-- This is just a dummy twitter feed with typed content (not generetad).
						Didn't include the twitter feed for performance benefits.
						See in documentation how to include a fully functional twitter feed widget -->
						<div class="sidebar-tweet clearfix">
							<i class="fa fa-twitter"></i>
							<p class="tweet-content">
								<a href="#" class="tweet-user">@foodsafety</a> 
								<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span> 
								<small>22 hours ago</small>
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				
				<div class="col-sm-6 col-md-4">
					<p class="copyright">&copy; Copyright 2010 - 2015 Aider Solutions</p>
				</div>
			</div>
		</div>
	</footer>