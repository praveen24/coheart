<?php session_start();
$upload_dir_url = "uploads/profilethumb/";
if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

	//makesure uploaded file is not empty
	if(!isset($_FILES['image_data']) || !is_uploaded_file($_FILES['image_data']['tmp_name'])){
				
				//generate a json response
				$response = json_encode(array( 
											'type'=>'error', 
											'msg'=>'Image file is Missing!'
										));
				die($response);
		}
        include('resize.php');
// settings
$max_file_size = 2194304; // 2mb
$valid_exts = array('jpeg', 'jpg', 'png', 'gif');
// thumbnail sizes
$sizes = array(300 => 300);
if ($_SERVER['REQUEST_METHOD'] == 'POST' AND isset($_FILES['image_data'])) {
  if( $_FILES['image_data']['size'] < $max_file_size ){
    // get file extension
    $ext = strtolower(pathinfo($_FILES['image_data']['name'], PATHINFO_EXTENSION));
    $nameoffile = $_SESSION['foodsafety_hoteladmin']['username'].".".$ext;
    if (in_array($ext, $valid_exts)) {
      // resize image 
      foreach ($sizes as $w => $h) {
        $files[] = resize($w, $h);
      }
	  require('dbconnect.php');
	  $query = mysqli_query($conn,"UPDATE registrations set image='$nameoffile' where id = {$_SESSION['foodsafety_hoteladmin']['id']}");
		$query2 = mysqli_query($conn,"SELECT * from registrations where id = {$_SESSION['foodsafety_hoteladmin']['id']}");
		$data = mysqli_fetch_assoc($query2);
		$_SESSION['foodsafety_hoteladmin'] = $data;
    $response = json_encode(array('type'=>'success', 'msg'=>'Image Uploaded! <br /><img src="'.$upload_dir_url. $nameoffile.'?'.time().'">','file'=>"$nameoffile?".time()));
   die($response);
   } else {
      $msg = 'Unsupported file';
    }
  } else{
    $msg = 'Please upload image smaller than 2MB';
  }
}
        }
?>