<?php 
	session_start();
	if(isset($_SESSION['foodsafety_hoteladmin']) && $_SESSION['foodsafety_hoteladmin']!="")
	{
		$id = $_SESSION['foodsafety_hoteladmin']['id'];

		require("dbconnect.php");
		$query = mysqli_query($conn, "SELECT * from menu where hotel = $id");
		echo "<table class='table table-bordered table-striped table-hover'>";
		echo "<th>Item Name</th>";
		echo "<th>Item Rate</th>";
		echo "<th>Description</th>";
		$flag = 0;
		while($data = mysqli_fetch_assoc($query))
		{
			echo "<tr>";
			echo "<td>{$data['item']}</td>";
			echo "<td>{$data['rate']}</td>";
			echo "<td>{$data['description']}</td>";
			echo "</tr>";
			$flag = 1;
		}
		if($flag == 0)
			echo "<tr><td colspan='3'>No items added yet!</td></tr>";
		echo "</table>";
	}
	else
		header("location: index.html");
?>