<?php
    session_start();
    if( (isset($_SESSION['foodsafety_superadmin']) && $_SESSION['foodsafety_superadmin']!="") || (isset($_SESSION['foodsafety_hoteladmin']) && $_SESSION['foodsafety_hoteladmin']!="") || (isset($_SESSION['foodsafety_inspector']) && $_SESSION['foodsafety_inspector']!="") )
        header("location: index");
    else
    {    
?>
<!DOCTYPE html>
<html lang="en-us">
<head>

	<meta charset="utf-8" >
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Food Safety Mission | Login</title>

	<meta name="author" content="abusinesstheme">
	<meta name="description" content="Palas is a Business HTML Template developed with the the latest HTML5 and CSS3 technologies.">

  	<!-- CSS files -->
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic|Montserrat:400,700' rel='stylesheet'>
	<link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="plugins/elegant_font/html_css/style.css">
	
	<!-- Main CSS file -->
	<link rel="stylesheet" href="css/style.css">
	
</head>
<body>

<?php require('preloader.php'); ?>

<!-- Global Wrapper -->
<div id="wrapper">

	<div class="h-wrapper">

		<!-- Top Bar -->
		<div class="topbar hidden-xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<ul class="top-menu">
							<li><a href="http://coheart.ac.in" target="_blank">COHEART , Kerala Veterinary & Animal Science University</a></li>
						</ul>
					</div>
					<!-- This column is hidden on mobiles -->
					<div class="col-sm-6">
						<div class="pull-right top-links">
							<a href="login" class="active">Sign In</a>
							<a href="">|</a>
							<a href="register">Sign Up</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Header -->
		<header class="header-wrapper header-transparent with-topbar">
			<div class="main-header">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-4">
							<!-- Logo - Read documentation to see how to change the logo-->
							<a href="index" class="logo"></a>
						</div>
						<div class="col-sm-12 col-md-8">
							<nav class="navbar-right">
								<ul class="menu">
									<!-- Toggle Menu - For Mobile Devices -->
									<li class="toggle-menu"><i class="fa icon_menu"></i></li> 
									
									<li class="first">
										<a href="index">Home</a>
									</li> <!-- END Home -->
									<li class="hidden-sm hidden-md hidden-lg">
										<a href="login">Sign In</a>
									</li> <!-- END Sign in -->
									<li class="hidden-sm hidden-md hidden-lg">
										<a href="register">Sign Up</a>
									</li> <!-- END Sign up -->
									<li>
										<a href="restaurants">Restaurants</a>
									</li> <!-- END Restaurants -->
									<li>
										<a href="news">News</a>
									</li> <!-- END News -->
									<li>
										<a href="downloads">Downloads</a>
									</li> <!-- END Downloads -->
									<li>
										<a href="contact" class="last">Contact</a>
									</li> <!-- END Contact -->
									
								</ul>
							</nav>
						</div>
					</div>
				</div> <!-- END Container -->
			</div> <!-- END Main Header -->
		</header>

	</div>

	<!-- Do not remove this class -->
	<div class="push-top"></div>

	<section class="section-intro bg-img bg07">
		<div class="bg-overlay op6"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-8">
					<h1 class="intro-title mb20">Sign In</h1>
					<p class="intro-p mb30">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit tenetur reiciendis molestias nostrum excepturi porro dolorum amet!</p>
				</div>
			</div>
		</div>
	</section>

	<section class="page-sign-in mb10">
		<div class="container">
			<div class="row">
				<div class="col-sm-10 col-md-6 col-md-push-6">
					<div class="sign-in-area">
						<h3 class="title-small br-bottom">Sign in to your account</h3>
						<p>Don't you have an account yet? &nbsp; <a href="register" class="xs-block">Register</a></p>
						<form class="form mb50" id="login-form" autocomplete="off">
						<div class="form-vaidation-error"></div>
		  					<div class="form-group">
								<div class="form-icon icon-user">
			 						<input type="text" name="username" id="username" class="form-control" placeholder=" User Name">
			 					</div>
							</div>
							<div class="form-group">
								<div class="form-icon icon-lock">
			 						<input type="password" name="password" id="password" class="form-control" placeholder=" Password">
			 					</div>
							</div>

		  					<!--<label class="form-remember"><input type="checkbox" name="remember" value="yes"> Remember Password </label>-->

			 				<a href="#" class="forgot"><small>Forgot your password?</small></a>

					 		<div>
					 			<input type="hidden" name="save" value="contact">
					 			<button type="button" class="btn btn-d" id="myButton" data-loading-text="Logging in..." value="login" name="login"> Sign In </button>
					 		</div>
		  				</form>
		  				<h3 class="title-small br-bottom">Connect with you social Profile</h3>
		  				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni, error.</p>
		  				<p class="mb0">
		  					<a href="#" class="btn btn-bg br2 btn-icon facebook xs-block"><i class="fa fa-facebook"></i> Facebook</a>
		  					<span class="mr10"></span>
		  					<a href="#" class="btn btn-bg br2 btn-icon twitter xs-block"><i class="fa fa-twitter"></i> Twitter</a>
		  					<span class="mr10"></span>
		  					<a href="#" class="btn btn-bg br2 btn-icon google-plus xs-block"><i class="fa fa-google-plus"></i> Google+</a>
		  				</p>
					</div>
				</div>
				<div class="col-sm-12 col-md-5 col-md-pull-6">
					<div class="info-area">
						<div class="box-services-a">
							<h3 class="title-small"><i class="fa icon_tools fa-bg"></i> Fully customizable <a href="#" class="link-read-more">read more</a></h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis maiores repudiandae, accusantium reiciendis!</p>
						</div>

						<div class="mb50"></div>

						<div class="box-services-a">
							<h3 class="title-small"><i class="fa icon_lightbulb_alt fa-bg"></i> Easy to use <a href="#" class="link-read-more">read more</a></h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis maiores repudiandae, accusantium reiciendis!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<?php require('footer.php');?>
	
</div> <!-- END Global Wrapper -->

	<!-- Javascript files -->
	<script src="plugins/jquery/jquery-2.1.0.min.js"></script>
	<script src="plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="plugins/jquery.appear.js"></script>
	<script src="plugins/retina.min.js"></script>
	<script src="plugins/stellar.min.js"></script>
	<script src="plugins/sticky.min.js"></script>

	<!-- Main javascript file -->
	<script src="js/button.js"></script>
	<script src="js/script.js"></script>
	<script src="js/login.js"></script>
</body>
</html>
<?php
    }
?>