<?php
	session_start();
	if((isset($_SESSION['foodsafety_superadmin']) && $_SESSION['foodsafety_superadmin']!="") || (isset($_SESSION['foodsafety_inspector']) && $_SESSION['foodsafety_inspector']!=""))
	{
        require('dbconnect.php');
        $id = $_GET['id'];
        $query = mysqli_query($conn, "SELECT * from registrations where id={$_GET['id']}");
        if($data = mysqli_fetch_assoc($query))
        {
            date_default_timezone_set("Asia/Kolkata"); 
            $time = date_create($data['date_of_registration']);
            $date = date_format($time, "F j, Y, g:i a");
?>
<!DOCTYPE html>
<html lang="en-us">
<head>

    <meta charset="utf-8" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Food Safety Mission | Registrations Details</title>

    <meta name="author" content="abusinesstheme">
    <meta name="description" content="Palas is a Business HTML Template developed with the the latest HTML5 and CSS3 technologies.">

    <!-- CSS files -->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic|Montserrat:400,700' rel='stylesheet'>
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="plugins/elegant_font/html_css/style.css">
    
    <link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
    
    <!-- Main CSS file -->
    <link rel="stylesheet" href="css/style.css">
    
</head>
<body>

<?php require('preloader.php'); ?>

<!-- Global Wrapper -->
<div id="wrapper">

    <div class="h-wrapper">

        <!-- Top Bar -->
        <div class="topbar hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <ul class="top-menu">
                            <li><a href="http://coheart.ac.in" target="_blank">COHEART , Kerala Veterinary & Animal Science University</a></li>
                        </ul>
                    </div>
                    <!-- This column is hidden on mobiles -->
                    <div class="col-sm-6">
                        <div class="pull-right top-links">
                            <a href="logout_superadmin"><i class="fa fa-user"></i> &nbsp;Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Header -->
        <header class="header-wrapper header-transparent with-topbar">
            <div class="main-header">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <!-- Logo - Read documentation to see how to change the logo-->
                            <a href="index" class="logo"></a>
                        </div>
                        <div class="col-sm-12 col-md-8">
                            <nav class="navbar-right">
                                <ul class="menu">
                                    <!-- Toggle Menu - For Mobile Devices -->
                                    <li class="toggle-menu"><i class="fa icon_menu"></i></li> 
                                    
                                    <li class="first">
                                        <a href="index">Home</a>
                                    </li> <!-- END Home -->
                                    <li>
                                        <a href="superadmin">Admin Page</a>
                                    </li> <!-- END Admin Page -->
                                    <li class="">
                                        <a href="restaurants">Restaurants</a>
                                    </li> <!-- END Restaurants -->
                                    <li>
                                        <a href="news">News</a>
                                    </li> <!-- END News -->
                                    <li>
                                        <a href="downloads">Downloads</a>
                                    </li> <!-- END Downloads -->
                                    <li>
                                        <a href="contact" class="last">Contact</a>
                                    </li> <!-- END Contact -->
                                    <li class="hidden-sm hidden-md hidden-lg">
                                        <a href="logout_superadmin"><i class="fa fa-user"></i> &nbsp;Logout</a>
                                    </li> <!-- END logout -->
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div> <!-- END Container -->
            </div> <!-- END Main Header -->
        </header>

    </div>

    <!-- Do not remove this class -->
    <div class="push-top"></div>

    <section class="section bg-img bg-admin-panel">
        <div class="bg-overlay op6"></div>
        <div class="container">
            <div class="row mt50 mb40">
                <div class="col-sm-12">
                    <h1 class="title-large title-larger color-on-img mb0 mt50"><?php echo ucfirst($data['name']); ?><span class="fs-small"> (Registration Details)</span></h1>
                    <div class="br-bottom mb0"></div>
                </div>
            </div>
        </div>
    </section>

   <section class="shop-section section-page" id="shop-section-no-p-b">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="row mb30">
						<div class="col-sm-12 col-md-6">
							<div id="owl-shop" class="owl-carousel mb30">
								
								<?php 
									if($data['image']=="")
									{
										echo '<div class="owl-item-shop">';
					                    echo "<img src='images/restaurant-default.png' class='img-rounded'>";
					                    echo '</div>';
					                }
									else
									{
										echo '<div class="owl-item-shop">';
					                    echo "<img src='uploads/profilethumb/{$data['image']}' class='img-responsive img-rounded' style='width: 100%;border-radius: 5px;'>";
					                    echo '</div>';
					                }
								?>
							</div>
						</div>
						<div class="col-sm-12 col-md-6 sm-box">
							<div class="shop-description-product">
								<div class="row">
									<div class="col-sm-12">
										<h3 class="shop-product-name br-bottom capitalize">
											<?php echo $data['name']; ?> <span><?php echo $data['type']; ?></span>
											<?php
											if($data['approved'] == 'yes')
												echo '<a href="" class="img-approved" data-toggle="tooltip" data-placement="top" title="Approved!"><img src=images/approved.png "></a>';
											?>
											
										</h3>
									</div>
								</div>
								<div class="row mb10">
									<div class="col-sm-12">
										<div class="clearfix">
											<span class="rating-system rate-product" data-score='4'></span> <small>125 Reviews</small> &nbsp;
											<small><a href="#" class="see-reviews" data-toggle="modal" data-target="#modal-reviews">see reviews</a></small>
										</div>
									</div>
								</div>
								<!--<div class="shop-price-wrapper">
									<del class="shop-price-off">$159</del> <ins class="shop-price">$129</ins>
								</div>-->
								<!--<div class="restaurant-description remove-style">
									<?php echo $data['description'];?>
								</div>-->
								<ul class="shop-specifications">
					  				<li class="title"><i class="fa fa-map-marker"></i> Location - <?php echo ucwords($data['location']); ?></li>
					  				<li class="remove-style" style="margin-left: 33px;">
					  					<?php echo ucfirst(nl2br($data['address'])); ?>
					  				</li>
					  			</ul>
					  			<ul class="shop-specifications">
					  				<li class="title"><i class="fa fa-phone"></i> Contact - <?php echo $data['phone']; ?></li>
					  				<!--<li class="remove-style">
					  					<?php echo $data['phone']; ?>
					  				</li>-->
					  			</ul>
					  			<ul class="shop-specifications">
					  				<li class="title"><i class="fa fa-envelope"></i> Email - <?php echo $data['email']; ?></li>
					  				<!--<li class="remove-style">
					  					<?php echo $data['email']; ?>
					  				</li>-->
					  			</ul>
					  			<ul class="shop-specifications">
					  				<li class="title"><i class="fa fa-envelope"></i> Category - <?php echo $data['type']; ?></li>
					  				<!--<li class="remove-style">
					  					<?php echo $data['email']; ?>
					  				</li>-->
					  			</ul>
					  			<ul class="shop-specifications">
					  				<li class="title"><i class="fa fa-envelope"></i> Contact Person - <?php echo $data['contactPerson']; ?></li>
					  				<!--<li class="remove-style">
					  					<?php echo $data['email']; ?>
					  				</li>-->
					  			</ul>
					  			<ul class="shop-specifications">
					  				<li class="title"><i class="fa fa-envelope"></i> Date of Registration - <?php echo $date; ?></li>
					  				<!--<li class="remove-style">
					  					<?php echo $data['email']; ?>
					  				</li>-->
					  			</ul>
								<div class="mt30">
									<button type="button" id="myButton" data-loading-text="Sending..." class="btn btn-success btn-disabled approve" autocomplete="off" value="approve" name="approve" <?php if($data['approved'] == "yes") echo "disabled"; ?>><i class="fa fa-thumbs-o-up"></i>
					                    <?php if($data['approved'] == "yes") echo "Approved!"; else echo "Approve"; ?>
					                </button>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="row mb20">
								<div class="col-sm-12">
									<h3 class="title-small title-small-a">Description</h3>
					 				<div class="remove-style well">
					 					<?php 
					 						if($data['description']!="")
					                            echo $data['description'];
					                        else
					                            echo "Not Available!";
					 					?>
					 				</div>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-sm-12">
									<h3 class="title-small title-small-a">Menu</h3>
					 				<div class="well">
					 				<?php
					 					$query1 = mysqli_query($conn, "SELECT * from menu where hotel = $id");
	                            		echo "<table class='table table-bordered'>";
	                            		echo "<th>Item Name</th>";
	                            		echo "<th>Item Rate</th>";
	                            		echo "<th>Description</th>";
	                            		$flag = 0;
	                            		while($data1 = mysqli_fetch_assoc($query1))
	                            		{
	                            			echo "<tr>";
	                            			echo "<td>".ucwords($data1['item'])."</td>";
	                            			echo "<td><i class='fa fa-inr' style='font-size: 12px;'></i> {$data1['rate']}</td>";
	                            			echo "<td class='desc'>".ucfirst($data1['description'])."</td>";
	                            			echo "</tr>";
	                            			$flag = 1;
	                            		}
	                            		if($flag == 0)
	                            			echo "<tr><td colspan='3'>Not Available!</td></tr>";
	                            		echo "</table>";

	                                ?>
					 				</div>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-sm-12">
									<h3 class="title-small title-small-a">Aminities</h3>
					 				<div id="amenimages" class="well">
									<?php
										$query3 = mysqli_query($conn, "SELECT * from extradetails where hotelid=$id");
										//echo mysqli_error($conn);
										$flag = 0;
										if($data3 = mysqli_fetch_assoc($query3))
										{
											$exdata=explode(",",$data3['amenities']);
											$exdata1=explode(",",$data3['cuisines']);
											$exdata2=explode(",",$data3['specialities']);
											$cnt=count($exdata);
											$cnt1=count($exdata1);
											$cnt2=count($exdata2);
											//print_r($exdata);
										}
										for($i=0;$i<$cnt;$i++)
										{
											echo "<a href='' class='img-approved' data-toggle='tooltip' data-placement='top' title='".ucwords($exdata[$i])."'><img src='images/aminities/aminities-{$exdata[$i]}.png' class='img-rounded'></a>";
											$flag = 1;
						          		}
						          		if($flag == 0)
						          			echo "Not Available!";
									?>
					                </div>
								</div>
							</div>
							<div class="row mb20">
								<div class="col-sm-12">
									<h3 class="title-small title-small-a">Cuisines</h3>
					 				<div class="content well">
					 					<div class="row">
					 					<?php
					 						$flag2 = 0;
											for($j=0;$j<$cnt1;$j++)
											{
												$flag2 = 1;
											?>
											<div class="portfolio portfolio-animation-off portfolio-center">
												
												<div class="col-xs-6 col-sm-3 el ">
													<div class="portfolio-el view">
						                            	<img src="images/cuisine/<?php echo $exdata1[$j]; ?>.jpg" alt="Cuisines" class="img-responsive img-rounded">
						                            	<a href="javascript:void(0);" class="mask">
						                            		<div class="portfolio-middle">
						                            			<!--<h3 class="project-title br-bottom"><?php echo ucwords($exdata1[$j]); ?></h3>-->
						                            			<p class="text text-center"><?php echo ucwords($exdata1[$j]); ?></p>
						                            		</div>
						                            		<!--<p class="date">15 june</p>-->
						                            	</a>
													</div>
													<div class="cuisine-title text-center"><?php echo ucwords($exdata1[$j]); ?></div>
							                    </div> <!-- END portfolio Item -->
							                    
							                </div>
							                
											<?php }
												if($flag == 0)
						          					echo "<div class='col-md-12'>Not Available!</div>";
											 ?>
											</div>
					 				</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>

				

			</div>
		</div>
	</section>  

    <?php require('footer.php');?>
    
</div> <!-- END Global Wrapper -->


<!-- Javascript files -->
    <script src="plugins/jquery/jquery-2.1.0.min.js"></script>
    <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/jquery.appear.js"></script>
    <script src="plugins/retina.min.js"></script>
    <script src="plugins/stellar.min.js"></script>
    <script src="plugins/sticky.min.js"></script>
    
    <script src="plugins/sharrre/jquery.sharrre.min.js"></script>
    <script src="plugins/owl-carousel/owl.carousel.min.js"></script>
    <script src="plugins/jquery.zoom.min.js"></script>
    <script src="plugins/raty/jquery.raty.min.js"></script>
    
    <!-- Main javascript file -->
    <script src="js/script.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $('#myButton').on('click',function(){
            $(this).html('Please wait...').attr('disabled',true);
            var button = $(this).val();
            var id = <?php echo $_GET['id'];?>;

            $.ajax(
              {
                url: 'approve.php',
                type: "POST",
                data: {approveButton:button,id:id},
                success: function(data,status)
                {
                    if(data == "Approved!!!")
                    {
                        //alert(data);
                        $('#myButton').html('<i class="fa fa-check"></i> Approved');
                    }
                    else
                    {
                        //alert(data);
                        $('#myButton').html('Approve').attr('disabled',false);
                    }
                }
            });
        });
    });
    </script>
</body>
</html>
<?php
        }
        else
            header("location: registrations");
	}
	else
		header("location: index");
?>