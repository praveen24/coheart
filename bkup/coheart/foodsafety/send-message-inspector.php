<?php
	session_start();
	if(isset($_SESSION['foodsafety_superadmin']) && $_SESSION['foodsafety_superadmin']!="")
	{
		if(isset($_POST['button']) && $_POST['button']!="")
		{
			$subject = $_POST['subject'];
			$message = $_POST['message'];
			$hotels = $_POST['inspectors'];
			if(gettype($hotels)=="array")
				$hotels = implode(",", $hotels);
			else
				$hotels = "";
			$flag = 0;

			if($subject == "")
			{
				echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please enter subject</div>";
				$flag = 1;
			}
			if($message == "")
			{
				echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please enter message</div>";
				$flag = 1;
			}
			if($hotels == "")
			{
				echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please select at least one from the dropdown</div>";
				$flag = 1;
			}
			if($flag == 0)
			{
				require('dbconnect.php');
				date_default_timezone_set("Asia/Kolkata"); 
				mysqli_autocommit($conn, false);
				$time = date("Y-m-d H:i:s");
				if($stmt = mysqli_prepare($conn,"INSERT into inspector_messages(subject,message,inspectors,dateofmessage) values(?,?,?,?)"))
				{
					mysqli_stmt_bind_param($stmt,'ssss',$subject,$message,$hotels,$time);
					if(mysqli_stmt_execute($stmt))
						mysqli_commit($conn);
					mysqli_stmt_close($stmt);
					echo "success";
					mysqli_close($conn);
				}
			}
			
		}
		else
			header("location: index");	
	}	
	else
		header("location: index");
?>