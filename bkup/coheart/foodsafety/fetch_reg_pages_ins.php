<?php session_start();

if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
	
	include("config.inc.php");
	
	$item_per_page = 8;
	
	if(isset($_POST["page"])){
		$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
		if(!is_numeric($page_number)){die('Invalid page number!');}
	}
	else
	{
		$page_number = 1;
	}

	$inspector = $_SESSION['foodsafety_inspector']['location']; 

	if(isset($_POST['search']) && $_POST['search']!="")
			$query = "SELECT count(*) from registrations where (name like '%{$_POST['search']}%' or address like '%{$_POST['search']}%' or location like '%{$_POST['search']}%') and location='$inspector'" ;
		else
			$query = "SELECT count(*) from registrations where location='$inspector'";
	//echo $query;

	$stmt = $mysqli_conn->prepare($query);
	$stmt->execute();
	$stmt->bind_result($get_total_rows);
	$stmt->fetch();
	$result1 = $stmt->store_result();
	//echo $get_total_rows;
	$total_pages = ceil($get_total_rows/$item_per_page);
	$page_position = (($page_number-1) * $item_per_page);
	//echo $page_position.$item_per_page;
	
	if(isset($_POST['search']) && $_POST['search']!="")
			$query = "SELECT id,name,type,location,approved from registrations where (name like '%{$_POST['search']}%' or address like '%{$_POST['search']}%' or location like '%{$_POST['search']}%') and location='$inspector' LIMIT $page_position, $item_per_page" ;
		else
			$query = "SELECT id,name,type,location,approved from registrations where location='$inspector' LIMIT $page_position, $item_per_page";
	//echo $query;
	$stmt = $mysqli_conn->prepare($query);
	$stmt->execute();
	$stmt->bind_result($id, $name, $type,$location,$approved);

	$flag = 0;
	date_default_timezone_set('Asia/Kolkata');

	/*echo '<div class="col-sm-12">';
			echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
			echo '</div>';*/

	echo '<div class="reg-contents">';
	echo '<div class="table-responsive">';
	echo '<table class="table table-bordered table-striped table-hover">';
	echo '<th>Name</th>';
	echo '<th>Type</th>';
	echo '<th>Location</th>';
	echo '<th class="text-center">Approved</th>';
	echo '<th class="text-center">Action</th>';

	while($stmt->fetch())
	{
		//$address = nl2br($row['address']);

		echo '<tr>';
		echo '<td>'.$name.'</td>';
		echo '<td>'.$type.'</td>';
		echo '<td>'.$location.'</td>';
		if($approved=="")
		echo '<td class="text-center"><span class="glyphicon glyphicon-remove" aria-hidden="true" style="color: red;"></span></td>';
		else
		echo '<td class="text-center"><span class="glyphicon glyphicon-ok" aria-hidden="true" style="color: green;"></span></td>';
		echo '<td class="text-center"><a href="view-details?id='.$id.'" class="btn btn-info btn-xs" target="_blank">View</a></td>';
		echo '</tr>';
		$flag = 1;
	}

	echo '</table>';  
	echo '</div>';
	echo '</div>';

	if($flag == 0)
	echo "<div class='alert alert-info text-danger'><div class='row'>
					<div class='col-xs-10'>Sorry, no matches found! </div><div class='col-xs-2 text-right'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span></div></div></div>"; 
		
	echo '<div class="text-center">';
			echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
			echo '</div>';
}

################ pagination function #########################################
	function paginate_function($item_per_page, $current_page, $total_records, $total_pages)
	{
	    $pagination = '';
	    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
	        $pagination .= '<ul class="pagination pagination-2 dark">';
	        
	        $right_links    = $current_page + 3; 
	        $previous       = $current_page - 1; //previous link 
	        $next           = $current_page + 1; //next link
	        $first_link     = true; //boolean var to decide our first link
	        
	        if($current_page > 1){
				$previous_link = ($previous==0)?1:$previous;
	            $pagination .= '<li class="first"><a href="#" data-page="1" title="First"><i class="fa fa-angle-double-left"></i></a></li>'; //first link
	            $pagination .= '<li><a href="#" data-page="'.$previous_link.'" title="Previous"><i class="fa fa-angle-left"></i></a></li>'; //previous link
	                for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
	                    if($i > 0){
	                        $pagination .= '<li><a href="#" data-page="'.$i.'" title="Page'.$i.'">'.$i.'</a></li>';
	                    }
	                }   
	            $first_link = false; //set first link to false
	        }
	        
	        if($first_link){ //if current active page is first link
	            $pagination .= '<li class="first active"><a href="#">'.$current_page.'</a></li>';
	        }elseif($current_page == $total_pages){ //if it's the last active link
	            $pagination .= '<li class="last active"><a href="#">'.$current_page.'</a></li>';
	        }else{ //regular current link
	            $pagination .= '<li class="active"><a href="#">'.$current_page.'</a></li>';
	        }
	                
	        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
	            if($i<=$total_pages){
	                $pagination .= '<li><a href="#" data-page="'.$i.'" title="Page '.$i.'">'.$i.'</a></li>';
	            }
	        }
	        if($current_page < $total_pages){ 
					$next_link = ($i > $total_pages)? $total_pages : $i;
	                $pagination .= '<li><a href="#" data-page="'.$next_link.'" title="Next"><i class="fa fa-angle-right"></i></a></li>'; //next link
	                $pagination .= '<li class="last"><a href="#" data-page="'.$total_pages.'" title="Last"><i class="fa fa-angle-double-right"></i></a></li>'; //last link
	        }
	        
	        $pagination .= '</ul>'; 
	    }
	    return $pagination; //return pagination links
	}
?>

