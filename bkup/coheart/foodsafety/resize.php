<?php session_start();
function resize($width, $height){
  /* Get original image x y*/
  list($w, $h) = getimagesize($_FILES['image_data']['tmp_name']);
  /* calculate new image size with ratio */
  $ratio = max($width/$w, $height/$h);
  $h = ceil($height / $ratio);
  $x = ($w - $width / $ratio) / 2;
  $w = ceil($width / $ratio);
 $ext = strtolower(pathinfo($_FILES['image_data']['name'], PATHINFO_EXTENSION));
    $nameoffile = $_SESSION['foodsafety_hoteladmin']['username'].".".$ext;
  /* new file name */
  $path = 'uploads/profilethumb/'.$nameoffile;
  /* read binary data from image file */
  $imgString = file_get_contents($_FILES['image_data']['tmp_name']);
  /* create image from string */
  $image = imagecreatefromstring($imgString);
  $tmp = imagecreatetruecolor($width, $height);
  imagecopyresampled($tmp, $image,
    0, 0,
    $x, 0,
    $width, $height,
    $w, $h);
  /* Save image */
  switch ($_FILES['image_data']['type']) {
    case 'image/jpeg':
      imagejpeg($tmp, $path, 100);
      break;
    case 'image/png':
      imagepng($tmp, $path, 0);
      break;
    case 'image/gif':
      imagegif($tmp, $path);
      break;
    default:
      exit;
      break;
  }
  return $path;
  imagedestroy($image);
  imagedestroy($tmp);
}?>