<?php
	session_start();
	if(isset($_SESSION['foodsafety_superadmin']) && $_SESSION['foodsafety_superadmin']!="")
	{
?>
<!DOCTYPE html>
<html lang="en-us">
<head>

	<meta charset="utf-8" >
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Food Safety Mission | Complaints</title>

	<meta name="author" content="abusinesstheme">
	<meta name="description" content="Palas is a Business HTML Template developed with the the latest HTML5 and CSS3 technologies.">

  	<!-- CSS files -->
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic|Montserrat:400,700' rel='stylesheet'>
	<link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="plugins/elegant_font/html_css/style.css">
	
	<link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
	
	<!-- Main CSS file -->
	<link rel="stylesheet" href="css/style.css">
	
</head>
<body>

<?php require('preloader.php'); ?>

<!-- Global Wrapper -->
<div id="wrapper">

	<div class="h-wrapper">

		<!-- Top Bar -->
		<div class="topbar hidden-xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<ul class="top-menu">
							<li><a href="http://coheart.ac.in" target="_blank">COHEART , Kerala Veterinary & Animal Science University</a></li>
						</ul>
					</div>
					<!-- This column is hidden on mobiles -->
					<div class="col-sm-6">
						<div class="pull-right top-links">
							<a href="logout_superadmin"><i class="fa fa-user"></i> &nbsp;Logout</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Header -->
		<header class="header-wrapper header-transparent with-topbar">
			<div class="main-header">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-4">
							<!-- Logo - Read documentation to see how to change the logo-->
							<a href="index" class="logo"></a>
						</div>
						<div class="col-sm-12 col-md-8">
							<nav class="navbar-right">
								<ul class="menu">
                                    <!-- Toggle Menu - For Mobile Devices -->
                                    <li class="toggle-menu"><i class="fa icon_menu"></i></li> 
                                    
                                    <li class="first">
                                        <a href="index">Home</a>
                                    </li> <!-- END Home -->
                                    <li>
                                        <a href="superadmin">Admin Page</a>
                                    </li> <!-- END Admin Page -->
                                    <li class="">
                                        <a href="restaurants">Restaurants</a>
                                    </li> <!-- END Restaurants -->
                                    <li>
                                        <a href="news">News</a>
                                    </li> <!-- END News -->
                                    <li>
                                        <a href="downloads">Downloads</a>
                                    </li> <!-- END Downloads -->
                                    <li>
                                        <a href="contact" class="last">Contact</a>
                                    </li> <!-- END Contact -->
                                    <li class="hidden-sm hidden-md hidden-lg">
                                        <a href="logout_superadmin"><i class="fa fa-user"></i> &nbsp;Logout</a>
                                    </li> <!-- END logout -->
                                </ul>
							</nav>
						</div>
					</div>
				</div> <!-- END Container -->
			</div> <!-- END Main Header -->
		</header>

	</div>

	<!-- Do not remove this class -->
	<div class="push-top"></div>

	<section class="section bg-img bg-admin-panel">
        <div class="bg-overlay op6"></div>
        <div class="container">
            <div class="row mt50 mb40">
                <div class="col-sm-12">
                    <h1 class="title-large title-larger color-on-img mb20 mt50">Admin Panel <span class="fs-small">( Complaints )</span></h1>
                    <div class="br-bottom mb0"></div>
                </div>
            </div>
        </div>
    </section>

	<!--<div class="search-form">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="pull-right">
						<form action="#" role="form" class="pull-right">
							<input type="text" class="input-search" placeholder="Search here" id="filter">
							<button type="submit" class="hidden"></button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>-->

	<section class="section-page">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-md-9 col-md-push-3 col-sm-push-4 space-left push-off">
					<div class="row blog-list remove-style" id="news-results">
					    
					</div> <!-- END Blog Posts -->
					
				</div>
				<div class="col-sm-4 col-md-3 col-md-pull-9 col-sm-pull-8 pull-off">
					<aside class="sidebar">
						<div class="sidebar-widget">
							<h3 class="sidebar-title br-bottom">Above and beyond</h3>
							<p>Lorem ipsum dolor sit amet, consect adipisicing elit. Impedit, recusandae corrupti voluptas atque voluptatibust!</p>
						</div>
						<div class="sidebar-widget">
							<h3 class="sidebar-title br-bottom">Get social</h3>
							<ul class="sidebar-socials">
								<li><a href="#"><i class="fa fa-twitter"></i> Follow us</a> <span>455 followers</span></li>
								<li><a href="#"><i class="fa fa-facebook"></i> Like us</a> <span>505 likes</span></li>
								<li><a href="#"><i class="fa fa-google-plus"></i> Circle us</a> <span>355 friends</span></li>
								<li><a href="#"><i class="fa fa-envelope"></i> Subscribe</a> <span>250 subscribers</span></li>
							</ul>
						</div>
						<!--<div class="sidebar-widget">
							<h3 class="sidebar-title br-bottom">Latest Tweets</h3>
							<div class="sidebar-tweet clearfix mb0">
								<i class="fa fa-twitter"></i>
								<p class="tweet-content"><a href="#" class="tweet-user">@abusinesstheme</a> <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span> <small>22 hours ago</small></p>
							</div>
						</div>-->
						
						<div class="sidebar-widget">
							<h3 class="sidebar-title br-bottom">Categories</h3>
							<ul class="sidebar-categories">
								<li><a href="#"><i class="fa fa-chevron-right"></i> Web Design</a></li>
								<li><a href="#"><i class="fa fa-chevron-right"></i> Software Engineering</a></li>
								<li><a href="#"><i class="fa fa-chevron-right"></i> Web Graphic</a></li>
								<li><a href="#"><i class="fa fa-chevron-right"></i> Web Programming</a></li>
								<li><a href="#"><i class="fa fa-chevron-right"></i> Software Design</a></li>
							</ul>
						</div>
						
					</aside>
				</div>
			</div>
		</div>
	</section> <!-- END Blog Page-->

	<?php require('footer.php');?>
	
</div> <!-- END Global Wrapper -->




	<!-- Javascript files -->
	<script src="plugins/jquery/jquery-2.1.0.min.js"></script>
	<script src="plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="plugins/jquery.appear.js"></script>
	<script src="plugins/retina.min.js"></script>
	<script src="plugins/stellar.min.js"></script>
	<script src="plugins/sticky.min.js"></script>
	
	<script src="plugins/sharrre/jquery.sharrre.min.js"></script>
	<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="plugins/jquery.zoom.min.js"></script>
	<script src="plugins/raty/jquery.raty.min.js"></script>
	
	


	<!-- Main javascript file -->
	<script src="js/script.js"></script>
	<script src="js/complaints.js"></script>

	

</body>
</html>
<?php
	}
	else
		header("location: index");
?>