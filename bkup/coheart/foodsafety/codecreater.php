<?php
include 'functions.php';
if(!isset($_SESSION))
{
	session_start();
	//header('Cache-control: private');
}
$width = 140;
$height = 75;
$image = imagecreate($width, $height);
$bg_color = imagecolorallocate($image, 232, 232, 232);
imagefilledrectangle($image, 0, 0, $width, $height, $bg_color);
$text = random_text(6);
$fg_color = imagecolorallocate($image, 0, 0, 0);
imagettftext($image, 22, 0, 23, 48, $fg_color, 'css/fonts/sourcesanspro-regular-webfont.ttf', $text);
$_SESSION['c'] = $text;
header('Content-type: image/png');
imagepng($image);
imagedestroy($image);
?>