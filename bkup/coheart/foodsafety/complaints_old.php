<?php
	session_start();
	if((isset($_SESSION['foodsafety_superadmin']) && $_SESSION['foodsafety_superadmin']!="") || (isset($_SESSION['foodsafety_inspector']) && $_SESSION['foodsafety_inspector']!=""))
	{
?>
	<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Food Safety - Super Admin</title>

    <link href="rateit/src/rateit.css" rel="stylesheet" type="text/css">    
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/business-casual.css" rel="stylesheet">
	<link href="css/flat-ui.css" rel="stylesheet"> 
	  
    <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css"> 
<link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="plugins/elegant_font/html_css/style.css">

	<!-- Main CSS file -->
	<link rel="stylesheet" href="css/style.css">
	</head>

<body>

    <div class="topbar hidden-xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<ul class="top-menu">
							<li><a href="http://coheart.ac.in" target="_blank">COHEART , Kerala Veterinary & Animal Science University</a></li>
						</ul>
					</div>
					<!-- This column is hidden on mobiles -->
					<div class="col-sm-6">
						<div class="pull-right top-links">
							Welcome Superadmin
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Header -->
		<header class="brand">
			<div class="main-header">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-4">
							<!-- Logo - Read documentation to see how to change the logo-->
							<a href="index" class="logo"></a>
						</div>
						<div class="col-sm-12 col-md-8">
							<nav class="navbar-right">
								<ul class="menu">
									<!-- Toggle Menu - For Mobile Devices -->
									<li class="toggle-menu"><i class="fa icon_menu"></i></li> 
									
									<li class="first">
										<a href="index">Home</a>
									</li> <!-- END Home -->
									<li>
										<a href="superadmin">Admin Page</a>
									</li> 
									<li>
										<a href="restaurants">Restaurants</a>
									</li> <!-- END Restaurants -->
									<li>
										<a href="news">News</a>
									</li> <!-- END News -->
									<li>
										<a href="downloads">Downloads</a>
									</li> <!-- END Downloads -->
									<li>
										<a href="contact" class="last">Contact</a>
									</li> <!-- END Contact -->
									
								</ul>
							</nav>
						</div>
					</div>
				</div> <!-- END Container -->
			<!-- END Main Header -->
		</header>


    <div class="container">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h2 class="panel-title text-center">List of Complaints</h2>
          </div>
          <div class="panel-body">
            <div class="table-responsive">
              <table class="table table-striped table-bordered">
              <?php
                require('dbconnect.php');
                $query = mysqli_query($conn, "SELECT * from complaints order by timeofcomp desc");
                while($data = mysqli_fetch_assoc($query))
                {
                    $query2 = mysqli_query($conn, "SELECT * from registrations where id={$data['hotel']}");
                    $data2 = mysqli_fetch_assoc($query2);
                    echo "<tr>";
                    echo "<th>";
                    echo "Complaint against {$data2['name']}, {$data['location']}";
                    echo "</th>";
                    echo "</tr>";
                    echo "<tr style='font-size: 13px;'>";
                    echo "<td><br>";
                    //date check
                    date_default_timezone_set("Asia/Kolkata"); 
                    $date = strtotime($data['timeofcomp']);
                    $date = strtotime(date("Y-m-d", $date));
                    $current = strtotime(date("Y-m-d"));
                    $datediff = $date - $current;
                    $differance = floor($datediff/(60*60*24));
                    $date = strtotime($data['timeofcomp']);
                    //end of date check
                    if($differance == 0)
                        echo "<span class='pull-right'>Today, ".date("g:i a", $date)."</span>";
                    else
                        echo "<span class='pull-right'>".date("F j, Y, g:i a", $date)."</span>"; 
                    echo "<b>Customer Name</b>: {$data['name']}<br>";
                    echo "<b>Contact Number</b>: {$data['phone']}<br>";
                    echo "<b>Bill Number</b>: {$data['bill']}<br>";
                    echo "<b>Location</b>: {$data['location']}<br><br>";
                    echo "Message:<br><p style='margin: 2%; font-size: 13px;'> {$data['complaint']}</p><br>";
                    echo "</td>";
                    echo "</tr>";
                }
              ?>   
              </table>
            </div>
          </div>
        </div>
    </div>
    <!-- /.container -->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>  
    <script src="rateit/src/jquery.rateit.js" type="text/javascript"></script>
</body>

</html>

<?php
	}
	else
		header("location: index");
?>