<?php
						if(isset($_SESSION['foodsafety_superadmin']) && $_SESSION['foodsafety_superadmin']!="")
							echo '<i class="fa fa-user"></i> Superadmin - <a href="logout_superadmin">Logout</a>';
						elseif(isset($_SESSION['foodsafety_hoteladmin']) && $_SESSION['foodsafety_hoteladmin']!="")
							echo '<i class="fa fa-user"></i> '.ucwords($_SESSION['foodsafety_hoteladmin']['name']). ' - <a href="logout_hoteladmin">Logout</a>';
						elseif(isset($_SESSION['foodsafety_inspector']) && $_SESSION['foodsafety_inspector']!="")
							echo '<i class="fa fa-user"></i> '.ucwords($_SESSION['foodsafety_inspector']['name']). ' - <a href="logout_inspector">Logout</a>';
						else { ?>
							<a href="login">Sign In</a>
							<a href="">|</a>
							<a href="register">Sign Up</a>
						<?php } ?>