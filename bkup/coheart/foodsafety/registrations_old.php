<?php
    session_start();
    if(isset($_SESSION['foodsafety_superadmin']) && $_SESSION['foodsafety_superadmin']!="")
    {
?>
    <!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Food Safety - Registrations</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/business-casual.css" rel="stylesheet">
    <link href="css/flat-ui.css" rel="stylesheet">
    		<!-- CSS files -->
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic|Montserrat:400,700' rel='stylesheet'>
	<link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="plugins/elegant_font/html_css/style.css">

	<!-- Main CSS file -->
	<link rel="stylesheet" href="css/style.css">
    
</head>

<body>
    <div class="loading-div">
            <div class="bubblingG">
                <span id="bubblingG_1">
                </span>
                <span id="bubblingG_2">
                </span>
                <span id="bubblingG_3">
                </span>
                </div>
    </div>
  

   <div class="topbar hidden-xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<ul class="top-menu">
							<li><a href="http://coheart.ac.in" target="_blank">COHEART , Kerala Veterinary & Animal Science University</a></li>
						</ul>
					</div>
					<!-- This column is hidden on mobiles -->
					<div class="col-sm-6">
						<div class="pull-right top-links">
							Welcome Superadmin
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Header -->
		<header class="brand">
			<div class="main-header">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-4">
							<!-- Logo - Read documentation to see how to change the logo-->
							<a href="index" class="logo"></a>
						</div>
						<div class="col-sm-12 col-md-8">
							<nav class="navbar-right">
								<ul class="menu">
									<!-- Toggle Menu - For Mobile Devices -->
									<li class="toggle-menu"><i class="fa icon_menu"></i></li> 
									
									<li class="first">
										<a href="index">Home</a>
									</li> 
								<li>
										<a href="superadmin">Admin Page</a>
									</li> <!-- END Restaurants -->
									<li>
										<a href="restaurants">Restaurants</a>
									</li> <!-- END Restaurants -->
									<li>
										<a href="news">News</a>
									</li> <!-- END News -->
									<li>
										<a href="downloads">Downloads</a>
									</li> <!-- END Downloads -->
									<li>
										<a href="contact" class="last">Contact</a>
									</li> <!-- END Contact -->
									
								</ul>
							</nav>
						</div>
					</div>
				</div> <!-- END Container -->
			<!-- END Main Header -->
		</header>

	</div>

    <div class="container registrations-page">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title text-center">Registrations</h3>
          </div>
          <div class="panel-body">
            <div id="results"></div>
          </div>
        </div>
        
    </div>
    <!-- /.container -->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>  
    <script type="text/javascript">
    $(document).ready(function() {
        $("#results" ).load( "fetch_reg_pages.php");
        
            $("#results").on( "click", ".pagination a", function (e){
            e.preventDefault();
            $(".loading-div").fadeIn();
            var page = $(this).attr("data-page");
            $("#results").load("fetch_reg_pages.php",{"page":page}, function(){
                $(".loading-div").fadeOut();
            });
            
        });
    });
    </script>
</body>

</html>

<?php
    }
    else
        header("location: index");
?>