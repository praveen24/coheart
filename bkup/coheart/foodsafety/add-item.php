<?php 
	session_start();
	if(isset($_SESSION['foodsafety_hoteladmin']) && $_SESSION['foodsafety_hoteladmin']!="")
	{
		if(isset($_POST['save']) && $_POST['save']!="")
		{
			$id = $_SESSION['foodsafety_hoteladmin']['id'];
			$item = $_POST['item'];
			$rate = $_POST['rate']; 
			$description = $_POST['description']; 
			$flag = 0;
			if($item=="")
			{		
				echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please enter item name</div>";
				$flag = 1;
			}
			if($rate=="")
			{		
				echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please enter rate</div>";
				$flag = 1;
			}
			if($description=="")
			{		
				echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please enter description</div>";
				$flag = 1;
			}

			if($flag == 0)
			{
				require('dbconnect.php');
				mysqli_autocommit($conn, false);
				if($stmt = mysqli_prepare($conn,"INSERT into menu(item,hotel,rate,description) values(?,?,?,?)"))
				{
					mysqli_stmt_bind_param($stmt,'sids',$item,$id,$rate,$description);
					if(mysqli_stmt_execute($stmt))
						mysqli_commit($conn);
					mysqli_stmt_close($stmt);
					echo "success";
					mysqli_close($conn);
				}
			}
		}
		else
			header("location: index.html");
	}	
	else
		header("location: index.html");
?>