<?php
	if(isset($_POST['voter']) && $_POST['voter']!="")
	{
		$id = $_POST['id'];
		$rating = $_POST['value'];
		$voter = $_POST['voter'];
		
		require('dbconnect.php');
		mysqli_autocommit($conn, false);

		$query = mysqli_query($conn,"SELECT id from rating where voter='$voter' and hotel=$id");
		if($data = mysqli_fetch_assoc($query))
		{
			if($stmt = mysqli_prepare($conn,"UPDATE rating set rating=? where voter=? and hotel=?"))
			{
				mysqli_stmt_bind_param($stmt,'dss',$rating,$voter,$id);
				mysqli_stmt_execute($stmt);
				mysqli_stmt_close($stmt);
				echo "Rating recorded";
			}	
		}
		else
		{
			if($stmt = mysqli_prepare($conn,"INSERT into rating(hotel,rating,voter) values(?,?,?)"))
			{
				mysqli_stmt_bind_param($stmt,'sds',$id,$rating,$voter);
				mysqli_stmt_execute($stmt);
				mysqli_stmt_close($stmt);
				echo "Rating recorded";
			}
		}
		mysqli_commit($conn);
		mysqli_close($conn);
	}
	else
		header("location: index.html")
?>