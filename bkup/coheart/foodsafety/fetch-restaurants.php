<?php

	if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
	{
		include("config.inc.php");
		//Get page number from Ajax POST
		if(isset($_POST["page"]))
		{
			$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
			if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
		}
		else
		{
			$page_number = 1; //if there's no page number, set it to 1
		}

		$page_position = (($page_number-1) * $item_per_page);

		if(isset($_POST['location'])) $location = $_POST['location'];
		if(isset($_POST['type'])) $type = $_POST['type'];
		if(isset($_POST['searchName'])) $searchName = $_POST['searchName'];
		if(isset($_POST['searchLocation'])) $searchLocation = $_POST['searchLocation'];

		if((isset($location) && $location!="") && (isset($type) && $type!=""))
		{
			$flag = 0;
			$query_num = "SELECT count(*) from registrations where (location ='{$location[0]}'";
			$query = "SELECT * from registrations where (location ='{$location[0]}'";
			if(count($location) > 1)
				for($i=1;$i<count($location);$i++)
				{
					$query_num .=" or location='{$location[$i]}'";
					$query .=" or location='{$location[$i]}'";
				}
			$query_num .=") and type='$type'";
			$query .=") and type='$type' LIMIT $page_position, $item_per_page";
			//echo $query_num.'<br>'.$query;
			
		}
		else if((isset($location) && $location!=""))
		{
			$flag = 0;
			$query_num = "SELECT count(*) from registrations where (location ='{$location[0]}'";
			$query = "SELECT * from registrations where (location ='{$location[0]}'";
			if(count($location) > 1)
				for($i=1;$i<count($location);$i++)
				{
					$query_num .=" or location='{$location[$i]}'";
					$query .=" or location='{$location[$i]}'";
				}
			$query_num .=")";
			$query .=") LIMIT $page_position, $item_per_page";
			//echo $query_num.'<br>'.$query;
			
		}
		else if((isset($type) && $type!=""))
		{
			$flag = 0;
			$query_num = "SELECT count(*) from registrations where type ='$type'";
			$query = "SELECT * from registrations where type ='$type' LIMIT $page_position, $item_per_page";
			//echo $query_num.'<br>'.$query;
		}
		else if( (isset($searchName) && $searchName!="") &&  (isset($searchLocation) && $searchLocation!="") )
		{
			$flag = 0;
			$query_num = "SELECT count(*) from registrations where name like '%$searchName%' and location like '%$searchLocation%'";
			$query = "SELECT * from registrations where name like '%$searchName%' and location like '%$searchLocation%' LIMIT $page_position, $item_per_page";
			//echo $query_num.'<br>'.$query;
		}
		else if((isset($searchName) && $searchName!=""))
		{
			$flag = 0;
			$query_num = "SELECT count(*) from registrations where name like '%$searchName%'";
			$query = "SELECT * from registrations where name like '%$searchName%' LIMIT $page_position, $item_per_page";
			//echo $query_num.'<br>'.$query;
		}
		else if((isset($searchLocation) && $searchLocation!=""))
		{
			$flag = 0;
			$query_num = "SELECT count(*) from registrations where location like '%$searchLocation%' or address like '%$searchLocation%'";
			$query = "SELECT * from registrations where location like '%$searchLocation%' or address like '%$searchLocation%' LIMIT $page_position, $item_per_page";
			//echo $query_num.'<br>'.$query;
		}
		else{
			$query_num = "SELECT count(*) from registrations";
			$query = "SELECT * from registrations LIMIT $page_position, $item_per_page";
		}

		if(isset($query_num))
		{
			$count = $mysqli_conn->query($query_num);

			$get_total_rows = $count->fetch_row(); 

			$total_pages = ceil($get_total_rows[0]/$item_per_page);
		}

		$flag = 0;
		
		date_default_timezone_set('Asia/Kolkata');
		//echo $query;
		if(isset($query))
		{
			$results = $mysqli_conn->query($query);

			echo "<div class='row'>";

			while($data = $results->fetch_assoc())
			{
				echo "<div class='col-sm-6 col-md-4'>
								<div class='shop-product'>
									<a href='restaurant?id={$data['id']}' class='shop-image'>";
					if($data['image']=="")
	                            echo "<img src='images/restaurant_thumb.png' class='img-responsive img-rounded'>";
	                        else 
	                            echo "<img src='uploads/profilethumb/{$data['image']}' class='img-responsive img-rounded'>";
					echo "</a>
									<h4 class='shop-description'><a href='restaurant?id={$data['id']}'>{$data['name']}</a></h4>
									<div class='row'>
										<div class='col-sm-6'>
											<div class='text-muted capitalize'>{$data['type']}</div>
											<div class='text-muted capitalize'>{$data['location']}</div>
										</div>
										<div class='col-sm-6'>";
						
						$query2 = $mysqli_conn->query("SELECT * from rating where hotel={$data['id']}");
						$ratingFlag = 0;
						$count = mysqli_num_rows($query2);
                        $rating = 0;
                        while($data2 = mysqli_fetch_assoc($query2))
                        {
                            $rating += $data2['rating'];
                            $ratingFlag = 1;
                        }
                        if($count != 0)
                            $rating = round($rating/$count,1);
                        if($ratingFlag == 1)
                        {
                            echo "<div class='text-right'><span class='rating-system rate-product' data-score='$rating'></span></div>";
                            echo "<div class='text-right text-muted'>($rating/5)</div>";
                        }
                        elseif($ratingFlag == 0)
                        {
                            echo "<div class='text-right'><span class='rating-system rate-product' data-score='$rating'></span></div>";
                            echo "<div class='text-right text-muted'>($rating/5)</div>";
                        }
					
					echo "					</div>
									</div>
									
								</div>
							</div>";

				$flag = 1;
			}

			echo '</div>';

			if($flag == 0)
			echo "<div class='alert alert-info text-danger'><div class='row'>
					<div class='col-xs-10'>Sorry, no matches found! </div><div class='col-xs-2 text-right'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span></div></div></div>"; 

			echo '<div class="col-sm-12">';
			echo paginate_function($item_per_page, $page_number, $get_total_rows[0], $total_pages);
			echo '</div>';
		}
		
	}

	################ pagination function #########################################
	function paginate_function($item_per_page, $current_page, $total_records, $total_pages)
	{
	    $pagination = '';
	    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
	        $pagination .= '<ul class="pagination pagination-2 dark">';
	        
	        $right_links    = $current_page + 3; 
	        $previous       = $current_page - 1; //previous link 
	        $next           = $current_page + 1; //next link
	        $first_link     = true; //boolean var to decide our first link
	        
	        if($current_page > 1){
				$previous_link = ($previous==0)?1:$previous;
	            $pagination .= '<li class="first"><a href="#" data-page="1" title="First"><i class="fa fa-angle-double-left"></i></a></li>'; //first link
	            $pagination .= '<li><a href="#" data-page="'.$previous_link.'" title="Previous"><i class="fa fa-angle-left"></i></a></li>'; //previous link
	                for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
	                    if($i > 0){
	                        $pagination .= '<li><a href="#" data-page="'.$i.'" title="Page'.$i.'">'.$i.'</a></li>';
	                    }
	                }   
	            $first_link = false; //set first link to false
	        }
	        
	        if($first_link){ //if current active page is first link
	            $pagination .= '<li class="first active"><a href="#">'.$current_page.'</a></li>';
	        }elseif($current_page == $total_pages){ //if it's the last active link
	            $pagination .= '<li class="last active"><a href="#">'.$current_page.'</a></li>';
	        }else{ //regular current link
	            $pagination .= '<li class="active"><a href="#">'.$current_page.'</a></li>';
	        }
	                
	        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
	            if($i<=$total_pages){
	                $pagination .= '<li><a href="#" data-page="'.$i.'" title="Page '.$i.'">'.$i.'</a></li>';
	            }
	        }
	        if($current_page < $total_pages){ 
					$next_link = ($i > $total_pages)? $total_pages : $i;
	                $pagination .= '<li><a href="#" data-page="'.$next_link.'" title="Next"><i class="fa fa-angle-right"></i></a></li>'; //next link
	                $pagination .= '<li class="last"><a href="#" data-page="'.$total_pages.'" title="Last"><i class="fa fa-angle-double-right"></i></a></li>'; //last link
	        }
	        
	        $pagination .= '</ul>'; 
	    }
	    return $pagination; //return pagination links
	}

?>