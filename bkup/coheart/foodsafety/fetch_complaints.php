<?php

if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
	
	include("config.inc.php");
	
	$item_per_page = 4;
	
	if(isset($_POST["page"])){
		$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
		if(!is_numeric($page_number)){die('Invalid page number!');}
	}
	else
	{
		$page_number = 1;
	}

	if(isset($_POST['search']) && $_POST['search']!="")
			$query = "SELECT count(*) from complaints where location like '%{$_POST['search']}%' or hotel like '%{$_POST['search']}%'" ;
		else
			$query = "SELECT count(*) from complaints";

	$stmt = $mysqli_conn->prepare($query);
	$stmt->execute();
	$stmt->bind_result($get_total_rows);
	$stmt->fetch();
	$result1 = $stmt->store_result();
	//echo $get_total_rows;
	$total_pages = ceil($get_total_rows/$item_per_page);
	$page_position = (($page_number-1) * $item_per_page);
	//echo $page_position.$item_per_page;
	
	if(isset($_POST['search']) && $_POST['search']!="")
			$query = "SELECT id,hotel,name,location,timeofcomp,phone,bill,complaint from complaints where location like '%{$_POST['search']}%' or hotel like '%{$_POST['search']}%' order by timeofcomp desc LIMIT $page_position, $item_per_page" ;
		else
			$query = "SELECT id,hotel,name,location,timeofcomp,phone,bill,complaint from complaints order by timeofcomp desc LIMIT $page_position, $item_per_page";
	//echo $query;
	$stmt = $mysqli_conn->prepare($query);
	$stmt->execute();
	$stmt->bind_result($id,$hotel,$name,$location,$timeofcomp,$phone,$bill,$complaint);

	$flag = 0;
	date_default_timezone_set('Asia/Kolkata');
	require('dbconnect.php');
	while($stmt->fetch())
	{
		$time = date_create($timeofcomp);
		$date = date_format($time, "F j, Y, g:i a");

		$query2 = mysqli_query($conn, "SELECT * from registrations where id=$hotel");
		echo mysql_error();
        $data2 = mysqli_fetch_assoc($query2);
		echo '<div class="col-sm-12">
							<div class="row blog-item">
								<div class="col-sm-12 blog-caption">';
		echo '<h3 class="post-title">Complaint against '.ucwords($data2['name']).','.$location.'</h3>';
		echo '<div class="sub-post-title">
										<span>'.$date.'</span>
									</div>';
		echo '<div class="news-content">';
		echo '<div class="row">';
		echo '<div class="col-md-4">';
		echo '<div>Customer Name: '.ucwords($name).'</div>';
		echo '<div>Contact Number: '.$phone.'</div>';
		echo '<div>Bill Number: '.$bill.'</div>';
		echo '<div>Location: '.ucwords($location).'</div>';
		echo '</div>';
		echo '<div class="col-md-8">';
		echo '<div>Message</div>';
		echo '<div class="remove-style">'.ucfirst($complaint).'</div>';
		echo '</div>';
		echo '</div>';
		echo '</div>';
		
		echo '</div>
							</div>
	                    </div> <!-- END Blog Item -->';
	    $flag = 1;	
	}

	if($flag == 0)
	echo "<div class='alert alert-info text-danger'><div class='row'>
					<div class='col-xs-10'>Sorry, no matches found! </div><div class='col-xs-2 text-right'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span></div></div></div>"; 
		
	echo '<div class="col-sm-12">';
			echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
			echo '</div>';
}

################ pagination function #########################################
	function paginate_function($item_per_page, $current_page, $total_records, $total_pages)
	{
	    $pagination = '';
	    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
	        $pagination .= '<ul class="pagination pagination-2 dark">';
	        
	        $right_links    = $current_page + 3; 
	        $previous       = $current_page - 1; //previous link 
	        $next           = $current_page + 1; //next link
	        $first_link     = true; //boolean var to decide our first link
	        
	        if($current_page > 1){
				$previous_link = ($previous==0)?1:$previous;
	            $pagination .= '<li class="first"><a href="#" data-page="1" title="First"><i class="fa fa-angle-double-left"></i></a></li>'; //first link
	            $pagination .= '<li><a href="#" data-page="'.$previous_link.'" title="Previous"><i class="fa fa-angle-left"></i></a></li>'; //previous link
	                for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
	                    if($i > 0){
	                        $pagination .= '<li><a href="#" data-page="'.$i.'" title="Page'.$i.'">'.$i.'</a></li>';
	                    }
	                }   
	            $first_link = false; //set first link to false
	        }
	        
	        if($first_link){ //if current active page is first link
	            $pagination .= '<li class="first active"><a href="#">'.$current_page.'</a></li>';
	        }elseif($current_page == $total_pages){ //if it's the last active link
	            $pagination .= '<li class="last active"><a href="#">'.$current_page.'</a></li>';
	        }else{ //regular current link
	            $pagination .= '<li class="active"><a href="#">'.$current_page.'</a></li>';
	        }
	                
	        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
	            if($i<=$total_pages){
	                $pagination .= '<li><a href="#" data-page="'.$i.'" title="Page '.$i.'">'.$i.'</a></li>';
	            }
	        }
	        if($current_page < $total_pages){ 
					$next_link = ($i > $total_pages)? $total_pages : $i;
	                $pagination .= '<li><a href="#" data-page="'.$next_link.'" title="Next"><i class="fa fa-angle-right"></i></a></li>'; //next link
	                $pagination .= '<li class="last"><a href="#" data-page="'.$total_pages.'" title="Last"><i class="fa fa-angle-double-right"></i></a></li>'; //last link
	        }
	        
	        $pagination .= '</ul>'; 
	    }
	    return $pagination; //return pagination links
	}
?>

