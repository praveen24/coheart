<?php
	session_start();
	if(isset($_SESSION['foodsafety_superadmin']) && $_SESSION['foodsafety_superadmin']!="")
	{
?>
<!DOCTYPE html>
<html lang="en-us">
<head>

    <meta charset="utf-8" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Food Safety - Health Inspectors</title>

    <meta name="author" content="abusinesstheme">
    <meta name="description" content="Palas is a Business HTML Template developed with the the latest HTML5 and CSS3 technologies.">

    <!-- CSS files -->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic|Montserrat:400,700' rel='stylesheet'>
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="plugins/elegant_font/html_css/style.css">
    
    <link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
    
    <!-- Main CSS file -->
    <link rel="stylesheet" href="css/style.css">
    
</head>
<body>

<?php require('preloader.php'); ?>

<!-- Global Wrapper -->
<div id="wrapper">

    <div class="h-wrapper">

        <!-- Top Bar -->
        <div class="topbar hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <ul class="top-menu">
                            <li><a href="http://coheart.ac.in" target="_blank">COHEART , Kerala Veterinary & Animal Science University</a></li>
                        </ul>
                    </div>
                    <!-- This column is hidden on mobiles -->
                    <div class="col-sm-6">
                        <div class="pull-right top-links">
                            <a href="logout_superadmin"><i class="fa fa-user"></i> &nbsp;Logout</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Header -->
        <header class="header-wrapper header-transparent with-topbar">
            <div class="main-header">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <!-- Logo - Read documentation to see how to change the logo-->
                            <a href="index" class="logo"></a>
                        </div>
                        <div class="col-sm-12 col-md-8">
                            <nav class="navbar-right">
                                <ul class="menu">
                                    <!-- Toggle Menu - For Mobile Devices -->
                                    <li class="toggle-menu"><i class="fa icon_menu"></i></li> 
                                    
                                    <li class="first">
                                        <a href="index">Home</a>
                                    </li> <!-- END Home -->
                                    <li>
                                        <a href="superadmin">Admin Page</a>
                                    </li> <!-- END Admin Page -->
                                    <li class="">
                                        <a href="restaurants">Restaurants</a>
                                    </li> <!-- END Restaurants -->
                                    <li>
                                        <a href="news">News</a>
                                    </li> <!-- END News -->
                                    <li>
                                        <a href="downloads">Downloads</a>
                                    </li> <!-- END Downloads -->
                                    <li>
                                        <a href="contact" class="last">Contact</a>
                                    </li> <!-- END Contact -->
                                    <li class="hidden-sm hidden-md hidden-lg">
                                        <a href="logout_superadmin"><i class="fa fa-user"></i> &nbsp;Logout</a>
                                    </li> <!-- END logout -->
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div> <!-- END Container -->
            </div> <!-- END Main Header -->
        </header>

    </div>

    <!-- Do not remove this class -->
    <div class="push-top"></div>

    <section class="section bg-img bg-admin-panel">
        <div class="bg-overlay op6"></div>
        <div class="container">
            <div class="row mt50 mb40">
                <div class="col-sm-12">
                    <h1 class="title-large title-larger color-on-img mb20 mt50">Admin Panel <span class="fs-small">( Health Inspectors )</span></h1>
                    <div class="br-bottom mb0"></div>
                </div>
            </div>
        </div>
    </section>

    <!--<div class="search-form">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="pull-right">
                        <form action="#" role="form" class="pull-right">
                            <input type="text" class="input-search" placeholder="Search here" id="filter">
                            <button type="submit" class="hidden"></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>-->

    <section class="section-page">
   <div class="container">
        <div class="panel panel-info" style="">
          <div class="panel-heading">
            <h3 class="panel-title text-center">Add Health Inspectors</h3>
          </div>
          <div class="panel-body" id="add-health-inspector">
            <form class="form" role="form" id="form-inspector" method="POST">
                <div class="form-vaidation-error col-md-12"></div>
                <div class="form-group col-md-6">
                    <label for="inspector">Name</label>
                    <input type="text" class="form-control" id="inspector" placeholder="Enter name" name="inspector">
                </div>
                <div class="form-group col-md-6">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" placeholder="Enter email" name="email">
                </div>
                <div class="form-group col-md-6">
                    <label for="location">Location</label>
                    <input type="text" class="form-control" id="location" placeholder="Enter location" name="location">
                </div>
                <div class="form-group col-md-6">
                    <label for="phone">Phone</label>
                    <input type="text" class="form-control" id="phone" placeholder="Enter phone" name="phone">
                </div>
                <div class="text-center">
                    <button type="button" id="myButton" data-loading-text="Adding..." class="btn btn-success" autocomplete="off" value="add" name="add" style="width: 150px;">Add</button>
                </div>
            </form>
          </div>
        </div>
        <div class="panel panel-info" style="min-height: 400px;">
          <div class="panel-heading">
            <h3 class="panel-title text-center">List of Health Inspectors</h3>
          </div>
          <div class="panel-body" id="panel-health-inspector" style="padding: 0;">
            
          </div>
        </div>
    </div>
    <!-- /.container -->
    </section>

    <?php require('footer.php');?>
    
</div> <!-- END Global Wrapper -->


<!-- Javascript files -->
    <script src="plugins/jquery/jquery-2.1.0.min.js"></script>
    <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/jquery.appear.js"></script>
    <script src="plugins/retina.min.js"></script>
    <script src="plugins/stellar.min.js"></script>
    <script src="plugins/sticky.min.js"></script>
    
    <script src="plugins/sharrre/jquery.sharrre.min.js"></script>
    <script src="plugins/owl-carousel/owl.carousel.min.js"></script>
    <script src="plugins/jquery.zoom.min.js"></script>
    <script src="plugins/raty/jquery.raty.min.js"></script>
    
    <!-- Main javascript file -->
    <script src="js/script.js"></script>
    <!--<script src="js/registrations.js"></script>--> 
    <script type="text/javascript">
    $(document).ready(function(){
        $('#panel-health-inspector').html("<div class='text-center'><img src='img/loading.gif'></div>");
        $('#panel-health-inspector').load('load-inspectors.php');

        $('#myButton').on('click',function()
        {
            var btn = $(this).button('loading');
            var name = $('#inspector').val();
            var email = $('#email').val();
            var location = $('#location').val();
            var phone = $('#phone').val();
            var button = $(this).val();

            $.ajax(
              {
                url: 'save-inspector.php',
                type: "POST",
                data: {save:button,name:name,email:email,phone:phone,location:location},
                success: function(data,status)
                {
                  if(data!="success")
                  {
                    $('.form-vaidation-error').hide();
                    $('.form-vaidation-error').html(data);
                    $('.form-vaidation-error').fadeIn();
                    $('#myButton').button('reset');
                  }
                  else
                  {
                    $('.form-vaidation-error').hide();
                    $('.form-vaidation-error').html("<div class='alert alert-success' role='alert'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span>  Added!.....</div>");
                    $('.form-vaidation-error').fadeIn();
                    $('#myButton').button('reset');
                    $('form#form-inspector') [0].reset();
                    setTimeout(function(){ $('.form-vaidation-error').fadeOut(); }, 3000);
                    $('#panel-health-inspector').html("<div class='text-center'><img src='img/loading.gif'></div>");
                    $('#panel-health-inspector').load('load-inspectors.php');                    
                  }
                }
              });
        });

        
    });
    </script>
</body>
</html>
<?php
	}
	else
		header("location: index");
?>