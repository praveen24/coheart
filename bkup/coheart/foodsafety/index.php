<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="en-us">
<head>

	<meta charset="utf-8" >
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Food Safety Mission | COHEART | KVASU</title>

	<meta name="author" content="abusinesstheme">
	<meta name="description" content="Palas is a Business HTML Template developed with the the latest HTML5 and CSS3 technologies.">

  	<!-- CSS files -->
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic|Montserrat:400,700' rel='stylesheet'>
	<link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="plugins/elegant_font/html_css/style.css">

	<link rel="stylesheet" href="plugins/rs-plugin/css/settings.css" media="screen">
	<link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">

	<!-- Main CSS file -->
	<link rel="stylesheet" href="css/style.css">

</head>
<body>

<?php require('preloader.php'); ?>
	
<!-- Global Wrapper -->
<div id="wrapper">

	<div class="h-wrapper">

		<!-- Top Bar -->
		<div class="topbar hidden-xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<ul class="top-menu">
							<li><a href="http://coheart.ac.in" target="_blank">COHEART , Kerala Veterinary & Animal Science University</a></li>
						</ul>
					</div>
					<!-- This column is hidden on mobiles -->
					<div class="col-sm-6">
						<div class="pull-right top-links">

						<!-- login check -->
						<?php include('login_check1.php'); ?>
						<!--/ login check -->

						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Header -->
		<header class="header-wrapper header-transparent with-topbar">
			<div class="main-header">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-4">
							<!-- Logo - Read documentation to see how to change the logo-->
							<a href="index" class="logo"></a>
						</div>
						<div class="col-sm-12 col-md-8">
							<nav class="navbar-right">
								<ul class="menu">
									<!-- Toggle Menu - For Mobile Devices -->
									<li class="toggle-menu"><i class="fa icon_menu"></i></li> 
									
									<li class="first active">
										<a href="index">Home</a>
									</li> <!-- END Home -->
									
									<!--login check -->
									<?php include('login_check2.php'); ?>
									<!--/ login check -->

									<li>
										<a href="restaurants">Restaurants</a>
									</li> <!-- END Restaurants -->
									<li>
										<a href="news">News</a>
									</li> <!-- END News -->
									<li>
										<a href="downloads">Downloads</a>
									</li> <!-- END Downloads -->
									<li>
										<a href="contact" class="last">Contact</a>
									</li> <!-- END Contact -->
									
								</ul>
							</nav>
						</div>
					</div>
				</div> <!-- END Container -->
			</div> <!-- END Main Header -->
		</header>

	</div>

	<!-- Do not remove this class -->
	<div class="push-top"></div>

	<!-- Slider -->
	<div class="tp-banner-container rs_fullwidth">
		<div class="tp-banner">
			<ul>
				
				<!-- SLIDE 1 -->
				<li data-transition="fade" data-masterspeed="500" data-slotamount="7" data-delay="8000" data-title="Slide 1">
					<!-- Background Image -->
				   <img src="images/slider/slider-image-4.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" alt="">

				   <!-- Include bg-overlay class if you want some background overlay over the image -->
				   <!-- Recomend to include it to have a dark overlay and change the opacity (op6, op4, op8 etc) so that the content inside to be visible and readalbe (text is white) -->
				   <div class="bg-overlay op6"></div>


					<div class="tp-caption sfb stt"
						data-x="left" data-hoffset="15"
						data-y="center" data-voffset="-50" 
						data-speed="600" data-start="600"
						data-endspeed="400" data-end="7900"
						data-easing="Power3.easeInOut" data-endeasing=""
						style="z-index: 4">
						<h3 class="title-slider-small mb10">Food safety involves everybody in the food chain</h3>
					</div>

					<div class="tp-caption sfb stt"
						data-x="left" data-hoffset="15"
						data-y="center" data-voffset="30" 
						data-speed="700" data-start="800"
						data-endspeed="500" data-end="7950"
						data-easing="Power3.easeInOut" data-endeasing=""
						style="z-index: 4">
						<h2 class="title-slider-large">Be Food Safe</h2>
					</div>

					<!--<div class="tp-caption sfb stt"
						data-x="left" data-hoffset="15"
						data-y="center" data-voffset="130" 
						data-speed="800" data-start="1000"
						data-endspeed="600" data-end="8000"
						data-easing="Power3.easeInOut" data-endeasing=""
						style="z-index: 4">
						<a href="#" class="btn-slider">Purchase</a>
					</div>-->

					
				</li>

				<!-- SLIDE 2 -->
				<li data-transition="fade" data-slotamount="7" data-delay="8000" data-title="Slide 2">
					<!-- Background Image -->
				   <img src="images/slider/slider-image-2.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" alt="">
				   <div class="bg-overlay op5"></div>

				   
					<div class="tp-caption sfl stt"
						data-x="left" data-hoffset="15"
						data-y="center" data-voffset="-10" 
						data-speed="600" data-start="600"
						data-endspeed="400" data-end="7800"
						data-easing="Power3.easeInOut" data-endeasing=""
						style="z-index: 4">
						<h2 class="title-slider-large mb20">Customizable <br> and Easy to Use</h2>
						<div class="br-bottom"></div>
					</div>

					<div class="tp-caption sfl stb"
						data-x="left" data-hoffset="15"
						data-y="center" data-voffset="110" 
						data-speed="700" data-start="800"
						data-endspeed="400" data-end="7900"
						data-easing="Power3.easeInOut" data-endeasing=""
						style="z-index: 4">
						<h4 class="title-slider-small">It could take you a few minutes to customize it!</h4>
					</div>
					
				</li>

				<!-- SLIDE 3 -->
				<li data-transition="fade" data-slotamount="7" data-delay="8000" data-title="Slide 3">
					<!-- Background Image -->
				   <img src="images/slider/slider-image-3.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" alt="">
				   <div class="bg-overlay op5"></div>

				   
					<div class="tp-caption sfl stl"
						data-x="left" data-hoffset="15"
						data-y="center" data-voffset="-15" 
						data-speed="700" data-start="600"
						data-endspeed="400" data-end="7800"
						data-easing="Power3.easeInOut" data-endeasing=""
						style="z-index: 4">
						<h2 class="title-slider-large">Free updates <br> Top notch Support</h2>
					</div>

					<div class="tp-caption sfl stl"
						data-x="left" data-hoffset="15"
						data-y="center" data-voffset="70" 
						data-speed="700" data-start="1000"
						data-endspeed="400" data-end="7900"
						data-easing="Power3.easeInOut" data-endeasing=""
						style="z-index: 4">
						<h3 class="title-slider-small uppercased">and so much more ...</h3>
					</div>


					<div class="tp-caption sfb stl"
						data-x="left" data-hoffset="15"
						data-y="center" data-voffset="120" 
						data-speed="900" data-start="1200"
						data-endspeed="600" data-end="7950"
						data-easing="Power3.easeInOut" data-endeasing=""
						style="z-index: 4">
						<a href="#" class="btn-slider reversed">see features</a>
					</div>
					
				</li>


				<!-- SLIDE 4 -->
				<li data-transition="fade" data-slotamount="7" data-delay="8000" data-title="Slide 4">
					<!-- Background Image -->
				   <img src="images/slider/slider-image-1.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" alt="">
				   <div class="bg-overlay op5"></div>

				   
					<div class="tp-caption sfr stb"
						data-x="right" data-hoffset="-15"
						data-y="center" data-voffset="0" 
						data-speed="900" data-start="600"
						data-endspeed="400" data-end="7980"
						data-easing="Power3.easeInOut" data-endeasing=""
						style="z-index: 4">
						<h2 class="title-slider-large text-right">Premium Plugins</h2>
					</div>

					<div class="tp-caption sfr stb"
						data-x="right" data-hoffset="-15"
						data-y="center" data-voffset="70" 
						data-speed="1000" data-start="1000"
						data-endspeed="400" data-end="7900"
						data-easing="Power3.easeInOut" data-endeasing=""
						style="z-index: 4">
						<h4 class="title-slider-small uppercased text-right">
						Bootstrap based <span class="py10"></span>
						responsive design <span class="py10"></span>
						clean code<br>
						working widgets  <span class="py10"></span>
						ajax contact form
						</h4>
					</div>

				</li>

			</ul>
		</div>
	</div> <!-- END Slider-->

	<section class="section mt40 search-tools">
		<div class="container">
			<div class="row col-p30">
				<div class="col-sm-4">
					<input class="todo-search-field form-control" type="search" value="" placeholder="Search by location" id="search" />
				</div>
				<div class="col-sm-2">
					<input type="button" class="btn btn-d form-control" value="Search" onclick="search()" >
				</div>
				<div class="col-sm-4">
					<input class="todo-search-field form-control" type="search" value="" placeholder="Search by name" id="searchname" />
				</div>
				<div class="col-sm-2">
					<input type="button" class="btn btn-d form-control" value="Search" onclick="searchname()" >
				</div>
			</div>
		</div>
	</section>

	<section class="section mt40">
		<div class="container">
			<div class="row col-p30">
				<div class="col-sm-4 xs-box2">
					<div class="box-services-c">
						<i class="icon_pencil-edit fa fa-style4"></i>
						<h3 class="title-small br-bottom-center">REGISTER NOW</h3>
						<p class="mb0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corrupti, quasi? Beatae cumque maiores.</p>
						<a href="register">Click Here</a>
					</div>
				</div>
				<div class="col-sm-4 xs-box2">
					<div class="box-services-c">
						<i class="icon_comment fa fa-style4"></i>
						<h3 class="title-small br-bottom-center">HAVE A COMPLAINT?</h3>
						<p class="mb0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corrupti, quasi? Beatae cumque maiores.</p>
						<a href="complaint">Click Here</a>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="box-services-c">
						<i class="icon_phone fa fa-style4"></i>
						<h3 class="title-small br-bottom-center">DIAL TOLL-FREE</h3>
						<p class="mb0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corrupti, quasi? Beatae cumque maiores.</p>
						<a href="">1800 425 1125</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section-bg p0 section-dark data-height-fix">

		<div class="row col-p0">
			<div class="col-sm-6 hidden-xs">
				<div class="box-services-d p0 bg-img bg16">
					<div class="bg-overlay"></div>
					<div class="set-height"></div>
				</div>
			</div>
		</div>

		<div class="section-caption">
			<div class="container">
				<div class="row col-p0">
					<div class="col-sm-5 col-sm-offset-7 get-height">
						<div class="mb50 mt50">
							<h3 class="title-medium color-on-dark">About us</h3>
							<div class="br-bottom mb20 color-on-dark"></div>
							<p class="mb50 color-on-dark">Foods borne illness are major concern of the public health agencies all over the world. 200 new diseases from food are being reported every year. This is time to ensure that food chain operators are strictly following standard measures and safe methods while handling food (raw/RTE). This document is a project proposal to ensure safe as well as quality food in catering and food serving establishments (FSE) like hotels, restaurants, mess, canteen, etc... <a href="#" class="read-more">read more</a> </p>
							<h3 class="title-small color-on-dark"><i class="fa-style5 icon_check mr10"></i> Be Food Safe</h3>
							<p class="mb0 color-on-dark">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis explicabo inventore, nisi sint consectetur distinctio! Asperiores totam dolorum, odio ex?</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section class="section">
		<div class="container">
			<div class="row col-p30">
				<div class="col-sm-12 col-md-6 sm-box2">
					<div class="box-services-b">
						<div class="box-left"><i class="fa fa-style1 icon_lifesaver"></i></div>
						<div class="box-right">
							<h3 class="title-small">Top notch Support <a href="#" class="link-read-more">read more</a></h3>
							<p class="mb10">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae temporibus quae!</p>
						</div>
					</div>

					<div class="mb50"></div>

					<div class="box-services-b">
						<div class="box-left"><i class="fa fa-style1 icon_box-checked"></i></div>
						<div class="box-right">
							<h3 class="title-small">Bootstrap Based <a href="#" class="link-read-more">read more</a></h3>
							<p class="mb10">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae temporibus quae!</p>
						</div>
					</div>
				</div>
				<div class="col-sm-12 col-md-6">
					<div class="box-services-b">
						<div class="box-left"><i class="fa fa-style1 icon_lightbulb_alt"></i></div>
						<div class="box-right">
							<h3 class="title-small">SEO Ready <a href="#" class="link-read-more">read more</a></h3>
							<p class="mb10">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae temporibus quae!</p>
						</div>
					</div>

					<div class="mb50"></div>

					<div class="box-services-b">
						<div class="box-left"><i class="fa fa-style1 icon_piechart"></i></div>
						<div class="box-right">
							<h3 class="title-small">Working widgets <a href="#" class="link-read-more">read more</a></h3>
							<p class="mb10">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim temporibus quae, aliquid! Ipsam officiis obcaecati beatae temporibus quae!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section max_height sm_max_height no-p-b">
		<div class="row col-p0">
			<div class="col-sm-12 col-md-4">
				<div class="box-services-d box-services-e green el_max_height">
					<div class="bg-overlay"></div>
					<div class="row col-p0">
						<div class="col-sm-12">
							<h3 class="title-medium title-shadow-a">Our Mission <a href="#" class="link-read-more">read more</a></h3>
							<p class="mb0 ">STATE MISSION ON FOOD SAFETY, a project motivated by FSS Act 2006, is to directly and indirectly ensure safe food handling and food hygiene practices by creating a unique technology enabled work- at- ease culture for catering and food serving establishments in the state of Kerala thereby reducing food borne illness to zero level and making the state food safe by 2021.</p>
							
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-4">
				<div class="box-services-d box-services-e dark el_max_height">
					<div class="bg-overlay"></div>
					<div class="row col-p0">
						<div class="col-sm-12">
							<h3 class="title-medium title-shadow-a">Our Policy <a href="#" class="link-read-more">read more</a></h3>
							<p class="mb0 ">
								<ul>
									<li>Food produced or sold in Kerala Catering and Food serving establishments is safe to eat.</li>
									<li>Caterers and Food serving establishments give priority to consumer interests in relation to food.</li>
									<li>Consumers have the right information and understanding they need to make informed choices about where and what they eat.</li>
									<li>Businesses are trained for safe food handling and food hygiene practices. Business compliance is encouraged because it delivers consumer protection.</li>
								</ul>
							</p>
							
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-4">
				<div class="box-services-d box-services-e el_max_height">
					<div class="bg-overlay"></div>
					<div class="row col-p0">
						<div class="col-sm-12">
							<h3 class="title-medium title-shadow-a">Our Motto <a href="#" class="link-read-more">read more</a></h3>
							<p class="mb0 ">
								<ul>
									<li>Putting the consumer first</li>
									<li>Openness and transparency</li>
									<li>Science- and evidence-based</li>
									<li>Acting independently</li>
									<li>Enforcing food law fairly</li>
								</ul>
							</p>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php require('footer.php');?>	
	
</div> <!-- END Global Wrapper -->

	<!-- Javascript files -->
	<script src="plugins/jquery/jquery-2.1.0.min.js"></script>
	<script src="plugins/bootstrap/js/bootstrap.min.js"></script>

	<script src="plugins/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
	<script src="plugins/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

	<script src="plugins/jquery.appear.js"></script>
	<script src="plugins/retina.min.js"></script>
	<script src="plugins/stellar.min.js"></script>
	<script src="plugins/sticky.min.js"></script>
	<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="plugins/isotope/isotope.pkgd.min.js"></script>
	<script src="plugins/isotope/imagesloaded.pkgd.min.js"></script>

	<!-- Main javascript file -->
	<script src="js/script.js"></script>
	<script src="js/search.js"></script>

</body>
</html>