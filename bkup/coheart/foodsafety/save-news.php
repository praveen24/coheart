<?php 
	session_start();
	if(isset($_SESSION['foodsafety_inspector']) && $_SESSION['foodsafety_inspector']!="")
	{
		if(isset($_POST['save']) && $_POST['save']!="")
		{
			$id = $_SESSION['foodsafety_inspector']['id'];
			$title = $_POST['title'];
			$content = $_POST['content']; 
			$flag = 0;
			if($title=="")
			{		
				echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please enter news title</div>";
				$flag = 1;
			}
			if($content=="")
			{		
				echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please enter news details</div>";
				$flag = 1;
			}
			
			if($flag == 0)
			{
				require('dbconnect.php');
				mysqli_autocommit($conn, false);
				date_default_timezone_set("Asia/Kolkata"); 
				$time = date("Y-m-d H:i:s");
				if($stmt = mysqli_prepare($conn,"INSERT into news(title,news,added) values(?,?,?)"))
				{
					mysqli_stmt_bind_param($stmt,'sss',$title,$content,$time);
					if(mysqli_stmt_execute($stmt))
						mysqli_commit($conn);
					mysqli_stmt_close($stmt);
					//echo $time;
					echo "success";
					mysqli_close($conn);
				}
			}
		}
		else
			header("location: index");
	}	
	else
		header("location: index");
?>