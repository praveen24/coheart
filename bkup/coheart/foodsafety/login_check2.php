<?php if(isset($_SESSION['foodsafety_superadmin']) && $_SESSION['foodsafety_superadmin']!="") { ?>
									<li>
										<a href="superadmin">Admin Page</a>
									</li> <!-- END Admin Page -->
									<?php }
									elseif(isset($_SESSION['foodsafety_hoteladmin']) && $_SESSION['foodsafety_hoteladmin']!="") { ?>
									<li>
										<a href="hoteladmin">Admin Page</a>
									</li> <!-- END Admin Page -->
									<?php } 
									elseif(isset($_SESSION['foodsafety_inspector']) && $_SESSION['foodsafety_inspector']!="") { ?>
									<li>
										<a href="inspector">Admin Page</a>
									</li> <!-- END Admin Page -->
									<?php 
									}
									else{
									?>
									<li class="hidden-sm hidden-md hidden-lg">
										<a href="login">Sign In</a>
									</li> <!-- END Sign in -->
									<li class="hidden-sm hidden-md hidden-lg">
										<a href="register">Sign Up</a>
									</li> <!-- END Sign up -->
									<?php } ?>