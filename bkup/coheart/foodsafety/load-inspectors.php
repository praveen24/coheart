<?php
	session_start();
	if(isset($_SESSION['foodsafety_superadmin']) && $_SESSION['foodsafety_superadmin']!="")
	{
		require("dbconnect.php");
		if(mysqli_connect_errno())
			die("Could not connect: ".mysqli_connect_error());

		$query = "SELECT name,location,email,phone from inspectors";
		$stmt = mysqli_prepare($conn, $query);
		mysqli_execute($stmt);
		mysqli_stmt_bind_result($stmt, $name, $location, $email, $phone);
		$flag = 0;
		echo "<div class='table-responsive'>";
		echo "<table class='table table-bordered table-striped'>";
		echo "<th></th>";
		echo "<th>Name</th>";
		echo "<th>Location</th>";
		echo "<th>Email</th>";
		echo "<th>Phone</th>";
		$i = 1;
		while(($res = mysqli_stmt_fetch($stmt)))
		{
	        echo "<tr>";
	        echo "<td class='text-right'>$i</td>";
	        echo "<td>$name</td>";
	        echo "<td>$location</td>";
	        echo "<td>$email</td>";
	        echo "<td>$phone</td>";
	        echo "</tr>";
	        $flag = 1;
	        $i++;
		}
		if($flag == 0)
			echo "<tr><td colspan='5'>No results found!</td></tr>";
		echo "</div>";
	}
	else
		header("location: index.html")
?>