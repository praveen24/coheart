<!DOCTYPE html>
<html lang="en-us">
<head>

	<meta charset="utf-8" >
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Food Safety Mission | Downloads</title>

	<meta name="author" content="abusinesstheme">
	<meta name="description" content="Palas is a Business HTML Template developed with the the latest HTML5 and CSS3 technologies.">

  	<!-- CSS files -->
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic|Montserrat:400,700' rel='stylesheet'>
	<link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="plugins/elegant_font/html_css/style.css">
	
	<link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
	
	<!-- Main CSS file -->
	<link rel="stylesheet" href="css/style.css">
	
</head>
<body>

<div id="preloader">
	<div id="status">&nbsp;</div>
</div>

<!-- Global Wrapper -->
<div id="wrapper">

	<div class="h-wrapper">

		<!-- Top Bar -->
		<div class="topbar hidden-xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<ul class="top-menu">
							<li><a href="http://coheart.ac.in" target="_blank">COHEART , Kerala Veterinary & Animal Science University</a></li>
						</ul>
					</div>
					<!-- This column is hidden on mobiles -->
					<div class="col-sm-6">
						<div class="pull-right top-links">
							<a href="login">Sign In</a>
							<a href="">|</a>
							<a href="register">Sign Up</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Header -->
		<header class="header-wrapper header-transparent with-topbar">
			<div class="main-header">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-4">
							<!-- Logo - Read documentation to see how to change the logo-->
							<a href="index" class="logo"></a>
						</div>
						<div class="col-sm-12 col-md-8">
							<nav class="navbar-right">
								<ul class="menu">
									<!-- Toggle Menu - For Mobile Devices -->
									<li class="toggle-menu"><i class="fa icon_menu"></i></li> 
									
									<li class="first">
										<a href="index">Home</a>
									</li> <!-- END Home -->
									<li class="hidden-sm hidden-md hidden-lg">
										<a href="login">Sign In</a>
									</li> <!-- END Sign in -->
									<li class="hidden-sm hidden-md hidden-lg">
										<a href="register">Sign Up</a>
									</li> <!-- END Sign up -->
									<li class="">
										<a href="restaurants">Restaurants</a>
									</li> <!-- END Restaurants -->
									<li>
										<a href="news">News</a>
									</li> <!-- END News -->
									<li class="active">
										<a href="downloads">Downloads</a>
									</li> <!-- END Downloads -->
									<li>
										<a href="contact" class="last">Contact</a>
									</li> <!-- END Contact -->
									
								</ul>
							</nav>
						</div>
					</div>
				</div> <!-- END Container -->
			</div> <!-- END Main Header -->
		</header>

	</div>

	<!-- Do not remove this class -->
	<div class="push-top"></div>

	<section class="section-intro bg-img bg18">
		<div class="bg-overlay op6"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-8">
					<h1 class="intro-title mb20">Downloads</h1>
					<p class="intro-p mb20">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit tenetur reiciendis molestias nostrum excepturi porro dolorum amet!</p>
				</div>
			</div>
		</div>
	</section>

	<div class="search-form">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="pull-right">
						<form action="#" role="form" class="pull-right">
							<input type="text" class="input-search" placeholder="Search here" id="filter">
							<button type="submit" class="hidden"></button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<section class="section-page">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-md-9 col-md-push-3 col-sm-push-4 space-left push-off">
					<div class="row blog-list remove-style" id="news-results">
		<ul>
			<?php
			$id=$_GET['dat'];
			$conn = mysqli_connect("localhost", "infoaide_fsadmin", "fsadmin", "infoaide_foodsafety");
                    $query = mysqli_query($conn, "SELECT * from downloads WHERE id='$id'");
                    while($data = mysqli_fetch_assoc($query))
                    { ?>
		<div class="row ">
		<div class="col-xs-12">
		<h2><?php echo $data['heading'];?></h2>
		<p><?php echo $data['description'];?></p>
		<?php


$file = explode(",",$data['file']); //Let say If I put the file name Bang.png
$cnt=count($file);
for($i=0;$i<$cnt-1;$i++)
{
	echo '<img src="/foodsafety/uploads/'.$file[$i].'" width="20px" height="20px"><br>';
	echo "<a href='downloading.php?nama=".$file[$i]."'>download</a><br> ";
	}



?>
		</div>
		</div>
		<?php } ?>
	
		</ul>
					</div> <!-- END Blog Posts -->
					
				</div>
				<div class="col-sm-4 col-md-3 col-md-pull-9 col-sm-pull-8 pull-off">
					<aside class="sidebar">
						<div class="sidebar-widget">
							<h3 class="sidebar-title br-bottom">Above and beyond</h3>
							<p>Lorem ipsum dolor sit amet, consect adipisicing elit. Impedit, recusandae corrupti voluptas atque voluptatibust!</p>
						</div>
						<div class="sidebar-widget">
							<h3 class="sidebar-title br-bottom">Get social</h3>
							<ul class="sidebar-socials">
								<li><a href="#"><i class="fa fa-twitter"></i> Follow us</a> <span>455 followers</span></li>
								<li><a href="#"><i class="fa fa-facebook"></i> Like us</a> <span>505 likes</span></li>
								<li><a href="#"><i class="fa fa-google-plus"></i> Circle us</a> <span>355 friends</span></li>
								<li><a href="#"><i class="fa fa-envelope"></i> Subscribe</a> <span>250 subscribers</span></li>
							</ul>
						</div>
						<!--<div class="sidebar-widget">
							<h3 class="sidebar-title br-bottom">Latest Tweets</h3>
							<div class="sidebar-tweet clearfix mb0">
								<i class="fa fa-twitter"></i>
								<p class="tweet-content"><a href="#" class="tweet-user">@abusinesstheme</a> <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span> <small>22 hours ago</small></p>
							</div>
						</div>-->
						
						<div class="sidebar-widget">
							<h3 class="sidebar-title br-bottom">Categories</h3>
							<ul class="sidebar-categories">
								<li><a href="#"><i class="fa fa-chevron-right"></i> Web Design</a></li>
								<li><a href="#"><i class="fa fa-chevron-right"></i> Software Engineering</a></li>
								<li><a href="#"><i class="fa fa-chevron-right"></i> Web Graphic</a></li>
								<li><a href="#"><i class="fa fa-chevron-right"></i> Web Programming</a></li>
								<li><a href="#"><i class="fa fa-chevron-right"></i> Software Design</a></li>
							</ul>
						</div>
						
					</aside>
				</div>
			</div>
		</div>
	</section> <!-- END Blog Page-->

	<?php require('footer.php');?>
	
</div> <!-- END Global Wrapper -->




	<!-- Javascript files -->
	<script src="plugins/jquery/jquery-2.1.0.min.js"></script>
	<script src="plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="plugins/jquery.appear.js"></script>
	<script src="plugins/retina.min.js"></script>
	<script src="plugins/stellar.min.js"></script>
	<script src="plugins/sticky.min.js"></script>
	
	<script src="plugins/sharrre/jquery.sharrre.min.js"></script>
	<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="plugins/jquery.zoom.min.js"></script>
	<script src="plugins/raty/jquery.raty.min.js"></script>
	
	


	<!-- Main javascript file -->
	<script src="js/script.js"></script>

	

</body>
</html>