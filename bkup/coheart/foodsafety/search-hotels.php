<?php
 session_start();
	if(isset($_POST['search']) && $_POST['search']!="")
	{
		$search = $_POST['search'];
		require("dbconnect.php");
		if(mysqli_connect_errno()) {
	        die("Could not connect: ".mysqli_connect_error());
	    }
	    $query = "SELECT id,name,type,location,contactPerson,phone,address,date_of_registration,email,image FROM registrations WHERE location like '%$search%' or address like '%$search%'";
	    $stmt = mysqli_prepare($conn, $query);
	    mysqli_execute($stmt);
	    mysqli_stmt_store_result($stmt);
	    mysqli_stmt_bind_result($stmt, $id, $name, $type, $location, $contactPerson, $phone, $address, $dateofreg, $email,$image);

	    $flag = 0;
	    while(($data = mysqli_stmt_fetch($stmt)))
	    {
	    	echo "<div class='row'>";
                        echo "<hr>";
                        echo "<div class='col-xs-3'>";
                        echo "<img src='uploads/profilethumb/$image' class='img-responsive'>";
                        echo "</div>";
                        echo "<div class='col-xs-6'>";
                        echo "<a href='restu.php?id=$id'><h3>$name</h3></a>";
                        echo "<div>Type: $type</div>";
                        echo "<p>Address<br><small>".nl2br($address)."</small></p>";
                        echo "<p><small>Contact Person: $contactPerson</small></p>";
                        echo "<p><small><span class='glyphicon glyphicon-earphone' aria-hidden='true'></span> &nbsp;&nbsp;&nbsp;$phone</small></p>";
                        echo "<p><small><span class='glyphicon glyphicon-map-marker' aria-hidden='true'></span> &nbsp;&nbsp;&nbsp;$location</small></p>";
                        echo "<p><small><span class='glyphicon glyphicon-envelope' aria-hidden='true'></span> &nbsp;&nbsp;&nbsp;$email</small></p>";
                        echo "</div>";
                        echo "<div class='col-xs-3'>";
                        $query2 = mysqli_query($conn,"SELECT * from rating where hotel=$id");
                        echo mysqli_error($conn);
                        $flag2 = 0;
                        $count = mysqli_num_rows($query2);
                        $rating = 0;
                        while($data2 = mysqli_fetch_assoc($query2))
                        {
                            $rating += $data2['rating'];
                            $flag2 = 1;
                        }
                        if($count != 0)
                                $rating = round($rating/$count,1);
                        if($flag2 == 1)
                        {
                            echo "<div class='rateit' data-rateit-value='$rating' data-rateit-ispreset='true' data-rateit-readonly='true'></div>";
                            echo "<div class='text-center'>($rating/5)</div>";
                        }
                        elseif($flag2 == 0)
                        {
                            echo "<div class='rateit' data-rateit-value='$rating' data-rateit-ispreset='true' data-rateit-readonly='true'></div>";
                            echo "<div class='text-center'>($rating/5)</div>";
                        }
                        echo "</div>";
                        echo "<hr>";
                        echo "</div>";
                        $flag = 1;
	    }
	    if($flag == 0)
	    	echo "<p class='no-results'>Sorry, no results found!</p>";
	}
	else
		header("location: index.php");
?>