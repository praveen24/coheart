<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en-us">
<head>

	<meta charset="utf-8" >
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Food Safety Mission | Restaurants</title>

	<meta name="author" content="abusinesstheme">
	<meta name="description" content="Palas is a Business HTML Template developed with the the latest HTML5 and CSS3 technologies.">

  	<!-- CSS files -->
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic|Montserrat:400,700' rel='stylesheet'>
	<link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="plugins/elegant_font/html_css/style.css">
	
	<link rel="stylesheet" href="plugins/rangeSlider/css/ion.rangeSlider.css">

	<!-- Main CSS file -->
	<link rel="stylesheet" href="css/style.css">
	
</head>
<body>

<?php require('preloader.php'); ?>

<!-- Global Wrapper -->
<div id="wrapper">

	<div class="h-wrapper">

		<!-- Top Bar -->
		<div class="topbar hidden-xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<ul class="top-menu">
							<li><a href="http://coheart.ac.in" target="_blank">COHEART , Kerala Veterinary & Animal Science University</a></li>
						</ul>
					</div>
					<!-- This column is hidden on mobiles -->
					<div class="col-sm-6">
						<div class="pull-right top-links">
						<!-- login check -->
						<?php include('login_check1.php'); ?>
						<!--/ login check -->
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Header -->
		<header class="header-wrapper header-transparent with-topbar">
			<div class="main-header">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-4">
							<!-- Logo - Read documentation to see how to change the logo-->
							<a href="index" class="logo"></a>
						</div>
						<div class="col-sm-12 col-md-8">
							<nav class="navbar-right">
								<ul class="menu">
									<!-- Toggle Menu - For Mobile Devices -->
									<li class="toggle-menu"><i class="fa icon_menu"></i></li> 
									
									<li class="first">
										<a href="index">Home</a>
									</li> <!-- END Home -->
									<!--login check -->
									<?php include('login_check2.php'); ?>
									<!--/ login check -->
									<li class="active">
										<a href="restaurants">Restaurants</a>
									</li> <!-- END Restaurants -->
									<li>
										<a href="news">News</a>
									</li> <!-- END News -->
									<li>
										<a href="downloads">Downloads</a>
									</li> <!-- END Downloads -->
									<li>
										<a href="contact" class="last">Contact</a>
									</li> <!-- END Contact -->
									
								</ul>
							</nav>
						</div>
					</div>
				</div> <!-- END Container -->
			</div> <!-- END Main Header -->
		</header>

	</div>

	<!-- Do not remove this class -->
	<div class="push-top"></div>

	

	<section class="section-intro bg-img bg-restaurants">
		<div class="bg-overlay op6"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-8">
					<h1 class="intro-title mb20">Restaurants</h1>
					<p class="intro-p mb0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit tenetur reiciendis molestias nostrum excepturi porro dolorum amet!</p>
				</div>
			</div>
		</div>
	</section>

	<section class="shop-section section-page">
		<div class="container">
			<div class="row">
				<div class="col-sm-8 col-md-9 col-md-push-3 col-sm-push-4 space-left push-off ">
					<div class="toolbar-shop form todo-search">
						<div class="row">
							<div class="col-md-6">
								<div class="form-icon icon-search">
			 						<input type="text" name="search-name" id="search-name" class="todo-search-field form-control" placeholder=" Search by name" id="search-name">
			 					</div>
							</div>
							<div class="col-md-6">
								<div class="form-icon icon-search">
			 						<input type="text" name="search-location" id="search-location" class="todo-search-field form-control" placeholder=" Search by location" id="search-location">
			 					</div>
			 				</div>
						</div>
					</div>
					
					<div id="results" style="display: none"></div>
					
				</div>
				<div class="col-sm-4 col-md-3 col-md-pull-9 col-sm-pull-8 pull-off ">
					<aside class="sidebar shop-sidebar">
						<div class="panel-group" id="toggle">
	  								<div class="panel ">
									    <div class="panel-heading">
									        <h4 class="panel-title">
									            <a data-toggle="collapse" href="#one1">
									            	Location<i class="fa fa-chevron-down upside-down pull-right"></i>
									            </a>
									      	</h4>
									    </div>
									    <div id="one1" class="panel-collapse collapse in todo">
										    <div class="panel-body">
										       	<ul id="filter-location" style="">
										            <?php
										                require('dbconnect.php');
										                $query = mysqli_query($conn, "SELECT distinct location from registrations limit 10");
										                while($data = mysqli_fetch_assoc($query))
										                {
										                    echo "<li>";
										                    echo "<div class='todo-content'>";
										                    echo $data['location'];
										                    echo "</div>";
										                    echo "</li>";
										                }
										            ?>
										              
										            </ul>
										    </div>
									    </div>
									</div>
	  								<div class="panel">
									    <div class="panel-heading">
									        <h4 class="panel-title">
									            <a data-toggle="collapse" href="#two1">
									            	Type<i class="fa fa-chevron-down upside-down pull-right"></i> 
									            </a>
									      	</h4>
									    </div>
									    <div id="two1" class="panel-collapse collapse in todo">
										    <div class="panel-body">
										        <ul id="filter-category"  style="">
									              <li><div class='todo-content'>Hotel</div></li><li><div class='todo-content'>Bakery</div></li><li><div class='todo-content'>Restaurant</div></li>
									            </ul>
										    </div>
									    </div>
									</div>
	  								
						</div>
						
						<!--<div class="sidebar-widget">
							<h3 class="sidebar-title">Most Popular</h3>
							<div class="clearfix">
								<ul class="top-rated-products unstyled-list">
									<li>
										<div class="product-image">
											<img src="images/demo/shop01.jpg" alt="product">
										</div>
										<div class="product-info">
											<h5><a href="#">Print Sweatshirt</a></h5>
											<div>
												<del class="shop-price-off">$349</del> <ins class="shop-price">$299</ins>
											</div>
										</div>
									</li>
									<li>
										<div class="product-image">
											<img src="images/demo/shop02.jpg" alt="product">
										</div>
										<div class="product-info">
											<h5><a href="#">Cotton T-shirt</a></h5>
											<div>
												<ins class="shop-price">$299</ins>
											</div>
										</div>
									</li>
									<li>
										<div class="product-image">
											<img src="images/demo/shop03.jpg" alt="product">
										</div>
										<div class="product-info">
											<h5><a href="#">Striped T-shirt</a></h5>
											<div>
												<del class="shop-price-off">$349</del> <ins class="shop-price">$299</ins>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
						<div class="sidebar-widget">
							<h3 class="sidebar-title br-bottom">Photos</h3>
							<div class="row mb10">
								<div class="col-xs-12">
									<a href="#"><img src="http://placehold.it/750x400" alt=""></a>
								</div>
							</div>
							<div class="row col-p5">
								<div class="col-xs-6">
									<a href="#"><img src="http://placehold.it/300x300" alt=""></a>
								</div>
								<div class="col-xs-6">
									<a href="#"><img src="http://placehold.it/300x300" alt=""></a>
								</div>
							</div>
						</div>-->
					</aside>
				</div>
			</div>
		</div>
	</section>

	<?php require('footer.php'); ?>
	
</div> <!-- END Global Wrapper -->




	<!-- Javascript files -->
	<script src="plugins/jquery/jquery-2.1.0.min.js"></script>
	<script src="plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="plugins/jquery.appear.js"></script>
	<script src="plugins/retina.min.js"></script>
	<script src="plugins/stellar.min.js"></script>
	<script src="plugins/sticky.min.js"></script>
	
	<script src="plugins/raty/jquery.raty.min.js"></script>
	<script src="plugins/rangeSlider/js/ion.rangeSlider.min.js"></script>
	
	<!-- Main javascript file -->
	<script src="js/script.js"></script>
	<script src="js/filters.js"></script>	

	

</body>
</html>