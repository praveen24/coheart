<!DOCTYPE html>
<html lang="en-us">
<head>

	<meta charset="utf-8" >
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Food Safety Mission | News & Alerts</title>

	<meta name="author" content="abusinesstheme">
	<meta name="description" content="Palas is a Business HTML Template developed with the the latest HTML5 and CSS3 technologies.">

  	<!-- CSS files -->
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic|Montserrat:400,700' rel='stylesheet'>
	<link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="plugins/elegant_font/html_css/style.css">
	
	<link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
	
	<!-- Main CSS file -->
	<link rel="stylesheet" href="css/style.css">
	
</head>
<body>

<?php require('preloader.php'); ?>

<!-- Global Wrapper -->
<div id="wrapper">

	<div class="h-wrapper">

		<!-- Top Bar -->
		<div class="topbar hidden-xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<ul class="top-menu">
							<li><a href="http://coheart.ac.in" target="_blank">COHEART , Kerala Veterinary & Animal Science University</a></li>
						</ul>
					</div>
					<!-- This column is hidden on mobiles -->
					<div class="col-sm-6">
						<div class="pull-right top-links">
							<a href="login">Sign In</a>
							<a href="">|</a>
							<a href="register">Sign Up</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Header -->
		<header class="header-wrapper header-transparent with-topbar">
			<div class="main-header">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-4">
							<!-- Logo - Read documentation to see how to change the logo-->
							<a href="index" class="logo"></a>
						</div>
						<div class="col-sm-12 col-md-8">
							<nav class="navbar-right">
								<ul class="menu">
									<!-- Toggle Menu - For Mobile Devices -->
									<li class="toggle-menu"><i class="fa icon_menu"></i></li> 
									
									<li class="first">
										<a href="index">Home</a>
									</li> <!-- END Home -->
									<li class="hidden-sm hidden-md hidden-lg">
										<a href="login">Sign In</a>
									</li> <!-- END Sign in -->
									<li class="hidden-sm hidden-md hidden-lg">
										<a href="register">Sign Up</a>
									</li> <!-- END Sign up -->
									<li class="">
										<a href="restaurants">Restaurants</a>
									</li> <!-- END Restaurants -->
									<li class="active">
										<a href="news">News</a>
									</li> <!-- END News -->
									<li>
										<a href="downloads">Downloads</a>
									</li> <!-- END Downloads -->
									<li>
										<a href="contact" class="last">Contact</a>
									</li> <!-- END Contact -->
									
								</ul>
							</nav>
						</div>
					</div>
				</div> <!-- END Container -->
			</div> <!-- END Main Header -->
		</header>

	</div>

	<!-- Do not remove this class -->
	<div class="push-top"></div>

	<section class="section-intro bg-img bg18">
		<div class="bg-overlay op6"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-8">
					<h1 class="intro-title mb20">News & Alerts</h1>
					<p class="intro-p mb20">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit tenetur reiciendis molestias nostrum excepturi porro dolorum amet!</p>
				</div>
			</div>
		</div>
	</section>

	<div class="search-form">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="pull-right">
						<form action="#" role="form" class="pull-right">
							<input type="text" class="input-search" placeholder="Search here" id="filter">
							<button type="submit" class="hidden"></button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

	<section class="section-page">
		 <div class="container">
	 <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Complaint
                        <strong>form</strong>
                    </h2>
                    <hr>
                    <p><!--Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat, vitae, distinctio, possimus repudiandae cupiditate ipsum excepturi dicta neque eaque voluptates tempora veniam esse earum sapiente optio deleniti consequuntur eos voluptatem.--></p>
                    <!--Registration form-->
                    <form role="form" id="registration-form" autocomplete="off">
                    	<div class="form-vaidation-error col-md-12"></div>
                        <div class="row">
                            <div class="form-group col-lg-4">
                                <label>Name of Customer</label>
                                <input type="text" class="form-control" name="name" id="name">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Contact Number</label>
                                <input type="text" class="form-control" name="phone" id="phone">
                            </div>
							<div class="form-group col-lg-4">
                                <label>Hotel Name</label>
                                <select class="form-control" name="hotel" id="hotel">
								<option value="">--Select one--</option>
								<?php
                                    require('dbconnect.php');
                                    $query = mysqli_query($conn, "SELECT * from registrations");
                                    while($data = mysqli_fetch_assoc($query))
                                    {
                                        echo "<option value='{$data['id']}'>{$data['name']}</option>";
                                    }
                                ?>
								</select>
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Bill Number</label>
                                <input type="text" class="form-control" name="bill" id="bill">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Location</label>
                                <input type="text" class="form-control" name="location" id="location">
                            </div>
							<div class="clearfix"></div>
                            <div class="form-group col-lg-12">
                                <label>Complaint</label>
                                <textarea class="form-control" rows="6" name="complaint" id="complaint"></textarea>
                            </div>
							 <div class="form-group col-lg-6">
                                <div class="col-sm-6"><img src="codecreaters.php" style="width: 70%;height: 50px;"></div>
							<div class="col-sm-6"><label><input type="text" name="captcha" id="captcha" required class="form-control" placeholder="Enter the text"></label>
                            </div>
                            <div class="form-group col-lg-12" style="margin-top: 2%">
                                <input type="hidden" name="save" value="contact">
                                <button type="button" id="myButton" data-loading-text="Submitting..." class="btn btn-default" autocomplete="off" value="save" name="save">
			                      Submit
			                    </button>
                            </div>
                       
                    </form>
                </div>
            </div>
        </div>

    </div>
   

 
			
    </div>
    <!-- /.container -->
	</section> <!-- END Blog Page-->

	<?php require('footer.php');?>
	
</div> <!-- END Global Wrapper -->




	<!-- Javascript files -->
	<script src="plugins/jquery/jquery-2.1.0.min.js"></script>
	<script src="plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="plugins/jquery.appear.js"></script>
	<script src="plugins/retina.min.js"></script>
	<script src="plugins/stellar.min.js"></script>
	<script src="plugins/sticky.min.js"></script>
	
	<script src="plugins/sharrre/jquery.sharrre.min.js"></script>
	<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="plugins/jquery.zoom.min.js"></script>
	<script src="plugins/raty/jquery.raty.min.js"></script>
	
	


	<!-- Main javascript file -->
	<script src="js/script.js"></script>
	 <script>
    $(document).ready(function(){
    	$('#myButton').on('click',function(){
    		var btn = $(this).button('loading');
    		var name = $('#name').val();
    		var phone = $('#phone').val();
    		var hotel = $('#hotel').val();
    		var location = $('#location').val();
    		var bill = $('#bill').val();
    		var complaint = $('#complaint').val();
			var captcha = $('#captcha').val();
    		var button = $(this).val();
    		$.ajax(
	          {
	            url: 'save-complaint.php',
	            type: "POST",
	            data: {save:button,name:name,phone:phone,hotel:hotel,location:location,bill:bill,complaint:complaint,captcha:captcha},
	            success: function(data,status)
	            {
	              if(data!="success")
	              {
	                $('.form-vaidation-error').hide();
	                $('.form-vaidation-error').html(data);
	                $('.form-vaidation-error').fadeIn();
	                $('#myButton').button('reset');
	              }
	              else
	              {
	                $('.form-vaidation-error').hide();
	                $('.form-vaidation-error').html("<div class='alert alert-success' role='alert'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span>  Complaint registered successfully.....</div>");
	                $('.form-vaidation-error').fadeIn();
	                $('#myButton').button('reset');
	                $('form#registration-form') [0].reset();
	                setTimeout(function(){ $('.form-vaidation-error').fadeOut(); }, 3000);
	                
	              }
	            }
	          });
    	});
    });
    </script>

	

</body>
</html>
