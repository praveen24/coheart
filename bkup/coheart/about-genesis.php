<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="coheart, kvasu, veterinary, one, health, university, kerala, animal, science, pookode,diploma,certification,course, education, hygiene,Education, Advocacy, Research, Training "/>
    <title>COHEART | About One Health</title>
	<link rel="shortcut icon" href="dist/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="dist/images/favicon.ico" type="image/x-icon">
	<!--Slider-->
	<link rel="stylesheet" href="dist/slider/responsiveslides.css">
	
	<!--FancyBox-->
	<link rel="stylesheet" href="dist/lightbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<!-- Bootstrap -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
	<link href="dist/css/style.css" rel="stylesheet">
	<link href="admin/table/css/footable.core.css" rel="stylesheet"/>
	<link href="admin/table/css/footable.metro.css" rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
	<!--<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>-->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>
    <div class=''>
		<?php require('header.php');?>
		<nav class="navbar navbar-default visible-xs " role="navigation">
					<div class="container-fluid">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
						  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
						  <a class="navbar-brand" href="#">Menu</a>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li><a href="index">Home</a></li>
						<li class="dropdown active">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<li><a href="about-one-health">What is One Health</a></li>
								<li><a href="about-genesis">Genesis of COHEART</a></li>
								<li><a href="about-mandate">Mandate, Vision and Mission of COHEART</a></li>
								<li><a href="about-scope">Scope of One Health</a></li>
								<li><a href="about-objectives">Objectives, Activities and Roap Map</a></li>
								<li><a href="about-faculties">Faculties</a></li>
							  </ul>
						</li>
						
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Courses <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<?php
									require('dbconnect.php');
									$query = mysqli_query($conn,"select id,course_name from courses");
									echo mysqli_error($conn);
									while($data = mysqli_fetch_array($query))
									{
										$course = strtolower($data['course_name']);
										$course = ucwords($course);
										echo "<li><a href='courses?id={$data['id']}'>$course</a></li>";
									}
								?>
							  </ul>
						</li>
						<li><a href="gallery">Gallery</a></li>
						<li><a href="partners">Partnering Institutes and supporters</a></li>
						<li><a href="downloads">Downloads</a></li>
						<li><a href="news">News & events</a></li>
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">COHEART Resources <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<li><a href="research-vid">Video Library</a></li>
								<li><a href="body">Human Body</a></li>
								<li><a href="map">COHEART Health Map</a></li>
							  </ul>
						</li>
						<li><a href="involve">Get Involved</a></li>
						<li><a href="contact">Contact Us</a></li>
							</ul>
							
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
		</nav>
		<div class='content'>
			<div class='row'>
				<div class='col-md-3 col-sm-4 visible-md visible-lg visible-sm'>

					<div class="panel-group" id="accordion">
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-parent="#accordion" href="index">
					          Home
					        </a>
					      </h4>
					    </div>
					   </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" >
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          About Us
					        </a>

					      </h4>
					    </div>
					    <div id="collapseTwo" class="panel-collapse in">
					      <div class="panel-body">
					        <ul>
					        	<li><a href="about-one-health">What is One Health</a></li>
								<li><a href="about-genesis" class="active">Genesis of COHEART</a></li>
								<li><a href="about-mandate">Mandate, Vision and Mission of COHEART</a></li>
								<li><a href="about-scope">Scope of One Health</a></li>
								<li><a href="about-objectives">Objectives, Activities and Roap Map</a></li>
								<li><a href="about-faculties">Faculties</a></li>
					        </ul>
					      </div>
					    </div>
					  </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          Courses
					        </a>
					      </h4>
					    </div>
					    <div id="collapseThree" class="panel-collapse collapse">
					      <div class="panel-body">
					        <ul>
					        	<?php
									require('dbconnect.php');
									$query = mysqli_query($conn,"select id,course_name from courses");
									echo mysqli_error($conn);
									while($data = mysqli_fetch_array($query))
									{
										$course = strtolower($data['course_name']);
										$course = ucwords($course);
										echo "<li><a href='courses?id={$data['id']}'>$course</a></li>";
									}
								?>
					        </ul>
					      </div>
					    </div>
					  </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="gallery">Gallery</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="partners">Partnering Institutes and supporters</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="downloads">Downloads</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="news">News & events</a>
					      </h4>
					    </div>
					   </div>

					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          COHEART Resources
					        </a>

					      </h4>
					    </div>
					    <div id="collapsefour" class="panel-collapse collapse">
					      <div class="panel-body">
					        <ul>
					        	<li><a href="research-vid">Video Library</a></li>
								<li><a href="body">Human Body</a></li>
								<li><a href="map">COHEART Health Map</a></li>
								
							</ul>
					      </div>
					    </div>
					  </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="involve">Get Involved</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="contact">Contact Us</a>
					      </h4>
					    </div>
					   </div>
					</div>
					<?php require('news-updates.php');?>
				</div>
				
				<div class='col-md-9 col-sm-8'>
					
					<div class='row'>
					<div class='col-md-12 div-about'>
							<h3>About Us</h3>
							<div class="content-2 content-about">
							<div class="panel panel-default text-justify">
								  <div class="panel-heading">
								    <h3 class="panel-title">Genesis of COHEART</h3>
								  </div>
								  <div class="panel-body">
								    <p>
										“One soul many hearts” dedicated to improve the lives of all species is what COHEART is all about. Center for One Health Education, Advocacy, Research and Training is a new initiative by Kerala Veterinary and Animal Sciences University (established as per the decisions of the 10th Academic Council and 26th Board of Management). The centre is located in scenic hilly terrain of Pookode in Wayanad district, Kerala, India. COHEART is the first of its kind in the country and aims to develop a tradition of One Health that challenges conventional thinking by building excellence on integrity, ethical behavior and respect for diversity. To understand the genesis of COHEART we need to explore the history of One Health. The approach of One Health stems from the original theory of One Medicine, developed in 1984 by Calvin Schwabe in his book titled <i>Veterinary Medicine and Human Health</i>, advocating a combined medical and veterinary approach to zoonotic disease. Yet, human and animal health developed during the nineteenth and twentieth centuries into fairly segregated disciplines or ‘silos’, separated at the academic, governance and application levels. In recent decades, the concept of ‘One Medicine’ evolving to ‘One Health’ has gained momentum worldwide after the SARS outbreak in 2003, and then driven by fears of a possible pandemic of H5N1 avian influenza. New emerging diseases have led to the return of One Medicine principles and the evolvement into One Health. The major difference between One Medicine and One Health is the addition of ecosystem health into the interface. Ecosystem health is included to incorporate the environment, as well as wildlife populations, and recognizes that sustainable development and continued human and animal health are dependent on healthy surrounding ecosystems. 
									</p>
									<p>
										The scientists across disciplines have now understood that there should be a link between the human and animal medicine, and this was well conveyed during the workshop organized by KVASU in Wayanad district of Kerala on 16th -17th December 2013. The recommendation to establish a Centre for One health was put forth by these experts since they recognized that good health for a nation can be achieved through organized efforts of society at multiple levels and by multiple actors. Dr. Gyanendra  Gongal, Scientist, Disease Surveillance and Epidemiology, WHO Regional Office, Dr. U. V. S. Rana, Joint Director (Rtd), NCDC, Delhi, Dr. Kumar Venkitanarayan, Professor, University of Connecticut, USA,  Dr. C. Ramani, Additional DHS, Directorate of Health Service, Kerala, Dr. K. Udayavarman, President, Kerala State Veterinary Council, Dr. Ravi Jacob Korula, Dean, DM Wayanad Institute of Medical Sciences etc. were some of the experts who put forth the recommendations (See gallery). The recommendations were valued by the University and approved by the Board of Management and an autonomous centre was formally established on 26/2/2014. 
									</p>
									
								  </div>
								</div>
								</div>

					</div>
					</div>
					<div class='row'>
						<div class='col-md-12 visible-xs'>
							<?php require('news-updates.php');?>
						</div>
					</div>
					</div>
				</div>
			
			+
		</div>
<?php require('footer.php');?>	
