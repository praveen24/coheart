<?php

if(isset($_POST) && isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
	
	include("config.inc.php");
	//Get page number from Ajax POST
	if(isset($_POST["page"])){
		$page_number = filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
		if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
	}else{
		$page_number = 1; //if there's no page number, set it to 1
	}
	if( (isset($_POST['search']) && $_POST['search']!="") )
		$results = $mysqli_conn->query("SELECT count(*) FROM news where heading like '%{$_POST['search']}%' or description like '%{$_POST['search']}%' order by date desc");
	else
		$results = $mysqli_conn->query("SELECT count(*) from news order by date desc");
	$get_total_rows = $results->fetch_row(); //hold total records in variable
	//break records into pages
	$total_pages = ceil($get_total_rows[0]/$item_per_page);
	
	//get starting position to fetch the records
	$page_position = (($page_number-1) * $item_per_page);
	
	if( (isset($_POST['search']) && $_POST['search']!="") )
		$results = $mysqli_conn->query("SELECT * FROM news where heading like '%{$_POST['search']}%' or description like '%{$_POST['search']}%' order by date desc LIMIT $page_position, $item_per_page");
	else
		$results = $mysqli_conn->query("SELECT * from news order by date desc LIMIT $page_position, $item_per_page");
	
	//echo '<div align="center">';
	/* We call the pagination function here to generate Pagination link for us. 
	As you can see I have passed several parameters to the function. */
	//echo paginate_function($item_per_page, $page_number, $get_total_rows[0], $total_pages);
	//echo '</div>';
	//Display records fetched from database.
	
	$flag = 0;
	date_default_timezone_set('Asia/Kolkata');	
	while($row = $results->fetch_assoc())
	{
		$date = date_create($row['date']);
		$fulldate = date_format($date,'F j, Y, g:i a');
		echo "<div class='row'><div class='col-md-9'><h4>{$row['heading']}</h4></div><div class='col-md-3 text-right text-muted'>$fulldate</div></div>";
		echo "<p>{$row['description']}</p>";
		$files=glob("admin/uploads/news/{$row['id']}/*.*");
		if(isset($files) && $files!="")
								{
									$imagearray = array(
												  "jpg", 
												  "png", 
												  "jpeg"
												);
									$pdfarray = array("pdf","PDF"); 
									foreach ($files as $key => $value)
									{
										$name = explode("/", $value);
										$name = array_pop($name);
										$ext = pathinfo($name,PATHINFO_EXTENSION);
										if((in_array($ext,$imagearray)))
											echo "<a class='fancybox' href='$value' data-fancybox-group='gallery'><img src='$value' style='width: 150px; vertical-align: text-top; margin-left: 10px; margin-top: 10px;' class='img-responsive img-thumbnail'></a>";
										elseif(in_array($ext, $pdfarray))
											echo "<a href='$value' target='_blank' class='btn btn-primary btn-xs' style='margin-bottom: 5px;'>Download PDF ($name)</a><br>";
									}
		}
		$flag = 1;			
	}
	if($flag == 0)
		echo "<div class='alert alert-info text-danger'>Sorry, no matches found! <span class='glyphicon glyphicon-exclamation-sign pull-right' aria-hidden='true'></span></div>"; 
		
	echo '<div align="center">';
	/* We call the pagination function here to generate Pagination link for us. 
	As you can see I have passed several parameters to the function. */
	echo paginate_function($item_per_page, $page_number, $get_total_rows[0], $total_pages);
	echo '</div>';
}
################ pagination function #########################################
function paginate_function($item_per_page, $current_page, $total_records, $total_pages)
{
    $pagination = '';
    if($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages){ //verify total pages and current page number
        $pagination .= '<ul class="pagination">';
        
        $right_links    = $current_page + 3; 
        $previous       = $current_page - 1; //previous link 
        $next           = $current_page + 1; //next link
        $first_link     = true; //boolean var to decide our first link
        
        if($current_page > 1){
			$previous_link = ($previous==0)?1:$previous;
            $pagination .= '<li class="first"><a href="#" data-page="1" title="First">&laquo;</a></li>'; //first link
            $pagination .= '<li><a href="#" data-page="'.$previous_link.'" title="Previous">&lt;</a></li>'; //previous link
                for($i = ($current_page-2); $i < $current_page; $i++){ //Create left-hand side links
                    if($i > 0){
                        $pagination .= '<li><a href="#" data-page="'.$i.'" title="Page'.$i.'">'.$i.'</a></li>';
                    }
                }   
            $first_link = false; //set first link to false
        }
        
        if($first_link){ //if current active page is first link
            $pagination .= '<li class="first active"><a href="#">'.$current_page.'</a></li>';
        }elseif($current_page == $total_pages){ //if it's the last active link
            $pagination .= '<li class="last active"><a href="#">'.$current_page.'</a></li>';
        }else{ //regular current link
            $pagination .= '<li class="active"><a href="#">'.$current_page.'</a></li>';
        }
                
        for($i = $current_page+1; $i < $right_links ; $i++){ //create right-hand side links
            if($i<=$total_pages){
                $pagination .= '<li><a href="#" data-page="'.$i.'" title="Page '.$i.'">'.$i.'</a></li>';
            }
        }
        if($current_page < $total_pages){ 
				$next_link = ($i > $total_pages)? $total_pages : $i;
                $pagination .= '<li><a href="#" data-page="'.$next_link.'" title="Next">&gt;</a></li>'; //next link
                $pagination .= '<li class="last"><a href="#" data-page="'.$total_pages.'" title="Last">&raquo;</a></li>'; //last link
        }
        
        $pagination .= '</ul>'; 
    }
    return $pagination; //return pagination links
}
?>

