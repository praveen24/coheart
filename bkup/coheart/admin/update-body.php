<?php 
	session_start();
	if( isset($_SESSION['admin'] ) && $_SESSION['admin']!="" )
	{
		require('header.php');
		
?>
	<script>
		window.document.title = 'COHEART | Update-Body';
	</script>
	<div class='div-profile shadow'>
	<div class='row'>
		<div class='col-md-3'>
			<div class="list-group">
				<a href="javascript:void(0);" class="list-group-item menu-top text-center">Menu</a>
				<a href="profile" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-book"></span>
					&nbsp;&nbsp;&nbsp;Add/Remove Courses
				</a>
				<a href="albums" class="list-group-item menu-item active-item">
					<span class="glyphicon glyphicon-picture"></span>
					&nbsp;&nbsp;&nbsp;Create/Remove Albums
				</a>
				<a href="resources" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-briefcase"></span>
					&nbsp;&nbsp;&nbsp;Downloads
				</a>
				<a href="newsevents" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-edit"></span>
					&nbsp;&nbsp;&nbsp;News & Events
				</a>
				<a href="research" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-search"></span>
					&nbsp;&nbsp;&nbsp;Research & Training
				</a>
				<a href="support" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-search"></span>
					&nbsp;&nbsp;&nbsp;Partnering Institutes & Supporters
				</a>
				<a href="chngpwd" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-lock"></span>
					&nbsp;&nbsp;&nbsp;Change Password
				</a>
				<a href="logout" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-log-out"></span>
					&nbsp;&nbsp;&nbsp;Logout
				</a>
			</div>
		</div>
		<div class='col-md-9'>
			<div class="panel panel-default form-panel">
				<div class="panel-heading text-center">
					Update Body
				</div>
				<div class="panel-body">
					<form role="form" name='update-form' method='post' enctype="multipart/form-data">
						<?php

						if(isset($_SESSION['deleteUpdate']) && $_SESSION['deleteUpdate']!="")
							{
								echo "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
								echo "<span class='glyphicon glyphicon-ok'></span> ".$_SESSION['deleteUpdate'];
									unset($_SESSION['deleteUpdate']);
								echo "</div>";
							}
							
							$con=mysqli_connect("localhost","infoaide_coheart","coheart","infoaide_coheart"); // change user_name and password 

					// Check connection
					if (mysqli_connect_errno()) {
					  echo "Failed to connect to MySQL: " . mysqli_connect_error();
					}

					
					// print_r($_REQUEST);
					if(($_REQUEST['heading']) AND ($_REQUEST['content']) ){
						$cont =$_REQUEST['content'];
						$head =$_REQUEST['heading'];
						$file = $_FILES["file"]["name"];

						if ($_FILES["file"]["error"] > 0) {
    					// echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
  						}else {
    					// echo "Upload: " . $_FILES["file"]["name"] . "<br>";
    					// echo "Type: " . $_FILES["file"]["type"] . "<br>";
    					// echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
					    // echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";
					    if (file_exists("uploads/body/" . $_FILES["file"]["name"])) {
					      echo $_FILES["file"]["name"] . " already exists. ";
					    } else {
					      move_uploaded_file($_FILES["file"]["tmp_name"],
					      "uploads/body/" . $_FILES["file"]["name"]);
					      // echo "Stored in: " . "upload/" . $_FILES["file"]["name"];
					    }
					  	}
					  	mysqli_query($con,"INSERT INTO pages (heading, content,img) VALUES ('$head','$cont','$file')");
						echo "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>Updated</div>";
					}

					mysqli_close($con);
						?>
						<div class="form-group">
						<label for="courseName">Heading</label>
						<input type="text" class="form-control" id="heading" name='heading' value=
						'<?php 
							if(isset($_SESSION['data']['albumName']) && $_SESSION['data']['albumName']!="")
							{
								echo $_SESSION['data']['albumName'];
								unset($_SESSION['data']['albumName']);
							}
						?>'>
						
					  </div>
					  <div class="form-group">
						<label for="description">Content</label>
						<textarea class="form-control tinymce" rows="3" id="description" name='content'>
						<?php 
							if(isset($_SESSION['data']['description']) && $_SESSION['data']['description']!="")
							{
								echo $_SESSION['data']['description'];
								unset($_SESSION['data']['description']);
							}
						?>
						</textarea>
						
					  </div>
					  <div class="form-group">
							<label for="upload">Upload File</label>
							<div class="input-group">
								<span class="input-group-btn">
									<span class="btn btn-primary btn-file">
										Browse&hellip; <input type="file" name='file'>
									</span>
								</span>
								<input type="text" class="form-control" readonly style='border:0;height:32px;'>
							</div>
					</div>
					  	<button type="submit" class="btn btn-primary" name='save' value='save'>Update</button>
						<!--<button type="reset" class="btn btn-info">Clear</button>-->
					</form>
				</div>
			</div>
			<div class="panel panel-default form-panel">
				<div class="panel-heading text-center">
					Existing Content
				</div>
				<div class="panel-body">
					<form role="form">
						<div class="form-group">
							Search <input id="filter" type="text" class="form-control"/>
						</div>
						<table class="table table-bordered footable metro-blue" data-filter="#filter" data-page-size="5" data-page-previous-text="prev" data-page-next-text="next" id='course-table'>
					<thead>
						<tr>
							<th>
								#
							</th>
							<th>
								Title
							</th>
							<th data-hide='phone' data-sort-ignore='true' style='text-align:center;'>
								Delete
							</th>
						</tr>
					</thead>
					<tbody>
					<?php
						require('../dbconnect.php');
						$i=1;
						$query = mysqli_query($conn,"select * from pages");
						while($result = mysqli_fetch_array($query))
						{
							echo "<tr>";
							echo "<td>$i</td>";
							echo "<td>{$result['heading']}</td>";
							echo "<td style='text-align:center;'><a href='{$result['id']}'><span class='glyphicon glyphicon-remove delete'></span></a></td>";
							echo "</tr>";
							$i++;
						}
					?>	
					</tbody>
					<tfoot class="hide-if-no-paging">
                <tr>
                    <td colspan="5" class='text-center'>
                        <div class="pagination pagination-centered"></div>
                    </td>
                </tr>
                </tfoot>
				</table>
					</form>
				</div>
			</div>
		</div>
	</div>
	</div>
<?php
		require('footer.php');
	}
	else
		header('location: index');
?>
<script>
	<?php $timestamp = time();?>
	$(function()
	{
		$('#course-table').footable();
		$('.delete').click(function(event)
		{
			event.preventDefault();
			id = $(this).parent().attr('href');
			BootstrapDialog.confirm('Are you sure you want to delete this item?', function(result)
			{
				if(result)
				{
					window.location = 'deleteUpdate?i='+id;
				}
			});
		});
		
	}); 
</script>
<script>
		$(document)
			.on('change', '.btn-file :file', function() {
				var input = $(this),
				numFiles = input.get(0).files ? input.get(0).files.length : 1,
				label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
				input.trigger('fileselect', [numFiles, label]);
		});
		
		$(document).ready( function() {
			$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
				
				var input = $(this).parents('.input-group').find(':text'),
					log = numFiles > 1 ? numFiles + ' files selected' : label;
				
				if( input.length ) {
					input.val(log);
				} else {
					if( log ) alert(log);
				}
				
			});
		});		
	</script>