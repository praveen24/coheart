<?php
	session_start();
	if(isset($_SESSION['admin']) && $_SESSION['admin']!="")
	{
		if(isset($_POST['save']) && $_POST['save']!="")
		{
			$oldpassword = $_POST['oldpwd'];
			$newpassword = $_POST['newpwd'];
			$cnewpassword = $_POST['cnewpwd'];
			$flag = 0;
			if($oldpassword == "")
			{
				$_SESSION['validate']['old']='Old Password field cannot be empty.';
				$flag = 1;
			}
			if($newpassword == "")
			{
				$_SESSION['validate']['new']='New Password field cannot be empty.';
				$flag = 1;
			}
			if($cnewpassword == "")
			{
				$_SESSION['validate']['cnew']='Confirm New Password field cannot be empty.';
				$flag = 1;
			}
			if(strcmp($newpassword,$cnewpassword))
			{
				$_SESSION['validate']['mismatch']='Passwords mismatch.';
				$flag = 1;
			}
			require('../dbconnect.php');
			$oldpassword = md5($oldpassword);
			$newpassword = md5($newpassword);
			$query = mysqli_query($conn,"select * from admin where id={$_SESSION['admin']} and password='$oldpassword'");
			if(!($result = mysqli_fetch_array($query)))
			{
				$flag = 1;
				$_SESSION['validate']['invalid']='Invalid old password.';
			}
			if($flag == 1)
			{
				$_SESSION['data'] = $_POST;
				header('location: chngpwd');
			}
			
				
			if($flag == 0)
			{
				mysqli_autocommit($conn,false);
				if ( mysqli_connect_errno() )
				{
					echo 'There was an error with your connection: '.mysqli_connect_error();
				}
				else
				{
					$query2 = mysqli_query($conn,"update admin set password='$newpassword' where id={$_SESSION['admin']}");
					if($query2)
							{
								mysqli_commit($conn);
								$_SESSION['saved'] = 'Password Changed Successfully.';
								header('location: chngpwd');
							}
							else
								mysqli_rollback($conn);
				}
			}
		}
		else
			header('location: index');
	}
	else
		header('location: index');
?>