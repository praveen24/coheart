		<div class='banner container-fluid'>
			<div class='row hidden-xs'>
				<div class='col-md-3 col-xs-3 text-center'>
					<img src='../dist/images/kvasu-university-logo-500x500.png' width='120' class='img-responsive'>
				</div>
				<div class='col-md-6 col-xs-6 text-center'>
					<h2><strong>CENTER FOR ONE HEALTH EDUCATION ADVOCACY RESEARCH AND TRAINING</strong></h2>
					<h4>Kerala Veterinary and Animal Sciences University</h4>
				</div>
				<div class='col-md-3 col-xs-3 text-center'>
					<img src='../dist/images/kvasu-coheart-logo-500x500.png' width='120' class='img-responsive'>
				</div>
			</div>
			<div class='row visible-xs'>
				<div class='col-xs-12 text-center'>
					<img src='../dist/images/coheart-kvasu-544x126.png' class='img-responsive'>
				</div>
			</div>
		</div>
