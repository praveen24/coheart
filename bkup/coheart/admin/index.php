<?php 
	session_start();
	require('header.php');
;?>
<div class='div-login shadow'>
	<form role="form" class='col-md-4 col-md-offset-4' action='login' method='post'>
		<div class="panel panel-primary">
		  <div class="panel-heading">
			<h3 class="panel-title text-center">Admin Login</h3>
		  </div>
		  <div class="panel-body">
			<div class="left-inner-addon form-group">
				<i class="glyphicon glyphicon-user"></i>
				<input type="text" class="form-control input-lg" placeholder='Username' name='user'/>
			</div>
			<div class="left-inner-addon form-group">
				<i class="glyphicon glyphicon-lock"></i>
				<input type="password" class="form-control input-lg" placeholder='Password' name='password'/>
			</div>
			<?php
			if(isset($_SESSION['log_error']) && $_SESSION['log_error']!="")
			{
				echo "<div class='alert alert-danger'>
						<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
						Invalid Login!
					</div>";
				unset($_SESSION['log_error']);
			}
			if(isset($_SESSION['validate']) && $_SESSION['validate']!="")
			{
				echo "<div class='alert alert-danger'>
						<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
						Please fill out both Username and Password fields!
					</div>";
				unset($_SESSION['validate']);
			}
			?>
			<div class="form-group text-center">
				<button type="submit" class="btn btn-default btn-lg login-button" name='login' value='login'>Login</button>
			</div>
			<div class="form-group text-center">
				<!--<a href='#'>Forgot Password?</a>-->
			</div>
		  </div>
		</div>
		
	</form>
</div>
<?php require('footer.php');?>