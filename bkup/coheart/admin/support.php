<?php 
	session_start();
	if( isset($_SESSION['admin'] ) && $_SESSION['admin']!="" )
	{
		require('header.php');
		
?>
	<script>
		window.document.title = 'COHEART - Partnering Institutes & Supporters';
	</script>
	<div class='div-profile shadow'>
	<div class='row'>
		<div class='col-md-3'>
			<div class="list-group">
				<a href="javascript:void(0);" class="list-group-item menu-top text-center">Menu</a>
				<a href="profile" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-book"></span>
					&nbsp;&nbsp;&nbsp;Add/Remove Courses
				</a>
				<a href="albums" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-picture"></span>
					&nbsp;&nbsp;&nbsp;Create/Remove Albums
				</a>
				<a href="student" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-user"></span>
					&nbsp;&nbsp;&nbsp;Add/Remove Students
				</a>
				<a href="resources" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-briefcase"></span>
					&nbsp;&nbsp;&nbsp;Downloads
				</a>
				<a href="newsevents" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-edit"></span>
					&nbsp;&nbsp;&nbsp;News & Events
				</a>
				<a href="research" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-search"></span>
					&nbsp;&nbsp;&nbsp;Research & Training
				</a>
				<a href="support" class="list-group-item menu-item active-item">
					<span class="glyphicon glyphicon-search"></span>
					&nbsp;&nbsp;&nbsp;Partnering Institutes & Supporters
				</a>
				<a href="map" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-search"></span>
					&nbsp;&nbsp;&nbsp;Health-map
				</a>
				<a href="chngpwd" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-lock"></span>
					&nbsp;&nbsp;&nbsp;Change Password
				</a>
				<a href="logout" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-log-out"></span>
					&nbsp;&nbsp;&nbsp;Logout
				</a>
			</div>
		</div>
		<div class='col-md-9'>
			<div class="panel panel-default form-panel">
				<div class="panel-heading text-center">
					Partnering Institutes & Supporters
				</div>
				<div class="panel-body">
					<form role="form">
					<div class="btn-group" style='margin-bottom: 10px;'>
					  <button type="button" class="btn btn-default active" id='ind'>Individuals</button>
					  <button type="button" class="btn btn-default" id='org'>Organizations</button>
					</div>
					<br/>
					<div class='div-indi'>
						<div class="form-group">
							Search <input id="filter" type="text" class="form-control"/>
						</div>
						<table class="table table-bordered footable metro-blue" data-filter="#filter" data-page-size="5" data-page-previous-text="prev" data-page-next-text="next" id='indi-table'>
					<thead>
						<tr>
							<th>
								#
							</th>
							<th>
								Name
							</th>
							<th data-hide='phone' data-sort-ignore='true' style='text-align:center;'>
								View Details
							</th>
							
						</tr>
						</tr>
					</thead>
					<tbody>
					<?php
						require('../dbconnect.php');
						$i=1;
						$query = mysqli_query($conn,"select * from individuals");
						while($result = mysqli_fetch_array($query))
						{
							echo "<tr>";
							echo "<td>$i</td>";
							echo "<td>{$result['name']}</td>";
							echo "<td style='text-align:center;'><a href='viewindividuals?id={$result['id']}' class='btn btn-primary btn-xs'>View</a></td>"; 
							echo "</tr>";
							$i++;
						}
					?>	
					</tbody>
					<tfoot class="hide-if-no-paging">
                <tr>
                    <td colspan="5" class='text-center'>
                        <div class="pagination pagination-centered"></div>
                    </td>
                </tr>
                </tfoot>
				</table>
				</div>
				<div class='div-org'>
				<div class="form-group">
							Search <input id="filter2" type="text" class="form-control"/>
						</div>
						<table class="table table-bordered footable metro-blue" data-filter="#filter2" data-page-size="5" data-page-previous-text="prev" data-page-next-text="next" id='org-table'>
					<thead>
						<tr>
							<th>
								#
							</th>
							<th>
								Name
							</th>
							<th data-hide='phone' data-sort-ignore='true' style='text-align:center;'>
								View
							</th>
							
						</tr>
					</thead>
					<tbody>
					<?php
						require('../dbconnect.php');
						$i=1;
						$query = mysqli_query($conn,"select * from organizations");
						while($result = mysqli_fetch_array($query))
						{
							echo "<tr>";
							echo "<td>$i</td>";
							echo "<td>{$result['name']}</td>";
							echo "<td style='text-align:center;'><a href='vieworganizations?id={$result['id']}' class='btn btn-primary btn-xs'>View</a></td>"; 
							echo "</tr>";
							$i++;
						}
					?>	
					</tbody>
					<tfoot class="hide-if-no-paging">
                <tr>
                    <td colspan="5" class='text-center'>
                        <div class="pagination pagination-centered"></div>
                    </td>
                </tr>
                </tfoot>
				</table>
				</div>
				
					</form>
				</div>
			</div>
			
			
		</div>
	</div>
	</div>
<?php
		require('footer.php');
	}
	else
		header('location: index');
?>
<script>
	$('#indi-table,#org-table').footable();
	$('#ind').click(function()
	{
		$('#org').removeClass('active');
		$(this).addClass('active');
		$(".div-org").hide();
		$(".div-indi").fadeIn();
	});
	$('#org').click(function()
	{
		$('#ind').removeClass('active');
		$(this).addClass('active');
		$(".div-indi").hide();
		$(".div-org").fadeIn();
	});
	//alert(document.referrer);
</script>