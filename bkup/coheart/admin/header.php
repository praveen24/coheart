<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>COHEART |Home</title>
	<link rel="shortcut icon" href="../dist/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="../dist/images/favicon.ico" type="image/x-icon">
  <!--Datepicker-->
  <link href="datepicker/default.css" rel="stylesheet">
	<!--Confirm-->
	<link rel="stylesheet" href="confirm/bootstrap-dialog.min.css">
	<!--Slider-->
	<link rel="stylesheet" href="../dist/slider/responsiveslides.css">
	<!-- Bootstrap -->
  <link href="../dist/css/bootstrap.css" rel="stylesheet">
	<link href="../dist/css/style.css" rel="stylesheet">
	<link href="table/css/footable.core.css" rel="stylesheet"/>
	<link href="table/css/footable.metro.css" rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
  </head>
  <body>
    <div>
		<?php require('banner.php')?>