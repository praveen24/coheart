<?php
	session_start();
	if(isset($_SESSION['admin']) && $_SESSION['admin']!="")
	{
		if(isset($_POST['save']) && $_POST['save']!="")
		{
			$news = $_POST['news'];
			$description = $_POST['description'];
			$flag = 0;
			
			if($news == "")
			{
				$_SESSION['validate']['course']='Heading cannot be empty';
				$flag = 1;
			}
			if($description == "")
			{
				$_SESSION['validate']['description']='Description cannot be empty';
				$flag = 1;
			}
			if(count($_FILES['upload']['name'])>5)
			{
				$_SESSION['validate']['filesize'] = "Only upto 4 files. Formats: jpg,png,pdf";
				$flag = 1;
			}
			/*if($_FILES['upload']['error']==1)
			{
				$_SESSION['validate']['filesize'] = "File size should be less than 1MB. Formats: jpg,png";
				$flag = 1;
			}*/
			elseif($_FILES['upload']['tmp_name'][0]!="")
			{
				$allowedExts = array(
				  "jpg", 
				  "png", 
				  "jpeg",
				  "pdf",
				  "PDF"
				); 

				$allowedMimeTypes = array( 
				  'image/jpeg',
				  'image/jpg',
				  'image/png',
				  'application/pdf'
				);
				for($i=0; $i<count($_FILES['upload']['name']); $i++)
				{
					$extension = pathinfo($_FILES["upload"]["name"][$i], PATHINFO_EXTENSION);
					
					if(($_FILES['upload']['size'][$i]>2097152) || (!(in_array($extension,$allowedExts))) || (!(in_array($_FILES['upload']['type'][$i],$allowedMimeTypes))))
					{
						$_SESSION['validate']['filesize'] = "File size should be less than 2MB. Formats: jpg,png,pdf";
						$flag = 1;
					}
				}
			}
			
			if($flag == 1)
			{
				$_SESSION['data'] = $_POST;
				header('location: newsevents');
			}
			
				
			if($flag == 0)
			{
				require('../dbconnect.php');
				$description = mysqli_real_escape_string($conn,$_POST['description']);
				mysqli_autocommit($conn,false);
				if ( mysqli_connect_errno() )
				{
					echo 'There was an error with your connection: '.mysqli_connect_error();
				}
				else
				{
					$query = mysqli_query($conn,"insert into news(heading,description,date) values('$news','$description',now())");
					if($query)
					{
						$id = mysqli_insert_id($conn);
						if($_FILES['upload']['tmp_name'][0]!="")
						{
							$dir ="uploads/news/$id";
							if( is_dir($dir) === false )
							{
								mkdir($dir);
							}
							for($i=0; $i<count($_FILES['upload']['name']); $i++)
							{
								$file=$_FILES['upload']['name'][$i];
								$name = explode(".", $file);
								if($file!="")
								{
									$ext= pathinfo($file,PATHINFO_EXTENSION);
									move_uploaded_file($_FILES['upload']['tmp_name'][$i],"uploads/news/$id/{$name[0]}_".$id."_".$i.".".$ext);
								}
							}
						}
						mysqli_commit($conn);
						$_SESSION['saved'] = 'News & Events Added Successfully.';
						header('location: newsevents');
						
					}
					else
						mysqli_rollback($conn);
				}
			}
		}
		else
			header('location: index');
	}
	else
		header('location: index');
?>