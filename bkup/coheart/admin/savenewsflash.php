<?php
	session_start();
	if(isset($_SESSION['admin']) && $_SESSION['admin']!="")
	{
		if(isset($_POST['save']) && $_POST['save']!="")
		{
			$news = $_POST['news2'];
			
			$flag = 0;
			if($news == "")
			{
				$_SESSION['validate2']['course2']='Heading cannot be empty';
				$flag = 1;
			}
			
			/*if($_FILES['upload']['error']==4)
			{
				$_SESSION['validate2']['file'] = "Please choose a file to upload.";
				$flag = 1;
			}*/
			
			else if($_FILES['upload']['error']==0)
			{
				$allowedExts = array(
				  "pdf", 
				  "doc", 
				  "docx"
				); 

				$allowedMimeTypes = array( 
				  'application/msword',
				  'application/pdf',
				  'text/pdf'
				);
				$extension = end(explode(".", $_FILES["upload"]["name"]));
			
				if((!(in_array($extension,$allowedExts))) || (!(in_array($_FILES['upload']['type'],$allowedMimeTypes))))
				{
					$_SESSION['validate2']['filesize2'] = "File Formats: pdf,doc";
					$flag = 1;
				}
				
			}	
			if($flag == 1)
			{
				$_SESSION['data2'] = $_POST;
				header('location: newsevents');
			}
			
				
			if($flag == 0)
			{
				require('../dbconnect.php');
				$news = mysqli_real_escape_string($conn,$_POST['news2']);
				mysqli_autocommit($conn,false);
				if ( mysqli_connect_errno() )
				{
					echo 'There was an error with your connection: '.mysqli_connect_error();
				}
				else
				{
					$query = mysqli_query($conn,"insert into newsflash(title,date) values('$news',curdate())");
					echo mysqli_error($conn);
					if($query)
					{
						$id = mysqli_insert_id($conn);
						$file=$_FILES['upload']['name'];
						if($file!="")
						{
						$files = explode(".", $file);
						$ext= pathinfo($file,PATHINFO_EXTENSION);
						if(move_uploaded_file($_FILES['upload']['tmp_name'],"uploads/news-flash/".$files[0]."-".$id.".".$ext))
						{
							$filename=$files[0]."-".$id.".".$ext;
							$query2 = mysqli_query($conn,"update newsflash set file='$filename' where id=$id");
							if($query2)
							{
								mysqli_commit($conn);
								$_SESSION['saved'] = 'News Flash Added Successfully.';
								header('location: newsevents');
							}
							else
								mysqli_rollback($conn);
						}
						}
						else
						{
							mysqli_commit($conn);
							$_SESSION['saved'] = 'News Flash Added Successfully.';
							header('location: newsevents');
						}
					}
					else
						mysqli_rollback($conn);
				}
			}
		}
		else
			header('location: index');
	}
	else
		header('location: index');
?>