<?php 
	session_start();
	if( isset($_SESSION['admin'] ) && $_SESSION['admin']!="" )
	{
		require('header.php');
		
?>
	<script>
		window.document.title = 'COHEART | Change Password';
	</script>
	<div class='div-profile shadow'>
	<div class='row'>
		<div class='col-md-3'>
			<div class="list-group">
				<a href="javascript:void(0);" class="list-group-item menu-top text-center">Menu</a>
				<a href="profile" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-book"></span>
					&nbsp;&nbsp;&nbsp;Add/Remove Courses
				</a>
				<a href="albums" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-picture"></span>
					&nbsp;&nbsp;&nbsp;Create/Remove Albums
				</a>
				<a href="student" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-user"></span>
					&nbsp;&nbsp;&nbsp;Add/Remove Students
				</a>
				<a href="resources" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-briefcase"></span>
					&nbsp;&nbsp;&nbsp;Downloads
				</a>
				<a href="newsevents" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-edit"></span>
					&nbsp;&nbsp;&nbsp;News & Events
				</a>
				<a href="research" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-search"></span>
					&nbsp;&nbsp;&nbsp;Research & Training
				</a>
				<a href="support" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-search"></span>
					&nbsp;&nbsp;&nbsp;Partnering Institutes & Supporters
				</a>
				<a href="map" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-search"></span>
					&nbsp;&nbsp;&nbsp;Health-map
				</a>
				<a href="chngpwd" class="list-group-item menu-item active-item">
					<span class="glyphicon glyphicon-lock"></span>
					&nbsp;&nbsp;&nbsp;Change Password
				</a>
				<a href="logout" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-log-out"></span>
					&nbsp;&nbsp;&nbsp;Logout
				</a>
			</div>
		</div>
		<div class='col-md-9'>
			<div class="panel panel-default form-panel">
				<div class="panel-heading text-center">
					Change Password
				</div>
				<div class="panel-body">
					<form role="form" name='album-form' method='post' action='savepassword' enctype='multipart/form-data'>
						<?php
							if(isset($_SESSION['validate']) && $_SESSION['validate']!="")
							{
								echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
								if(isset($_SESSION['validate']['old']) && $_SESSION['validate']['old']!="")
								{	
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['old'].".<br/>";
									unset($_SESSION['validate']['old']);
								}
								if(isset($_SESSION['validate']['new']) && $_SESSION['validate']['new']!="")
								{
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['new'].".<br/>";
									unset($_SESSION['validate']['new']);		
								}
								if(isset($_SESSION['validate']['cnew']) && $_SESSION['validate']['cnew']!="")
								{
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['cnew'].".<br/>";
									unset($_SESSION['validate']['cnew']);		
								}
								if(isset($_SESSION['validate']['mismatch']) && $_SESSION['validate']['mismatch']!="")
								{
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['mismatch'].".<br/>";
									unset($_SESSION['validate']['mismatch']);		
								}
								if(isset($_SESSION['validate']['invalid']) && $_SESSION['validate']['invalid']!="")
								{
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['invalid'].".<br/>";
									unset($_SESSION['validate']['invalid']);		
								}
								echo "</div>";
								unset($_SESSION['validate']);
							}
							if(isset($_SESSION['saved']) && $_SESSION['saved']!="")
							{
								echo "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
								echo "<span class='glyphicon glyphicon-ok'></span> ".$_SESSION['saved'];
									unset($_SESSION['saved']);
								echo "</div>";
							}
							
						?>
						<div class="form-group">
							<label for="old">Old Password</label>
							<input type="password" class="form-control" id="old" name='oldpwd'>
						</div>
						<div class="form-group">
							<label for="new">New Password</label>
							<input type="password" class="form-control" id="new" name='newpwd'>
						</div>
						<div class="form-group">
							<label for="new">Confirm New Password</label>
							<input type="password" class="form-control" id="cnew" name='cnewpwd'>
						</div>
					  	<button type="submit" class="btn btn-primary" name='save' value='save'>Submit</button>
						<!--<button type="reset" class="btn btn-info">Clear</button>-->
					</form>
				</div>
			</div>
			
		</div>
	</div>
	</div>
<?php
		require('footer.php');
	}
	else
		header('location: index');
?>
