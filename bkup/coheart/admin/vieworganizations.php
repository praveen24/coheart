<?php 
	session_start();
	if( isset($_SESSION['admin'] ) && $_SESSION['admin']!="" )
	{
		require('header.php');
		
?>
	<script>
		window.document.title = 'COHEART - Partnering Institutes & Supporters';
	</script>
	<div class='div-profile shadow'>
	<div class='row'>
		<div class='col-md-3'>
			<div class="list-group">
				<a href="javascript:void(0);" class="list-group-item menu-top text-center">Menu</a>
				<a href="profile" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-book"></span>
					&nbsp;&nbsp;&nbsp;Add/Remove Courses
				</a>
				<a href="albums" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-picture"></span>
					&nbsp;&nbsp;&nbsp;Create/Remove Albums
				</a>
				<a href="resources" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-briefcase"></span>
					&nbsp;&nbsp;&nbsp;Downloads
				</a>
				<a href="newsevents" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-edit"></span>
					&nbsp;&nbsp;&nbsp;News & Events
				</a>
				<a href="research" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-search"></span>
					&nbsp;&nbsp;&nbsp;Research & Training
				</a>
				<a href="support" class="list-group-item menu-item active-item">
					<span class="glyphicon glyphicon-search"></span>
					&nbsp;&nbsp;&nbsp;Partnering Institutes & Supporters
				</a>
				<a href="chngpwd" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-lock"></span>
					&nbsp;&nbsp;&nbsp;Change Password
				</a>
				<a href="logout" class="list-group-item menu-item">
					<span class="glyphicon glyphicon-log-out"></span>
					&nbsp;&nbsp;&nbsp;Logout
				</a>
			</div>
		</div>
		<div class='col-md-9'>
			<div class="panel panel-default form-panel">
				<div class="panel-heading text-center">
					Details of Individuals
				</div>
				<div class="panel-body">
					<form role="form">
						<div class='well' style='color: #000000'>
							<?php
							if(isset($_GET['id']) && $_GET['id']!="")
							{
								require('../dbconnect.php');
								$query = mysqli_query($conn,"select * from organizations where id={$_GET['id']}");
								if($data = mysqli_fetch_array($query))
								{
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Organization Name</div>";
									echo "<div class='col-md-8'>{$data['name']}</div>";
									echo "</div>";
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Narrative Description</div>";
									echo "<div class='col-md-8'>{$data['narrative']}</div>";
									echo "</div>";
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Purpose</div>";
									echo "<div class='col-md-8'>{$data['purpose']}</div>";
									echo "</div>";
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Type of Organization</div>";
									echo "<div class='col-md-8'>{$data['type']}</div>";
									echo "</div>";
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Country, State & City</div>";
									echo "<div class='col-md-8'>{$data['csc']}</div>";
									echo "</div>";
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Organization Objective</div>";
									echo "<div class='col-md-8'>{$data['activities']}</div>";
									echo "</div>";
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Contact Person's Name</div>";
									echo "<div class='col-md-8'>{$data['contact']}</div>";
									echo "</div>";
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Email</div>";
									echo "<div class='col-md-8'>{$data['email']}</div>";
									echo "</div>";
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Organization Website</div>";
									echo "<div class='col-md-8'><a href='{$data['website']}' target='_blank'>{$data['website']}</a></div>";
									echo "</div>";
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Participants and Key Collaborators</div>";
									echo "<div class='col-md-8'>{$data['participants']}</div>";
									echo "</div>";
									
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Additional Information</div>";
									echo "<div class='col-md-8'>{$data['additional']}</div>";
									echo "</div>";
								}
							}
							?>
							
						</div>
						
					</form>
					<button type="button" class="btn btn-warning btn-sm pull-right" onclick='window.history.go(-1);'><span class="glyphicon glyphicon-chevron-left"></span> Go Back</button>
				</div>
			</div>
			
			
		</div>
	</div>
	</div>
<?php
		require('footer.php');
	}
	else
		header('location: index');
?>
