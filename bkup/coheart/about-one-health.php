<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>COHEART | About One Health</title>
	<link rel="shortcut icon" href="dist/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="dist/images/favicon.ico" type="image/x-icon">
	<!--Slider-->
	<link rel="stylesheet" href="dist/slider/responsiveslides.css">
	
	<!--FancyBox-->
	<link rel="stylesheet" href="dist/lightbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<!-- Bootstrap -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
	<link href="dist/css/style.css" rel="stylesheet">
	<link href="admin/table/css/footable.core.css" rel="stylesheet"/>
	<link href="admin/table/css/footable.metro.css" rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
	<!--<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>-->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>
    <div class=''>
		<?php require('header.php');?>
		<nav class="navbar navbar-default visible-xs " role="navigation">
					<div class="container-fluid">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
						  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
						  <a class="navbar-brand" href="#">Menu</a>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li><a href="index">Home</a></li>
						<li class="dropdown active">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<li><a href="about-one-health">What is One Health</a></li>
								<li><a href="about-genesis">Genesis of COHEART</a></li>
								<li><a href="about-mandate">Mandate, Vision and Mission of COHEART</a></li>
								<li><a href="about-scope">Scope of One Health</a></li>
								<li><a href="about-objectives">Objectives, Activities and Roap Map</a></li>
								<li><a href="about-faculties">Faculties</a></li>
							  </ul>
						</li>
						
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Courses <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<?php
									require('dbconnect.php');
									$query = mysqli_query($conn,"select id,course_name from courses");
									echo mysqli_error($conn);
									while($data = mysqli_fetch_array($query))
									{
										$course = strtolower($data['course_name']);
										$course = ucwords($course);
										echo "<li><a href='courses?id={$data['id']}'>$course</a></li>";
									}
								?>
							  </ul>
						</li>
						<li><a href="gallery">Gallery</a></li>
						<li><a href="partners">Partnering Institutes and supporters</a></li>
						<li><a href="downloads">Downloads</a></li>
						<li><a href="news">News & events</a></li>
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">COHEART Resources <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<li><a href="research-vid">Video Library</a></li>
								<li><a href="body">Human Body</a></li>
								<li><a href="map">COHEART Health Map</a></li>
							  </ul>
						</li>
						<li><a href="research">Research & Training</a></li>
						<li><a href="involve">Get Involved</a></li>
						<li><a href="contact">Contact Us</a></li>
							</ul>
							
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
		</nav>
		<div class='content'>
			<div class='row'>
				<div class='col-md-3 col-sm-4 visible-md visible-lg visible-sm'>

					<div class="panel-group" id="accordion">
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-parent="#accordion" href="index">
					          Home
					        </a>
					      </h4>
					    </div>
					   </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" class="active">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          About Us
					        </a>

					      </h4>
					    </div>
					    <div id="collapseTwo" class="panel-collapse in">
					      <div class="panel-body">
					        <ul>
					        	<li><a href="about-one-health" class="active">What is One Health</a></li>
								<li><a href="about-genesis">Genesis of COHEART</a></li>
								<li><a href="about-mandate">Mandate, Vision and Mission of COHEART</a></li>
								<li><a href="about-scope">Scope of One Health</a></li>
								<li><a href="about-objectives">Objectives, Activities and Roap Map</a></li>
								<li><a href="about-faculties">Faculties</a></li>
					        </ul>
					      </div>
					    </div>
					  </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          Courses
					        </a>
					      </h4>
					    </div>
					    <div id="collapseThree" class="panel-collapse collapse">
					      <div class="panel-body">
					        <ul>
					        	<?php
									require('dbconnect.php');
									$query = mysqli_query($conn,"select id,course_name from courses");
									echo mysqli_error($conn);
									while($data = mysqli_fetch_array($query))
									{
										$course = strtolower($data['course_name']);
										$course = ucwords($course);
										echo "<li><a href='courses?id={$data['id']}'>$course</a></li>";
									}
								?>
					        </ul>
					      </div>
					    </div>
					  </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="gallery">Gallery</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="partners">Partnering Institutes and supporters</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="downloads">Downloads</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="news">News & events</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          COHEART Resources
					        </a>

					      </h4>
					    </div>
					    <div id="collapsefour" class="panel-collapse collapse">
					      <div class="panel-body">
					        <ul>
					        	<li><a href="research-vid">Video Library</a></li>
								<li><a href="body">Human Body</a></li>
								<li><a href="map">COHEART Health Map</a></li>
								
							</ul>
					      </div>
					    </div>
					  </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="research">Research & Training</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="involve">Get Involved</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="contact">Contact Us</a>
					      </h4>
					    </div>
					   </div>
					</div>
					<?php require('news-updates.php');?>
				</div>
				
				<div class='col-md-9 col-sm-8'>
					
					<div class='row'>
					<div class='col-md-12 div-about'>
							<h3>About Us</h3>
							<div class="content-1 content-about">
							<div class="panel panel-default text-justify">
								  <div class="panel-heading">
								    <h3 class="panel-title">What is One Health</h3>
								  </div>
								  <div class="panel-body">
								    <p>
										The idea of One Medicine, One Health means that we recognize interconnection of human, animal, and environmental health. There is no generally accepted definition of One Health. One definition, used by the American Veterinary Medical Association (AVMA), FAO, OIE, WHO, UNSIC, UNICEF and World Bank in the ―Strategic Framework on One Health- published in 2008. They mentioned that One Health is: <b>the collaborative efforts of multiple disciplines working locally, nationally and globally to attain optimal health for people, animals and our environment</b>One Health (OH) means both ―better cooperation between physicians and veterinarians and ―working at the interaction [interface] between human/ animal health and the ecosystem. This concept dates back to the Greek physician Hippocrates (ca. 460 BCE–ca.370) who traced the impact of environment on human health in his text "On Airs, Waters and Places”. In the 1800s, Rudolf Virchow stated that there should be no dividing line between human and animal medicine. In the 1900s, Calvin Schwabe traced the roots of Western medicine to the “one medicine” of ancient Egyptian and Dinka priests, and argued that we need to return to those roots. Similar pathways of understanding can be traced through ancient Chinese, Arab, and Indian cultures. In India, linkages of nature with human and animal can also be seen in vedas, Galenic scripts and the manuscripts of Kautilya and Ashoka.
									</p>
									<p>
										The convergence of people, animals, and our environment has created a new dynamic in which the health of each group is inextricably interconnected. Out of the 1,461 diseases now recognized in humans, approximately 60% are due to multi-host pathogens characterized by their movement across species lines and, over the last three decades, approximately 75% of new emerging human infectious diseases have been identified as zoo-notic. Our increasing interdependence with animals and their products may well be the single most critical risk factor to our health and well-being with regard to infectious diseases. Healthy environment and healthy animals are essential for healthy life; but it also depends on how community uses both of them. Drastic ‘economic development’ that caused money melting affected the human life. Cross-disciplinary international collaborations are required in tackling difficult global health problems.
									</p>
									<h5>
										<i>Why is a 'One Health' Approach important now?</i>
									</h5>
									<ul>
										<li>
											Despite research, there is steady increase in human diseases acquired from animals. About 75% of new emerging human infectious diseases are zoonotic.
										</li>
										<li>
											Local communities are affected by global issues such as rabies control, vector borne diseases, and emergency preparedness, among many others. One Health recognizes the importance of 'thinking globally while acting locally.'
										</li>
										<li>
											Despite sophisticated laboratory facilities in the country, there is a lack of communication of research findings.
										</li>
										<li>
											Despite vaccination programs, we were not been able to control outbreaks of many economically important diseases.
										</li>
										<li>
											Despite 64 years of freedom, green revolution, economic liberalization, 8% economic growth rate in trade, economic liberalization and scientific advancement, a large number of children (42%?) remain under-nourished. 
										</li>
										<li>
											Despite white revolution, majority of the milk marketed in India is reported to be adulterated (media report).
										</li>
										<li>
											Despite 12% budget on drugs & advance in molecular biology newer diseases, deficiencies, crises keep coming up.
										</li>
										<li>
											Despite good education, close community living: conflicts, up rise, terrorism, war, pollution, waste, disasters are on the rise
										</li>
										<li>
											Despite hype of advanced technology, increasing training institutions, modern hospitals, broader insurance policies, good laws & media coverage, health service does not get universal coverage 
										</li>
										<li>
											Despite decentralization community participation is less than optimal 
										</li>
									</ul>
									<p>
										<em>One Health</em>, a new interdisciplinary approach, has been embraced as a way forward by many groups of professional experts. One Health thinking seeks to include human, animal and environmental experts in problem solving. Internationally applauded, the One Health approach is strongly endorsed by human medicine, public health, veterinary medicine, the CDC, and various other national and international institutions and organizations. The Prime Minister of India, Dr. Manmohan Singh while addressing the delegates of “International Ministerial Conference on Avian and Pandemic Influenza”on 5th December, 2007 stated that the Government of India supports the concept of 'ONE HEALTH' based on an integrated approach to both animal and human health.Having understood the need for One Health, we should develop strategies to adopt One Health practices. This could be facilitated by establishment of Centre for One Health. 
									</p>
								  </div>
								</div>
								</div>

					</div>
					</div>
					<div class='row'>
						<div class='col-md-12 visible-xs'>
							<?php require('news-updates.php');?>
						</div>
					</div>
					</div>
				</div>
		
		</div>
<?php require('footer.php');?>	
