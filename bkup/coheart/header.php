<div class='banner'>
			<div class='container'>
				<div class='row hidden-xs'>
					<div class='col-md-1 col-md-offset-1 col-xs-1 col-md-offset-1 text-center'>
											<embed height="150" width="130" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" wmode="transparent" menu="false" quality="high" src="dist/images/coheart-logo.swf" style="display: block !important;">
					</div>
					<div class="col-md-6 col-xs-6">
						<div class="row">
							<div class="col-md-12 text-center">
								<h3 style="color: #fff;"><strong>CENTER for ONE HEALTH EDUCATION ADVOCACY RESEARCH and TRAINING</strong></h3>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 text-center">
								<h4>Kerala Veterinary and Animal Sciences University</h4>
								<h4>Pookode, Wayanad Kerala - 673576</h4>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-xs-4">
						<div class="row">
							<div class="col-md-12 text-right">
								<h4 class="man"><i>For multidisciplinary holistic approach for harmony among Man, Animal and Nature (M.A.N)</i></h4>
							</div>
						</div>
						<div class="row">
							<div class="col-md-offset-3 col-md-9 text-right google-search">
								<script>
								  (function() {
								    var cx = '001763096455298922418:gkxx4zxqozc';
								    var gcse = document.createElement('script');
								    gcse.type = 'text/javascript';
								    gcse.async = true;
								    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
								        '//www.google.com/cse/cse.js?cx=' + cx;
								    var s = document.getElementsByTagName('script')[0];
								    s.parentNode.insertBefore(gcse, s);
								  })();
								</script>
								<gcse:search></gcse:search>
							</div>
						</div>
					</div>
				</div>
			</div><!--/container-->
		
			<div class='row visible-xs'>
				<div class='col-xs-12 text-center'>
					<a href="index"><img src='dist/images/kvasu-coheart-logo-800X212.png' alt="kvasu logo" width='544' height='126' class='img-responsive'></a>
				</div>
			</div>
			<div class="row visible-xs">
				<div class="col-md-12">
					<script>
					  (function() {
						var cx = '001763096455298922418:gkxx4zxqozc';
						var gcse = document.createElement('script');
						gcse.type = 'text/javascript';
						gcse.async = true;
						gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							'//www.google.com/cse/cse.js?cx=' + cx;
						var s = document.getElementsByTagName('script')[0];
						s.parentNode.insertBefore(gcse, s);
					  })();
					</script>
					<gcse:search></gcse:search>
				</div>
			</div>
		</div><!--/banner-->