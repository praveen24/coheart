<div id="footer">
      <div class="container">
		<div class='row'>
		<div class='col-md-6 col-sm-6 col-lg-6 text-left' style='padding-left: 30px'>
			<p class="text-muted">© COHEART. All Rights Reserved</p>
		</div>
		<div class='col-md-6 col-sm-6 col-lg-6 text-right' style='padding-right: 30px'>
			<p class="text-muted">Powered by : <a href='http://aidersolutions.in' target='_blank'>Aider Solutions</a></p>
		</div>
		</div>
      </div>
</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="dist/js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="dist/js/bootstrap.min.js"></script>
	<!--Slider-->
	<script src="dist/slider/responsiveslides.min.js"></script>
	<!--FancyBox-->
	<script src="dist/lightbox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
	<script src="dist/lightbox/source/jquery.fancybox.js?v=2.1.5"></script>
	<!--Table-->
	<script src="admin/table/js/footable.js"></script>
	<script src="admin/table/js/footable.sort.js"></script>
	<script src="admin/table/js/footable.filter.js"></script>
	<script src="admin/table/js/footable.paginate.js"></script>
	<!--Validation-->
	<script src="validator/jquery.form-validator.js"></script>
	<script type="text/javascript" src="ticker/jquery.webticker.js"></script>
	<script>
		$(document).on('click.collapse.data-api', '.accordion-toggle', function(event) {
			var $this = $(this),
				parent = $this.data('parent'),
				$parent = parent && $(parent);

			if ($parent) {
				$parent.find('[data-toggle=collapse][data-parent=' + parent + ']').not($this).addClass('collapsed');
			}
		});
		$(function(){
		$("#webticker").webTicker(
		{
			speed: 60,
			 duplicate: true
		});
		});
		$(function(){
			$("#slider1").responsiveSlides();
			$(".merged input").on({
				focus: function() {
					$(this).prev().addClass("focusedInput")
				},
				blur: function() {
					$(this).prev().removeClass("focusedInput")
				}
			});
			});
		
	</script>
  </body>
</html>