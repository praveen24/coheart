<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="coheart, kvasu, veterinary, one, health, university, kerala, animal, science, pookode,diploma,certification,course, education, hygiene,Education, Advocacy, Research, Training "/>
   	<meta name="description" content="One Health concept is based on the understanding that the health of the humans, animals and the environment is inextricably linked, and that promoting the well being of all species can only be achieved through a holistic multidisciplinary approach at the human-animal-ecosystems interface. Kerala Veterinary and Animal Sciences University (KVASU) has taken a pioneer step in this regard following the establishment of the Centre for One Health Education Advocacy Research and Training, the first of its kind in India aiming at the sustained health of the community by addressing various issue of concern today like the food safety and security, zoonoses, diseases from natural origins like soil, water and air."/>
    <meta name="google-site-verification" content="Sg2CCDPE0zio41cI6pF2SkUWVUX1nSGT8waFfR8S6f0" />
    <title>COHEART | Student Admin Panel</title>
	<link rel="shortcut icon" href="dist/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="dist/images/favicon.ico" type="image/x-icon">
	<!--jscrollpane-->
	<link type="text/css" href="jScrollPane/jquery.jscrollpane.css" rel="stylesheet" media="all" />
	<!--Slider-->
	<link rel="stylesheet" href="dist/slider/responsiveslides.css">
	<!--flash-->
	<link rel="stylesheet" href="ticker/example/assets/webticker.css" type="text/css" media="screen">
	<!--FancyBox-->
	<!--tabs-->
	<link rel="stylesheet" type="text/css" href="tabs/css/easy-responsive-tabs.css " />
	<link rel="stylesheet" href="dist/lightbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<!-- Bootstrap -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
	<link href="dist/css/style.css" rel="stylesheet">
	<link href="admin/table/css/footable.core.css" rel="stylesheet"/>
	<link href="admin/table/css/footable.metro.css" rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
	<!--<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>-->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
  </head>
  <body>
   
		<div class='banner'>
			<div class='container'>
				<div class='row hidden-xs'>
					<div class='col-md-1 col-md-offset-1 col-xs-1 col-md-offset-1 text-center'>
						<embed height="150" width="130" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" wmode="transparent" menu="false" quality="high" src="dist/images/coheart-logo.swf" style="display: block !important;">
					</div>
					<div class="col-md-6 col-xs-6">
						<div class="row">
							<div class="col-md-12 text-center">
								<h3 style="color: #fff;"><strong>CENTER for ONE HEALTH EDUCATION ADVOCACY RESEARCH and TRAINING</strong></h3>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 text-center">
								<h4>Kerala Veterinary and Animal Sciences University</h4>
								<h4>Pookode, Wayanad Kerala - 673576</h4>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-xs-4">
						<div class="row">
							<div class="col-md-12 text-right">
								<h4 class="man"><i>"For multidisciplinary holistic approach for harmony among Man, Animal and Nature (M.A.N)"</i></h4>
							</div>
						</div>
						<div class="row">
							<div class="col-md-offset-3 col-md-9 text-right google-search">
								<!--<script>
								  (function() {
								    var cx = '001763096455298922418:gkxx4zxqozc';
								    var gcse = document.createElement('script');
								    gcse.type = 'text/javascript';
								    gcse.async = true;
								    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
								        '//www.google.com/cse/cse.js?cx=' + cx;
								    var s = document.getElementsByTagName('script')[0];
								    s.parentNode.insertBefore(gcse, s);
								  })();
								</script>
								<gcse:search></gcse:search>-->
							</div>
						</div>
					</div>
				</div>
			</div><!--/container-->
		
			<div class='row visible-xs'>
				<div class='col-xs-12 text-center'>
					<a href="index"><img src='dist/images/kvasu-coheart-logo-800X212.png' alt="kvasu logo" width='544' height='126' class='img-responsive'></a>
				</div>
			</div>
			<div class="row visible-xs">
				<div class="col-md-12">
					<!--<script>
					  (function() {
						var cx = '001763096455298922418:gkxx4zxqozc';
						var gcse = document.createElement('script');
						gcse.type = 'text/javascript';
						gcse.async = true;
						gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							'//www.google.com/cse/cse.js?cx=' + cx;
						var s = document.getElementsByTagName('script')[0];
						s.parentNode.insertBefore(gcse, s);
					  })();
					</script>
					<gcse:search></gcse:search>-->
				</div>
			</div>
		</div><!--/banner-->
	
	<div class="student-blog">
		<div class="container full-width table-container">
		<div class="row table-row">
			<div class="col-md-3 sidebar table-col no-padding">
				<div class="side-menu text-center">
					<a href="studentBlog?id=<?php echo $_GET['id'];?>"><span class="glyphicon glyphicon-circle-arrow-left" title="Go Back" data-toggle="tooltip" data-placement="bottom"></span></a>
					<br><br>
					<img src="dist/images/default-pic.png" class="img-responsive img-thumbnail">
					<br><br>
					<div class="student-name">Student Name</div>
					<br>
					<a class="btn btn-default logout" href=""  title="Logout" data-toggle="tooltip" data-placement="bottom">Logout <span class="glyphicon glyphicon-log-out"></span> </a>
				</div>
			</div>
			<div class="col-md-9 table-col">
				<div id="blog-content">
					<div class="about-author">
						<h4>About Author</h4>
					</div>
					<div class="tab-container">
						<div id="parentHorizontalTab">
				            <ul class="resp-tabs-list hor_1">
				                <li>Change Password </li>
				                <li>My Blogs </li>
				                <li>My Profile </li>
				            </ul>
				            <div class="resp-tabs-container hor_1">
				                 <div>
				                    adada
				                    <br>
				                    <br>
				                    
				                </div>
				                <div>
				                    2 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nibh urna, euismod ut ornare non, volutpat vel tortor. Integer laoreet placerat suscipit. Sed sodales scelerisque commodo. Nam porta cursus lectus. Proin nunc erat, gravida a facilisis quis, ornare id lectus. Proin consectetur nibh quis urna gravida mollis.
				                    <br>
				                    <br>
				                    
				                </div>
				                <div>
				                    3 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nibh urna, euismod ut ornare non, volutpat vel tortor. Integer laoreet placerat suscipit. Sed sodales scelerisque commodo. Nam porta cursus lectus. Proin nunc erat, gravida a facilisis quis, ornare id lectus. Proin consectetur nibh quis urna gravida mollis.
				                    <br>
				                    <br>
				                    
				                </div>
				            </div>
				        </div>
			        </div>
				</div>
			</div>
		</div>
		</div>
	</div>


	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-50095931-2', 'coheart.ac.in');
	  ga('send', 'pageview');
	</script>

<?php require('footer.php');?>	
<script src="tabs/js/easyResponsiveTabs.js"></script>
<script type="text/javascript" src="jScrollPane/jquery.mousewheel.js"></script>
<script type="text/javascript" src="jScrollPane/jquery.jscrollpane.min.js"></script>
<script type="text/javascript">
		$(function () {
		  $('[data-toggle="tooltip"]').tooltip();
		  
		  $('#parentHorizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            tabidentify: 'hor_1', // The tab groups identifier
            
        });

		});
	</script>