<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="coheart, kvasu, veterinary, one, health, university, kerala, animal, science, pookode,diploma,certification,course, education, hygiene,Education, Advocacy, Research, Training, about "/>
    <title>COHEART | About One Health</title>
	<link rel="shortcut icon" href="dist/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="dist/images/favicon.ico" type="image/x-icon">
	<!--Slider-->
	<link rel="stylesheet" href="dist/slider/responsiveslides.css">
	
	<!--FancyBox-->
	<link rel="stylesheet" href="dist/lightbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<!-- Bootstrap -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
	<link href="dist/css/style.css" rel="stylesheet">
	<link href="admin/table/css/footable.core.css" rel="stylesheet"/>
	<link href="admin/table/css/footable.metro.css" rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
	<!--<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>-->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

  </head>
  <body>
    <div class=''>
		<?php require('header.php');?>
		<nav class="navbar navbar-default visible-xs " role="navigation">
					<div class="container-fluid">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
						  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
						  <a class="navbar-brand" href="#">Menu</a>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li><a href="index">Home</a></li>
						<li class="dropdown active">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<li><a href="about-one-health">What is One Health</a></li>
								<li><a href="about-genesis">Genesis of COHEART</a></li>
								<li><a href="about-mandate">Mandate, Vision and Mission of COHEART</a></li>
								<li><a href="about-scope">Scope of One Health</a></li>
								<li><a href="about-objectives">Objectives, Activities and Roap Map</a></li>
								<li><a href="about-faculties">Faculties</a></li>
							  </ul>
						</li>
						
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Courses <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<?php
									require('dbconnect.php');
									$query = mysqli_query($conn,"select id,course_name from courses");
									echo mysqli_error($conn);
									while($data = mysqli_fetch_array($query))
									{
										$course = strtolower($data['course_name']);
										$course = ucwords($course);
										echo "<li><a href='courses?id={$data['id']}'>$course</a></li>";
									}
								?>
							  </ul>
						</li>
						<li><a href="gallery">Gallery</a></li>
						<li><a href="partners">Partnering Institutes and supporters</a></li>
						<li><a href="downloads">Downloads</a></li>
						<li><a href="news">News & events</a></li>
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">COHEART Resources <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<li><a href="research-vid">Video Library</a></li>
								<li><a href="body">Human Body</a></li>
								<li><a href="map">COHEART Health Map</a></li>
							  </ul>
						</li>
						<li><a href="involve">Get Involved</a></li>
						<li><a href="contact">Contact Us</a></li>
							</ul>
							
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
		</nav>
		<div class='content'>
			<div class='row'>
				<div class='col-md-3 col-sm-4 visible-md visible-lg visible-sm'>

					<div class="panel-group" id="accordion">
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-parent="#accordion" href="index">
					          Home
					        </a>
					      </h4>
					    </div>
					   </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" >
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          About Us
					        </a>

					      </h4>
					    </div>
					    <div id="collapseTwo" class="panel-collapse in">
					      <div class="panel-body">
					        <ul>
					        	<li><a href="about-one-health">What is One Health</a></li>
								<li><a href="about-genesis">Genesis of COHEART</a></li>
								<li><a href="about-mandate">Mandate, Vision and Mission of COHEART</a></li>
								<li><a href="about-scope">Scope of One Health</a></li>
								<li><a href="about-objectives" class="active">Objectives, Activities and Roap Map</a></li>
								<li><a href="about-faculties">Faculties</a></li>
					        </ul>
					      </div>
					    </div>
					  </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          Courses
					        </a>
					      </h4>
					    </div>
					    <div id="collapseThree" class="panel-collapse collapse">
					      <div class="panel-body">
					        <ul>
					        	<?php
									require('dbconnect.php');
									$query = mysqli_query($conn,"select id,course_name from courses");
									echo mysqli_error($conn);
									while($data = mysqli_fetch_array($query))
									{
										$course = strtolower($data['course_name']);
										$course = ucwords($course);
										echo "<li><a href='courses?id={$data['id']}'>$course</a></li>";
									}
								?>
					        </ul>
					      </div>
					    </div>
					  </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="gallery">Gallery</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="partners">Partnering Institutes and supporters</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="downloads">Downloads</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="news">News & events</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          COHEART Resources
					        </a>

					      </h4>
					    </div>
					    <div id="collapsefour" class="panel-collapse collapse">
					      <div class="panel-body">
					        <ul>
					        	<li><a href="research-vid">Video Library</a></li>
								<li><a href="body">Human Body</a></li>
								<li><a href="map">COHEART Health Map</a></li>
								
							</ul>
					      </div>
					    </div>
					  </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="involve">Get Involved</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="contact">Contact Us</a>
					      </h4>
					    </div>
					   </div>
					</div>
					<?php require('news-updates.php');?>
				</div>
				
				<div class='col-md-9 col-sm-8'>
					
					<div class='row'>
					<div class='col-md-12 div-about'>
							<h3>About Us</h3>
							<div class="content-5 content-about">
							<div class="panel panel-default text-justify">
								  <div class="panel-heading">
								    <h3 class="panel-title">OBJECTIVES, ACTIVITIES AND ROAD MAP </h3>
								  </div>
								  <div class="panel-body">
								    <h5><strong>OBJECTIVES OF COHEART</strong></h5>
									<p>
									<blockquote>
										<ol>
											<li>Promote and propagate One Health concepts</li>
											<li>Strengthen inter-sectoral collaboration framework</li>
											<li>Conduct interdisciplinary research of priority zoonotic diseases</li>
											<li>Build regional and cross border collaboration</li>
											<li>Build capacities for practising One Health approach</li>
										</ol>
									</blockquote>
									<div class="table-responsive">
									<table class="table table-striped table-bordered text-left">
										<th colspan='5'>PROPOSED ACTIVITIES AND ROAD MAP</th>
										<tr>
											<td rowspan='2' style='width: 60%;'>Activities</td>
											<td colspan='3' class='text-center'>Timeframe (YR)</td>
											<td rowspan='2' class='text-center'>Supporting partner</td>
										</tr>
										<tr>
											<td class='text-center'>1</td>
											<td class='text-center'>2</td>
											<td class='text-center'>3</td>
										</tr>
										<th colspan='5'>Objective 1: Promote and propagate One Health practices</th>
										<tr>
											<td>Create national awareness within the veterinary and medical professions; the broad scientific community; government institutions; the political leadership; and the general public of the power of One Health to improve the health of people, animals and the environment
											</td>
											<td class='text-center'>x</td>
											<td class='text-center'>x</td>
											<td class='text-center'>x</td>
											<td class='text-center'>COHEART, WIMS, DMO, DAHO and other willing organization</td>
										</tr>
										<tr>
											<td>Conducting national and international seminars/ conferences on One Health
											</td>
											<td></td>
											<td></td>
											<td class='text-center'>x</td>
											<td class='text-center'>-do-</td>
										</tr>
										<tr>
											<td>Conducting joint educational, training and extension activities between human medicine, veterinary medicine and environment
											</td>
											<td></td>
											<td class='text-center'>x</td>
											<td></td>
											<td class='text-center'>-do-</td>
										</tr>
										<tr>
											<td>Create a website and update activities
											</td>
											<td></td>
											<td class='text-center'>x</td>
											<td></td>
											<td></td>
										</tr>
										<th colspan='5'>Objective 2:  Strengthen inter-sectoral collaboration framework</th>
										<tr>
											<td>Conduct debriefing meeting with all partnering units in Kerala
											</td>
											<td class='text-center'>x</td>
											<td></td>
											<td></td>
											<td class='text-center'>COHEART, WIMS, DMO, DAHO&  other willing organization</td>
										</tr>
										<tr>
											<td>Establishing a One Health Working group in Kerala
											</td>
											<td></td>
											<td class='text-center'>x</td>
											<td></td>
											<td class='text-center'>-do-</td>
										</tr>
										<tr>
											<td>Develop advocacy strategy and tools
											</td>
											<td></td>
											<td></td>
											<td class='text-center'>x</td>
											<td class='text-center'>-do-</td>
										</tr>
										<tr>
											<td>Conduct advocacy meeting for the high level officials 
											</td>
											<td></td>
											<td></td>
											<td class='text-center'>x</td>
											<td class='text-center'>Key sectors & Partners</td>
										</tr>
										<tr>
											<td>Conduct policy review and harmonisation
											</td>
											<td></td>
											<td class='text-center'>x</td>
											<td></td>
											<td class='text-center'>Key sectors</td>
										</tr>
										<tr>
											<td>Develop guidelines for inter-sectoral coordination
											</td>
											<td></td>
											<td class='text-center'>x</td>
											<td></td>
											<td class='text-center'>-do-</td>
										</tr>
										<tr>
											<td>Develop a One health National Strategy and Plan of Action for the control of zoonotic diseases
											</td>
											<td></td>
											<td class='text-center'>x</td>
											<td></td>
											<td class='text-center'>Key stakeholders</td>
										</tr>
										<th colspan='5'>Objective 3: Conduct interdisciplinary research of priority zoonotic diseases</th>
										<tr>
											<td>Identify priority diseases</td>
											<td class='text-center'>x</td>
											<td class='text-center'>x</td>
											<td></td>
											<td class='text-center'>COHEART, WIMS, DMO, DAHO & other willing organization</td>
										</tr>
										<tr>
											<td>Map the available laboratory epidemio-surveillance tools</td>
											<td></td>
											<td class='text-center'>x</td>
											<td class='text-center'>x</td>
											<td class='text-center'>-do-</td>
										</tr>
										<tr>
											<td>Develop guidelines for reporting and communication</td>
											<td></td>
											<td class='text-center'>x</td>
											<td class='text-center'>x</td>
											<td class='text-center'>-do-</td>
										</tr>
										<tr>
											<td>Develop mechanism for data storage and exchange</td>
											<td></td>
											<td class='text-center'>x</td>
											<td></td>
											<td class='text-center'>-do-</td>
										</tr>
										<tr>
											<td>Harmonise strategies for preventing and control of priority zoonotic diseases</td>
											<td></td>
											<td class='text-center'>x</td>
											<td class='text-center'>x</td>
											<td class='text-center'>-do-</td>
										</tr>
										<tr>
											<td>Establish a research agenda for One Health</td>
											<td></td>
											<td class='text-center'>x</td>
											<td class='text-center'>x</td>
											<td class='text-center'>-do-</td>
										</tr>
										<th colspan='5'>Objective 4: Build regional and cross border collaboration</th>
										<tr>
											<td>Conduct regional and cross border meeting</td>
											<td></td>
											<td class='text-center'>x</td>
											<td class='text-center'>x</td>
											<td class='text-center'>-do-</td>
										</tr>
										<tr>
											<td>Share information with  regional bodies (ICAR,UGC, SAU)</td>
											<td></td>
											<td class='text-center'>x</td>
											<td class='text-center'>x</td>
											<td class='text-center'>-do-</td>
										</tr>
										<th colspan='5'>Objective5: Build capacities for practising One Health approach</th>
										<tr>
											<td>Conduct One Health curricula review</td>
											<td class='text-center'>x</td>
											<td class='text-center'>x</td>
											<td></td>
											<td class='text-center'>-do-</td>
										</tr>
										<tr>
											<td>Conduct pre and in-service training</td>
											<td></td>
											<td class='text-center'>x</td>
											<td></td>
											<td class='text-center'>-do-</td>
										</tr>
										<tr>
											<td>Strengthen laboratory capacities (surveillance, training)</td>
											<td></td>
											<td class='text-center'>x</td>
											<td class='text-center'>x</td>
											<td class='text-center'>-do-</td>
										</tr>
										<tr>
											<td>Launch PG certificate program on One Health </td>
											<td class='text-center'>x</td>
											<td></td>
											<td></td>
											<td class='text-center'>-do-</td>
										</tr>
										<tr>
											<td>Launch PG Diploma program on One Health</td>
											<td class='text-center'>x</td>
											<td></td>
											<td></td>
											<td class='text-center'>-do-</td>
										</tr>
										<tr>
											<td>Launch MPH program on One Health</td>
											<td></td>
											<td></td>
											<td class='text-center'>x</td>
											<td class='text-center'>-do-</td>
										</tr>
									</table>
									
							</div>

								  </div>
								</div>
								</div>

					</div>
					</div>
					<div class='row'>
						<div class='col-md-12 visible-xs'>
							<?php require('news-updates.php');?>
						</div>
					</div>
					</div>
				</div>
			
			+
		</div>
<?php require('footer.php');?>	
