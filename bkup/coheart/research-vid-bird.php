﻿<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	 <meta name="keywords" content="Bird flu,pakshi pani,kerala, Avian influenza,virus,epidemic,alapuzha,duck, documentry, government">
    <title>COHEART | Bird flu | video</title>
	<link rel="shortcut icon" href="dist/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="dist/images/favicon.ico" type="image/x-icon">
	<!--Slider-->
	<link rel="stylesheet" href="dist/slider/responsiveslides.css">
	
	<!--FancyBox-->
	<link rel="stylesheet" href="dist/lightbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<!-- Bootstrap -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
	<link href="dist/css/style.css" rel="stylesheet">
	<link href="admin/table/css/footable.core.css" rel="stylesheet"/>
	<link href="admin/table/css/footable.metro.css" rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
	<!--<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>-->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<meta
property="og:url" content="http://www.coheart.ac.in/research-vid-bird"/><meta
property="og:title" content="പക്ഷിപനി ഭീതി വേണ്ട -  Kerala Veterinary and Animal Sciences University" /><meta
property="og:description" content="This documentary is released by Kerala Veterinary and Animal Sciences University in association with Animal Husbandry department as a part of creating awareness on Avian influenza (or Bird flu) in Kerala.." /><meta
property="og:type" content="article" /><meta
property="og:image" content="http://www.coheart.ac.in/dist/images/coheart-bird-flue.JPG" /><meta
property="og:image:width" content="500" /><meta
property="og:image:height" content="300" />

<meta property="og:site_name" content="COHEART - KVASU"/>
  </head>
  <body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=421004608026326&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <div class=''>
		<?php require('header.php');?>
		<nav class="navbar navbar-default visible-xs " role="navigation">
					<div class="container-fluid">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
						  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
						  <a class="navbar-brand" href="#">Menu</a>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li><a href="index">Home</a></li>
						<li class="dropdown active">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<li><a href="about-one-health">What is One Health</a></li>
								<li><a href="about-genesis">Genesis of COHEART</a></li>
								<li><a href="about-mandate">Mandate, Vision and Mission of COHEART</a></li>
								<li><a href="about-scope">Scope of One Health</a></li>
								<li><a href="about-objectives">Objectives, Activities and Roap Map</a></li>
								<li><a href="about-faculties">Faculties</a></li>
							  </ul>
						</li>
						
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Courses <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<?php
									require('dbconnect.php');
									$query = mysqli_query($conn,"select id,course_name from courses");
									echo mysqli_error($conn);
									while($data = mysqli_fetch_array($query))
									{
										$course = strtolower($data['course_name']);
										$course = ucwords($course);
										echo "<li><a href='courses?id={$data['id']}'>$course</a></li>";
									}
								?>
							  </ul>
						</li>
						<li><a href="gallery">Gallery</a></li>
						<li><a href="partners">Partnering Institutes and supporters</a></li>
						<li><a href="downloads">Downloads</a></li>
						<li><a href="news">News & events</a></li>
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Research & Training <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<li><a href="research-vid">Videos</a></li>
								<li><a href=#>Notes</a></li>
								<li><a href=#>Others</a></li>
							  </ul>
						</li>
						<li><a href="involve">Get Involved</a></li>
						<li><a href="contact">Contact Us</a></li>
							</ul>
							
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
		</nav>
		<div class='content'>
			<div class='row'>
				<div class='col-md-3 col-sm-4 visible-md visible-lg visible-sm'>

					<div class="panel-group" id="accordion">
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-parent="#accordion" href="index">
					          Home
					        </a>
					      </h4>
					    </div>
					   </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" >
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          About Us
					        </a>

					      </h4>
					    </div>
					    <div id="collapseTwo" class="panel-collapse collapse">
					      <div class="panel-body">
					        <ul>
					        	<li><a href="about-one-health" class="active">What is One Health</a></li>
								<li><a href="about-genesis">Genesis of COHEART</a></li>
								<li><a href="about-mandate">Mandate, Vision and Mission of COHEART</a></li>
								<li><a href="about-scope">Scope of One Health</a></li>
								<li><a href="about-objectives">Objectives, Activities and Roap Map</a></li>
								<li><a href="about-faculties">Faculties</a></li>
					        </ul>
					      </div>
					    </div>
					  </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          Courses
					        </a>
					      </h4>
					    </div>
					    <div id="collapseThree" class="panel-collapse collapse">
					      <div class="panel-body">
					        <ul>
					        	<?php
									require('dbconnect.php');
									$query = mysqli_query($conn,"select id,course_name from courses");
									echo mysqli_error($conn);
									while($data = mysqli_fetch_array($query))
									{
										$course = strtolower($data['course_name']);
										$course = ucwords($course);
										echo "<li><a href='courses?id={$data['id']}'>$course</a></li>";
									}
								?>
					        </ul>
					      </div>
					    </div>
					  </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="gallery">Gallery</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="partners">Partnering Institutes and supporters</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="downloads">Downloads</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="news">News & events</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour"class="active">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          COHEART Resources
					        </a>

					      </h4>
					    </div>
					    <div id="collapsefour" class="panel-collapse in">
					      <div class="panel-body">
					        <ul>
					        	<li><a href="research-vid">Video Library</a></li>
								<li><a href="body">Human Body</a></li>
								<li><a href="map">COHEART Health Map</a></li>
							</ul>
					      </div>
					    </div>
					  </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="involve">Get Involved</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="contact">Contact Us</a>
					      </h4>
					    </div>
					   </div>
					</div>
					<?php require('news-updates.php');?>
				</div>
				
				<div class='col-md-9 col-sm-8'>


					
					<div class='row'>
					<div class='col-md-12 div-vid'>
							<h3>Videos</h3>								
								<div class="flex-video widescreen" style="text-align: center">								
								<iframe  style="width:90%"  height="400" allowfullscreen="" src="http://www.youtube.com/embed/N2EGkLw4Xdo" frameborder="0"></iframe>
								</div>								
							<div class='row'>
							<div class='col-md-12 col-sm-12 col-xs-12 div-vid'>
								<div class="content-1 content-vid">
									<div class="panel panel-default text-justify">
										<div class="panel-heading">
										<h3 class="panel-title"><b>പക്ഷിപനി ഭീതി വേണ്ട ..</b> </h3>
										<div class="panel-body">
										<p>This documentary is released by Kerala Veterinary and Animal Sciences University in association with Animal Husbandry department as a part of creating awareness on Avian influenza (or Bird flu) in Kerala. The outbreak of the infection claimed over thousands of ducks in water-logged areas of Kuttanad in Alappuzha and has caused a huge loss to poultry rearers. The government has issued a statewide alert following the confirmation of this outbreak. The documentary entitled ‘Pakshipani Beethi venda’ is intended for the media, farmers, local body representatives and public to create awareness on this disease so as to alleviate their apprehensions. This film also intends to help in containing the disease by equipping the public with information on general preventive measures to be adopted in face of an outbreak.
										</p>
										<div class="fb-like" data-href="http://www.coheart.ac.in/research-vid-bird" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>

										</div>
								  
									</div>
								</div>
							</div>
							
							
					
						</div>
					</div>
					</div>
					</div>
					<div class='row'>
						<div class='col-md-12 visible-xs'>
							<?php require('news-updates.php');?>
						</div>
					</div>
					</div>
				</div>
			
			+
		</div>
<?php require('footer.php');?>	
