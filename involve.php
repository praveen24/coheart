
		<?php require('header.php');?>
		<div class='content about-content'>
			<div class="container">
				<div class='row'>
					<div class='col-md-12 col-sm-12'>
						<div class='row'>
							<div class='col-md-12 div-about'>
								<div class="content-4 content-about">
									<div class="panel panel-primary text-justify">
								  		<div class="panel-heading">
								  			<h3 class="panel-title">Get Involved</h3>
								  		</div>
								  		<div class="panel-body">
								  			<div class="row">
								  				<div class="col-md-12">
													<?php
														if(isset($_SESSION['saved']) && $_SESSION['saved']!="")
														{
															echo "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
															echo "<span class='glyphicon glyphicon-ok'></span> ".$_SESSION['saved'];
																unset($_SESSION['saved']);
															echo "</div>";
														}
														if(isset($_SESSION['captcha1']) && $_SESSION['captcha1']!="")
														{
															echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
															echo "<span class='glyphicon glyphicon-thumbs-down'></span> ".$_SESSION['captcha1'];
																unset($_SESSION['captcha1']);
															echo "</div>";
														}
														if(isset($_SESSION['captcha2']) && $_SESSION['captcha2']!="")
														{
															echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
															echo "<span class='glyphicon glyphicon-thumbs-down'></span> ".$_SESSION['captcha2'];
																//unset($_SESSION['captcha2']);
															echo "</div>";
														}
													?>
												</div>
											</div>
											<div class='row text-justify'>
												<div class="col-md-12">
													<h4>Membership</h4>
													<p>
														COHEART Membership is open to all especially Organization, Institutes, Individual Scientists, faculty, students and staff, as well as the community at large. If you would like to join the COHEART, you may join at one of our upcoming meetings and events. So, become a member of COHEART. Apply your passion and skills to improving the health of humans, animals, plants and the environment. Your involvement with COHEART is not only of great benefit to the Centre but it can be of great benefit to you as well. It provides you with the opportunity to network with other professionals and build on your current skills. 
													</p>
													<h4>What individuals can do</h4>
													<p>
														There are many things which concerned individuals can do to get involved with us and improve health for of Man, Animal and Environment:
														<ul>
															<li>Learn about COHEART in our ABOUT US and ACTIVTIES, NEWS and EVENT sections</li>
															<li>Read the Global Strategy for One Health and advocate that you or your organization support the Strategy and its initiatives;</li>
															<li>Join our mailing list and stay informed about the latest One Health news and updates and information on key events and meetings;</li>
															<li>Check our COHEART Members Database to find local COHEART members who are active in this area</li>
															<li>In our News & Events section, find news about partner campaigns and special days promoting One  health</li>
															
														</ul>
													</p>
													<br/>
													<div class=''>
														<ul class="nav nav-pills" role="tablist" style="margin-bottom: 15px;">
														<?php
															if(isset($_SESSION['captcha2']) && $_SESSION['captcha2']!="")
															{
																echo "<li><a href='#home' role='tab' data-toggle='pill' id='tab-indi'>Individuals</a></li>";
																echo "<li class='active'><a href='#profile' role='tab' data-toggle='pill' id='tab-org'>Organizations</a></li>";
															}
															else
															{
																echo "<li class='active'><a href='#home' role='tab' data-toggle='pill' id='tab-indi'>Individuals</a></li>";
																echo "<li><a href='#profile' role='tab' data-toggle='pill' id='tab-org'>Organizations</a></li>";
															}
														?>
															
														</ul>
														<!-- Tab panes -->
														<div class="tab-content">
															<?php
																if(isset($_SESSION['captcha2']) && $_SESSION['captcha2']!="")
																	echo "<div class='tab-pane fade' id='home'>";
																else
																	echo "<div class='tab-pane fade in active' id='home'>";
															?>
														  	<div style="border: 1px solid #ddd" id='form-1'>
														  		<div class="panel-heading">
																	<h3 class="panel-title">Form for Individuals</h3>
														  		</div>
														  		<div class="panel-body">
																	<form role="form" id='involve-form1' method='post' action='save.php' enctype='multipart/form-data'>
																		<div class='row'>
															  				<div class="form-group col-md-6">
																				<label>Full Name</label>
																				<input type="text" class="form-control" placeholder="Enter Full Name" name='fullname' data-validation='required'>
															  				</div>

															  				<div class="form-group col-md-6">
																				<label>Job Title</label>
																				<input type="text" class="form-control" placeholder="Job Title" name='job' data-validation='required'>
															  				</div>
																		</div>
																		<div class='row'>
															  				<div class="form-group col-md-6">
																				<label>Organization</label>
																				<input type="text" class="form-control" placeholder="Organization" name='organization' data-validation='required'>
															  				</div>
															  				<div class="form-group col-md-6">
																				<label>Country</label>
																				<input type="text" class="form-control" placeholder="Country" name='country' data-validation='required country'>
															  				</div>
																		</div>	
																		<div class='row'>
															  				<div class="form-group col-md-6">
															  					<label>Email</label>
																				<input type="text" class="form-control" placeholder="Email" name='email'data-validation='required email'>
															  				</div>
															  				<div class="form-group col-md-6">
																				<label>Phone</label>
																				<input type="text" class="form-control" placeholder="Phone" name='phone'data-validation='required number' data-validation-error-msg="Please enter a valid phone number">
															  				</div>
																		</div>
																		<div class='row'>
															  				<div class="form-group col-md-6">
																				<label>Address</label>
																				<textarea class="form-control" rows="5" name='address' data-validation='required' ></textarea>
															  				</div>
															  				<div class="form-group col-md-6">
																				<label>Short bio brief about you (Not more than 200 words)</label>
																				<textarea class="form-control" rows="5" name='summary' data-validation='required wordcount' id="summary"></textarea>
															  				</div>								
															  			</div>
																		<div class='row'>
															  				<div class="form-group col-md-6">
																				<label>Describe your area of interest / expertise in 10 words (The description will appear in supporters section next to your name)</label>
																				<textarea class="form-control" rows="5" name='skills' data-validation='required wordcount2'></textarea>
															  				</div>
															  				<div class="form-group col-md-6">
															  					<p>
																					<label for="qs">How would you like us to contact you?- By email or phone</label>
																					<select class="form-control" name='contact'  data-validation='required'>
																	  					<option value=''>--Select--</option>
																	  					<option value='email'>Email</option>
																	  					<option value='phone'>Phone</option>
																					</select>
																				</p>
																				<div id='captcha1' class="captcha1">
																					<?php
																						require_once('recaptchalib.php');
																						$publickey = "6Ld7uPYSAAAAAGHXz3JdAgU_5D67sqw4gs6lwIC_"; 
																						//echo recaptcha_get_html($publickey);
																					?>
																				</div>
																			</div>
																		</div>
																		<div class='row'>
															  				<div class="form-group text-center">
																				<label>
																  					<input type="checkbox" data-validation='required' data-validation-error-msg="You have to agree to our terms & conditions" checked="true"> I agree with the <a href="javascript: void(0);" class='terms'>terms & conditions</a>.
																				</label>
															  				</div>
																		</div>
																		<div class='row'>
															  				<div class="form-group col-md-12 text-center">
																				<button type="submit" name='btn-indi' class="btn btn-primary" id="btn-indi" value='save'>Submit</button>
															  				</div>
																		</div>
																	</form>		
														  		</div>
															</div>  	
														</div>
														<?php
															if(isset($_SESSION['captcha2']) && $_SESSION['captcha2']!="")
																echo "<div class='tab-pane fade in active' id='profile'>";
															else
																echo "<div class='tab-pane fade' id='profile'>";
															
														?>
													   	<div style="border: 1px solid #ddd" id='form-2'>
															<div class="panel-heading">
																<h3 class="panel-title">Form for Organization</h3>
															</div>
															<div class="panel-body">
																<form role="form" id='involve-form2' method='post' action='save2.php' enctype='multipart/form-data'>
																	<div class='row'>
																  		<div class="form-group col-md-6">
																  			<p>
																				<label>Organization Name</label>
																				<input type="text" class="form-control" placeholder="Organization Name" name='orgname' data-validation='required'>
																			</p>
																			<p>	
																				<label>Purpose</label>
																				<input type="text" class="form-control" placeholder="Purpose" name='purpose' data-validation='required'>
																			</p>
																  		</div>
																
																  		<div class="form-group col-md-6">
																			<label>Narrative Description</label>
																			<textarea class="form-control" rows="5" name='description' data-validation='required' ></textarea>
																  		</div>
																	</div>
																	<div class='row'>
																  		<div class="form-group col-md-6">
																			<label>Country, State & City</label>
																			<input type="text" class="form-control" placeholder="Country, State & City" name='csc' data-validation='required'>
																  		</div>
																  		<div class="form-group col-md-6">
																			<label>Type of Organization</label>
																			<select class="form-control typeoforg" data-validation='required' name='type'>
																	  			<option value=''>--Select--</option>
																	  			<option value='Academic Institution'>	Academic Institution</option>
																			  	<option value='Private Foundation'>Private Foundation</option>
																			  	<option value='Government Institution'>Government Institution</option>
																			  	<option value='Private, Non-Profit Organization'>Private, Non-Profit Organization</option>
																			  	<option value='Private, For Profit / Corporate'>Private, For Profit / Corporate</option>
																			  	<option value='Disease diagnostic labs'>Disease diagnostic labs</option>
																				<option value='Other'>Other, describe</option>
																			</select>
																	
																			<div id='extra'></div>
																  		</div>
																	</div>						
																	<div class='row'>
																  		<div class="form-group col-md-6">
																			<label>“Describe organization  objective in 10 words” (The description will appear in supporters section next to organization name)</label>
																			<textarea class="form-control" rows="5" name='activities' data-validation='required wordcount2' ></textarea>
																  		</div>
																  		<div class="form-group col-md-6">
																  			<p>
																				<label>Contact Person's Name</label>
																				<input type="text" class="form-control" placeholder="Contact Person's Name" name='contact' data-validation='required'>
																			</p>
																			<p>
																				<label>Email</label>
																				<input type="text" class="form-control" placeholder="Email" name='email' data-validation='required email'>
																			</p>
																  		</div>
																	</div>
																	<div class='row'>
																   		<div class="form-group col-md-6">
																			<label>Organization Website</label>
																			<input type="text" class="form-control" placeholder="Website" name='website' data-validation="required url">
																  		</div>
																  		<div class="form-group col-md-6">
																			<label>Participants and Key Collaborators</label>
																			<textarea class="form-control" rows="5" name='participants' data-validation='required' ></textarea>
																  		</div>
																	</div>
																	<div class='row'>
																  		<div class="form-group col-md-12">
																			<label>Additional Information (Include whatever else you would like to have posted about your Institution’s / Organization’s One Health efforts)</label>
																			<textarea class="form-control" rows="5" name='additional' data-validation='required'></textarea>
																  		</div>
																	</div>
																	<div class='row'>
																  		<div class="form-group col-md-6 text-center">
																  			<p>
																				<label>
																	  				<input type="checkbox" data-validation='required' data-validation-error-msg="You have to agree to our terms & conditions" checked="true"> I agree with the <a href="javascript: void(0);" class='terms'>terms & conditions</a>.
																				</label>
																  			</p>
																  		</div>
																  		<p>
																  			<div class="form-group col-md-6">
																  				<div id="captcha2" class="captcha2"></div>		
																  			</div>
																  		</p>
																	</div>
																	<div class='row'>
																  		<div class="form-group col-md-12 text-center">
																			<button type="submit" name='btn-org' class="btn btn-primary" value='save'>Submit</button>
																  		</div>
																	</div>
																</form>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class='row news'>
			<div class="col-md-12">
				<div class="container">
					<div class="row">
						<div class='col-md-12'>
							<h3 class="text-center">NEWS & EVENTS</h3>
							<?php require('news-updates.php');?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php require('footer.php');?>
<script type="text/javascript" src="http://www.google.com/recaptcha/api/js/recaptcha_ajax.js"></script>
<script>
	$(function(){
		var recapExist = false;
		 <?php
			if(isset($_SESSION['captcha2']) && $_SESSION['captcha2']!="")
				echo "Recaptcha.create('$publickey', 'captcha2');";
			else
				echo "Recaptcha.create('$publickey', 'captcha1');";
			unset($_SESSION['captcha2']);
		?>
      	
        jQuery('#tab-indi').click(function() {
            if(recapExist == false) {
                Recaptcha.create("<?php echo $publickey; ?>", "captcha1");
                recapExist = "captcha2";
            } else if(recapExist == 'captcha1') {
                Recaptcha.destroy(); 
                Recaptcha.create("<?php echo $publickey; ?>", "captcha1");
                recapExist = "captcha2";
            }
        });
        jQuery('#tab-org').click(function() {
            if(recapExist == false) {
                Recaptcha.create("<?php echo $publickey; ?>", "captcha2");
                recapExist = "captcha1";
            } else if(recapExist == 'captcha2') {
                Recaptcha.destroy(); 
                Recaptcha.create("<?php echo $publickey; ?>", "captcha2");
                recapExist = "captcha1";
            }
        });

		$('.typeoforg').change(function()
		{
			if($(this).val()=='Other')
			{
				$('#extra').html('<label>Describe</label><textarea class="form-control" rows="5" name="desc" data-validation="required" ></textarea>');
			}
			else
				$('#extra').html("");
		});
		$('.terms').click(function()
		{
			//window.open('terms', '_blank','toolbar=0,location=0,menubar=0');
			window.open("terms","_blank","toolbar=yes, scrollbars=yes, resizable=yes, width=600 height=500");
		});
		$.formUtils.addValidator({
			  name : 'wordcount',
			  validatorFunction : function(value, $el, config, language, $form) {
			  	count = value.split(" ").length;
			    return count<=200;
			  },
			  errorMessage : 'Not more than 200 words',
			  errorMessageKey: 'wordcount'
			});
		$.formUtils.addValidator({
			  name : 'wordcount2',
			  validatorFunction : function(value, $el, config, language, $form) {
			  	count = value.split(" ").length;
			    return count<=10;
			  },
			  errorMessage : 'Not more than 10 words',
			  errorMessageKey: 'wordcount2'
			});
		$.validate({
			  form : '#involve-form1, #involve-form2',
			  modules: 'location, file',
			  validateOnBlur : false,
			  //errorMessagePosition : 'top',
			  scrollToTopOnError : true,
			  onModulesLoaded : function() {
					$('input[name="country"]').suggestCountry();
					
				  }
			});
	});
	
</script>		
<script>
		$(document)
			.on('change', '.btn-file :file', function() {
				var input = $(this),
				numFiles = input.get(0).files ? input.get(0).files.length : 1,
				label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
				input.trigger('fileselect', [numFiles, label]);
		});
		
		$(document).ready( function() {
			$('.btn-file :file').on('fileselect', function(event, numFiles, label) {
				
				var input = $(this).parents('.input-group').find(':text'),
					log = numFiles > 1 ? numFiles + ' files selected' : label;
				
				if( input.length ) {
					input.val(log);
				} else {
					if( log ) alert(log);
				}
				
			});
		});		
	</script>