
			<?php require('header.php');?>
			<div class='content about-content'>
				<div class="container">
					<div class="row">						
						<div class='col-md-12 col-sm-12'>							
							<div class='row'>
								<div class='col-md-12 div-about'>
									<div class="content-1 content-about">
										<div class="panel panel-primary text-justify">
											<div class="panel-heading">
												<h3 class="panel-title">What is One Health</h3>
											</div>
											<div class="panel-body">
											    <p>
													The idea of One Health is the growing recognition of interconnectedness of human, animal, and environmental health that requires new tools for cooperation and collaboration between professionals working in these sectors. 
												</p>
												<p>
													This concept dates back to the Greek physician Hippocrates (ca. 460 BCE–ca.370) who traced the impact of environment on human health in his text "On Airs, Waters and Places”. In the 1800s, Rudolf Virchow stated that there should be no dividing line between human and animal medicine. In the 1900s, Calvin Schwabe traced the roots of Western medicine to the “one medicine” of ancient Egyptian and Dinka priests, and argued that we need to return to those roots. Similar pathways of understanding can be traced through ancient Chinese, Arab, and Indian cultures. In India, linkages of nature with human and animal can also be seen in vedas, Galenic scripts and the manuscripts of Kautilya and Ashoka. Further, the approach of One Health stems from the original theory of One Medicine, developed in 1984 by Calvin Schwabe in his book titled Veterinary Medicine and Human Health, advocating a combined medical and veterinary approach to zoonotic disease. Yet, human and animal health developed during the nineteenth and twentieth centuries into fairly segregated disciplines or ‘silos’, separated at the academic, governance and application levels. Now, it is well understood that there is an urgent need to control the spreading of infectious diseases and to prevent the future threats. Growing antimicrobial resistance, rapidly spreading new zoonotic diseases, food safety issues, environment pollution, climate changes, growing demand for food – these are only a few of many challenges that need to be urgently addressed by One Health approach.
												</p>
												<p>The convergence of people, animals, and our environment has created a new dynamic in which the health of each group is inextricably interconnected. Out of the 1,461 diseases now recognized in humans, approximately 60% are due to multi-host pathogens characterized by their movement across species lines and, over the last three decades, approximately 75% of new emerging human infectious diseases have been identified as zoonotic. Our increasing interdependence with animals and their products may well be the single most critical risk factor to our health and well-being with regard to infectious diseases. Healthy environment and healthy animals are essential for healthy life; but it also depends on how community uses both of them. Drastic ‘economic development’ that caused money melting affected the human life. Cross-disciplinary international collaborations are required in tackling difficult global health problems.</p>
												<h5>
													<i>Why is a 'One Health' Approach important now?</i>
												</h5>
												<ul>
													<li>
														Despite research, there is steady increase in human diseases acquired from animals. About 75% of new emerging human infectious diseases are zoonotic.
													</li>
													<li>
														Local communities are affected by global issues such as rabies control, vector borne diseases, and emergency preparedness, among many others. One Health recognizes the importance of 'thinking globally while acting locally.'
													</li>
													<li>
														Despite sophisticated laboratory facilities in the country, there is a lack of communication of research findings as well as lack of knowledge of laboratory capacities.
													</li>
													<li>
														Despite vaccination programs, we were not been able to control outbreaks of many economically important diseases.
													</li>
													<li>
														Despite 68 years of freedom in the country, green revolution, economic liberalization, 8% economic growth rate in trade, economic liberalization and scientific advancement, a large number of children (42%?) remain under-nourished.
													</li>
													<li>
														Despite white revolution, majority of the milk marketed in India is reported to be adulterated (media report).
													</li>
													<li>
														Despite 12% budget on drugs & advance in molecular biology newer diseases, deficiencies, crises keep coming up.
													</li>
													<li>
														Despite good education, close community living: conflicts, up rise, terrorism, war, pollution, waste, childhood mortality, disasters are on the rise.
													</li>
													<li>
														Despite hype of advanced technology, increasing training institutions, modern hospitals, broader insurance policies, good laws & media coverage, health service does not get universal coverage.
													</li>
													<li>
														Despite decentralization community participation is less than optimal.
													</li>
												</ul>
												<p>
													<em>One Health</em>, a new interdisciplinary approach, has been embraced as a way forward by many groups of professional experts. This is global-wide approach that aims to tighten the collaboration, communication and better cooperation between physicians, veterinarians and allied health professionals―working at the interaction [interface] between human/ animal health and the ecosystem. One Health thinking seeks to include human, animal and environmental experts in problem solving. Internationally applauded, the One Health approach is strongly endorsed by human medicine, public health, veterinary medicine, the CDC, and various other national and international institutions and organizations. The Prime Minister of India, Dr. Manmohan Singh while addressing the delegates of “International Ministerial Conference on Avian and Pandemic Influenza”on 5th December, 2007 stated that the Government of India supports the concept of 'ONE HEALTH' based on an integrated approach to both animal and human health. Having understood the need for One Health, we should develop strategies to adopt One Health practices.   
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>				
				</div>
				<div class="container-fluid">					
					<div class='row news'>
						<div class='col-md-12'>
							<div class="container">
								<div class="row">
									<div class="col-md-12">
										<h3 class="text-center">NEWS & EVENTS</h3>
										<?php require('news-updates.php');?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>		
			<?php require('footer.php');?>	
