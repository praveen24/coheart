<!DOCTYPE html>
<html>
  <head>
    <style type="text/css">
      html, body, #map-canvas { height: 100%; margin: 0; padding: 0;}
    </style>
    <style>
      html, body, #map-canvas {
        height: 100%;
        margin: 0px;
        padding: 0px
      }
    </style>
    <!--<script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDuKzv2c9I6f_1etOsLgM5weiFSDSuvdY8">
    </script>-->
    <script type="text/javascript">
      function initialize()
      {
        var myLatlngc = new google.maps.LatLng(23.1289955,82.7792201);
        <?php
          require('dbconnect.php');
          $query = mysqli_query($conn,"select * from map");
          $i = 1;
          while($data = mysqli_fetch_array($query))
          {
            echo "var myLatlng$i = new google.maps.LatLng({$data['district']});\n";
            
            $i++;
          }
        ?>
        var mapOptions = {
          zoom: 5,
          center: myLatlngc
        }
        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        var image = 'http://coheart.ac.in/dist/images/favicon.ico';
        <?php
          for($j=1;$j<$i;$j++)
          {
            echo "var marker$j = new google.maps.Marker({
                position: myLatlng$j,
                map: map,
                icon: image
            });\n";
          }
          $query = mysqli_query($conn,"select * from map");
          $i = 1;
          while($data = mysqli_fetch_array($query))
          {
            //echo "var contentString$i = '{$data['description']}';\n";
            $buffer = str_replace(array("\r", "\n"), "", $data['description']);
            echo "var contentString$i = '$buffer';\n";

            $i++;
          }
         
          for($j=1;$j<$i;$j++)
          {
            echo "var infowindow$j = new google.maps.InfoWindow({
      content: contentString$j
  });\n";
          }
          for($j=1;$j<$i;$j++)
          {
          echo "var marker$j = new google.maps.Marker({
      position: myLatlng$j,
      map: map,
      icon: image
  });\n";
          }
          for($j=1;$j<$i;$j++)
          {
          echo "google.maps.event.addListener(marker$j, 'click', function() {
      infowindow$j.open(map,marker$j);});\n";
          }
        ?>
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    
  </head>
  <body>
 
<div id="map-canvas"></div>
  </body>
</html>