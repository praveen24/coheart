<html>
<head>
	<link rel="shortcut icon" href="dist/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="dist/images/favicon.ico" type="image/x-icon">
	<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
	<title>COHEART - Terms & Conditions</title>
	<style>
		body
		{
			font-family: 'Ubuntu', sans-serif;
			font-size: 14px;
		}
		div
		{
			text-align: center;
		}
	</style>
</head>
<body>
	<div>
		<h3>Membership agreement and responsibilities</h3>
	</div>
	<ol>
		<li>I have read and agree with the mission and vision of COHEART, the operational principles and the principles of membership with COHEART.</li>
		<li>I understand and agree with the criteria and responsibilities of members, as defined herein
			<ol type='a'>
				<li>Actively initiate and participate in collaborative activities to achieve health for all</li>
				<li>Willingness to devote time and effort for One Health </li>
				<li>Willingness to support efforts of the COHEART and the policies and practices COHEART applies to best serve the field of One Health</li>
				<li>Share knowledge and information on lessons learned, success stories, case studies, program results and policy approaches that help to accelerate action towards the One Health.</li>
			</ol>
		</li>
		<li>Members are encouraged to:
			<ul type='disc'>
				<li>Link to the COHEART website from their organization's website;</li>
				<li>Keep their profile contact information up-to-date by communicating changes to our administrator</li>
				<li>Contribute to COHEARTs activities and newsletter by sharing new ideas, data, best practices and other important and relevant news related to the One Health; </li>
				<li>Attend and contribute to the Global Health and satellite events; </li>
				<li>Advocate One Health</li>
			</ul>
		</li>
	</ol>
</body>
</html>