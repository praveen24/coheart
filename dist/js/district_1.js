$(function(){
	$('#district').html('<option value="">--Select--</option>');
	$('#state').change(function(){
		$('#district').html('<option value="">--Select--</option>');
		if($(this).val()=='ke')
		{
			$('#district').html('<option value="">--Select--</option><option value="9.4989551,76.3405891">Alappuzha</option><option value="9.9708746,76.3082933">Ernakulam</option><option value="9.8143524,77.0207443">Idukki</option><option value="11.8693897,75.3760254">Kannur</option><option value="12.5000429,74.9840015">Kasaragod</option><option value="8.8912018,76.6010506">Kollam</option><option value="9.5937652,76.5238677">Kottayam</option><option value="11.2541597,75.7933044">Kozhikode</option><option value="11.056216,76.0683918">Malappuram</option><option value="10.7855952,76.6547655">Palakkad</option><option value="9.2602876,76.7780614">Pathanamthitta</option><option value="8.499733,76.9244767">Thiruvananthapuram</option><option value="10.5306792,76.2128448">Thrissur</option><option value="11.7142605,76.1091843">Wayanad</option>');
		}
		else if($(this).val()=='an')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="	17.15914,81.7275249	">	East Godavari	</option><option	
			value="	16.3235706,80.4377486	">	Guntur	</option><option	
			value="	14.4760249,78.7081699	">	YSR Kadapa	</option><option	
			value="	16.42953,80.7796414	">	Krishna	</option><option	
			value="	15.8118442,78.039802	">	Kurnool	</option><option	
			value="	15.632495,79.6053999	">	Prakasam	</option><option	
			value="	14.2999643,79.6747185	">	Sri Potti Sriramulu Nellore	</option><option	
			value="	18.3031495,83.8904856	">	Srikakulam	</option><option	
			value="	17.7385936,83.2625211	">	Vishakhapatnam	</option><option	
			value="	18.0998918,83.4031409	">	Vizianagaram	</option><option	
			value="	16.8993821,81.3642684	">	West Godavari	</option>');	

		}
		else if($(this).val()=='ap')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="	28.0480165,94.4806805	'>	Anjaw	</option><option
value="	27.135993,95.7389724	">	Changlang	</option><option
value="	27.4915377,93.0084582	">	East Kameng	</option><option
value="	28.2657038,95.0928504	">	East Siang	</option><option
value="	27.9100969,96.1701965	">	Lohit	</option><option
value="	26.8727038,95.305748	">	Longding	</option><option
value="	27.6312284,93.8349152	">	Lower Subansiri	</option><option
value="	27.1571672,93.6934662	">	Papum Pare	</option><option
value="	27.6729077,92.0135664	">	Tawang	</option><option
value="	27.0144832,95.5700684	">	Tirap	</option><option
value="	28.1428723,95.8431018	">	Lower Dibang Valley	</option><option
value="	28.7286759,94.9246216	">	Upper Siang	</option><option
value="	27.802644,94.3560791	">	Upper Subansiri	</option><option
value="	27.3087186,92.4013032	">	West Kameng	</option><option
value="	28.1664586,94.7673798	">	West Siang	</option><option
value="	28.9169872,96.165905	">	Upper Dibang Valley	</option><option
value="	28.0480165,94.4806805	">	Kurung Kumey	</option>');	

		}
		else if($(this).val()=='as')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="	26.3227307,90.9945318	'>	Barpeta	</option><option
value="	26.4895577,90.5358985	">	Bongaigaon	</option><option
value="	24.7457856,92.8357939	">	Cachar	</option><option
value="	26.4134397,92.0900407	">	Darrang	</option><option
value="	27.4774009,94.5541596	">	Dhemaji	</option><option
value="	26.0257422,89.9759459	">	Dhubri	</option><option
value="	27.4703319,94.9175936	">	Dibrugarh	</option><option
value="	26.1610891,90.6298993	">	Goalpara	</option><option
value="	26.5137624,93.9658714	">	Golaghat	</option><option
value="	24.6798179,92.5631559	">	Hailakandi	</option><option
value="	26.7428816,94.2024813	">	Jorhat	</option><option
value="	26.0621483,93.0227397	">	Karbi Anglong	</option><option
value="	24.5828861,92.397921	">	Karimganj	</option><option
value="	26.4018633,90.2695083	">	Kokrajhar	</option><option
value="	27.1711819,94.1489598	">	Lakhimpur	</option><option
value="	26.2761125,92.2672561	">	Morigaon	</option><option
value="	26.1612415,92.8868151	">	Nagaon	</option><option
value="	26.4445015,91.4379258	">	Nalbari	</option><option
value="	25.3884811,93.019379	">	Dima Hasao	</option><option
value="	26.9877493,94.6390038	">	Sivasagar	</option><option
value="	26.7402686,93.0656816	">	Sonitpur	</option><option
value="	27.4925561,95.3553413	">	Tinsukia	</option><option
value="	26.1575507,91.3736794	">	Kamrup	</option><option
value="	26.0457401,91.7722424	">	Kamrup Metropolitan	</option><option
value="	26.657129,91.3397668	">	Baksa	</option><option
value="	26.7402034,92.0945262	">	Udalguri	</option><option
value="	26.6347866,90.6400562	">	Chirang	</option>');	

		}
		else if($(this).val()=='bh')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="	25.9035865,85.803761	">	Bihar	</option>');	

		}
		else if($(this).val()=='ch')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="20.9506201,82.319949">	Chhattisgarh	</option>');	

		}
		else if($(this).val()=='go')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="15.3472129,74.0149304">	Goa	</option>');	

		}
		else if($(this).val()=='gu')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="22.4168315,71.334013">	Gujarat  </option>');	

		}
		else if($(this).val()=='hr')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="29.282929,76.026532">	Haryana	</option>');	

		}
		else if($(this).val()=='hp')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="31.8202135,77.2953899">	Himachal Pradesh	</option>');	

		}
		else if($(this).val()=='jk')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="33.8988484,76.5281788">	Jammu and Kashmir	</option>');	

		}
		else if($(this).val()=='jh')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="23.650877,85.6356785">	Jharkhand	</option>');	

		}
		else if($(this).val()=='ka')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="15.0172604,76.317812">	Karnataka	</option>');	

		}
		else if($(this).val()=='mp')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="23.9740114,78.422961">	Madhya Pradesh	</option>');	

		}
		else if($(this).val()=='mh')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="18.8154265,76.7751435">	Maharashtra	</option>');	

		}
		else if($(this).val()=='mn')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="24.763961,93.857159">	Manipur	</option>');	

		}
		else if($(this).val()=='mg')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="25.5768819,91.30897">	Meghalaya	</option>');	

		}
		else if($(this).val()=='mz')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="23.2320484,92.84802">	Mizoram	</option>');	

		}
		else if($(this).val()=='ng')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="26.1170955,94.2861465">	Nagaland	</option>');	

		}
		else if($(this).val()=='or')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="20.1910045,84.4359044">	Odisha	</option>');	

		}
		else if($(this).val()=='pb')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="31.0176411,75.396323">	Punjab	</option>');	

		}
		else if($(this).val()=='rj')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="26.6291954,73.8735575">	Rajasthan	</option>');	

		}
		else if($(this).val()=='sk')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="27.60401,88.45858">	Sikkim	</option>');	

		}
		else if($(this).val()=='tn')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="10.7870476,78.2885026">	Tamil Nadu </option>');	

		}
		else if($(this).val()=='tg')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="17.8760325,79.2727297">	Telangana	</option>');	

		}
		else if($(this).val()=='tr')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="23.7313935,91.7434664">	Tripura	</option>');	

		}
		else if($(this).val()=='up')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="27.141237,80.8833819">	Uttar Pradesh	</option>');	

		}
		else if($(this).val()=='uk')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="30.08237,79.3171363">	Uttarakhand	</option>');	

		}
		else if($(this).val()=='wb')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="24.3745337,87.8504985">	West Bengal	</option>');	

		}
		else if($(this).val()=='ld')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="10.0760115,73.6303446">	Lakshdweep	</option>');	

		}
		else if($(this).val()=='pd')
		{
			$('#district').html('<option value="">--Select--</option><option 
			value="11.9088673,79.7361023">	Puducherry	</option>');	

		}
		//Add rest of the states by copy pasting else if loop and change val() to the respective keyword like an for andhra, ar for arunachal etc. Refer the page names map.php for the keywords.
	});
});