
		<?php require('header.php');?>
		<div class='content about-content'>
			<div class="container">
				<div class='row'>			
					<div class='col-md-12 col-sm-12'>				
						<div class='row'>
							<div class='col-md-12 div-about'>
								<div class="content-4 content-about">
									<div class="panel panel-primary text-justify">
								  		<div class="panel-heading">
								    		<h3 class="panel-title">SCOPE OF ONE HEALTH</h3>
								  		</div>
								  		<div class="panel-body">
								    		<div class='row'>
												<div class='col-md-6'>
													<ul style='list-style: none; text-align: left;'>
														<li><span class="glyphicon glyphicon-ok"></span> Agro-and bio-terrorism</li>
														<li><span class="glyphicon glyphicon-ok"></span>  Antimicrobial resistance</li>
														<li><span class="glyphicon glyphicon-ok"></span> Biomedical research</li>
														<li><span class="glyphicon glyphicon-ok"></span> Combating existing and emerging diseases and zoonoses</li>
														<li><span class="glyphicon glyphicon-ok"></span> Conservation medicine</li>
														<li><span class="glyphicon glyphicon-ok"></span> Disaster management</li>
														<li><span class="glyphicon glyphicon-ok"></span> Ethics</li>
														<li><span class="glyphicon glyphicon-ok"></span> Global food and water systems</li>
														<li><span class="glyphicon glyphicon-ok"></span> Health communications</li>
														<li><span class="glyphicon glyphicon-ok"></span> Infectious disease ecology</li>
														<li><span class="glyphicon glyphicon-ok"></span> Land use and production systems and practice</li>
														<li><span class="glyphicon glyphicon-ok"></span> Microbiology education</li>
														<li><span class="glyphicon glyphicon-ok"></span> Occupational health</li>
														<li><span class="glyphicon glyphicon-ok"></span> Public health and public policy</li>
														<li><span class="glyphicon glyphicon-ok"></span> Scientific discovery and knowledge creation</li>
														<li><span class="glyphicon glyphicon-ok"></span> Training</li>
														<li><span class="glyphicon glyphicon-ok"></span> Wildlife promotion and protection</li>
													</ul>
												</div>
												<div class='col-md-6'>
													<ul style='list-style: none; text-align: left;'>
														<li><span class="glyphicon glyphicon-ok"></span> Animal agriculture and animal sciences</li>
														<li><span class="glyphicon glyphicon-ok"></span> Basic and translational research</li>
														<li><span class="glyphicon glyphicon-ok"></span> Clinical human and veterinary medicine</li>							<li><span class="glyphicon glyphicon-ok"></span> Comparative medicine</li>
														<li><span class="glyphicon glyphicon-ok"></span>  Consumer support</li>
														<li><span class="glyphicon glyphicon-ok"></span> Entomology</li>
														<li><span class="glyphicon glyphicon-ok"></span> Food safety and security</li>
														<li><span class="glyphicon glyphicon-ok"></span> Global trade and commerce</li>
														<li><span class="glyphicon glyphicon-ok"></span>  Implications of climate change</li>
														<li><span class="glyphicon glyphicon-ok"></span>  Integrated systems for detection</li>
														<li><span class="glyphicon glyphicon-ok"></span>  Mental health</li>
														<li><span class="glyphicon glyphicon-ok"></span> Public awareness and public communications</li>
														<li><span class="glyphicon glyphicon-ok"></span>  Regulatory enforcement</li>
														<li><span class="glyphicon glyphicon-ok"></span>  Support of biodiversity</li>
														<li><span class="glyphicon glyphicon-ok"></span> Veterinary and environment health professionals and organizations</li>
														<li><span class="glyphicon glyphicon-ok"></span>  Health of the environment and environmental preservation</li>
													</ul>	
												</div>
											</div>
								  		</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class='row news'>
					<div class="col-md-12">
						<div class="container">
							<div class="row">
								<div class='col-md-12'>
									<h3 class="text-center">NEWS & EVENTS</h3>
									<?php require('news-updates.php');?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php require('footer.php');?>	
