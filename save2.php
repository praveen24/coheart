<?php
	session_start();
	require_once('recaptchalib.php');
	  $privatekey = "6Ld7uPYSAAAAACcRzgfXaGOcb7hfsGhm_6hWt-5B";
	  $resp = recaptcha_check_answer ($privatekey,
	                                $_SERVER["REMOTE_ADDR"],
	                                $_POST["recaptcha_challenge_field"],
	                                $_POST["recaptcha_response_field"]);

	  if (!$resp->is_valid) {
	   		$_SESSION['captcha2'] = "The CAPTCHA wasn't entered correctly. Please try it again.";
	   		header("location: involve"); 
	  } 
	  else {
	    // Your code here to handle a successful verification
	if(isset($_POST['btn-org']) && $_POST['btn-org']!="")
	{
		require('dbconnect.php');
		$name = mysqli_real_escape_string($conn,$_POST['orgname']);
		$description = mysqli_real_escape_string($conn,$_POST['description']);
		$purpose = mysqli_real_escape_string($conn,$_POST['purpose']);
		//$scope = mysqli_real_escape_string($conn,$_POST['scope']);
		$csc = mysqli_real_escape_string($conn,$_POST['csc']);
		$activities = mysqli_real_escape_string($conn,$_POST['activities']);
		$contact = mysqli_real_escape_string($conn,$_POST['contact']);
		$email = mysqli_real_escape_string($conn,$_POST['email']);
		$website = mysqli_real_escape_string($conn,$_POST['website']);
		$participants = mysqli_real_escape_string($conn,$_POST['participants']);
		//$history = mysqli_real_escape_string($conn,$_POST['history']);
		$additional = mysqli_real_escape_string($conn,$_POST['additional']);
		if(isset($_POST['desc']) && $_POST['desc']!="")
			$type = $_POST['desc'];
		else
			$type = $_POST['type'];
		$flag = 0;
		if($name == "")
		{
			$flag = 1;
		}
		if($description == "")
		{
			$flag = 1;
		}
		if($purpose == "")
		{
			$flag = 1;
		}
		if($csc == "")
		{
			$flag = 1;
		}
		if($activities == "")
		{
			$flag = 1;
		}
		if($contact == "")
		{
			$flag = 1;
		}
		if($email == "")
		{
			$flag = 1;
		}
		if($website == "")
		{
			$flag = 1;
		}
		if($participants == "")
		{
			$flag = 1;
		}
		if($additional == "")
		{
			$flag = 1;
		}
		if($type == "")
		{
			$flag = 1;
		}
		if($flag == 1)
		{
			header("location: involve");
		}
		else if($flag == 0)
		{
			mysqli_autocommit($conn,false);
			if ( mysqli_connect_errno() )
			{
				echo 'There was an error with your connection: '.mysqli_connect_error();
			}
			else
			{
				$query = mysqli_query($conn,"insert into organizations(name,narrative,purpose,type,csc,activities,contact,email,website,participants,additional,date) values('$name','$description','$purpose','$type','$csc','$activities','$contact','$email','$website','$participants','$additional',now())");
				echo mysqli_error($conn); 
				if($query)
				{
					mysqli_commit($conn);
					$_SESSION['saved'] = 'Submit Successfull.';
					header('location: involve');
				}
				else
					mysqli_rollback($conn);
			}
		}
			
	}
	else
		header('location: involve');
}
?>