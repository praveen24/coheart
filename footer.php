		
		<div class="container-fluid">
			<div class="row subSection">
				<div class="col-sm-6 text-center col-centered">
					<h3>Subscribe to Newsletter</h3>
					<form>
						<input type="email" name="email" placeholder="Enter Your Email" class="inputSub">
						<input type="button" name="" value="Subscribe" class="btnSub">
					</form>
				</div>
			</div>
		</div>
		<footer>
			<div class="container-fluid">
				<div class="row">
					<div class="container">
						<div class="row">
							<div class="col-md-2 col-sm-6">
								<h3>NAVIGATION</h3>
								<ul>
									<li><a href="about-one-health">About us</a></li>
									<li><a href="gallery">Gallery</a></li>
									<li><a href="news">News & Events</a></li>
									<li><a href="partners">Partners</a></li>
									<li><a href="involve">Get Involved</a></li>
									<li><a href="downloads">Downloads</a></li>
									<li><a href="contact">Contact</a></li>
								</ul>
							</div>
							<div class="col-md-3 col-sm-6" style="margin-top: 40px;">
								<h4>Education</h4>
								<ul>
									<li><a href="pg-diploma-in-one-health">PG Diploma in One Health</a></li>
									<li><a href="pg-certificate-in-one-health">PG Certificate in One Health Surveillance</a></li>
									<li><a href="pg-certificate-in-cbdm">PG Certificate in CBDM</a></li>
									<li><a href="how-to-apply">How to Apply</a></li>
									<div class="clearfix"></div>
									<li><a href="#">E-learning</a></li>
									<div class="clearfix"></div>
									<li><a href="past-and-present-students">Past and Present Students</a></li>
								</ul>
							</div>
							<div class="col-md-2 col-sm-6" style="margin-top: 40px;">
								<h4>Advocacy</h4>
								<ul>
									<li><a href="#">Dr. Coheart channel</a></li>
									<li><a href="#">Vets Info</a></li>
									<div class="clearfix"></div>
									<li><a href="#">WISDOM</a></li>
									<div class="clearfix"></div>
									<li><a href="#">Food Safety Know. Con.</a></li>
									<div class="clearfix"></div>
									<li><a href="#">VetSign Pro</a></li>
									<div class="clearfix"></div>
									<li><a href="#">Health Map</a></li>
									<div class="clearfix"></div>
									<li><a href="#">Curofy</a></li>
									<div class="clearfix"></div>
									<li><a href="#">Important Links</a></li>
									<div class="clearfix"></div>
									<li><a href="advocacy-news">Advocacy News</a></li>
								</ul>
							</div>
							<div class="col-md-3 col-sm-6" style="margin-top: 40px;">
								<h4>Research</h4>
								<ul>
									<li><a href="research-projects">Research projects</a></li>
									<li><a href="research-highlights">Research Highlights</a></li>
									<div class="clearfix"></div>
									<li><a href="publications">Publications</a></li>
									<div class="clearfix"></div>
									<li><a href="#">Publication partner links</a></li>
								</ul>
							</div>
							<div class="col-md-2 col-sm-6" style="margin-top: 40px;">
								<h4>Training</h4>
								<ul>
									<li><a href="training">Training</a></li>
									<div class="clearfix"></div>
									<li><a href="seminar">Seminar</a></li>
									<div class="clearfix"></div>
									<li><a href="workshop">Workshop</a></li>
									<div class="clearfix"></div>
									<li><a href="important-day-commemoration">Imp. Day commemoration</a></li>
									<li><a href="student-events">Student Events</a></li>
									<div class="clearfix"></div>
									<li><a href="camp">Camp</a></li>
									<div class="clearfix"></div>
									<li><a href="awareness-class">Awareness Class</a></li>
									<div class="clearfix"></div>
									<li><a href="interface-meet">Interface meet</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<div id="footer">
		    <div class="container-fluid">
				<div class='row'>
					<div class="container">
						<div class="row">
							<div class='col-md-4 col-sm-4 col-lg-4'>
								<p>Powered by Soarmorrow Solutions <a href='http://aidersolutions.in' target='_blank'>(Aider Solutions)</a></p>
							</div>
							<div class='col-md-4 col-sm-4 col-lg-4 text-right'>
								<p>Copyright 2018 @ COHEART</p>
							</div>
							<div class='col-md-4 col-sm-4 col-lg-4 text-right social'>
								<a href="#">f</a>
								<a href="#">t</a>
								<a href="#">g</a>
							</div>
						</div>
					</div>
				</div>
		    </div>
		</div>
	    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	    <script src="dist/js/jquery.js"></script>
	    <!-- Include all compiled plugins (below), or include individual files as needed -->
	    <script src="dist/js/bootstrap.min.js"></script>
		<!--Slider-->
		<!--Slider-->
		<script src="dist/js/pgwslider.min.js"></script>
		<script src="dist/slider/responsiveslides.min.js"></script>
		<!--FancyBox-->
		<script src="dist/lightbox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
		<script src="dist/lightbox/source/jquery.fancybox.js?v=2.1.5"></script>
		<!--Table-->
		<script src="admin/table/js/footable.js"></script>
		<script src="admin/table/js/footable.sort.js"></script>
		<script src="admin/table/js/footable.filter.js"></script>
		<script src="admin/table/js/footable.paginate.js"></script>
		<!--Validation-->
		<script src="validator/jquery.form-validator.js"></script>
		<script type="text/javascript" src="ticker/jquery.webticker.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js"></script>
		<script>
			$(document).on('click.collapse.data-api', '.accordion-toggle', function(event) {
				var $this = $(this),
					parent = $this.data('parent'),
					$parent = parent && $(parent);

				if ($parent) {
					$parent.find('[data-toggle=collapse][data-parent=' + parent + ']').not($this).addClass('collapsed');
				}
			});
			$(function(){
				$("#webticker").webTicker(
				{
					speed: 60,
				 	duplicate: true
				});
			});
			$(function(){
				$("#slider1").responsiveSlides();
				$(".merged input").on({
					focus: function() {
						$(this).prev().addClass("focusedInput")
					},
					blur: function() {
						$(this).prev().removeClass("focusedInput")
					}
				});
			});
			$(document).ready(function(){

	    		$("#owl-demo").owlCarousel({
	    			autoPlay: 10000, //Set AutoPlay to 3 seconds
	 				items : 6,
				    itemsDesktop : [1199,3],
				    itemsDesktopSmall : [979,3]
			    });
			})
			
		</script>
  	</body>
</html>