
		<?php require('header.php');?>
		<div class='content about-content'>
			<div class="container">
				<div class='row'>
					<div class='col-md-12 col-sm-12'>
						<div class='row'>
							<div class='col-md-12 div-about'>
								<div class="content-4 content-about">
									<div class="panel panel-primary text-justify">
								  		<div class="panel-heading">
								  			<h3 class="panel-title">Downloads</h3>
								  		</div>
								  		<div class="panel-body">
											<div class="row">
												<div class="col-md-12">
													<div class="form-group">
														Search <input id="filter" type="text" class="form-control"/>
													</div>
													<table class="table table-bordered footable metro-blue" data-filter="#filter" data-page-size="50" data-page-previous-text="prev" data-page-next-text="next" id='course-table'>
														<tbody>
															<?php
															require('dbconnect.php');
															$query = mysqli_query($conn,'select * from resources');
															if(mysqli_num_rows($query)){
																while($data = mysqli_fetch_array($query))
																{
																	echo "<tr>";
																	echo "<td>";
																	echo "<h4>{$data['resource']}</h4>";
																	echo "<p>{$data['description']}</p>";
																	echo "<a href='admin/uploads/resources/{$data['uploads']}' class='btn btn-default btn-xs pull-right'>Download</a>";
																	echo "</td>";
																	//echo "<td style='vertical-align: middle;'><a href='admin/uploads/resources/{$data['uploads']}' class='btn btn-default btn-xs'>Download</a></td>";
																	echo "</tr>";
																}
															}
															else{
																echo "<tr>";
																echo "<td>";
																echo "<h4>Sorry, No data found</h4>";
															}
															?>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class='row news'>
					<div class="col-md-12">
						<div class="container">
							<div class="row">
								<div class='col-md-12'>
									<?php require('news-updates.php');?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
<?php require('footer.php');?>		
<script>
	$(function()
	{
		$('table').footable();
	});
</script>