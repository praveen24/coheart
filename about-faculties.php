
		<?php require('header.php');?>
		<div class='content about-content'>
			<div class="container">
				<div class='row'>
					<div class='col-md-12 col-sm-12'>					
						<div class='row'>
							<div class='col-md-12 div-about'>	
								<div class="content-6 content-about">
									<div class="panel panel-primary text-justify">
									  	<div class="panel-heading">
									    	<h3 class="panel-title"><strong>FACULTIES</strong></h3>
									  	</div>
									  	<div class="panel-body">
									  		<img src='dist/images/IMG_5505.jpg' class="img-thumbnail" style="float: left; margin: 0 16px 5px 0;width: 130px;">
									    	<p>
									    		<strong>Dr. PREJIT, is the Officer-In- Charge of COHEART from its inception in the year 26/02/2014 and is still continuing.</strong> He did his Ph. D in Veterinary Public Health from Indian Veterinary Research Institute (IVRI), Bareilly, UP. Dr. Prejit is a recipient of various awards such as G.P Sen Endowment award, Dr. SP Singh Best research paper award, Young scientist award and KVASU’s Best teacher award. He has also received international travel grants for various short term assignments at Istanbul Turkey, Nepal, Scotland United Kingdom, Spain, Italy, Denver, United States, Bangkok etc.  He has secured “Outstanding One Health Surveillance Award” during the Annual Conference of International Society for Disease Surveillance (ISDS) held at Colorado. He is one of the founder member of South Asian One Health Disease Surveillance Network. He has guided many MVSc and PG Diploma students and has published several research articles in journals of national and international repute. He is in the editorial board of several journals. Dr. PREJIT is the Course Director of PG Diploma/ Certificate courses on One Health offered by Veterinary University. He is also the Advisor of International Journal of One Health and the spokesperson for Asian countries identified by US One Health Commission.  Apart from his routine duty as Officer-In-Charge of the Centre, he also holding charge of Department of Veterinary Public Health, CVAS, Pookode and Incharge of NABL, Food Quality Assurance Lab, Pookode. 
											</p>
											<br/><br/>
											<img src='dist/images/Jess P 1620.jpg' class="img-thumbnail" style="float: left; margin: 0 16px 5px 0;width: 130px;">
									    	<p>
									    		<strong>Dr. JESS VERGIS, is the Co-ordinator to COHEART from 13/06/2018</strong>. He did his Ph.D. in Veterinary Public Health from ICAR- Indian Veterinary Research Institute (IVRI), Bareilly, Uttar Pradesh. His areas of research interest comprise of Antimicrobial Resistance (AMR), Molecular epidemiology of zoonotic diseases and One health. He was awarded the Department of Science & Technology, INSPIRE fellowship as well as ICAR- SRF and ICAR- JRF fellowships for his Doctoral and Master’s degree. He is the recipient of prestigious Sardar Patel Outstanding ICAR Institution Award-Endowment Gold Medal for B.V.Sc. & A.H. and received Best Oral Presentations and Best Poster Awards in scientific sessions. Dr. Jess is the Course Director of PG Certificate course on Community-based Disaster Management offered by KVASU. (For further details: <a href="http://www.kvasu.ac.in/faculty/details/3/18/18/168" target="_blank">http://www.kvasu.ac.in/faculty/details/3/18/18/168</a>). 
											</p>									
									  	</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="row news">
					<div class="col-md-12">
						<div class="container">							
							<div class='row'>
								<div class='col-md-12'>
										<h3 class="text-center">NEWS & EVENTS</h3>
									<?php require('news-updates.php');?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php require('footer.php');?>	
