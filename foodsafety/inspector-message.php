<?php
	session_start();
	if(isset($_SESSION['foodsafety_inspector']) && $_SESSION['foodsafety_inspector']!="")
	{
?>
<!DOCTYPE html>
<html lang="en-us">
<head>

    <meta charset="utf-8" >
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Food Safety Mission | Messages</title>

    <meta name="author" content="abusinesstheme">
    <meta name="description" content="Palas is a Business HTML Template developed with the the latest HTML5 and CSS3 technologies.">

    <!-- CSS files -->
    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic|Montserrat:400,700' rel='stylesheet'>
    <link href="autocomplete/magicsuggest-min.css" rel="stylesheet">
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="plugins/elegant_font/html_css/style.css">
    
    <link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
    
    <!-- Main CSS file -->
    <link rel="stylesheet" href="css/style.css">
    
</head>
<body>

<?php require('preloader.php'); ?>

<!-- Global Wrapper -->
<div id="wrapper">

    <div class="h-wrapper">

        <!-- Top Bar -->
        <div class="topbar hidden-xs">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <ul class="top-menu">
                            <li><a href="http://coheart.ac.in" target="_blank">COHEART , Kerala Veterinary & Animal Science University</a></li>
                        </ul>
                    </div>
                    <!-- This column is hidden on mobiles -->
                    <div class="col-sm-6">
                        <div class="pull-right top-links">
                           <?php include('login_check1.php'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Header -->
        <header class="header-wrapper header-transparent with-topbar">
            <div class="main-header">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <!-- Logo - Read documentation to see how to change the logo-->
                            <a href="index" class="logo"></a>
                        </div>
                        <div class="col-sm-12 col-md-8">
                            <nav class="navbar-right">
                                <ul class="menu">
                                    <!-- Toggle Menu - For Mobile Devices -->
                                    <li class="toggle-menu"><i class="fa icon_menu"></i></li> 
                                    
                                    <li class="first">
                                        <a href="index">Home</a>
                                    </li> <!-- END Home -->
                                    <li>
                                        <a href="inspector">Admin Page</a>
                                    </li> <!-- END Admin Page -->
                                    <li class="">
                                        <a href="restaurants">Restaurants</a>
                                    </li> <!-- END Restaurants -->
                                    <li>
                                        <a href="news">News</a>
                                    </li> <!-- END News -->
                                    <li>
                                        <a href="downloads">Downloads</a>
                                    </li> <!-- END Downloads -->
                                    <li>
                                        <a href="contact" class="last">Contact</a>
                                    </li> <!-- END Contact -->
                                    <li class="hidden-sm hidden-md hidden-lg">
                                        <a href="logout_superadmin"><i class="fa fa-user"></i> &nbsp;Logout</a>
                                    </li> <!-- END logout -->
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div> <!-- END Container -->
            </div> <!-- END Main Header -->
        </header>

    </div>

    <!-- Do not remove this class -->
    <div class="push-top"></div>

    <section class="section bg-img bg-admin-panel">
        <div class="bg-overlay op6"></div>
        <div class="container">
            <div class="row mt50 mb40">
                <div class="col-sm-12">
                    <h1 class="title-large title-larger color-on-img mb20 mt50">Admin Panel <span class="fs-small">( Messages to Hotels )</span></h1>
                    <div class="br-bottom mb0"></div>
                </div>
            </div>
        </div>
    </section>

    <section class="section">
         <div class="container messsges-page">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h3 class="panel-title text-center" style="font-size: 16px;font-weight: 600;">Message to Hotels</h3>
                  </div>
                  <div class="panel-body">
                    <form id="form-hotel">
                      <div class="form-vaidation-error1"></div>  
                      <div class="form-group">
                        <label for="subject-hotel">Enter Subject</label>
                        <input type="email" class="form-control" id="subject-hotel" placeholder="Enter Subject" name="subject-hotel">
                      </div>
                      <div class="form-group">
                        <label for="hotels">Select Hotels/Restaurents/Cafes</label>
                        <div id="magicsuggest"></div>
                      </div>
                      <div class="form-group">
                        <label for="message-hotel">Enter your message</label>
                        <textarea class="form-control" rows="4" name="message-hotel" id="message-hotel" placeholder="Message"></textarea>
                      </div>
                      <div class="form-group text-center">
                        <button type="button" class="btn btn-primary" id="button-hotel" value="send" name="send">Send Message</button>
                      </div>
                    </form>
                  </div>
                </div>
            </div>
            
        </div>
    </div>
    </section>      

    <?php require('footer.php');?>
    
</div> <!-- END Global Wrapper -->


<!-- Javascript files -->
    <script src="plugins/jquery/jquery-2.1.0.min.js"></script>
    <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="plugins/jquery.appear.js"></script>
    <script src="plugins/retina.min.js"></script>
    <script src="plugins/sticky.min.js"></script>
    
    <!-- Main javascript file -->
    <script src="js/script.js"></script>
	<script src="autocomplete/magicsuggest-min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        var hotel = "";
        var inspector = "";
        var ms1 = $('#magicsuggest').magicSuggest({
            data: 'ajax_suggest1.php',
            allowFreeEntries: false
        });
        
        $(ms1).on('collapse', function(){
            hotel = ms1.getValue();
        });
        
        $('#button-hotel').click(function()
        {   
            $(this).text('Sending').attr('disabled',true);
            var button = $(this).val();
            var subjectHotel = $('#subject-hotel').val();
            var messageHotel = $('#message-hotel').val();
            
            $.ajax(
              {
                url: 'send-message-hotel.php',
                type: "POST",
                data: {button:button,subject:subjectHotel,message:messageHotel,hotels:hotel},
                success: function(data,status)
                {
                    if(data=="success")
                    {
                        $('#button-hotel').text('Send Message').attr('disabled',false);
                        $('form#form-hotel') [0].reset();
                        $('.form-vaidation-error1').hide();
                        $('.form-vaidation-error1').html("<div class='alert alert-success' role='alert'><span class='glyphicon glyphicon-ok' aria-hidden='true'></span>  Message sent!</div>");
                        $('.form-vaidation-error1').fadeIn();
                        setTimeout(function(){ $('.form-vaidation-error1').fadeOut(); }, 3000);
                        ms1.clear();
                    }
                    else
                    {
                        $('#button-hotel').text('Send Message').attr('disabled',false);
                        $('.form-vaidation-error1').hide();
                        $('.form-vaidation-error1').html(data);
                        $('.form-vaidation-error1').fadeIn();
                    }
                }
            });
        });
        
    });
    </script>
</body>
</html>
<?php
	}
	else
		header("location: index");
?>