<?php session_start();
?>
<!DOCTYPE html>
<html lang="en-us">
<head>

	<meta charset="utf-8" >
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Food Safety Mission | Contact Us</title>

	<meta name="author" content="abusinesstheme">
	<meta name="description" content="Palas is a Business HTML Template developed with the the latest HTML5 and CSS3 technologies.">

  	<!-- CSS files -->
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic|Montserrat:400,700' rel='stylesheet'>
	<link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="plugins/elegant_font/html_css/style.css">
	
	<link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
	
	<!-- Main CSS file -->
	<link rel="stylesheet" href="css/style.css">
	
</head>
<body>

<?php require('preloader.php'); ?>

<!-- Global Wrapper -->
<div id="wrapper">

	<div class="h-wrapper">

		<!-- Top Bar -->
		<div class="topbar hidden-xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<ul class="top-menu">
							<li><a href="http://coheart.ac.in" target="_blank">COHEART , Kerala Veterinary & Animal Science University</a></li>
						</ul>
					</div>
					<!-- This column is hidden on mobiles -->
					<div class="col-sm-6">
						<div class="pull-right top-links">
						<!-- login check -->
						<?php include('login_check1.php'); ?>
						<!--/ login check -->
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Header -->
		<header class="header-wrapper header-transparent with-topbar">
			<div class="main-header">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-4">
							<!-- Logo - Read documentation to see how to change the logo-->
							<a href="index" class="logo"></a>
						</div>
						<div class="col-sm-12 col-md-8">
							<nav class="navbar-right">
								<ul class="menu">
									<!-- Toggle Menu - For Mobile Devices -->
									<li class="toggle-menu"><i class="fa icon_menu"></i></li> 
									
									<li class="first">
										<a href="index">Home</a>
									</li> <!-- END Home -->
									<!--login check -->
									<?php include('login_check2.php'); ?>
									<!--/ login check -->
									<li class="">
										<a href="restaurants">Restaurants</a>
									</li> <!-- END Restaurants -->
									<li>
										<a href="news">News</a>
									</li> <!-- END News -->
									<li>
										<a href="downloads">Downloads</a>
									</li> <!-- END Downloads -->
									<li class="active">
										<a href="contact" class="last">Contact</a>
									</li> <!-- END Contact -->
									
								</ul>
							</nav>
						</div>
					</div>
				</div> <!-- END Container -->
			</div> <!-- END Main Header -->
		</header>

	</div>

	<!-- Do not remove this class -->
	<div class="push-top"></div>

	<section class="section-intro bg-img bg06">
		<div class="bg-overlay op6"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-sm-8">
					<h1 class="intro-title mb20">Get in Touch</h1>
					<p class="intro-p mb20">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sit tenetur reiciendis molestias nostrum excepturi porro dolorum amet!</p>
				</div>
			</div>
		</div>
	</section>

	<section>
		<div class="row col-p0 max_height xs_max_height">
			<div class="col-sm-6 col-md-3">
				<div class="box-services-d box-services-e el_max_height">
					<div class="bg-overlay"></div>
					<div class="row col-p0">
						<div class="col-sm-12">
							<p class="mb0 ">55 Cyan Avenue, Suite 65 <br> Los Angeles, CA 8008</p>
							<i class="fa fa-map-marker"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="box-services-d box-services-e dark el_max_height">
					<div class="bg-overlay"></div>
					<div class="row col-p0">
						<div class="col-sm-12">
							<p class="mb0 ">support@palas.com <br> billing@palas.com</p>
							<i class="fa fa-envelope-o"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="box-services-d box-services-e green el_max_height">
					<div class="bg-overlay"></div>
					<div class="row col-p0">
						<div class="col-sm-12">
							<p class="mb0 ">0 800-55-22-55 <br> 0 500-22-44-55</p>
							<i class="fa fa-phone"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-3">
				<div class="box-services-d box-services-e orange el_max_height">
					<div class="bg-overlay"></div>
					<div class="row col-p0">
						<div class="col-sm-12">
							<p class="mb0 ">Mon - Fri: 9:00 - 18:00 <br> Sat - Sun: Closed</p>
							<i class="fa fa-clock-o"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="section page-contact mt30 mb30">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-7 sm-box3">
					<form class="form ajax-contact-form" method="post" action="php/contact.php"><!--class="form ajax-contact-form"-->
						<div class="alert alert-success hidden" id="contact-success">
							<span class="glyphicon glyphicon-ok "></span> &nbsp;
							<strong>Success!</strong> Thank you for your message.
						</div>
						<div class="alert alert-danger hidden" id="contact-error">
							<span class="glyphicon glyphicon-remove "></span> &nbsp;
							<strong>Error!</strong> Oops, something went wrong.
						</div>
						<div class="row col-p10">
							<div class="col-sm-6">
		 						<label class="mb10"><input type="text" name="val_fname" id="val_fname" class="form-control" placeholder=" First Name" data-placement="auto"></label>
							</div>
							<div class="col-sm-6">
		 						<label class="mb10"><input type="text" name="val_lname" id="val_lname" class="form-control" placeholder=" Last Name"></label>
							</div>
						</div>
						<div class="row col-p10">
							<div class="col-sm-6">
		 						<label class="mb10"><input type="text" name="val_subject" id="val_subject" required class="form-control" placeholder=" Subject *"></label>
							</div>
							<div class="col-sm-6">
		 						<label class="mb10"><input type="email" name="val_email" id="val_email" required class="form-control" placeholder=" Email Address *"></label>
							</div>
						</div>
						<label>
		 					<textarea name="val_message" id="val_message"  cols="30" rows="10" required class="form-control" placeholder=" Message *"></textarea>
		 				</label>
		 				<div class="mt20 clearfix">
		 					<!-- Enter your google site key here for captcha -->
		 					<div class="col-sm-6"><img src="codecreaters.php" style="width: 70%;height: 50px;"></div>
							<div class="col-sm-6"><label><input type="text" name="captcha" id="captcha" required class="form-control" placeholder="Enter the text"></label>
							</div>
							<div class="mb30"></div>
	 						<button type="submit" class="btn btn-d">Submit</button>
							
				 		</div>
					</form>
				</div>
				<div class="col-sm-12 col-md-5">
					<h3 class="title-small br-bottom">Contact us</h3>
					<p class="mb40">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veniam quasi modi delectus aliquid doloribus. Accusantium iste earum saepe provident sapiente fugit, vel perspiciatis harum, tempore id, porro sequi aliquid.</p>
					<h3 class="title-small br-bottom">Get social</h3>
					<div class="row">
						<div class="col-sm-5 col-md-7">
							<ul class="sidebar-socials">
								<li><a href="#"><i class="fa fa-twitter"></i> Follow us</a> <span>455 followers</span></li>
								<li><a href="#"><i class="fa fa-facebook"></i> Like us</a> <span>505 likes</span></li>
								<li><a href="#"><i class="fa fa-envelope"></i> Subscribe</a> <span>250 subscribers</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="page-contact">
		<div class="row col-p0">
			<div class="col-sm-12">
				<iframe class="map" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d125103.42732759577!2d76.068389!3d11.5172338!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xbd6e6b60a593c691!2sPookode+Veterinary+College!5e0!3m2!1sen!2sin!4v1428302645570" ></iframe>
			</div>
		</div>
	</section>

	<?php require('footer.php');?>
	
</div> <!-- END Global Wrapper -->




	<!-- Javascript files -->
	<script src="plugins/jquery/jquery-2.1.0.min.js"></script>
	<script src="plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="plugins/jquery.appear.js"></script>
	<script src="plugins/retina.min.js"></script>
	<script src="plugins/stellar.min.js"></script>
	<script src="plugins/sticky.min.js"></script>
	
	<script src="plugins/sharrre/jquery.sharrre.min.js"></script>
	<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="plugins/jquery.zoom.min.js"></script>
	<script src="plugins/raty/jquery.raty.min.js"></script>
	
	<script src="js/jquery.validate.js"></script>
	<script src="js/additional-methods.js"></script>
    <script src="js/jquery-validate.bootstrap-tooltip.js"></script>
    <script src="js/jquery-validate.bootstrap-tooltip.flickerfix.js"></script>


	<!-- Main javascript file -->
	<script src="js/script.js"></script>
	
</body>
</html>