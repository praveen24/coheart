<?php
	session_start();
	if(isset($_SESSION['foodsafety_inspector']) && $_SESSION['foodsafety_inspector']!="")
	{
?>
	<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Food Safety - Inspector</title>

    <link href="rateit/src/rateit.css" rel="stylesheet" type="text/css">    
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">
	<link href="css/business-casual.css" rel="stylesheet">
	<link href="css/flat-ui.css" rel="stylesheet"> 
	     <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:100,300,400,600,700,900,400italic|Montserrat:400,700' rel='stylesheet'>
    <link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="plugins/elegant_font/html_css/style.css">
    
    <link rel="stylesheet" href="plugins/owl-carousel/owl.carousel.css">
    
    <!-- Main CSS file -->
    <link rel="stylesheet" href="css/style.css">
</head>

<body>

<?php require('preloader.php'); ?>

<!-- Global Wrapper -->
<div id="wrapper">

	<div class="h-wrapper">

		<!-- Top Bar -->
		<div class="topbar hidden-xs">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<ul class="top-menu">
							<li><a href="http://coheart.ac.in" target="_blank">COHEART , Kerala Veterinary & Animal Science University</a></li>
						</ul>
					</div>
					<!-- This column is hidden on mobiles -->
					<div class="col-sm-6">
						<div class="pull-right top-links">
							<?php include('login_check1.php'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Header -->
		<header class="header-wrapper header-transparent with-topbar">
			<div class="main-header">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-4">
							<!-- Logo - Read documentation to see how to change the logo-->
							<a href="index" class="logo"></a>
						</div>
						<div class="col-sm-12 col-md-8">
							<nav class="navbar-right">
								<ul class="menu">
									<!-- Toggle Menu - For Mobile Devices -->
									<li class="toggle-menu"><i class="fa icon_menu"></i></li> 
									
									<li class="first">
										<a href="index">Home</a>
									</li> <!-- END Home -->
									<li class="active">
										<a href="inspector">Admin Page</a>
									</li> <!-- END Admin Page -->
									<li class="">
										<a href="restaurants">Restaurants</a>
									</li> <!-- END Restaurants -->
									<li>
										<a href="news">News</a>
									</li> <!-- END News -->
									<li>
										<a href="downloads">Downloads</a>
									</li> <!-- END Downloads -->
									<li>
										<a href="contact" class="last">Contact</a>
									</li> <!-- END Contact -->
									<li class="hidden-sm hidden-md hidden-lg">
										<a href="logout_superadmin"><i class="fa fa-user"></i> &nbsp;Logout</a>
									</li> <!-- END logout -->
								</ul>
							</nav>
						</div>
					</div>
				</div> <!-- END Container -->
			</div> <!-- END Main Header -->
		</header>

	</div>

	<!-- Do not remove this class -->
	<div class="push-top"></div>

	<section class="section bg-img bg-admin-panel">
		<div class="bg-overlay op6"></div>
		<div class="container">
			<div class="row mt50 mb40">
				<div class="col-sm-12">
					<h1 class="title-large title-larger color-on-img mb20 mt50">Admin Panel <span class="fs-small">( Inspector )</span></h1>
					<div class="br-bottom mb0"></div>
				</div>
			</div>
		</div>
	</section>

	<section class="section" id="superadmin-links">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<!-- Box services style 3 -->
						<div class="row col-p30">
							<div class="col-sm-4 xs-box2">
								<div class="box-services-c well">
									<i class="fa fa-list-alt fa-style1 circle"></i>
									<h3 class="title-small br-bottom-center">View Registrations</h3>
									<a href="ins-regview">Click Here</a>
								</div>
							</div>
							<div class="col-sm-4 xs-box2">
								<div class="box-services-c well">
									<i class="fa fa-exclamation-triangle fa-style1 circle"></i>
									<h3 class="title-small br-bottom-center">View Complaints</h3>
									<a href="ins-complaints">Click Here</a>
								</div>
							</div>
							<div class="col-sm-4 xs-box2">
								<div class="box-services-c well">
									<i class="fa fa-download fa-style1 circle"></i>
									<h3 class="title-small br-bottom-center">News</h3>
									<a href="add-news">Click Here</a>
								</div>
							</div>
							<div class="col-sm-4 xs-box2">
								<div class="box-services-c well">
									<i class="fa fa-comment fa-style1 circle"></i>
									<h3 class="title-small br-bottom-center">Messages</h3>
									<a href="inspector-message">Click Here</a>
								</div>
							</div>
							
						</div>
				</div>
			</div>
		</div>
	</section>		

	<?php require('footer.php');?>
	
</div> <!-- END Global Wrapper -->


<!-- Javascript files -->
	<script src="plugins/jquery/jquery-2.1.0.min.js"></script>
	<script src="plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="plugins/jquery.appear.js"></script>
	<script src="plugins/retina.min.js"></script>
	<script src="plugins/stellar.min.js"></script>
	<script src="plugins/sticky.min.js"></script>
	
	<script src="plugins/sharrre/jquery.sharrre.min.js"></script>
	<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
	<script src="plugins/jquery.zoom.min.js"></script>
	<script src="plugins/raty/jquery.raty.min.js"></script>
	
	<!-- Main javascript file -->
	<script src="js/script.js"></script>
	<script src="js/downloads.js"></script>

	

</body>

</html>

<?php
	}
	else
		header("location: index");
?>