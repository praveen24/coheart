<?php
	session_start();
	if((isset($_SESSION['foodsafety_superadmin']) && $_SESSION['foodsafety_superadmin']!="") || (isset($_SESSION['foodsafety_inspector']) && $_SESSION['foodsafety_inspector']!=""))
	{
		if(isset($_POST['button']) && $_POST['button']!="")
		{
			$subject = $_POST['subject'];
			$message = $_POST['message'];
			$filename = $_POST['filename'];
			$primeimage = $_POST['primeimage'];
			$flag = 0;

			if($subject == "")
			{
				echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please enter a heading</div>";
				$flag = 1;
			}
			if($message == "")
			{
				echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Please enter description</div>";
				$flag = 1;
			}
			if($filename == "")
			{
				echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Choose file to upload</div>";
				$flag = 1;
			}
			if($primeimage == "")
			{
				echo "<div class='alert alert-danger' role='alert'><span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'></span>  Choose one image as cover of album</div>";
				$flag = 1;
			}
			if($flag == 0)
			{
				require('dbconnect.php');
				date_default_timezone_set("Asia/Kolkata"); 
				mysqli_autocommit($conn, false);
				$time = date("Y-m-d H:i:s");
				if($stmt = mysqli_prepare($conn,"INSERT into downloads(heading,description,file,thumb) values(?,?,?,?)"))
				{
					mysqli_stmt_bind_param($stmt,'ssss',$subject,$message,$filename,$primeimage);
					if(mysqli_stmt_execute($stmt))
						mysqli_commit($conn);
					mysqli_stmt_close($stmt);
					echo "success";
					mysqli_close($conn);
				}
			}
			
		}
		else
			header("location: index");	
	}	
	
?>