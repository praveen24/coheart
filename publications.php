<?php require('header.php');?>
<?php require('config.inc.php');?>
<?php

$item = $mysqli_conn->query("SELECT * from `publications` order by id limit 1")->fetch_assoc();

?>
<div class='content about-content'>
    <div class="container">
        <div class="row">
            <div class='col-md-12 col-sm-12'>
                <div class='row'>
                    <div class='col-md-12 div-about'>
                        <div class="content-1 content-about">
                            <div class="panel panel-primary text-justify">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><?= isset($item) && $item ? $item['title'] : 'Publications' ?></h3>
                                </div>
                                <div class="panel-body">
                                    <?= isset($item) && $item ? $item['description'] : '--' ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class='row news'>
            <div class='col-md-12'>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="text-center">NEWS & EVENTS</h3>
                            <?php require('news-updates.php');?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require('footer.php');?>