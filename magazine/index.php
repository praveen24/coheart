<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="coheart, kvasu, veterinary, one, health, university, kerala, animal, science, pookode,diploma,certification,course, education, hygiene,Education, Advocacy, Research, Training "/>
   	<meta name="description" content="One Health concept is based on the understanding that the health of the humans, animals and the environment is inextricably linked, and that promoting the well being of all species can only be achieved through a holistic multidisciplinary approach at the human-animal-ecosystems interface. Kerala Veterinary and Animal Sciences University (KVASU) has taken a pioneer step in this regard following the establishment of the Centre for One Health Education Advocacy Research and Training, the first of its kind in India aiming at the sustained health of the community by addressing various issue of concern today like the food safety and security, zoonoses, diseases from natural origins like soil, water and air."/>
    <meta name="google-site-verification" content="Sg2CCDPE0zio41cI6pF2SkUWVUX1nSGT8waFfR8S6f0" />
    <title>COHEART | Center for One Health Education Advocacy Research and Training</title>
    
	<link rel="shortcut icon" href="dist/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="dist/images/favicon.ico" type="image/x-icon">
	<!--Slider-->
	<link rel="stylesheet" href="dist/slider/responsiveslides.css">
	<!--flash-->
	<link rel="stylesheet" href="ticker/example/assets/webticker.css" type="text/css" media="screen">
	<!--FancyBox-->
	<link rel="stylesheet" href="dist/lightbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
	<!-- Bootstrap -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
	<link href="dist/css/style.css" rel="stylesheet">
	<link href="admin/table/css/footable.core.css" rel="stylesheet"/>
	<link href="admin/table/css/footable.metro.css" rel="stylesheet"/>
	<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
	<!--<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>-->
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="booklet/jquery.booklet.latest.css" type="text/css" rel="stylesheet" media="screen, projection, tv" />
	
  </head>
  <body>
    <div class=''>
		<div class='banner'>
			<div class='container'>
				<div class='row hidden-xs'>
					<div class='col-md-1 col-md-offset-1 col-xs-1 col-md-offset-1 text-center'>
						<embed height="150" width="130" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" wmode="transparent" menu="false" quality="high" src="dist/images/coheart-logo.swf" style="display: block !important;">
					</div>
					<div class="col-md-6 col-xs-6">
						<div class="row">
							<div class="col-md-12 text-center">
								<h3 style="color: #fff;"><strong>CENTER for ONE HEALTH EDUCATION ADVOCACY RESEARCH and TRAINING</strong></h3>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 text-center">
								<h4>Kerala Veterinary and Animal Sciences University</h4>
								<h4>Pookode, Wayanad Kerala - 673576</h4>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-xs-4">
						<div class="row">
							<div class="col-md-12 text-right">
								<h4 class="man"><i>"For multidisciplinary holistic approach for harmony among Man, Animal and Nature (M.A.N)"</i></h4>
							</div>
						</div>
						<div class="row">
							<div class="col-md-offset-3 col-md-9 text-right google-search">
								<script>
								  (function() {
								    var cx = '001763096455298922418:gkxx4zxqozc';
								    var gcse = document.createElement('script');
								    gcse.type = 'text/javascript';
								    gcse.async = true;
								    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
								        '//www.google.com/cse/cse.js?cx=' + cx;
								    var s = document.getElementsByTagName('script')[0];
								    s.parentNode.insertBefore(gcse, s);
								  })();
								</script>
								<gcse:search></gcse:search>
							</div>
						</div>
					</div>
				</div>
			</div><!--/container-->
		
			<div class='row visible-xs'>
				<div class='col-xs-12 text-center'>
					<a href="index"><img src='dist/images/kvasu-coheart-logo-800X212.png' alt="kvasu logo" width='544' height='126' class='img-responsive'></a>
				</div>
			</div>
			<div class="row visible-xs">
				<div class="col-md-12">
					<script>
					  (function() {
						var cx = '001763096455298922418:gkxx4zxqozc';
						var gcse = document.createElement('script');
						gcse.type = 'text/javascript';
						gcse.async = true;
						gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							'//www.google.com/cse/cse.js?cx=' + cx;
						var s = document.getElementsByTagName('script')[0];
						s.parentNode.insertBefore(gcse, s);
					  })();
					</script>
					<gcse:search></gcse:search>
				</div>
			</div>
		</div><!--/banner-->
		
		<nav class="navbar navbar-default visible-xs " role="navigation">
					<div class="container-fluid">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
						  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
						  <a class="navbar-brand" href="#">Menu</a>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li class="active"><a href="../index.php">Home</a></li>
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<li><a href="about-one-health">What is One Health</a></li>
								<li><a href="about-genesis">Genesis of COHEART</a></li>
								<li><a href="about-mandate">Mandate, Vision and Mission of COHEART</a></li>
								<li><a href="about-scope">Scope of One Health</a></li>
								<li><a href="about-objectives">Objectives, Activities and Roap Map</a></li>
								<li><a href="about-faculties">Faculties</a></li>
							  </ul>
						</li>
						
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Courses <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<?php
									// require('dbconnect.php');
									// $query = mysqli_query($conn,"select id,course_name from courses");
									// echo mysqli_error($conn);
									// while($data = mysqli_fetch_array($query))
									// {
									// 	$course = strtolower($data['course_name']);
									// 	$course = ucwords($course);
									// 	echo "<li><a href='courses?id={$data['id']}'>$course</a></li>";
									// }
								?>
							  </ul>
						</li>
						<li><a href="gallery">Gallery</a></li>
						<li><a href="partners">Partnering Institutes and supporters</a></li>
						<li><a href="downloads">Downloads</a></li>
						<li><a href="news">News & events</a></li>
						<li><a href="research">Research & Training</a></li>
						<li><a href="involve">Get Involved</a></li>
						<li><a href="contact">Contact Us</a></li>
							</ul>
							
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
		</nav>
		<div class="content">
			<div class='row'>
				<div class='col-md-3 col-sm-4 visible-md visible-lg visible-sm'>
					<div class="panel-group" id="accordion">
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-parent="#accordion" href="../index" class="active">
					          Home
					        </a>
					      </h4>
					    </div>
					   </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          About Us
					        </a>

					      </h4>
					    </div>
					    <div id="collapseTwo" class="panel-collapse collapse">
					      <div class="panel-body">
					        <ul>
					        	<li><a href="about-one-health">What is One Health</a></li>
								<li><a href="about-genesis">Genesis of COHEART</a></li>
								<li><a href="about-mandate">Mandate, Vision and Mission of COHEART</a></li>
								<li><a href="about-scope">Scope of One Health</a></li>
								<li><a href="about-objectives">Objectives, Activities and Roap Map</a></li>
								<li><a href="about-faculties">Faculties</a></li>
					        </ul>
					      </div>
					    </div>
					  </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          Courses
					        </a>
					      </h4>
					    </div>
					    <div id="collapseThree" class="panel-collapse collapse">
					      <div class="panel-body">
					        
					      </div>
					    </div>
					  </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="gallery">Gallery</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="partners">Partnering Institutes and supporters</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="downloads">Downloads</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="news">News & events</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="research">Research & Training</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="involve">Get Involved</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="contact">Contact Us</a>
					      </h4>
					    </div>
					   </div>
					</div>
					
			
					
					
				</div>

				<?php
				// connect to database by adil


				$con=mysqli_connect("localhost","infoaide_coheart","coheart","infoaide_coheart"); // change user_name and password 

					// Check connection
					if (mysqli_connect_errno()) {
					  echo "Failed to connect to MySQL: " . mysqli_connect_error();
					}

					// mysqli_close($con);






				?>






				<div class='col-md-9 col-sm-8'>
					
					<div class='row'>
						<div class='col-md-12'>
							<div class="panel panel-default panel-custom">
								<div class='well text-justify'>
									

									<div id="mybook">
									<?php

									// printing data by adil
									$result = mysqli_query($con,"SELECT * FROM pages");

										while($row = mysqli_fetch_array($result)) {
									?>
										<div>
								            <h3><?php echo $row['heading'] ?></h3>
								            <div>	
								            	<?php if(!empty($row['img'])) { 
								            		echo"<img class =\"image\" src="."../admin/uploads/body/".$row['img'].">";
								            		
								            	
								            		}
								            	?>
								            	<p style="word-wrap: break-word;"><?php echo $row['content'] ?></p>
								            </div>
								        </div>
								    <?php
										
										}
									?>
								       
								    </div>


								
								</div>
							</div>
						</div>
					</div>
					
					
					<div class='row'>
						<div class='col-md-12 visible-xs'>
					<div class='row'>
						<div class='col-md-12 col-sm-12 text-center' style="margin-bottom: 10px;">
							<div class="panel panel-info panel-custom " style='margin-top: 20px; margin-bottom: 20px;'>
								<!--<div class="panel-heading panel-update">
									<h3 class="panel-title text-center">DE's Message</h3>
								</div>-->
							
							</div>
						</div>
						<div class='col-md-12 col-sm-12 text-center'>
							<div class="panel panel-info panel-custom " style='margin-top: 20px; margin-bottom: 20px;'>
								<!--<div class="panel-heading panel-update">
									<h3 class="panel-title text-center">DE's Message</h3>
								</div>-->
								
							</div>
						</div>
					</div>


						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php require('../footer.php');?>	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script> window.jQuery || document.write('<script src="booklet/jquery-2.1.0.min.js"><\/script>') </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<script> window.jQuery.ui || document.write('<script src="booklet/jquery-ui-1.10.4.min.js"><\/script>') </script>
    <script src="booklet/jquery.easing.1.3.js"></script>
    <script src="booklet/jquery.booklet.latest.js"></script>
	<script>
	   $(function() {

    $('#mybook').booklet({

        width:  '94%',

        height: 600

    });

});

    </script>	
    <style type="text/css">
    #mybook{
    	/*width: 100% !important;*/
    }
    
    .b-wrap{
    	box-sizing: content-box !important;
    }
    .image{
    	width: 100%;
    }

    </style>
<?php //require('../footer.php');?>		
