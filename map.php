<!DOCTYPE html>
<html lang="en">   
  <head> 
     <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="coheart, kvasu, veterinary, one, health, university, kerala, animal, science, pookode,diploma,certification,course, education, hygiene,Education, Advocacy, Research, Training "/>
   	<meta name="description" content="One Health concept is based on the understanding that the health of the humans, animals and the environment is inextricably linked, and that promoting the well being of all species can only be achieved through a holistic multidisciplinary approach at the human-animal-ecosystems interface. Kerala Veterinary and Animal Sciences University (KVASU) has taken a pioneer step in this regard following the establishment of the Centre for One Health Education Advocacy Research and Training, the first of its kind in India aiming at the sustained health of the community by addressing various issue of concern today like the food safety and security, zoonoses, diseases from natural origins like soil, water and air."/>
    <meta name="google-site-verification" content="Sg2CCDPE0zio41cI6pF2SkUWVUX1nSGT8waFfR8S6f0" />
    <title>COHEART | Center for One Health Education Advocacy Research and Training</title>
<link rel="shortcut icon" href="dist/images/favicon.ico" type="image/x-icon">
	<link rel="icon" href="dist/images/favicon.ico" type="image/x-icon">
<!--Datepicker-->
  	<link href="admin/datepicker/default.css" rel="stylesheet">
	<!--Slider-->
	<link rel="stylesheet" href="dist/slider/responsiveslides.css">
	<!--flash-->
	<link rel="stylesheet" href="ticker/example/assets/webticker.css" type="text/css" media="screen">
	<!--FancyBox-->
	<link rel="stylesheet" href="dist/lightbox/source/jquery.fancybox.css?v=2.1.5" media="screen" />
<!-- Bootstrap -->
    <link href="dist/css/bootstrap.css" rel="stylesheet">
	<link href="dist/css/style.css" rel="stylesheet">
	<link href="admin/table/css/footable.core.css" rel="stylesheet"/>
	<link href="admin/table/css/footable.metro.css" rel="stylesheet"/>
<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script> 
    <script type="text/javascript" src="js/gmap3.js"></script>
    <style>
      .gmap3{
     
        height: 800px;
      }
      .cluster{
  			color: #FFFFFF;
  			text-align:center;
  			font-family: 'Arial, Helvetica';
  			font-size:11px;
  			font-weight:bold;
      }
      .cluster-1{
        background-image:url(img/m1.png);
        line-height:53px;
  			width: 53px;
  			height: 52px;
      }
      .cluster-2{
        background-image:url(img/m2.png);
        line-height:53px;
  			width: 56px;
  			height: 55px;
      }
      .cluster-3{
        background-image:url(img/m3.png);
        line-height:66px;
  			width: 66px;
  			height: 65px;
      }
    </style>
    
    <script type="text/javascript">
     

      var globalCluster;
    
   $(function(){
				lists = [];
				var image='';
			  <?php
				   require('dbconnect.php');
					$flag = 0;
					 $query = "select * from map";
					   if(isset($_POST['apply']) && $_POST['apply']!="")
						 {
					   if(isset($_POST['type']) && $_POST['type']!="")
						  {
							$query .= " where type='".$_POST['type']."'";
							$flag = 1;
							echo "$('#type').val('{$_POST['type']}');";
						   }
		      if(isset($_POST['category']) && $_POST['category']!="")
	          {
	          		if($flag == 0)
	          			$query .= " where category='".$_POST['category']."'";
	          		else
	          		{
	          			$query .= " and category='".$_POST['category']."'";
	          			$flag = 1;
	          		}
	          		echo "$('#category').val('{$_POST['category']}');";
	          }
	          if(isset($_POST['state']) && $_POST['state']!="")
	          {
	          		if($flag == 0)
	          			$query .= " where state='".$_POST['state']."'";
	          		else
	          		{
	          			$query .= " and state='".$_POST['state']."'";
	          			$flag = 1;
	          		}
	          		echo "$('#state').val('{$_POST['state']}');";
	          }
	          if(isset($_POST['date1']) && isset($_POST['date2']) && $_POST['date1']!="" && $_POST['date2']!="")
	          {
	          		$date1 = explode("/", $_POST['date1']);
					$date1 = array_reverse($date1);
					$date1 = implode("/", $date1);
					$date2 = explode("/", $_POST['date2']);
					$date2 = array_reverse($date2);
					$date2 = implode("/", $date2);
	          		if($flag == 0)
	          			$query .= " where reportdate between '".$date1."' and '".$date2."'";
	          		else
	          		{
	          			$query .= " and reportdate between '".$date1."' and '".$date2."'";
	          			$flag = 1;
	          		}
	          		echo "$('#date1').val('{$_POST['date1']}');";
	          		echo "$('#date2').val('{$_POST['date2']}');";
	          }
	          unset($_POST);
	        }
$i = 1;
          $colors = array();
          $category = array();
          $latlong= array();
$descr= array();
          if($result = mysqli_query($conn,$query))
          {
while($data = mysqli_fetch_array($result))
{                   if($data['type']=='human')
{
$clr="#FF3300";
}
else if($data['type']=='animal')
{
$clr="#FFCC00";
}
else{
$clr="#00CC33";
}
if($data['category']=='virus')
{
$clr_categ="#33FFFF";
}
else if($data['category']=='bacteria')
{
$clr_categ="#000099";
}
else{
$clr_categ="#CCCCCC";
}
                    $buffer = str_replace(array("\r", "\n"), "", $data['description']);
	            $buffer = mysqli_real_escape_string($conn,$buffer);
                    $buffer = '<div id="boxes" style="width:98%;border-bottom:1px solid #CCC;padding-top:2px;"><span style="font-size:15px;border:1px solid #CCC;padding:2px;color:'.$clr.'">'.ucfirst($data['type']).'</span>&nbsp;&nbsp;<span style="font-size:15px;border:1px solid #CCC;padding:2px;color:'.$clr_categ.'">'.ucfirst($data['category']).'</span><br>'.$buffer.'</div>';
        array_push($descr, $buffer);
        array_push($latlong, $data['district']);
        array_push($colors, $data['type']);
        array_push($category, $data['category']);
$i++;
}
	          
      		}
$cnt=count($latlong);
for($k=0;$k<$cnt;$k++)
{
if($colors[$k-1].$category[$k-1]=="animalbacteria")
          		echo "image = 'http://coheart.ac.in/mapicon/animal-bacteria.png';";
          	else if($colors[$k-1].$category[$k-1]=="animalothers")
          		echo "image = 'http://coheart.ac.in/mapicon/animal-others.png';";
          	else if($colors[$k-1].$category[$k-1]=="animalvirus")
          		echo "image = 'http://coheart.ac.in/mapicon/animal-virus.png';";
          	else if($colors[$k-1].$category[$k-1]=="humanbacteria")
          		echo "image = 'http://coheart.ac.in/mapicon/human-bacteria.png';";
          	else if($colors[$k-1].$category[$k-1]=="humanothers")
          		echo "image = 'http://coheart.ac.in/mapicon/human-others.png';";
          	else if($colors[$k-1].$category[$k-1]=="humanvirus")
          		echo "image = 'http://coheart.ac.in/mapicon/human-virus.png';";
          	else
          		echo "image = 'http://coheart.ac.in/mapicon/environment.png';";
echo "lists.push({latLng:[$latlong[$k]],data:'$descr[$k]',options:{icon:image}});";


          	
}
?>

        $('#test1').gmap3({ 
          map:{
            options:{
              center:[23.1289955,82.7792201],
              zoom: 5,
              mapTypeId: google.maps.MapTypeId.ROAD
            },
            events:{
            
            },
            callback: function(map){
              waitForBounds(map);
            }
          }
        });
      });
      
      function waitForBounds(map){
        var ne, sw, bounds = map.getBounds();
        if (!bounds){
          google.maps.event.addListenerOnce(map, 'bounds_changed', 
            function() {
              waitForBounds(map)
            }
          );
          return;
        }
        ne = bounds.getNorthEast();
        sw = bounds.getSouthWest();
        randomMarkers(ne.lat(), sw.lng(), sw.lat(), ne.lng());
      }
      
      function randomMarkers(lat1, lng1, lat2, lng2){

var i, list = [], rlat=lat2-lat1, rlng=lng2-lng1, lat, lng;
 
        $('#test1').gmap3({
          marker:{
            values: lists,
events:{
click: function(marker, event, context){
                var map = $(this).gmap3("get"),
                  infowindow = $(this).gmap3({get:{name:"infowindow"}});
                if (infowindow){
                  infowindow.open(map, marker);
                  infowindow.setContent(context.data);
                } else {
                  $(this).gmap3({
                    infowindow:{
                      anchor:marker,
                      options:{content: context.data,disableAutoPan:true}
					 
                    }
                  });
                }
              }
			 
             
},
            cluster:{
              radius: 100,
              events:{
            	 click: function(cluster, event, context){
				  var infowindow = $(this).gmap3({get:{name:"infowindow"}});
				   if (infowindow){
                  infowindow.close();
                }
//alert(data);

$(this).gmap3({
    infowindow:{
      latLng: context.data.latLng,
      options:{
		disableAutoPan:true,
        content: $.map(context.data.markers, function(marker){return marker.data}).join("<br>")
      }
    }
	
  });
  
            	   
            	 }
            	 
            	},
          		// This style will be used for clusters with more than 0 markers
          		0: {
          		  content: '<div class="cluster cluster-1">CLUSTER_COUNT</div>',
          			width: 53,
          			height: 52
          		},
          		// This style will be used for clusters with more than 20 markers
          		20: {
          		  content: '<div class="cluster cluster-1">CLUSTER_COUNT</div>',
          			width: 53,
          			height: 52
          		},
          		// This style will be used for clusters with more than 50 markers
          		50: {
          		  content: '<div class="cluster cluster-1">CLUSTER_COUNT</div>',
          			width: 53,
          			height: 52
          		}
          	},
          	callback: function(cluster){ // get the cluster and save it in global variable
          	 globalCluster = cluster;
          	}
          }
        });
		
      }
      
      
      
      
    </script>
  <body>
 
<div class='banner'>
			<div class='container'>
				<div class='row hidden-xs'>
					<div class='col-md-1 col-md-offset-1 col-xs-1 col-md-offset-1 text-center'>
						<embed height="150" width="130" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" wmode="transparent" menu="false" quality="high" src="dist/images/coheart-logo.swf" style="display: block !important;">
					</div>
					<div class="col-md-6 col-xs-6">
						<div class="row">
							<div class="col-md-12 text-center">
								<h3 style="color: #fff;"><strong>CENTER for ONE HEALTH EDUCATION ADVOCACY RESEARCH and TRAINING</strong></h3>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 text-center">
								<h4>Kerala Veterinary and Animal Sciences University</h4>
								<h4>Pookode, Wayanad Kerala - 673576</h4>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-xs-4">
						<div class="row">
							<div class="col-md-12 text-right">
								<h4 class="man"><i>"For multidisciplinary holistic approach for harmony among Man, Animal and Nature (M.A.N)"</i></h4>
							</div>
						</div>
						<div class="row">
							<div class="col-md-offset-3 col-md-9 text-right google-search">
								<script>
								  (function() {
								    var cx = '001763096455298922418:gkxx4zxqozc';
								    var gcse = document.createElement('script');
								    gcse.type = 'text/javascript';
								    gcse.async = true;
								    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
								        '//www.google.com/cse/cse.js?cx=' + cx;
								    var s = document.getElementsByTagName('script')[0];
								    s.parentNode.insertBefore(gcse, s);
								  })();
								</script>
								<gcse:search></gcse:search>
							</div>
						</div>
					</div>
				</div>
			</div><!--/container-->
		
			<div class='row visible-xs'>
				<div class='col-xs-12 text-center'>
					<a href="index"><img src='dist/images/kvasu-coheart-logo-800X212.png' alt="kvasu logo" width='544' height='126' class='img-responsive'></a>
				</div>
			</div>
			<div class="row visible-xs">
				<div class="col-md-12">
					<script>
					  (function() {
						var cx = '001763096455298922418:gkxx4zxqozc';
						var gcse = document.createElement('script');
						gcse.type = 'text/javascript';
						gcse.async = true;
						gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
							'//www.google.com/cse/cse.js?cx=' + cx;
						var s = document.getElementsByTagName('script')[0];
						s.parentNode.insertBefore(gcse, s);
					  })();
					</script>
					<gcse:search></gcse:search>
				</div>
			</div>
		</div><!--/banner-->
<nav class="navbar navbar-default visible-xs " role="navigation">
					<div class="container-fluid">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
						  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
						  <a class="navbar-brand" href="#">Menu</a>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li class="active"><a href="index">Home</a></li>
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">About Us <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<li><a href="about-one-health">What is One Health</a></li>
								<li><a href="about-genesis">Genesis of COHEART</a></li>
								<li><a href="about-mandate">Mandate, Vision and Mission of COHEART</a></li>
								<li><a href="about-scope">Scope of One Health</a></li>
								<li><a href="about-objectives">Objectives, Activities and Roap Map</a></li>
								<li><a href="about-faculties">Faculties</a></li>
							  </ul>
						</li>
						
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Courses <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<?php
									require('dbconnect.php');
									$query = mysqli_query($conn,"select id,course_name from courses");
									echo mysqli_error($conn);
									while($data = mysqli_fetch_array($query))
									{
										$course = strtolower($data['course_name']);
										$course = ucwords($course);
										echo "<li><a href='courses?id={$data['id']}'>$course</a></li>";
									}
								?>
							  </ul>
						</li>
						<li><a href="gallery">Gallery</a></li>
						<li><a href="partners">Partnering Institutes and supporters</a></li>
						<li><a href="downloads">Downloads</a></li>
						<li><a href="news">News & events</a></li>
						<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">COHEART Resources <b class="caret"></b></a>
							  <ul class="dropdown-menu">
								<li><a href="research-vid">Video Library</a></li>
								<li><a href="body">Human Body</a></li>
								<li><a href="map">COHEART Health Map</a></li>
							  </ul>
						</li>
						<li><a href="research">Research & Training</a></li>
						<li><a href="involve">Get Involved</a></li>
						<li><a href="contact">Contact Us</a></li>
							</ul>
							
						</div><!-- /.navbar-collapse -->
					</div><!-- /.container-fluid -->
		</nav>
<div class="content">
			<div class='row'>
				<div class='col-md-3 col-sm-4 visible-md visible-lg visible-sm'>

					<div class="panel-group" id="accordion">
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-parent="#accordion" href="index">
					          Home
					        </a>
					      </h4>
					    </div>
					   </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" >
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          About Us
					        </a>

					      </h4>
					    </div>
					    <div id="collapseTwo" class="panel-collapse collapse">
					      <div class="panel-body">
					        <ul>
					        	<li><a href="about-one-health" class="active">What is One Health</a></li>
								<li><a href="about-genesis">Genesis of COHEART</a></li>
								<li><a href="about-mandate">Mandate, Vision and Mission of COHEART</a></li>
								<li><a href="about-scope">Scope of One Health</a></li>
								<li><a href="about-objectives">Objectives, Activities and Roap Map</a></li>
								<li><a href="about-faculties">Faculties</a></li>
					        </ul>
					      </div>
					    </div>
					  </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          Courses
					        </a>
					      </h4>
					    </div>
					    <div id="collapseThree" class="panel-collapse collapse">
					      <div class="panel-body">
					        <ul>
					        	<?php
									require('dbconnect.php');
									$query = mysqli_query($conn,"select id,course_name from courses");
									echo mysqli_error($conn);
									while($data = mysqli_fetch_array($query))
									{
										$course = strtolower($data['course_name']);
										$course = ucwords($course);
										echo "<li><a href='courses?id={$data['id']}'>$course</a></li>";
									}
								?>
					        </ul>
					      </div>
					    </div>
					  </div>
					  <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="gallery">Gallery</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="partners">Partnering Institutes and supporters</a>
					      </h4>
					    </div>
					   </div>
					    <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="studentHome">Student Profile</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="downloads">Downloads</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="news">News & events</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a data-toggle="collapse" data-parent="#accordion" href="#collapsefour">
					          <span class="glyphicon glyphicon-chevron-down pull-right"></span>
					          COHEART Resources
					        </a>

					      </h4>
					    </div>
					    <div id="collapsefour" class="panel-collapse in">
					      <div class="panel-body">
					        <ul>
					        	<li><a href="research-vid">Video Library</a></li>
								<li><a href="body">Human Body</a></li>
								<li><a href="map" class="active">COHEART Health Map</a></li>
								
							</ul>
					      </div>
					    </div>
					  </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="involve">Get Involved</a>
					      </h4>
					    </div>
					   </div>
					   <div class="panel panel-default">
					    <div class="panel-heading">
					      <h4 class="panel-title">
					        <a href="contact">Contact Us</a>
					      </h4>
					    </div>
					   </div>
					</div>
					<?php require('news-updates.php');?>
				</div>
<div class='col-md-9 col-sm-8'>
				<div class='row'>
						<div class='col-md-12'>
							<h3>COHEART Health Map</h3>
						</div>
					</div>
					<form class="" role="form" style="padding: 0; font-size: 12px;" method="POST">
					<div class="form-group row">
						<div class="col-md-2">
							<select class="form-control" name='type' id="type">
								<option value=''>-- Category --</option>
								<option value='human'>Human</option>
								<option value='animal'>Animal</option>
								<option value='environment'>Environment</option>
							</select>
						</div>
						<div class="col-md-2">
							<select class="form-control" name='category' id="category">
								<option value=''>-- Subcategory --</option>
								<option value='virus'>Virus</option>
								<option value='bacteria'>Bacteria</option>
								<!--<option value='fungus'>Fungus</option>-->
								<option value='others'>Others</option>
						  	</select>
						</div>
						<div class="col-md-2">
							<select class="form-control" name='state' id="state">
													  <option value=''>-- State --</option>
													  <option value='ke'>Kerala</option>
													  <option value='an'>Andhra Pradesh</option>
													  <option value='ar'>Arunachal Pradesh</option>
													  <option value='as'>Assam</option>
													  <option value='bi'>Bihar</option>
													  <option value='ch'>Chhattisgarh</option>
													  <option value='go'>Goa</option>
													  <option value='gu'>Gujarat</option>
													  <option value='ha'>Haryana</option>
													  <option value='hi'>Himachal Pradesh</option>
													  <option value='ja'>Jammu and Kashmir</option>
													  <option value='jh'>Jharkhand</option>
													  <option value='ka'>Karnataka</option>
													  <option value='mp'>Madhya Pradesh</option>
													  <option value='mh'>Maharashtra</option>
													  <option value='mn'>Manipur</option>
													  <option value='me'>Meghalaya</option>
													  <option value='mi'>Mizoram</option>
													  <option value='ng'>Nagaland</option>
													  <option value='od'>Odisha</option>
													  <option value='pc'>Puducherry</option>
													  <option value='pu'>Punjab</option>
													  <option value='ra'>Rajasthan</option>
													  <option value='sk'>Sikkim</option>
													  <option value='tn'>Tamil Nadu</option>
													  <option value='te'>Telangana</option>
													  <option value='tr'>Tripura</option>
													  <option value='up'>Uttar Pradesh</option>
													  <option value='ut'>Uttarakhand</option>
													  <option value='wb'>West Bengal</option>
													  <option value='ld'>Lakshdweep</option>
													  <option value='pd'>Puducherry</option>
													  <option value='de'>Delhi</option>
													</select>
						</div>
						<div class="col-md-2">
							<input type="text" class="form-control" id="date1" placeholder="Enter date 1" name="date1">
						</div>
						<div class="col-md-2">
							<input type="text" class="form-control" id="date2" placeholder="Enter date 2" name="date2">
						</div>
						<div class="col-md-2">
						<button type="submit" class="btn btn-primary" name="apply" value="apply">Apply</button>
						<button type="submit" class="btn btn-primary" name="reset" value="reset">Reset</button>
						</div>
					</div>
					
					</form>
<div class='row'>
						<div class='col-md-12'>
							<div class="panel panel-default panel-custom">
								<div class='well text-justify'>
    <div id="test1" class="gmap3"></div>
</div>
							</div>
						</div>
					</div>
<div class='row'>
						<div class='col-md-12 visible-xs'>
					<div class='row'>
						<div class='col-md-12 col-sm-12 text-center' style="margin-bottom: 10px;">
							<div class="panel panel-info panel-custom " style='margin-top: 20px; margin-bottom: 20px;'>
								<!--<div class="panel-heading panel-update">
									<h3 class="panel-title text-center">DE's Message</h3>
								</div>-->
							
							</div>
						</div>
						<div class='col-md-12 col-sm-12 text-center'>
							<div class="panel panel-info panel-custom " style='margin-top: 20px; margin-bottom: 20px;'>
								<!--<div class="panel-heading panel-update">
									<h3 class="panel-title text-center">DE's Message</h3>
								</div>-->
								
							</div>
						</div>
					</div>


						</div>
					</div>
					<div class="row">
						<div class="col-md-2 col-sm-2">
							<img src="mapicon/animal.png" height="20" width="20"> Animals
						</div>
						<div class="col-md-2 col-sm-2">
							<img src="mapicon/human.png" height="20" width="20"> Human
						</div>
						<div class="col-md-2 col-sm-2">
							<img src="mapicon/environment.png" height="20" width="20"> Environmental
						</div>
						
						<div class="col-md-2 col-sm-2">
							<img src="mapicon/bacteria.png" height="20" width="20">Bacteria
						</div>
						<div class="col-md-2 col-sm-2">
							<img src="mapicon/virus.png" height="20" width="20"> Virus
						</div>
						<div class="col-md-2 col-sm-2">
							<img src="mapicon/others.png" height="20" width="20"> Others
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>	
<div id="footer">
      <div class="container">
		<div class='row'>
		<div class='col-md-6 col-sm-6 col-lg-6 text-left' style='padding-left: 30px'>
			<p class="text-muted">© COHEART. All Rights Reserved</p>
		</div>
		<div class='col-md-6 col-sm-6 col-lg-6 text-right' style='padding-right: 30px'>
			<p class="text-muted">Powered by : <a href='http://aidersolutions.in' target='_blank'>Aider Solutions</a></p>
		</div>
		</div>
      </div>
</div>
 <script src="dist/js/bootstrap.min.js"></script>
  </body>
</html>
<script src="admin/datepicker/zebra_datepicker.js"></script>
<script>
	$(function()
	{
		$('#date1').Zebra_DatePicker({
		  pair: $('#date2'),
		  first_day_of_week: 0,
		  format: 'd/m/Y'
		});

		$('#date2').Zebra_DatePicker({
		  direction: 2,
		  format: 'd/m/Y',
		  first_day_of_week: 0
		});
	});
</script>

	
 