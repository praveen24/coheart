
		<?php require('header.php');?>
		<div class='content about-content'>
			<div class="container">
				<div class='row'>
					<div class='col-md-12 col-sm-12'>					
						<div class='row'>
							<div class='col-md-12 div-about'>
								<div class="content-2 content-about">
									<div class="panel panel-primary text-justify">
								  		<div class="panel-heading">
								    		<h3 class="panel-title">Genesis of COHEART</h3>
								  		</div>
								  		<div class="panel-body">
								    		<p>
												One soul many hearts” dedicated to improve the lives of all species is what COHEART is all about. Center for One Health Education, Advocacy, Research and Training commonly referred to as “COHEART” is a new initiative by Kerala Veterinary and Animal Sciences University. This was established as per the decisions of the 10th Academic Council and 26th Board of Management. The centre is located in scenic hilly terrain of Pookode in Wayanad district, Kerala, India. COHEART is the first of its kind in the country and aims to develop a tradition of One Health that challenges conventional thinking by building excellence on integrity, ethical behavior and respect for diversity.  
											</p>
											<p>
												The centre has emerged to dtrengthen the link between the human and animal medicine, and this was well conveyed during the workshop organized by Kerala Veterinary and Animal Sciences university in Wayanad district of Kerala on 16th -17th December 2013. The recommendation to establish a Centre for One health was put forth by these experts who felt the need for organized efforts at multiple levels and by multiple actors. Dr. Gyanendra Gongal, Scientist, Disease Surveillance and Epidemiology, WHO Regional Office, Dr. U. V. S. Rana, Joint Director (Rtd), NCDC, Delhi, Dr. Kumar Venkitanarayan, Professor, University of Connecticut, USA, Dr. C. Ramani, Additional DHS, Directorate of Health Service, Kerala, Dr. K. Udayavarman, President, Kerala State Veterinary Council, Dr. Ravi Jacob Korula, Dean, DM Wayanad Institute of Medical Sciences etc. were some of the experts who put forth the recommendations. The recommendations were valued by the University and approved by the Board of Management and an autonomous centre was formally established on 26/2/2014. 
											</p>									
								  		</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="row news">
					<div class="col-md-12">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<h3 class="text-center">NEWS & EVENTS</h3>
									<?php require('news-updates.php');?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php require('footer.php');?>	
