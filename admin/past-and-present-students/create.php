<?php
session_start();
if( isset($_SESSION['admin'] ) && $_SESSION['admin']!="" )
{
    require('../innerheader.php');

    $table = 'past_and_present_students';
    $pageTitle = 'Past and Present Students';

    if(isset($_POST['save']) && $_POST['save']!="")
    {
        $title = $_POST['name'];
        $description = $_POST['description'];
        $photo = '';
        $flag = 0;
        if($title == "")
        {
            $_SESSION['validate']['name']='Title cannot be empty';
            $flag = 1;
        }
        if($description == "")
        {
            $_SESSION['validate']['description']='Description cannot be empty';
            $flag = 1;
        }
        if($_FILES['upload']['error']==4)
        {
            $_SESSION['validate']['filesize'] = "File size should be less than 4MB. Formats: jpg,png";
            $flag = 1;
        }
        elseif($_FILES['upload']['tmp_name'][0]!="")
        {
            $allowedExts = array(
                "jpg",
                "png",
                "jpeg",
                "pdf",
                "PDF"
            );

            $allowedMimeTypes = array(
                'image/jpeg',
                'image/jpg',
                'image/png',
                'application/pdf'
            );
            for($i=0; $i<count($_FILES['upload']['name']); $i++)
            {
                $extension = pathinfo($_FILES["upload"]["name"][$i], PATHINFO_EXTENSION);

                if(($_FILES['upload']['size'][$i]>8097152) || (!(in_array($extension,$allowedExts))) || (!(in_array($_FILES['upload']['type'][$i],$allowedMimeTypes))))
                {
                    $_SESSION['validate']['filesize'] = "File size should be less than 4MB. Formats: jpg,png,pdf";
                    $flag = 1;
                }
            }
        }
        if($flag == 1)
        {
            $_SESSION['data'] = $_POST;
            header('location: create');
        }
        if($flag == 0)
        {
            require('../../dbconnect.php');
            $description = mysqli_real_escape_string($conn,$_POST['description']);
            if ( mysqli_connect_errno() )
            {
                echo 'There was an error with your connection: '.mysqli_connect_error();
            }
            else
            {
                if($_FILES['upload']['tmp_name'][0]!="")
                {
                    $dir ="uploads/past-and-present/".uniqid();
                    if( is_dir(__DIR__. '/../' . $dir) === false )
                    {
                        @mkdir(__DIR__. '/../' . $dir, 0777, true);
                    }
                    for($i=0; $i<count($_FILES['upload']['name']); $i++)
                    {
                        $file=$_FILES['upload']['name'][$i];
                        $name = explode(".", $file);
                        if($file!="")
                        {
                            $ext= pathinfo($file,PATHINFO_EXTENSION);
                            $photo = $dir."/{$name[0]}_".$id."_".$i.".".$ext;
                            move_uploaded_file($_FILES['upload']['tmp_name'][$i],__DIR__. '/../' . $photo);
                        }
                    }
                }
                $query = mysqli_query($conn,"insert into {$table}(name,description, photo) values('$title','$description', '$photo')");
                if($query)
                {
                    $_SESSION['saved'] = 'Item Added Successfully.';
                    header('location: index');
                }
            }
        }
    }


    ?>
    <script>
        window.document.title = 'COHEART | <?= $pageTitle ?>';
    </script>
    <div class='div-profile shadow'>
        <div class='row'>
            <?php require_once '../sidebar.php'?>
            <div class='col-md-9'>
                <div class="panel panel-default form-panel">
                    <div class="panel-heading text-center">
                        <?=  $pageTitle ?>
                    </div>
                    <div class="panel-body">
                        <form role="form" name='course-form' enctype="multipart/form-data" method='post' action='create'>
                            <?php
                            if(isset($_SESSION['validate']) && $_SESSION['validate'])
                            {
                                echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
                                if(isset($_SESSION['validate']['title']) && $_SESSION['validate']['title']!="")
                                {
                                    echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['course'].".<br/>";
                                    unset($_SESSION['validate']['course']);
                                }
                                if(isset($_SESSION['validate']['description']) && $_SESSION['validate']['description']!="")
                                {
                                    echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['description'].".";
                                    unset($_SESSION['validate']['description']);
                                }

                                if(isset($_SESSION['validate']['file']) && $_SESSION['validate']['file']!="")
                                {
                                    echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['file'].".<br/>";
                                    unset($_SESSION['validate']['file']);
                                }
                                if(isset($_SESSION['validate']['filesize']) && $_SESSION['validate']['filesize']!="")
                                {
                                    echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['filesize'].".<br/>";
                                    unset($_SESSION['validate']['filesize']);
                                }
                                echo "</div>";
                                unset($_SESSION['validate']);
                            }
                            if(isset($_SESSION['saved']) && $_SESSION['saved']!="")
                            {
                                echo "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
                                echo "<span class='glyphicon glyphicon-ok'></span> ".$_SESSION['saved'];
                                unset($_SESSION['saved']);
                                echo "</div>";
                            }
                            if(isset($_SESSION['delete']) && $_SESSION['delete']!="")
                            {
                                echo "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
                                echo "<span class='glyphicon glyphicon-ok'></span> ".$_SESSION['delete'];
                                unset($_SESSION['delete']);
                                echo "</div>";
                            }
                            ?>
                            <div class="form-group">
                                <label for="courseName">Name</label>
                                <input type="text" class="form-control" id="courseName" required name='name' value=
                                '<?php
                                if(isset($_SESSION['data']['name']) && $_SESSION['data']['name']!="")
                                {
                                    echo $_SESSION['data']['name'];
                                    unset($_SESSION['data']['name']);
                                }
                                ?>'>

                            </div>
                            <div class="form-group">
                                <label for="description">Background</label>
                                <textarea class="form-control tinymce" rows="3" required id="description" name='description'>
						<?php
                        if(isset($_SESSION['data']['description']) && $_SESSION['data']['description']!="")
                        {
                            echo $_SESSION['data']['description'];
                            unset($_SESSION['data']['description']);
                        }
                        ?>
						</textarea>

                            </div>
                            <div class="form-group">
                                <label for="upload">Upload Photo</label>
                                <div class="input-group">
								<span class="input-group-btn">
									<span class="btn btn-primary btn-file">
										Browse&hellip; <input type="file" accept="image/*" required name='upload[]'>
									</span>
								</span>
                                    <input type="text" class="form-control" readonly style='border:0;height:32px;'>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary" name='save' value='save'>Save</button>
                            <button type="reset" class="btn btn-info">Clear</button>
                            <a href="index" class="btn btn-danger">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    require('../innerfooter.php');
}
else
    header('location: ../index');
?>
<script>
    $(document)
        .on('change', '.btn-file :file', function() {
            var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
        });

    $(document).ready( function() {
        $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }

        });
    });
</script>
