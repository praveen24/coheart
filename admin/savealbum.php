<?php
	session_start();
	if(isset($_SESSION['admin']) && $_SESSION['admin']!="")
	{
		if(isset($_POST['save']) && $_POST['save']!="")
		{
			$albumName = $_POST['albumName'];
			require('../dbconnect.php');
			if ( mysqli_connect_errno() )
			{
				echo 'There was an error with your connection: '.mysqli_connect_error();
			}
			else
			{
				$description = mysqli_real_escape_string($conn,$_POST['description']);
				$flag = 0;
				if($albumName == "")
				{
					$_SESSION['validate']['course']='Album Name cannot be empty';
					$flag = 1;
				}
				if($description == "")
				{
					$_SESSION['validate']['description']='Description cannot be empty';
					$flag = 1;
				}
				if($flag == 1)
				{
					$_SESSION['data'] = $_POST;
					header('location: albums');
					
				}
				if($flag == 0)
				{
					$query = mysqli_query($conn,"insert into albums(name,description) values('$albumName','$description')");
						if($query)
						{
							$id = mysqli_insert_id($conn);
							
							$_SESSION['saved'] = 'Album Added Successfully.';
							//mkdir("fileupload/server/php/files/$id");
							header('location: albums');
						}
					
				}
			}
		}
		else
			header('location: index');
	}
	else
		header('location: index');
?>