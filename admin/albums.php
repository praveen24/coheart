<?php 
	session_start();
	if( isset($_SESSION['admin'] ) && $_SESSION['admin']!="" )
	{
		require('header.php');
		
?>
	<script>
		window.document.title = 'COHEART | Albums';
	</script>
	<div class='div-profile shadow'>
	<div class='row'>
		<?php require_once 'sidebar.php' ?>
		<div class='col-md-9'>
			<div class="panel panel-default form-panel">
				<div class="panel-heading text-center">
					Albums
				</div>
				<div class="panel-body">
					<form role="form" name='album-form' method='post' action='savealbum'>
						<?php
							if(isset($_SESSION['validate']) && $_SESSION['validate']!="")
							{
								echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
								if(isset($_SESSION['validate']['course']) && $_SESSION['validate']['course']!="")
								{	
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['course'].".<br/>";
									unset($_SESSION['validate']['course']);
								}
								if(isset($_SESSION['validate']['description']) && $_SESSION['validate']['description']!="")
								{
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['description'].".";
									unset($_SESSION['validate']['description']);		
								}
								echo "</div>";
								unset($_SESSION['validate']);
							}
							if(isset($_SESSION['saved']) && $_SESSION['saved']!="")
							{
								echo "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
								echo "<span class='glyphicon glyphicon-ok'></span> ".$_SESSION['saved'];
									unset($_SESSION['saved']);
								echo "</div>";
							}
							if(isset($_SESSION['delete']) && $_SESSION['delete']!="")
							{
								echo "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
								echo "<span class='glyphicon glyphicon-ok'></span> ".$_SESSION['delete'];
									unset($_SESSION['delete']);
								echo "</div>";
							}
						?>
						<div class="form-group">
						<label for="courseName">Album Name</label>
						<input type="text" class="form-control" id="albumName" name='albumName' value=
						'<?php 
							if(isset($_SESSION['data']['albumName']) && $_SESSION['data']['albumName']!="")
							{
								echo $_SESSION['data']['albumName'];
								unset($_SESSION['data']['albumName']);
							}
						?>'>
						
					  </div>
					  <div class="form-group">
						<label for="description">Description</label>
						<textarea class="form-control tinymce" rows="3" id="description" name='description'>
						<?php 
							if(isset($_SESSION['data']['description']) && $_SESSION['data']['description']!="")
							{
								echo $_SESSION['data']['description'];
								unset($_SESSION['data']['description']);
							}
						?>
						</textarea>
						
					  </div>
					  <!--<div class="form-group">
							<div id="queue"></div>
								<input id="file_upload" name="file_upload" type="file" multiple="true">
							
					  </div>-->
					  	<button type="submit" class="btn btn-primary" name='save' value='save'>Create Album</button>
						<!--<button type="reset" class="btn btn-info">Clear</button>-->
					</form>
				</div>
			</div>
			<div class="panel panel-default form-panel">
				<div class="panel-heading text-center">
					Existing Albums
				</div>
				<div class="panel-body">
					<form role="form">
						<div class="form-group">
							Search <input id="filter" type="text" class="form-control"/>
						</div>
						<table class="table table-bordered footable metro-blue" data-filter="#filter" data-page-size="5" data-page-previous-text="prev" data-page-next-text="next" id='course-table'>
					<thead>
						<tr>
							<th>
								#
							</th>
							<th>
								Album Name
							</th>
							<th data-hide='phone' data-sort-ignore='true' style='text-align:center;'>
								View Album
							</th>
							<th data-hide='phone' data-sort-ignore='true' style='text-align:center;'>
								Delete
							</th>
							<!--<th data-hide="phone,tablet">
								DOB
							</th>
							<th data-hide="phone">
								Status
							</th>-->
						</tr>
					</thead>
					<tbody>
					<?php
						require('../dbconnect.php');
						$i=1;
						$query = mysqli_query($conn,"select * from albums");
						while($result = mysqli_fetch_array($query))
						{
							echo "<tr>";
							echo "<td>$i</td>";
							echo "<td>{$result['name']}</td>";
							echo "<td style='text-align:center;'><a href='fileupload/index?album={$result['id']}' class='btn btn-primary btn-xs'>View</a></td>";
							echo "<td style='text-align:center;'><a href='{$result['id']}'><span class='glyphicon glyphicon-remove delete'></span></a></td>";
							echo "</tr>";
							$i++;
						}
					?>	
					</tbody>
					<tfoot class="hide-if-no-paging">
                <tr>
                    <td colspan="5" class='text-center'>
                        <div class="pagination pagination-centered"></div>
                    </td>
                </tr>
                </tfoot>
				</table>
					</form>
				</div>
			</div>
		</div>
	</div>
	</div>
<?php
		require('footer.php');
	}
	else
		header('location: index');
?>
<script>
	<?php $timestamp = time();?>
	$(function()
	{
		$('#course-table').footable();
		$('.delete').click(function(event)
		{
			event.preventDefault();
			id = $(this).parent().attr('href');
			BootstrapDialog.confirm('Are you sure you want to delete this item?', function(result)
			{
				if(result)
				{
					window.location = 'deletealbum?i='+id;
				}
			});
		});
		
	}); 
</script>