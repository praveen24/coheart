<div id="footer">
      <div class="container-fluid">
		<div class='row'>
		<div class='col-md-6 col-sm-6 col-lg-6 text-left' style='padding-left: 30px'>
			<p class="text-muted">© COHEART. All Rights Reserved</p>
		</div>
		<div class='col-md-6 col-sm-6 col-lg-6 text-right' style='padding-right: 30px'>
			<p class="text-muted">Powered by : <a href='http://aidersolutions.in' target='_blank'>Aider Solutions</a></p>
		</div>
		</div>
      </div>
</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../../dist/js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../../dist/js/bootstrap.min.js"></script>
	<!--Slider-->
	<script src="../../dist/slider/responsiveslides.min.js"></script>
	<script src="../../dist/js/jquery.popconfirm.js"></script>
	<script src="../tinymce/tinymce.min.js"></script>
	<script src="../table/js/footable.js"></script>
	<script src="../table/js/footable.sort.js"></script>
	<script src="../table/js/footable.filter.js"></script>
	<script src="../table/js/footable.paginate.js"></script>
	<script src="../confirm/run_prettify.js"></script>
	<script src="../confirm/bootstrap-dialog.min.js"></script>
	
	<script>
		$(function(){
			$("#slider1").responsiveSlides({
				speed: 3000
			});
			tinymce.init
			({
				selector: "textarea.tinymce",
				plugins:
				[
					"advlist autolink lists link image charmap print preview anchor",
					"searchreplace visualblocks code fullscreen",
					"insertdatetime media table contextmenu paste"
				],
				toolbar: "insertfile | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
				height: 200,
				skin: 'custom2'
			});
		});
	</script>
  </body>
</html>