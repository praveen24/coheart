<?php session_start();
	if( isset($_SESSION['admin'] ) && $_SESSION['admin']!="" )
	{
		if(isset($_GET['i']) && $_GET['i']!="")
		{
			$id = $_GET['i'];
			require('../dbconnect.php');
			$query = mysqli_query($conn,"select * from news where id=$id");
			if($result = mysqli_fetch_array($query))
			{
				$query2 = mysqli_query($conn,"delete from news where id=$id");
                // remove image directory as well
                require_once '../functions.php';
                $assetDir = __DIR__ . "/uploads/{$table}/{$id}";
                if (is_dir($assetDir)){
                    deleteDir($assetDir);
                }
				if(isset($result['images']) && $result['images']!="")
					unlink("uploads/news/{$result['images']}");
				$_SESSION['delete'] = 'Selected News/Events Deleted!';
				header('location: newsevents');
			}
			else
				header('location: index');
		}
		else
			header('location: index');
	}
	else
		header('location: index');
?>