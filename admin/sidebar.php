

<div class='col-md-3'>
    <div class="list-group">
        <a href="javascript:void(0);" class="list-group-item menu-top text-center">Menu</a>
        <a href="<?= url('admin/profile') ?>" class="list-group-item menu-item <?= queryHas('admin/profile') ? ' active-item' : '' ?>">
            <span class="glyphicon glyphicon-book"></span>
            &nbsp;&nbsp;&nbsp;Add/Remove Courses
        </a>
        <a href="<?= url('admin/albums') ?>" class="list-group-item menu-item <?= queryHas('admin/album') ? ' active-item' : '' ?>">
            <span class="glyphicon glyphicon-picture"></span>
            &nbsp;&nbsp;&nbsp;Create/Remove Albums
        </a>
        <a href="<?= url('admin/student') ?>" class="list-group-item menu-item <?= queryHas('admin/student') ? ' active-item' : '' ?>">
            <span class="glyphicon glyphicon-user"></span>
            &nbsp;&nbsp;&nbsp;Add/Remove Students
        </a>

        <a href="<?= url('admin/pgdonehealth') ?>" class="list-group-item menu-item <?= queryHas('admin/pgdonehealth') ? ' active-item' : '' ?>">
            <span class="glyphicon glyphicon-tower"></span>
            &nbsp;&nbsp;&nbsp;Manage PG Diploma in One Health
        </a>

        <a href="<?= url('admin/pgconehealth') ?>" class="list-group-item menu-item <?= queryHas('admin/pgconehealth') ? ' active-item' : '' ?>">
            <span class="glyphicon glyphicon-book"></span>
            &nbsp;&nbsp;&nbsp;Manage PG Certificate in One Health
        </a>

        <a href="<?= url('admin/pgccbdm') ?>" class="list-group-item menu-item <?= queryHas('admin/pgccbdm') ? ' active-item' : '' ?>">
            <span class="glyphicon glyphicon-certificate"></span>
            &nbsp;&nbsp;&nbsp;Manage PG Certificate in CBDM
        </a>

        <a href="<?= url('admin/how-to-apply') ?>" class="list-group-item menu-item <?= queryHas('admin/how-to-apply') ? ' active-item' : '' ?>">
            <span class="glyphicon glyphicon-pencil"></span>
            &nbsp;&nbsp;&nbsp;How To Apply
        </a>

        <a href="<?= url('admin/past-and-present-students') ?>" class="list-group-item menu-item <?= queryHas('admin/past-and-present-students') ? ' active-item' : '' ?>">
            <span class="glyphicon glyphicon-user"></span>
            &nbsp;&nbsp;&nbsp;Past &amp; Present Students
        </a>
        <a href="<?= url('admin/projects') ?>" class="list-group-item menu-item <?= queryHas('admin/projects') ? ' active-item' : '' ?>">
            <span class="glyphicon glyphicon-tasks"></span>
            &nbsp;&nbsp;&nbsp;Research Projects
        </a>
        <a href="<?= url('admin/highlights') ?>" class="list-group-item menu-item <?= queryHas('admin/highlights') ? ' active-item' : '' ?>">
            <span class="glyphicon glyphicon-stats"></span>
            &nbsp;&nbsp;&nbsp;Research Highlights
        </a>

        <a href="<?= url('admin/publications') ?>" class="list-group-item menu-item <?= queryHas('admin/publications') ? ' active-item' : '' ?>">
            <span class="glyphicon glyphicon-bookmark"></span>
            &nbsp;&nbsp;&nbsp;Publications
        </a>


        <?php
        $menus = [
                'advocacy_news',
                'training',
                'seminar',
                'workshop',
                'important_day_commemoration',
                'student_events',
                'camp',
                'awareness_class',
                'interface_meet'
        ];
        foreach ($menus as $menu) {
            ?>
            <a href="<?= url('admin/'.$menu) ?>" class="list-group-item menu-item <?= queryHas('admin/'.$menu) ? ' active-item' : '' ?>">
                <span class="glyphicon glyphicon-chevron-right"></span>
                &nbsp;&nbsp;&nbsp;<?= ucwords(str_replace('_', ' ', $menu)) ?>
            </a>
            <?php
        }

        ?>

        <a href="<?= url('admin/resources') ?>" class="list-group-item menu-item <?= queryHas('admin/resources') ? ' active-item' : '' ?>">
            <span class="glyphicon glyphicon-briefcase"></span>
            &nbsp;&nbsp;&nbsp;Downloads
        </a>
        <a href="<?= url('admin/newsevents') ?>" class="list-group-item menu-item <?= queryHas('admin/newsevents') ? ' active-item' : '' ?>">
            <span class="glyphicon glyphicon-edit"></span>
            &nbsp;&nbsp;&nbsp;News & Events
        </a>
        <a href="<?= url('admin/research') ?>" class="list-group-item menu-item <?= queryHas('admin/research') ? ' active-item' : '' ?>">
            <span class="glyphicon glyphicon-search"></span>
            &nbsp;&nbsp;&nbsp;Research & Training
        </a>
        <a href="<?= url('admin/support') ?>" class="list-group-item menu-item <?= queryHas('admin/support') ? ' active-item' : '' ?>">
            <span class="glyphicon glyphicon-search"></span>
            &nbsp;&nbsp;&nbsp;Partnering Institutes & Supporters
        </a>
        <a href="<?= url('admin/map') ?>" class="list-group-item menu-item <?= queryHas('admin/map') ? ' active-item' : '' ?>">
            <span class="glyphicon glyphicon-search"></span>
            &nbsp;&nbsp;&nbsp;Health-map
        </a>
        <a href="<?= url('admin/chngpwd') ?>" class="list-group-item menu-item <?= queryHas('admin/chngpwd') ? ' active-item' : '' ?>">
            <span class="glyphicon glyphicon-lock"></span>
            &nbsp;&nbsp;&nbsp;Change Password
        </a>
        <a href="<?= url('admin/logout') ?>" class="list-group-item menu-item <?= queryHas('admin/logout') ? ' active-item' : '' ?>">
            <span class="glyphicon glyphicon-log-out"></span>
            &nbsp;&nbsp;&nbsp;Logout
        </a>
    </div>
</div>