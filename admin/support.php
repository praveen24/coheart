<?php 
	session_start();
	if( isset($_SESSION['admin'] ) && $_SESSION['admin']!="" )
	{
		require('header.php');
		
?>
	<script>
		window.document.title = 'COHEART - Partnering Institutes & Supporters';
	</script>
	<div class='div-profile shadow'>
	<div class='row'>
        <?php require_once 'sidebar.php' ?>
		<div class='col-md-9'>
			<div class="panel panel-default form-panel">
				<div class="panel-heading text-center">
					Partnering Institutes & Supporters
				</div>
				<div class="panel-body">
					<form role="form">
					<div class="btn-group" style='margin-bottom: 10px;'>
					  <button type="button" class="btn btn-default active" id='ind'>Individuals</button>
					  <button type="button" class="btn btn-default" id='org'>Organizations</button>
					</div>
					<br/>
					<div class='div-indi'>
						<div class="form-group">
							Search <input id="filter" type="text" class="form-control"/>
						</div>
						<table class="table table-bordered footable metro-blue" data-filter="#filter" data-page-size="5" data-page-previous-text="prev" data-page-next-text="next" id='indi-table'>
					<thead>
						<tr>
							<th>
								#
							</th>
							<th>
								Name
							</th>
							<th data-hide='phone' data-sort-ignore='true' style='text-align:center;'>
								View Details
							</th>
							
						</tr>
						</tr>
					</thead>
					<tbody>
					<?php
						require('../dbconnect.php');
						$i=1;
						$query = mysqli_query($conn,"select * from individuals");
						while($result = mysqli_fetch_array($query))
						{
							echo "<tr>";
							echo "<td>$i</td>";
							echo "<td>{$result['name']}</td>";
							echo "<td style='text-align:center;'><a href='viewindividuals?id={$result['id']}' class='btn btn-primary btn-xs'>View</a></td>"; 
							echo "</tr>";
							$i++;
						}
					?>	
					</tbody>
					<tfoot class="hide-if-no-paging">
                <tr>
                    <td colspan="5" class='text-center'>
                        <div class="pagination pagination-centered"></div>
                    </td>
                </tr>
                </tfoot>
				</table>
				</div>
				<div class='div-org'>
				<div class="form-group">
							Search <input id="filter2" type="text" class="form-control"/>
						</div>
						<table class="table table-bordered footable metro-blue" data-filter="#filter2" data-page-size="5" data-page-previous-text="prev" data-page-next-text="next" id='org-table'>
					<thead>
						<tr>
							<th>
								#
							</th>
							<th>
								Name
							</th>
							<th data-hide='phone' data-sort-ignore='true' style='text-align:center;'>
								View
							</th>
							
						</tr>
					</thead>
					<tbody>
					<?php
						require('../dbconnect.php');
						$i=1;
						$query = mysqli_query($conn,"select * from organizations");
						while($result = mysqli_fetch_array($query))
						{
							echo "<tr>";
							echo "<td>$i</td>";
							echo "<td>{$result['name']}</td>";
							echo "<td style='text-align:center;'><a href='vieworganizations?id={$result['id']}' class='btn btn-primary btn-xs'>View</a></td>"; 
							echo "</tr>";
							$i++;
						}
					?>	
					</tbody>
					<tfoot class="hide-if-no-paging">
                <tr>
                    <td colspan="5" class='text-center'>
                        <div class="pagination pagination-centered"></div>
                    </td>
                </tr>
                </tfoot>
				</table>
				</div>
				
					</form>
				</div>
			</div>
			
			
		</div>
	</div>
	</div>
<?php
		require('footer.php');
	}
	else
		header('location: index');
?>
<script>
	$('#indi-table,#org-table').footable();
	$('#ind').click(function()
	{
		$('#org').removeClass('active');
		$(this).addClass('active');
		$(".div-org").hide();
		$(".div-indi").fadeIn();
	});
	$('#org').click(function()
	{
		$('#ind').removeClass('active');
		$(this).addClass('active');
		$(".div-indi").hide();
		$(".div-org").fadeIn();
	});
	//alert(document.referrer);
</script>