<?php
session_start();
if( isset($_SESSION['admin'] ) && $_SESSION['admin']!="" )
{
    require('../innerheader.php');

    $table = 'publications';
    $pageTitle = 'Publications';

    ?>
    <script>
        window.document.title = 'COHEART | <?= $pageTitle ?>';
    </script>
    <div class='div-profile shadow'>
        <div class='row'>
            <?php require_once '../sidebar.php'?>
            <div class='col-md-9'>
                <div class="panel panel-default form-panel">
                    <div class="panel-heading text-center">
                        <?= $pageTitle ?>
                        <a href="create" class="btn btn-success pull-right">Create</a>
                    </div>
                    <div class="panel-body">
                        <form role="form">
                            <div class="form-group">
                                Search <input id="filter" type="text" class="form-control"/>
                            </div>
                            <table class="table table-bordered footable metro-blue" data-filter="#filter" data-page-size="5" data-page-previous-text="prev" data-page-next-text="next" id='course-table'>
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Title
                                    </th>

                                    <th data-hide='phone' data-sort-ignore='true' style='text-align:center;'>
                                        Delete
                                    </th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                require('../../dbconnect.php');
                                $i=1;
                                $query = mysqli_query($conn,"select * from $table");
                                while($result = mysqli_fetch_array($query))
                                {
                                    echo "<tr>";
                                    echo "<td>$i</td>";
                                    echo "<td>{$result['title']}</td>";
                                    echo "<td style='text-align:center;'><a href='{$result['id']}'><span class='glyphicon glyphicon-remove delete'></span></a></td>";
                                    echo "</tr>";
                                    $i++;
                                }
                                ?>
                                </tbody>
                                <tfoot class="hide-if-no-paging">
                                <tr>
                                    <td colspan="5" class='text-center'>
                                        <div class="pagination pagination-centered"></div>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    require('../innerfooter.php');
}
else
    header('location: index');
?>
<script>
    $(function()
    {
        $('#course-table').footable();
        $('.delete').click(function(event)
        {
            event.preventDefault();
            id = $(this).parent().attr('href');
            BootstrapDialog.confirm('Are you sure you want to delete this item?', function(result)
            {
                if(result)
                {
                    window.location = 'delete?i='+id;
                }
            });
        });
    });
</script>