<?php 
	session_start();
	if( isset($_SESSION['admin'] ) && $_SESSION['admin']!="" )
	{
		require('header.php');
		
?>
	<script>
		window.document.title = 'COHEART | Students';
	</script>
	<div class='div-profile shadow'>
	<div class='row'>
        <?php require_once 'sidebar.php' ?>
		<div class='col-md-9'>
			<div class="panel panel-default form-panel">
				<div class="panel-heading text-center">
					Edit Student Details
				</div>
				<div class="panel-body">
					<form role="form" name='student-form' method='post' action='updatestudent'>
						<?php
							if(isset($_SESSION['validate']) && $_SESSION['validate']!="")
							{
								echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
								if(isset($_SESSION['validate']['student']) && $_SESSION['validate']['student']!="")
								{	
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['student'].".<br/>";
									unset($_SESSION['validate']['student']);
								}
								/*if(isset($_SESSION['validate']['address']) && $_SESSION['validate']['address']!="")
								{
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['address'].".<br/>";
									unset($_SESSION['validate']['address']);		
								}
								if(isset($_SESSION['validate']['dob']) && $_SESSION['validate']['dob']!="")
								{
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['dob'].".<br/>";
									unset($_SESSION['validate']['dob']);		
								}*/
								if(isset($_SESSION['validate']['userid']) && $_SESSION['validate']['userid']!="")
								{
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['userid'].".<br/>";
									unset($_SESSION['validate']['userid']);		
								}
								/*if(isset($_SESSION['validate']['contact']) && $_SESSION['validate']['contact']!="")
								{
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['contact'].".<br/>";
									unset($_SESSION['validate']['contact']);		
								}
								if(isset($_SESSION['validate']['profdet']) && $_SESSION['validate']['profdet']!="")
								{
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['profdet'].".<br/>";
									unset($_SESSION['validate']['profdet']);		
								}*/
								if(isset($_SESSION['validate']['pswd']) && $_SESSION['validate']['pswd']!="")
								{
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['pswd'].".<br/>";
									unset($_SESSION['validate']['pswd']);		
								}
								echo "</div>";
								unset($_SESSION['validate']);
							}
							if(isset($_SESSION['saved']) && $_SESSION['saved']!="")
							{
								echo "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
								echo "<span class='glyphicon glyphicon-ok'></span> ".$_SESSION['saved'];
									unset($_SESSION['saved']);
								echo "</div>";
							}
							if(isset($_SESSION['delete']) && $_SESSION['delete']!="")
							{
								echo "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
								echo "<span class='glyphicon glyphicon-ok'></span> ".$_SESSION['delete'];
									unset($_SESSION['delete']);
								echo "</div>";
							}
						?><?php require('../dbconnect.php');

						$id=$_GET['id']; 
						$query = mysqli_query($conn,"select * from students where id='$id'");
						$result = mysqli_fetch_array($query);
						?>
						<input type="hidden" name="id" value="<?php echo $id; ?>">
				       <div class="form-group">
						<label for="Category">Category</label>
						<select id="category" name="category" class="form-control" style="background: #50597b;border-color: #1f253d;color: #9099b7;border-radius: 4px;">
							<option <?php if($result['category']=="student"){ ?>selected<?php } ?> value="student">Student</option>
							<option <?php if($result['category']=="member"){ ?>selected<?php } ?> value="member">Member</option>
						</select>
					  </div>
						<div class="form-group">
						<label for="studentName">Name</label>
						<input type="text" class="form-control" id="studentName" name='studentName' value=
						'<?php echo $result['student_name'];
							if(isset($_SESSION['data']['studentName']) && $_SESSION['data']['studentName']!="")
							{
								echo $_SESSION['data']['studentName'];
								unset($_SESSION['data']['studentName']);
							}
						?>'>
						
					  </div>
					   <div class="form-group">
						<label for="address">Address</label>
						<textarea class="form-control tinymce" rows="3" id="address" name='address'>
						
						<?php echo $result['address'];
							if(isset($_SESSION['data']['address']) && $_SESSION['data']['address']!="")
							{
								echo $_SESSION['data']['address'];
								unset($_SESSION['data']['address']);
							}
						?>
						</textarea>
					  </div>
					   <div class="form-group">
						<label for="dob">Date of Birth</label>
						<input style="background-color: #50597b;" type="text" class="form-control" id="dob" name='dob' value=
						'<?php echo $result['dob'];
							if(isset($_SESSION['data']['dob']) && $_SESSION['data']['dob']!="")
							{
								echo $_SESSION['data']['dob'];
								unset($_SESSION['data']['dob']);
							}
						?>'>
						
					  </div>
					    <div class="form-group">
						<label for="userid">E-mail</label>
						<input style="background-color: #50597b;" type="text" class="form-control" id="userid" name='userid' onchange="checkmail()" value=
						'<?php echo $result['userid'];
							if(isset($_SESSION['data']['userid']) && $_SESSION['data']['userid']!="")
							{
								echo $_SESSION['data']['userid'];
								unset($_SESSION['data']['userid']);
							}
						?>'>
						
					  </div>
					   <div class="form-group">
						<label for="contact">Contact No</label>
						<input type="text" class="form-control" id="contact" name='contact' value=
						'<?php echo $result['contact'];
							if(isset($_SESSION['data']['contact']) && $_SESSION['data']['contact']!="")
							{
								echo $_SESSION['data']['contact'];
								unset($_SESSION['data']['contact']);
							}
						?>' onchange="numcheck(this.value)">
						
					  </div>
					   <div class="form-group">
						<label for="profdet">Professional Details</label>
						<textarea class="form-control tinymce" rows="3" id="profdet" name='profdet'>
						
						<?php echo $result['profdet'];
							if(isset($_SESSION['data']['profdet']) && $_SESSION['data']['profdet']!="")
							{
								echo $_SESSION['data']['profdet'];
								unset($_SESSION['data']['profdet']);
							}
						?>
						</textarea>
					  </div>
						 <!--<div class="form-group">
						<label for="pswd">Password</label>
						<input style="background-color: #50597b;" type="password" class="form-control" id="pswd" name='pswd'>
					  </div>-->
					  	<button type="submit" class="btn btn-primary" name='save' value='save'>Update</button>
						
					</form>
				</div>
			</div>
		</div>
	</div>
	</div>
<?php
		require('footer.php');
	}
	else
		header('location: index');
?>
<script src="datepicker/zebra_datepicker.js"></script>

	

<script>
	function checkmail()
	{
		var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/; 
		var mail=document.getElementById("userid").value;
		if(mail.match(re))
		{
		}else{
			BootstrapDialog.alert("Invalid E-mail id");
			document.getElementById("userid").value="";
		}
	}
	function passcreat() {
		var dob=document.getElementById("dob").value;
		if (dob!=="") {		
		var val=dob.split("/");
		var pswd=val[2]+val[1]+val[0];
		document.getElementById("pswd").value=pswd;
		BootstrapDialog.alert("Password will be the date of birth like 'yyyymmdd'");
	}
	}
	function numcheck(num) {
	if (isNaN(num) || num.length!==10) {
		BootstrapDialog.alert("invalid number");
		document.getElementById("contact").value="";
	}
	}
	$(function()
	{
			$('#student-table').footable();
		$('#dob').Zebra_DatePicker({
			format: 'd/m/Y',
			view: 'years',
			direction: 0,
			first_day_of_week: 0  
		});
		$('#student-table').footable();
		$('.delete').click(function(event)
		{
			event.preventDefault();
			id = $(this).parent().attr('href');
			BootstrapDialog.confirm('Are you sure you want to delete this student?', function(result)
			{
				if(result)
				{
					window.location = 'deleteStudent?i='+id;
				}
			});
		});
	}); 
</script>