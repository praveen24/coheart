<?php
session_start();
if( isset($_SESSION['admin'] ) && $_SESSION['admin']!="" )
{
    require('../innerheader.php');

    $table = 'pgconehealth';
    $pageTitle = 'PG Certificate in One Health';

    if(isset($_POST['save']) && $_POST['save']!="")
    {
        $title = $_POST['title'];
        $description = $_POST['description'];
        $flag = 0;
        if($title == "")
        {
            $_SESSION['validate']['title']='Title cannot be empty';
            $flag = 1;
        }
        if($description == "")
        {
            $_SESSION['validate']['description']='Description cannot be empty';
            $flag = 1;
        }
        if($flag == 1)
        {
            $_SESSION['data'] = $_POST;
            header('location: create');
        }
        if($flag == 0)
        {
            require('../../dbconnect.php');
            $description = mysqli_real_escape_string($conn,$_POST['description']);
            if ( mysqli_connect_errno() )
            {
                echo 'There was an error with your connection: '.mysqli_connect_error();
            }
            else
            {
                $query = mysqli_query($conn,"insert into {$table}(title,description) values('$title','$description')");
                if($query)
                {
                    $_SESSION['saved'] = 'Item Added Successfully.';
                    header('location: index');
                }
            }
        }
    }


    ?>
    <script>
        window.document.title = 'COHEART | <?= $pageTitle ?>';
    </script>
    <div class='div-profile shadow'>
        <div class='row'>
            <?php require_once '../sidebar.php'?>
            <div class='col-md-9'>
                <div class="panel panel-default form-panel">
                    <div class="panel-heading text-center">
                        <?=  $pageTitle ?>
                    </div>
                    <div class="panel-body">
                        <form role="form" name='course-form' method='post' action='create'>
                            <?php
                            if(isset($_SESSION['validate']) && $_SESSION['validate'])
                            {
                                echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
                                if(isset($_SESSION['validate']['title']) && $_SESSION['validate']['title']!="")
                                {
                                    echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['course'].".<br/>";
                                    unset($_SESSION['validate']['course']);
                                }
                                if(isset($_SESSION['validate']['description']) && $_SESSION['validate']['description']!="")
                                {
                                    echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['description'].".";
                                    unset($_SESSION['validate']['description']);
                                }
                                echo "</div>";
                                unset($_SESSION['validate']);
                            }
                            if(isset($_SESSION['saved']) && $_SESSION['saved']!="")
                            {
                                echo "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
                                echo "<span class='glyphicon glyphicon-ok'></span> ".$_SESSION['saved'];
                                unset($_SESSION['saved']);
                                echo "</div>";
                            }
                            if(isset($_SESSION['delete']) && $_SESSION['delete']!="")
                            {
                                echo "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
                                echo "<span class='glyphicon glyphicon-ok'></span> ".$_SESSION['delete'];
                                unset($_SESSION['delete']);
                                echo "</div>";
                            }
                            ?>
                            <div class="form-group">
                                <label for="courseName">Title</label>
                                <input type="text" class="form-control" id="courseName" required name='title' value=
                                '<?php
                                if(isset($_SESSION['data']['title']) && $_SESSION['data']['title']!="")
                                {
                                    echo $_SESSION['data']['title'];
                                    unset($_SESSION['data']['title']);
                                }
                                ?>'>

                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control tinymce" rows="3" required id="description" name='description'>
						<?php
                        if(isset($_SESSION['data']['description']) && $_SESSION['data']['description']!="")
                        {
                            echo $_SESSION['data']['description'];
                            unset($_SESSION['data']['description']);
                        }
                        ?>
						</textarea>

                            </div>
                            <button type="submit" class="btn btn-primary" name='save' value='save'>Save</button>
                            <button type="reset" class="btn btn-info">Clear</button>
                            <a href="index" class="btn btn-danger">Cancel</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    require('../innerfooter.php');
}
else
    header('location: index');
?>