<?php 
	session_start();
	if( isset($_SESSION['admin'] ) && $_SESSION['admin']!="" )
	{
		require('header.php');
		
?>
	<script>
		window.document.title = 'COHEART - Partnering Institutes & Supporters';
	</script>
	<div class='div-profile shadow'>
	<div class='row'>
        <?php require_once 'sidebar.php' ?>
		<div class='col-md-9'>
			<div class="panel panel-default form-panel">
				<div class="panel-heading text-center">
					Details of Individuals
				</div>
				<div class="panel-body">
					<form role="form">
						<div class='well' style='color: #000000'>
							<?php
							if(isset($_GET['id']) && $_GET['id']!="")
							{
								require('../dbconnect.php');
								$query = mysqli_query($conn,"select * from organizations where id={$_GET['id']}");
								if($data = mysqli_fetch_array($query))
								{
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Organization Name</div>";
									echo "<div class='col-md-8'>{$data['name']}</div>";
									echo "</div>";
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Narrative Description</div>";
									echo "<div class='col-md-8'>{$data['narrative']}</div>";
									echo "</div>";
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Purpose</div>";
									echo "<div class='col-md-8'>{$data['purpose']}</div>";
									echo "</div>";
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Type of Organization</div>";
									echo "<div class='col-md-8'>{$data['type']}</div>";
									echo "</div>";
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Country, State & City</div>";
									echo "<div class='col-md-8'>{$data['csc']}</div>";
									echo "</div>";
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Organization Objective</div>";
									echo "<div class='col-md-8'>{$data['activities']}</div>";
									echo "</div>";
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Contact Person's Name</div>";
									echo "<div class='col-md-8'>{$data['contact']}</div>";
									echo "</div>";
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Email</div>";
									echo "<div class='col-md-8'>{$data['email']}</div>";
									echo "</div>";
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Organization Website</div>";
									echo "<div class='col-md-8'><a href='{$data['website']}' target='_blank'>{$data['website']}</a></div>";
									echo "</div>";
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Participants and Key Collaborators</div>";
									echo "<div class='col-md-8'>{$data['participants']}</div>";
									echo "</div>";
									
									echo "<div class='row'>";
									echo "<div class='col-md-4 bold'>Additional Information</div>";
									echo "<div class='col-md-8'>{$data['additional']}</div>";
									echo "</div>";
								}
							}
							?>
							
						</div>
						
					</form>
					<button type="button" class="btn btn-warning btn-sm pull-right" onclick='window.history.go(-1);'><span class="glyphicon glyphicon-chevron-left"></span> Go Back</button>
				</div>
			</div>
			
			
		</div>
	</div>
	</div>
<?php
		require('footer.php');
	}
	else
		header('location: index');
?>
