<?php 
	session_start();
	if( isset($_SESSION['admin'] ) && $_SESSION['admin']!="" )
	{
		require('header.php');
		
?>
	<script>
		window.document.title = 'COHEART | Health-map';
	</script>
	<div class='div-profile shadow'>
	<div class='row'>
        <?php require_once 'sidebar.php' ?>
		<div class='col-md-9'>
			<div class="panel panel-default form-panel">
				<div class="panel-heading text-center">
					Health-Map
				</div>
				<div class="panel-body">
					<form role="form" name='map-form' method='post' action='savemap' id="map-form">
						<?php
							if(isset($_SESSION['validate']) && $_SESSION['validate']!="")
							{
								echo "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
								if(isset($_SESSION['validate']['heading']) && $_SESSION['validate']['heading']!="")
								{	
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['heading'].".<br/>";
									unset($_SESSION['validate']['heading']);
								}
								if(isset($_SESSION['validate']['description']) && $_SESSION['validate']['description']!="")
								{
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['description'].".<br/>";
									unset($_SESSION['validate']['description']);		
								}
								if(isset($_SESSION['validate']['state']) && $_SESSION['validate']['state']!="")
								{
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['state'].".<br/>";
									unset($_SESSION['validate']['state']);		
								}
								if(isset($_SESSION['validate']['district']) && $_SESSION['validate']['district']!="")
								{
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['district'].".<br/>";
									unset($_SESSION['validate']['district']);		
								}
								if(isset($_SESSION['validate']['type']) && $_SESSION['validate']['type']!="")
								{
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['type'].".<br/>";
									unset($_SESSION['validate']['type']);		
								}
								if(isset($_SESSION['validate']['category']) && $_SESSION['validate']['category']!="")
								{
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['category'].".<br/>";
									unset($_SESSION['validate']['category']);		
								}
								if(isset($_SESSION['validate']['date']) && $_SESSION['validate']['date']!="")
								{
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['date'].".<br/>";
									unset($_SESSION['validate']['date']);		
								}
								if(isset($_SESSION['validate']['name']) && $_SESSION['validate']['name']!="")
								{
									echo "<span class='glyphicon glyphicon-exclamation-sign'></span> ".$_SESSION['validate']['name'].".<br/>";
									unset($_SESSION['validate']['name']);		
								}
								echo "</div>";
								unset($_SESSION['validate']);
							}
							if(isset($_SESSION['saved']) && $_SESSION['saved']!="")
							{
								echo "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
								echo "<span class='glyphicon glyphicon-ok'></span> ".$_SESSION['saved'];
									unset($_SESSION['saved']);
								echo "</div>";
							}
							if(isset($_SESSION['delete']) && $_SESSION['delete']!="")
							{
								echo "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
								echo "<span class='glyphicon glyphicon-ok'></span> ".$_SESSION['delete'];
									unset($_SESSION['delete']);
								echo "</div>";
							}
						?>
						<div class="form-group">
						<label for="heading">Heading</label>
						<input type="text" class="form-control" id="heading" name='heading' value=
						'<?php 
							if(isset($_SESSION['data']['heading']) && $_SESSION['data']['heading']!="")
							{
								echo $_SESSION['data']['heading'];
								unset($_SESSION['data']['heading']);
							}
						?>'>
						
					  </div>
					  <div class="form-group">
						<label for="description">Description</label>
						<textarea class="form-control tinymce" rows="3" id="description" name='description'>
						<?php 
							if(isset($_SESSION['data']['description']) && $_SESSION['data']['description']!="")
							{
								echo $_SESSION['data']['description'];
								unset($_SESSION['data']['description']);
							}
						?>
						</textarea>
						
					  </div>
					  <div class="row">
					  	<div class="form-group col-md-6">
											  	<p>
													<label for="type">Type</label>
													<select class="form-control" name='type' id="type">
													  <option value=''>--Select--</option>
													  <option value='human'>Human</option>
													  <option value='animal'>Animal</option>
													  <option value='environment'>Environment</option>
													</select>
												</p>
						</div>
						<div class="form-group col-md-6">
											  	<p>
													<label for="category">Category</label>
													<select class="form-control" name='category' id="category">
													  <option value=''>--Select--</option>
													  <option value='virus'>Virus</option>
													  <option value='bacteria'>Bacteria</option>
													  <!--<option value='fungus'>Fungus</option>-->
													  <option value='others'>Others</option>
													  
													</select>
												</p>
						</div>
					  </div>
					  <div class="row">
					  	<div class="form-group col-md-6">
											  	<p>
													<label for="state">State</label>
													<select class="form-control" name='state' id="state">
													  <option value=''>--Select--</option>
													  <option value='ke'>Kerala</option>
													  <option value='an'>Andhra Pradesh</option>
													  <option value='ar'>Arunachal Pradesh</option>
													  <option value='as'>Assam</option>
													  <option value='bi'>Bihar</option>
													  <option value='ch'>Chhattisgarh</option>
													  <option value='go'>Goa</option>
													  <option value='gu'>Gujarat</option>
													  <option value='ha'>Haryana</option>
													  <option value='hi'>Himachal Pradesh</option>
													  <option value='ja'>Jammu and Kashmir</option>
													  <option value='jh'>Jharkhand</option>
													  <option value='ka'>Karnataka</option>
													  <option value='mp'>Madhya Pradesh</option>
													  <option value='mh'>Maharashtra</option>
													  <option value='mn'>Manipur</option>
													  <option value='me'>Meghalaya</option>
													  <option value='mi'>Mizoram</option>
													  <option value='ng'>Nagaland</option>
													  <option value='od'>Odisha</option>
													  <option value='pc'>Puducherry</option>
													  <option value='pu'>Punjab</option>
													  <option value='ra'>Rajasthan</option>
													  <option value='sk'>Sikkim</option>
													  <option value='tn'>Tamil Nadu</option>
													  <option value='te'>Telangana</option>
													  <option value='tr'>Tripura</option>
													  <option value='up'>Uttar Pradesh</option>
													  <option value='ut'>Uttarakhand</option>
													  <option value='wb'>West Bengal</option>
													</select>
												</p>
						</div>
						<div class="form-group col-md-6">
											  	<p>
													<label for="district">District</label>
													<select class="form-control" name='district' id="district">
													  <option value=''>--Select--</option>
													  
													</select>
												</p>
						</div>
					  </div>
					  <div class="row">
					  	<div class="form-group col-md-6">
					  		<label for="date">Reported Date</label>
							<input type="text" class="form-control" id="date" name='date' value=
							'<?php 
								if(isset($_SESSION['data']['date']) && $_SESSION['data']['date']!="")
								{
									echo $_SESSION['data']['date'];
									unset($_SESSION['data']['date']);
								}
							?>'>
					  	</div>
					  	<div class="form-group col-md-6">
					  		<label for="disease">Name of the Disease</label>
							<input type="text" class="form-control" id="disease" name='disease' value=
							'<?php 
								if(isset($_SESSION['data']['disease']) && $_SESSION['data']['disease']!="")
								{
									echo $_SESSION['data']['disease'];
									unset($_SESSION['data']['disease']);
								}
							?>'>
					  	</div>
					  </div>
					  	<button type="submit" class="btn btn-primary" name='save' value='save'>Save</button>
						<button type="reset" class="btn btn-info">Clear</button>
					</form>
				</div>
			</div>
			<!--<div class="panel panel-default form-panel">
				<div class="panel-heading text-center">
					Existing Courses
				</div>
				<div class="panel-body">
					<form role="form">
						<div class="form-group">
							Search <input id="filter" type="text" class="form-control"/>
						</div>
						<table class="table table-bordered footable metro-blue" data-filter="#filter" data-page-size="5" data-page-previous-text="prev" data-page-next-text="next" id='course-table'>
					<thead>
						<tr>
							<th>
								#
							</th>
							<th>
								Course Name
							</th>
							
							<th data-hide='phone' data-sort-ignore='true' style='text-align:center;'>
								Delete
							</th>
							
						</tr>
					</thead>
					<tbody>
					<?php
						/*require('../dbconnect.php');
						$i=1;
						$query = mysqli_query($conn,"select * from courses");
						while($result = mysqli_fetch_array($query))
						{
							echo "<tr>";
							echo "<td>$i</td>";
							echo "<td>{$result['course_name']}</td>";
							echo "<td style='text-align:center;'><a href='{$result['id']}'><span class='glyphicon glyphicon-remove delete'></span></a></td>";
							echo "</tr>";
							$i++;
						}*/
					?>	
					</tbody>
					<tfoot class="hide-if-no-paging">
                <tr>
                    <td colspan="5" class='text-center'>
                        <div class="pagination pagination-centered"></div>
                    </td>
                </tr>
                </tfoot>
				</table>
					</form>
				</div>
			</div>-->
		</div>
	</div>
	</div>
<?php
		require('footer.php');
	}
	else
		header('location: index');
?>
<script src='../dist/js/district.js'></script>
<script src="datepicker/zebra_datepicker.js"></script>
<script>
	$(function()
	{
		$('#course-table').footable();
		$('#date').Zebra_DatePicker({
			format: 'd/m/Y',
			view: 'years',
			direction: 0,
			first_day_of_week: 0  
		});
		$('.delete').click(function(event)
		{
			event.preventDefault();
			id = $(this).parent().attr('href'); 
			BootstrapDialog.confirm('Are you sure you want to delete this course?', function(result)
			{
				if(result)
				{
					//window.location = 'deleteCourse?i='+id;
				}
			});
		});
	}); 
</script>