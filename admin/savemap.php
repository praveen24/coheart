<?php
	session_start();
	if(isset($_SESSION['admin']) && $_SESSION['admin']!="")
	{
		if(isset($_POST['save']) && $_POST['save']!="")
		{
			$heading = $_POST['heading'];
			$description = $_POST['description'];
			$state = $_POST['state'];
			$district = $_POST['district'];
			$type = $_POST['type'];
			$category = $_POST['category'];
			$reportdate = $_POST['date'];
			$name = $_POST['disease'];
			$flag = 0;
			if($heading == "")
			{
				$_SESSION['validate']['heading']='Heading cannot be empty';
				$flag = 1;
			}
			if($description == "")
			{
				$_SESSION['validate']['description']='Description cannot be empty';
				$flag = 1;
			}
			if($state == "")
			{
				$_SESSION['validate']['state']='State cannot be empty';
				$flag = 1;
			}
			if($district == "")
			{
				$_SESSION['validate']['district']='District cannot be empty';
				$flag = 1;
			}
			if($type == "")
			{
				$_SESSION['validate']['type']='Type cannot be empty';
				$flag = 1;
			}
			if($category == "")
			{
				$_SESSION['validate']['category']='Category cannot be empty';
				$flag = 1;
			}
			if($reportdate == "")
			{
				$_SESSION['validate']['date']='Reported date cannot be empty';
				$flag = 1;
			}
			if($name == "")
			{
				$_SESSION['validate']['name']='Name cannot be empty';
				$flag = 1;
			}
			if($flag == 1)
			{
				header('location: map');
				$_SESSION['data'] = $_POST;
			}
			if($flag == 0)
			{
				require('../dbconnect.php');
				$heading = mysqli_real_escape_string($conn,$_POST['heading']);
				$description = mysqli_real_escape_string($conn,$_POST['description']);
				$name = mysqli_real_escape_string($conn,$_POST['disease']);
				$reportdate = explode("/", $reportdate);
				$reportdate = array_reverse($reportdate);
				$reportdate = implode("/", $reportdate);
				if ( mysqli_connect_errno() )
				{
					echo 'There was an error with your connection: '.mysqli_connect_error();
				}
				else
				{
					$query = mysqli_query($conn,"insert into map(heading,description,state,district,type,category,reportdate,name) values('$heading','$description','$state','$district','$type','$category','$reportdate','$name')");
					echo mysqli_error($conn);
					if($query)
					{
						$_SESSION['saved'] = 'Map Added Successfully.';
						header('location: map');
					}
				}
			}
		}
		else
			header('location: index');
	}
	else
		header('location: index');
?>