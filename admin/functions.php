<?php
/**
 * Helper functions
 *
 * author: Vineeth N Krishnan
 * email : vineeth@soarmorrow.com
 * date: 08-01-2019
 */
if (!function_exists('queryHas')){
    /**
     * @param $str
     * @return bool
     */
    function queryHas($str){
       return stripos(strtolower($_SERVER['REQUEST_URI']), $str) !== false;
    }
}

if (!function_exists('url')){

    /**
     * @param string $location
     * @return string
     */
    function url($location = ''){

        $parts = explode('/', $_SERVER['REQUEST_URI']);

        return sprintf(
            "%s://%s/%s/%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME'],
            '',
            $location
        );
    }
}

if (!function_exists('deleteDir')){
    function deleteDir($dirPath) {
        if (! is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                deleteDir($file);
            } else {
                @unlink($file);
            }
        }
        rmdir($dirPath);
    }
}
