<?php
	session_start();
	if(isset($_SESSION['admin']) && $_SESSION['admin']!="")
	{
		if(isset($_POST['save']) && $_POST['save']!="")
		{
			$resource = $_POST['resource'];
			$description = $_POST['description'];
			$flag = 0;
			if($resource == "")
			{
				$_SESSION['validate']['course']='Heading cannot be empty';
				$flag = 1;
			}
			if($description == "")
			{
				$_SESSION['validate']['description']='Description cannot be empty';
				$flag = 1;
			}
			if($_FILES['upload']['error']==4)
			{
				$_SESSION['validate']['file'] = "Please choose a file to upload.";
				$flag = 1;
			}
			/*if($_FILES['upload']['error']==1)
			{
				$_SESSION['validate']['filesize'] = "File size should be less than 1MB. Formats: pdf,doc,docx";
				$flag = 1;
			}*/
			else if($_FILES['upload']['error']==0)
			{
				$allowedExts = array(
				  "pdf", 
				  "doc", 
				  "docx"
				); 

				$allowedMimeTypes = array( 
				  'application/msword',
				  'application/pdf',
				  'text/pdf'
				);
				$extension = end(explode(".", $_FILES["upload"]["name"]));
			
				if(/*($_FILES['upload']['size']>10240000) ||*/ (!(in_array($extension,$allowedExts))) || (!(in_array($_FILES['upload']['type'],$allowedMimeTypes))))
				{
					$_SESSION['validate']['filesize'] = "File size should be less than 1MB. Formats: pdf,doc,docx";
					$flag = 1;
				}
				
			}	
			if($flag == 1)
			{
				$_SESSION['data'] = $_POST;
				header('location: resources');
			}
			
				
			if($flag == 0)
			{
				require('../dbconnect.php');
				$description = mysqli_real_escape_string($conn,$_POST['description']);
				mysqli_autocommit($conn,false);
				if ( mysqli_connect_errno() )
				{
					echo 'There was an error with your connection: '.mysqli_connect_error();
				}
				else
				{
					$query = mysqli_query($conn,"insert into resources(resource,description) values('$resource','$description')");
					if($query)
					{
						$id = mysqli_insert_id($conn);
						$file=$_FILES['upload']['name'];
						$ext= pathinfo($file,PATHINFO_EXTENSION);
						if(move_uploaded_file($_FILES['upload']['tmp_name'],"uploads/resources/".$id.".".$ext))
						{
							$filename=$id.".".$ext;
							$query2 = mysqli_query($conn,"update resources set uploads='$filename' where id=$id");
							if($query2)
							{
								mysqli_commit($conn);
								$_SESSION['saved'] = 'Resource Added Successfully.';
								header('location: resources');
							}
							else
								mysqli_rollback($conn);
						}
					}
					else
						mysqli_rollback($conn);
				}
			}
		}
		else
			header('location: index');
	}
	else
		header('location: index');
?>