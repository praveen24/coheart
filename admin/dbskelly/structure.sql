-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 08, 2019 at 01:24 PM
-- Server version: 5.7.22-0ubuntu0.17.10.1
-- PHP Version: 7.1.17-0ubuntu0.17.10.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `coheart`
--

-- --------------------------------------------------------

--
-- Table structure for table `pgdonehealth`
--

CREATE TABLE `pgdonehealth` (
                              `id` bigint(20) NOT NULL,
                              `title` varchar(250) NOT NULL,
                              `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pgdonehealth`
--
ALTER TABLE `pgdonehealth`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pgdonehealth`
--
ALTER TABLE `pgdonehealth`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;


--
-- Table structure for table `pgconehealth`
--

CREATE TABLE `pgconehealth` (
                              `id` bigint(20) NOT NULL,
                              `title` varchar(250) NOT NULL,
                              `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pgconehealth`
--
ALTER TABLE `pgconehealth`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pgconehealth`
--
ALTER TABLE `pgconehealth`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;



--
-- Table structure for table `pgccbdm`
--

CREATE TABLE `pgccbdm` (
                         `id` bigint(20) NOT NULL,
                         `title` varchar(250) NOT NULL,
                         `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pgccbdm`
--
ALTER TABLE `pgccbdm`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pgccbdm`
--
ALTER TABLE `pgccbdm`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;


--
-- Table structure for table `how_to_apply`
--

CREATE TABLE `how_to_apply` (
                         `id` bigint(20) NOT NULL,
                         `title` varchar(250) NOT NULL,
                         `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pgccbdm`
--
ALTER TABLE `how_to_apply`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pgccbdm`
--
ALTER TABLE `how_to_apply`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;


CREATE TABLE `past_and_present_students` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(100) NOT NULL , `description` TEXT NULL , `photo` VARCHAR(200) NULL , `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `research_projects` ( `id` INT NOT NULL AUTO_INCREMENT , `title` VARCHAR(100) NOT NULL , `description` TEXT NULL , `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `highlights` ( `id` INT NOT NULL AUTO_INCREMENT , `title` VARCHAR(100) NOT NULL , `description` TEXT NULL , `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `publications` ( `id` INT NOT NULL AUTO_INCREMENT , `title` VARCHAR(100) NOT NULL , `description` TEXT NULL , `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;

CREATE TABLE `advocacy_news` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `heading` VARCHAR(250) NOT NULL , `description` LONGTEXT NOT NULL , `images` TEXT NULL DEFAULT NULL , `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `training` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `heading` VARCHAR(250) NOT NULL , `description` LONGTEXT NOT NULL , `images` TEXT NULL DEFAULT NULL , `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `seminar` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `heading` VARCHAR(250) NOT NULL , `description` LONGTEXT NOT NULL , `images` TEXT NULL DEFAULT NULL , `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `workshop` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `heading` VARCHAR(250) NOT NULL , `description` LONGTEXT NOT NULL , `images` TEXT NULL DEFAULT NULL , `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `important_day_commemoration` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `heading` VARCHAR(250) NOT NULL , `description` LONGTEXT NOT NULL , `images` TEXT NULL DEFAULT NULL , `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `student_events` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `heading` VARCHAR(250) NOT NULL , `description` LONGTEXT NOT NULL , `images` TEXT NULL DEFAULT NULL , `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `camp` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `heading` VARCHAR(250) NOT NULL , `description` LONGTEXT NOT NULL , `images` TEXT NULL DEFAULT NULL , `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `awareness_class` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `heading` VARCHAR(250) NOT NULL , `description` LONGTEXT NOT NULL , `images` TEXT NULL DEFAULT NULL , `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;
CREATE TABLE `interface_meet` ( `id` BIGINT NOT NULL AUTO_INCREMENT , `heading` VARCHAR(250) NOT NULL , `description` LONGTEXT NOT NULL , `images` TEXT NULL DEFAULT NULL , `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)) ENGINE = InnoDB;