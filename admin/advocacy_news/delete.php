<?php session_start();
	if( isset($_SESSION['admin'] ) && $_SESSION['admin']!="" )
	{
		if(isset($_GET['i']) && $_GET['i']!="")
		{

            $table = ltrim(strrchr(__DIR__, '/'), '/');
            $pageTitle = ucwords(str_replace('_',' ',$table));
			$id = $_GET['i'];
			require('../../dbconnect.php');
			$query = mysqli_query($conn,"select * from $table where id=$id");
			if($result = mysqli_fetch_array($query))
			{
				$query2 = mysqli_query($conn,"delete from $table where id=$id");
				// remove image directory as well
                require_once '../functions.php';
                $assetDir = __DIR__ . "/../uploads/{$table}/{$id}";
                if (is_dir($assetDir)){
                    deleteDir($assetDir);
                }
				if(isset($result['images']) && $result['images']!="")
					unlink(__DIR__."/../uploads/{$table}/{$result['images']}");
				$_SESSION['delete'] = 'Selected Item Deleted!';
				header('location: index');
			}
			else
				header('location: index');
		}
		else
			header('location: index');
	}
	else
		header('location: ../index');
?>