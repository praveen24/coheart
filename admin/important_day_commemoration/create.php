<?php
	session_start();

    $table = ltrim(strrchr(__DIR__, '/'), '/');
    $pageTitle = ucwords(str_replace('_',' ',$table));

if(isset($_SESSION['admin']) && $_SESSION['admin']!="")
	{
	    
		if(isset($_POST['save']) && $_POST['save']!="")
		{
			$item = $_POST['news'];
			$description = $_POST['description'];
			$flag = 0;
			
			if($item == "")
			{
				$_SESSION['validate']['course']='Heading cannot be empty';
				$flag = 1;
			}
			if($description == "")
			{
				$_SESSION['validate']['description']='Description cannot be empty';
				$flag = 1;
			}
			if(count($_FILES['upload']['name'])>4)
			{
				$_SESSION['validate']['filesize'] = "Only upto 4 files. Formats: jpg,png,pdf";
				$flag = 1;
			}
			if($_FILES['upload']['error']==4)
			{
				$_SESSION['validate']['filesize'] = "File size should be less than 4MB. Formats: jpg,png";
				$flag = 1;
			}
			elseif($_FILES['upload']['tmp_name'][0]!="")
			{
				$allowedExts = array(
				  "jpg", 
				  "png", 
				  "jpeg",
				  "pdf",
				  "PDF"
				); 

				$allowedMimeTypes = array( 
				  'image/jpeg',
				  'image/jpg',
				  'image/png',
				  'application/pdf'
				);
				for($i=0; $i<count($_FILES['upload']['name']); $i++)
				{
					$extension = pathinfo($_FILES["upload"]["name"][$i], PATHINFO_EXTENSION);
					
					 if(($_FILES['upload']['size'][$i]>8097152) || (!(in_array($extension,$allowedExts))) || (!(in_array($_FILES['upload']['type'][$i],$allowedMimeTypes))))
					{
						$_SESSION['validate']['filesize'] = "File size should be less than 4MB. Formats: jpg,png,pdf";
						$flag = 1;
					}
				}
			}
			
			if($flag == 1)
			{
				$_SESSION['data'] = $_POST;
				header('location: index');
			}
			
				
			if($flag == 0)
			{
				require('../../dbconnect.php');
				$description = mysqli_real_escape_string($conn,$_POST['description']);
				mysqli_autocommit($conn,false);
				if ( mysqli_connect_errno() )
				{
					echo 'There was an error with your connection: '.mysqli_connect_error();
				}
				else
				{
					$query = mysqli_query($conn,"insert into {$table}(heading,description,date) values('$item','$description',now())");
					if($query)
					{
						$id = mysqli_insert_id($conn);
						if($_FILES['upload']['tmp_name'][0]!="")
						{
							$dir ="uploads/{$table}/$id";
                            if( is_dir(__DIR__. '/../' . $dir) === false )
                            {
                                @mkdir(__DIR__. '/../' . $dir, 0777, true);
                            }
							for($i=0; $i<count($_FILES['upload']['name']); $i++)
							{
								$file=$_FILES['upload']['name'][$i];
								$name = explode(".", $file);
								if($file!="")
								{
									$ext= pathinfo($file,PATHINFO_EXTENSION);
                                    $photo = $dir."/{$name[0]}_".$id."_".$i.".".$ext;
									move_uploaded_file($_FILES['upload']['tmp_name'][$i],__DIR__. '/../' . $photo);
								}
							}
						}
						mysqli_commit($conn);
						$_SESSION['saved'] = 'Item Added Successfully.';
						header('location: index');
						
					}
					else
					{
						mysqli_rollback($conn);
						header('location: index');
					}
				}
			}
		}
		else
			header('location: index');
	}
	else
		header('location: ../index');
?>