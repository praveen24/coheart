<?php
	session_start();
	if(isset($_SESSION['admin']) && $_SESSION['admin']!="")
	{
		if(isset($_POST['save']) && $_POST['save']!="")
		{
			$coursename = $_POST['courseName'];
			$description = $_POST['description'];
			$flag = 0;
			if($coursename == "")
			{
				$_SESSION['validate']['course']='Course Name cannot be empty';
				$flag = 1;
			}
			if($description == "")
			{
				$_SESSION['validate']['description']='Description cannot be empty';
				$flag = 1;
			}
			if($flag == 1)
			{
				header('location: profile');
				$_SESSION['data'] = $_POST;
			}
			if($flag == 0)
			{
				require('../dbconnect.php');
				$description = mysqli_real_escape_string($conn,$_POST['description']);
				if ( mysqli_connect_errno() )
				{
					echo 'There was an error with your connection: '.mysqli_connect_error();
				}
				else
				{
					$query = mysqli_query($conn,"insert into courses(course_name,description) values('$coursename','$description')");
					if($query)
					{
						$_SESSION['saved'] = 'Course Added Successfully.';
						header('location: profile');
					}
				}
			}
		}
		else
			header('location: index');
	}
	else
		header('location: index');
?>