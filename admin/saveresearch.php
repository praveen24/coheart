<?php
	session_start();
	if(isset($_SESSION['admin']) && $_SESSION['admin']!="")
	{
		if(isset($_POST['save']) && $_POST['save']!="")
		{
			$research = $_POST['research'];
			$description = $_POST['description'];
			$flag = 0;
			if($research == "")
			{
				$_SESSION['validate']['course']='Heading cannot be empty';
				$flag = 1;
			}
			if($description == "")
			{
				$_SESSION['validate']['description']='Description cannot be empty';
				$flag = 1;
			}
			/*if($_FILES['upload']['error']==4)
			{
				$_SESSION['validate']['file'] = "Please choose a file to upload.";
				$flag = 1;
			}*/
			if($_FILES['upload']['error']==1)
			{
				$_SESSION['validate']['filesize'] = "File size should be less than 1MB. Formats: jpg,png";
				$flag = 1;
			}
			if($_FILES['upload']['error']==0)
			{
				$allowedExts = array(
				  "jpg", 
				  "png", 
				  "jpeg"
				); 

				$allowedMimeTypes = array( 
				  'image/jpeg',
				  'image/jpg',
				  'image/png'
				);
				$extension = end(explode(".", $_FILES["upload"]["name"]));
			
				if(($_FILES['upload']['size']>1024000) || (!(in_array($extension,$allowedExts))) || (!(in_array($_FILES['upload']['type'],$allowedMimeTypes))))
				{
					$_SESSION['validate']['filesize'] = "File size should be less than 1MB. Formats: jpg,png";
					$flag = 1;
				}
				
			}	
			if($flag == 1)
			{
				$_SESSION['data'] = $_POST;
				header('location: research');
			}
			
				
			if($flag == 0)
			{
				require('../dbconnect.php');
				$description = mysqli_real_escape_string($conn,$_POST['description']);
				mysqli_autocommit($conn,false);
				if ( mysqli_connect_errno() )
				{
					echo 'There was an error with your connection: '.mysqli_connect_error();
				}
				else
				{
					$query = mysqli_query($conn,"insert into research(heading,description) values('$research','$description')");
					echo mysqli_error($conn);
					if($query)
					{
						$id = mysqli_insert_id($conn);
						$file=$_FILES['upload']['name'];
						if($file!="")
						{
						$ext = pathinfo($file,PATHINFO_EXTENSION);
						if(move_uploaded_file($_FILES['upload']['tmp_name'],"uploads/research/".$id.".".$ext))
						{
							$filename=$id.".".$ext;
							$query2 = mysqli_query($conn,"update research set images='$filename' where id=$id");
							echo mysqli_error($conn);
							if($query2)
							{
								mysqli_commit($conn);
								$_SESSION['saved'] = 'Research & Training Added Successfully.';
								header('location: research');
							}
							else
								mysqli_rollback($conn);
						}
						}
						else
						{
							mysqli_commit($conn);
							$_SESSION['saved'] = 'Research & Training Added Successfully.';
							header('location: research');
						}
					}
					else
						mysqli_rollback($conn);
				}
			}
		}
		else
			header('location: index');
	}
	else
		header('location: index');
?>