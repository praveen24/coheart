
		<?php require('header.php');?>
		<div class='content about-content'>
			<div class="container">
				<div class='row'>				
					<div class='col-md-12 col-sm-12'>					
						<div class='row'>
							<div class='col-md-12 div-about'>
								<div class="content-3 content-about">
									<div class="panel panel-primary text-justify">
									  	<div class="panel-heading">
										    <h3 class="panel-title">More About COHEART</h3>
									  	</div>
									  	<div class="panel-body">
									  		<h4><strong>“For harmony among Man, Animal and Nature”</strong></h4><br>
									  		<div class="row">
									  			<div class="col-sm-4">
									  				<img src="dist/images/kvasu-coheart-logo-200x200.png" class="img-responsive center-block">
									  			</div>
									  			<div class="col-sm-8">
												    <p>
														The logo embodies COHEART’s motto of harmony among Man, Animal and Nature–with the epicenter of all new ideas and inspirations–“Heart” at its masthead and a catchy acronym “COHEART“ for a concept embedded in the philosophy of “co-existence”. It shows “C” depicting collaboration and “O with globe” depicting One Health as a global movement and the Heart formed by hands of Man and Animal embedded in leaf symbolizing strong relationship of Man and Animal with their environment. KVASU logo is depicted in the form of SUN whose importance goes beyond any obvious benefits being the creator of this centre.
													</p>
												</div>
											</div>
											<br>
											<h5><strong>OUR VISION</strong></h5>
											<p>
												<blockquote>COHEART aims to achieve One Health motto of harmony among Man, Animal and Nature through our dedicated Courses, Research and Capacity building programmes
												</blockquote>
											</p>
											<h5><strong>OUR MISSION</strong></h5>
											<p>
												<blockquote>
													COHEART aims to achieve One Health motto of harmony among Man, Animal and Nature through our dedicated Courses, Research and Capacity building programmes
												</blockquote>
											</p>
											<h5><strong>OUR OBJECTIVES AND WORKS</strong></h5>
											<p>
												<blockquote>
													<ul>
														<li><b>EDUCATION</b> of a new cadre of health professionals about One Health - the linkage between animal, human and environmental health . We do this by offering 3 courses- PG Diploma in One Health, PG Certificate courses in One Health Surveillance and PG Certificate in Community Based Disaster Management.  All the courses are suitable for health related professionals interested in integrating the knowledge and application of human medicine, veterinary medicine and public health. The exploration of interconnectedness helps us to realise various factors and conditions of healthy existence.</li>
														<li><b>ADVOCACY</b> for collaboration as an encouragement for professionals to work together inorder to attain “optimal health for people, animals and the environment". We do this by our Dr. Coheart’s YouTube channel where a virtual 2D character educates general public, farmers and professional on emerging public health issues. </li>
														<li><b>RESEARCH</b> to understand the health threats and disease processes that occur at the interface of human and animal activities and their effects on the environment. We conduct research to improve health outcomes of human and animal populations and their surrounding environment. The major research focusses on Animal Health Hazards, Antimicrobial Resistance, Human Animal Conflict, Environmental Health Hazards, Food Safety, Infectious and Zoonotic Diseases, Occupational Health Hazards, Public Health Hazards and Wildlife Research </li>
														<li><b>TRAINING AND CAPACITY BUILDING</b> to improve our community’s preparedness and response to hazards affecting man, animal and the environment. As a part of capacity building activities, the Centre hosts series of awareness programmes, camps, interface meets, important day commemorations, seminars, workshop, student event etc. on cross-cutting topic areas.</li>
													</ul>
												</blockquote>
											</p>
											<h5><strong>OUR TARGETS</strong></h5>
											<p>
												<blockquote>
													<ul>
														<li>Establishment of suitable facility for academic, research and training on One Health and developing liaison to facilitate One Health concept and its practice</li>
														<li>Empowering the human resources to face the challenging needs of Health sector with special reference to zoonoses control</li>
														<li>Develop One Health competency among various stake holders and graduates through dedicated courses and training</li>
														<li>Develop, implement and sustain strategies for One Health practices by 'thinking globally while acting locally'.</li>
													</ul>
												</blockquote>
											</p>
									  	</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="row news">
					<div class="col-md-12">
						<div class="container">							
							<div class='row'>
								<div class='col-md-12	'>
									<h3 class="text-center">NEWS & EVENTS</h3>
									<?php require('news-updates.php');?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php require('footer.php');?>	
